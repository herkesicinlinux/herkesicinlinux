import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import Translate, { translate } from '@docusaurus/Translate';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: translate({
      message: 'Easy to Use',
      description: 'The homepage feature text #1',
    }),
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        {translate({
          message:
            'Docusaurus was designed from the ground up to be easily installed and used to get your website up and running quickly.',
          description: 'The homepage feature description text #1',
        })}
      </>
    ),
  },
  {
    title: translate({
      message: 'Focus on What Matters',
      description: 'The homepage feature text #2',
    }),
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        {translate({
          message:
            "Docusaurus lets you focus on your docs, and we'll do the chores. Go ahead and move your docs into the <code>docs</code> directory.",
          description: 'The homepage feature description text #2',
        })}
      </>
    ),
  },
  {
    title: translate({
      message: 'Powered by React',
      description: 'The homepage feature text #3',
    }),
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        {translate({
          message:
            'Extend or customize your website layout by reusing React. Docusaurus can be extended while reusing the same header and footer.',
          description: 'The homepage feature description text #3',
        })}
      </>
    ),
  },
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p dangerouslySetInnerHTML={{ __html: description.props.children }}></p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout title={siteConfig.title} description={siteConfig.tagline}>
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}
            >
              <Translate
                id="homepage.getStarted"
                description="The homepage button that takes the user to docs"
                // values={{ blog: <Link to="https://docusaurus.io/blog">blog</Link> }}
              >
                {'Get Started'}
              </Translate>
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
