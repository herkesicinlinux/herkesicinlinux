/**
 * Converts raw path into url slug.
 * @param rawPath Raw path
 * @param wordSeperator Word seperator. Defaults to '-'.
 */
export const pathify = (rawPath: string, wordSeperator = '-'): string => {
  // Convert to lower case
  let finalSlug = rawPath.toLowerCase();

  // Replace Turkish chars
  const chars = [
    ['ç', 'c'],
    ['ğ', 'g'],
    ['ı', 'i'],
    ['ö', 'o'],
    ['ş', 's'],
    ['ü', 'u'],
  ];
  chars.forEach((charSet) => {
    const re = new RegExp(charSet[0], 'g');
    finalSlug = finalSlug.replace(re, charSet[1]);
  });

  // Convert spaces and dots to wordSeperator, keep forward slashes, remove remaining non-alphanumeric chars
  finalSlug = finalSlug
    .replace(/\.|\s+/g, wordSeperator) // handle dots and spaces
    .replace(new RegExp(`[^0-9a-z/${wordSeperator}]`, 'g'), '') // remove unnecessary chars
    .replace(new RegExp(`${wordSeperator}+`, 'g'), wordSeperator); // reduce multiple wordSeperators to one

  return finalSlug;
};
