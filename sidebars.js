module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Docusaurus',
      items: ['doc1', 'doc2', 'doc3'],
    },
    {
      type: 'category',
      label: 'Features',
      items: ['mdx'],
    },
  ],
};
