# Contributing

0. Make sure [NodeJS](https://nodejs.org/) is installed (preferably via [nvm](https://github.com/nvm-sh/nvm)).

1. Obtain the source code.

   a. Team members:

   1. Clone the upstream repository into your workspace.

      ```bash
      git clone git@gitlab.com:herkesicinlinux/herkesicinlinux.git
      ```

   b. Contributors:

   1. [Fork](https://gitlab.com/herkesicinlinux/herkesicinlinux/-/forks/new) the repository.

   2. Clone your own copy into your workspace.

      ```bash
      git clone git@gitlab.com:<username>/herkesicinlinux.git
      ```

2. Install dependencies.

   ```bash
   npm install
   ```

3. Create a branch for your work.

   ```bash
   git checkout -b my_branch
   # OR
   git switch -c my_branch
   ```

4. Perform your magic and do some commits.

5. Switch back to `main` branch when you complete your work.

   ```bash
   git checkout main
   Switched to branch 'main'

   ```

6. DO NOT MERGE your branch into `main` branch. Push your branch to `origin` instead.

   ```bash
   git push origin my_branch
   ```

7. Open a Merge Request for your branch.

8. Remove your working branch from your local environment **after your branch has been merged into main**.

   ```bash
   git branch -d my_branch
   ```

   (Optional) Remove branch from remote too.

   ```bash
   git push -d origin my_branch
   ```

9. Pull changes to get in sync with origin.

   ```bash
   git pull
   ```

10. You're done. Return back to step 3!
