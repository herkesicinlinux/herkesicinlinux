---
title: Giriş ve Öğrenme Hedefleri
---

## Video: Bölüm 11 Giriş

<!-- [Video](https://player.vimeo.com/video/509752867) | [Alt yazı](video-01-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/509752867?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Öğrenme Hedefleri

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Bu bölümün sonunda, şunlara aşina olmalısınız:

- Mevcut Linux metin düzenleyicilerini kullanarak dosya oluşturma ve düzenleme.
- `nano`, basit bir metin tabanlı düzenleyici.
- **gedit**, basit bir grafiksel metin düzenleyici.
- `vi` ve `emacs`, hem metin tabanlı hem de grafik arabirimlere sahip iki gelişmiş düzenleyici.
