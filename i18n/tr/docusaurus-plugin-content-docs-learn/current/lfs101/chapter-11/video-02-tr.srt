﻿1
00:00:00,614 --> 00:00:04,660
Vi'de modları ve imleç hareketlerini kullanmaya başlayalım.

2
00:00:04,762 --> 00:00:08,892
1. vi ve ardından dosya adını yazarak vi'yi açın.

3
00:00:09,251 --> 00:00:11,470
Vi, Komut modunda açılır.

4
00:00:11,709 --> 00:00:13,927
2. Giriş moduna girmek için i yazın.

5
00:00:14,030 --> 00:00:16,675
Vi'nin Giriş modunda olduğu görüntülenir.

6
00:00:16,880 --> 00:00:25,414
3. Şu cümleleri yazın: "The quick brown fox jumped over the lazy dog. Nobody expects the Spanish Inquisition!"

7
00:00:25,807 --> 00:00:30,142
4. Giriş modundan çıkmak ve Komut moduna geçmek için ESC'ye basın

8
00:00:30,620 --> 00:00:36,354
5. vi'den çıkmak ve dosyayı kaydetmek için, :wq yazın ve Enter'a basın.

9
00:00:37,515 --> 00:00:44,240
6. En son kaydedilen dosyayı açın ve imleci ilk cümledeki "quick" kelimesindeki k harfinin üzerine getirin.

10
00:00:44,581 --> 00:00:49,753
İmleci dört karakter sola hareket ettirmek için dört kez h yazın.

11
00:00:49,958 --> 00:00:53,200
İmleç, "quick" kelimesinin q harfine taşınır.

12
00:00:53,491 --> 00:00:56,546
7. İmleci sonraki satıra taşımak için j yazın.

13
00:00:57,143 --> 00:01:01,086
8. İmleci bir sonraki kelimenin başlangıcına taşımak için w yazın.

14
00:01:01,325 --> 00:01:05,284
İmleç, "expects" sözcüğün başına taşınır.

15
00:01:05,506 --> 00:01:08,749
9. İmleci sona taşımak için $ yazın.

16
00:01:08,988 --> 00:01:11,992
İmleç, ikinci cümlenin sonuna taşınır.

17
00:01:12,094 --> 00:01:14,620
10. Giriş moduna girmek için i yazın.

18
00:01:15,030 --> 00:01:17,403
Vi Giriş modu görüntülenir.

19
00:01:17,590 --> 00:01:22,779
11. İkinci cümlenin sonuna metin eklemek için "A History" yazın.

20
00:01:22,950 --> 00:01:26,175
"History" sözcüğü cümlenin sonunda görüntülenir.

21
00:01:26,363 --> 00:01:30,459
12. Ekle modundan çıkmak ve Komut moduna geçmek için ESC'ye basın

22
00:01:30,920 --> 00:01:37,013
13. vi'den çıkmak ve dosyayı kaydetmek için :wq yazın ve Enter tuşuna basın.

