---
title: Vi'de Gezinme
---

1.  `myfile` dosyasının mevcut olmadığını varsayarak, oluşturun ve şunu yazarak açın:

    ```bash
    student:/tmp> vi myfile

    ```

2.  İmleciniz en üst satırda olmak üzere artık çoğunlukla boş bir ekranınız var. Başlangıçta Komut modundasınız.

3.  Ekleme yapmak ve giriş moduna geçmek için `a` yazın.

4.  Şu cümleyi yazın:

    ```bash
    The quick brown fox jumped over the lazy dog.
    ```

    Yazarken bir hata yaparsanız, düzeltmek için geri alabileceğinizi unutmayın.

5.  `ESC` tuşuna basın. İmleciniz, cümlenin sonundaki nokta üzerinde olmalıdır. Bu noktada komut moduna geri döndünüz.

6.  Yeniden `a` yazın ve ardından `Enter` tuşuna basın. Az önce yazdığınız satırın altında yeni bir satırda olmalısınız.

7.  Şu cümleyi yazın:

    ```bash
    Nobody expects the Spanish Inquisition!
    ```

    `!` karakterini yazdıktan sonra `Enter` tuşuna basın.

8.  Üçüncü satırın başına şu cümleyi yazın:

    ```bash
    This is the third line.
    ```

    `ESC` tuşuna basın. Artık komut moduna geri döndünüz.

9.  Şimdi bu dosya içinde biraz hareket edelim. İmleciniz, yeni yazdığınız üçüncü satırın sonundaki noktanın üzerine konumlandırılmış olmalıdır. `h` tuşuna üç kez basın. `k` tuşuna bir kez basın. İmleciniz hangi harfin üzerinde? Hangi kelimedesiniz?

    (İmleciniz, `Spanish` kelimesindeki `S`'nin üzerinde olmalıdır.)

10. `h`'ye dört kez basın ve ardından `j`'ye basın. Şimdi imlecinizin altında hangi harf var? Hangi kelimedesiniz?

    (İmleciniz, `third` kelimesindeki `r` nin üzerinde olmalıdır.)

11. `k`'ye iki kez ve ardından `l`'ye üç kez basın. İmleciniz hangi harfin üzerinde? Hangi kelime?

    (İmleciniz `fox` kelimesindeki `x`'in üzerinde olmalıdır.)

12. Şimdi sekiz kez `w` tuşuna basın. Ne fark ettiniz? İmleciniz hangi harfin üzerinde? İmleciniz hangi kelimenin üzerinde?

    (Artık sözcükler arasında geçiş yaptığınızı fark etmelisiniz. Aslında, `w`'ye sekiz dokunuş sizi ilk satırdan ikinci satıra götürecektir. İmleciniz şimdi ikinci satırdaki `expects` kelimesindeki `e`'nin üzerinde olmalıdır.)

13. `k` ve ardından `$` tuşuna basın. Şimdi iki kez `b`'ye basın. İmleciniz hangi harfin üzerinde ve hangi kelimede?

    (İmleciniz `lazy` kelimesindeki `l`'nin üzerinde olmalıdır.)

14. An itibariyle, hem ileri hem de geri yönde, karakter karakter, satır satır ve kelime kelime hareket etmeye alışıyor olmalısınız.

    Şimdi `0` (sıfır) tuşuna, ardından iki kez `w` tuşuna ve üç kez `l` tuşuna basın. İmleciniz hangi harfin üzerinde ve hangi kelimedesiniz?

    (İmleciniz `brown` kelimesindeki `w` harfinin üzerinde olmalıdır.)

15. Şimdi üç kez `e`'ye basın. İmleciniz hangi harfin üzerinde ve hangi kelimede?

    (`jumped` kelimesindeki `d` nin üzerinde olmalısınız.)

16. `:w` komutunu kullanarak dosyayı kaydedin. Bu dosyayı bir sonraki alıştırmada kullanacaksınız. Son olarak `:q` komutunu yazarak **vi**'den çıkın. İsterseniz bu komutları `:wq` yazarak da birleştirebilirsiniz.

Umarım bu egzersiz, imlecinizi bir dosya içinde nasıl hareket ettirebileceğinize alışmanızı sağlar. Bir süre sonra, bu hareket tuşlarının kullanımı alışkanlık haline gelecektir.
