﻿1
00:00:01,240 --> 00:00:07,580
Şimdi, günlük işlerinizde Emacs ile yapabileceğiniz işlemlerden bazılarını göstereceğiz.

2
00:00:07,580 --> 00:00:16,700
İlk olarak, düzenlenecek bir dosya bulalım. /tmp dizininde çalışarak, /etc/passwd'nin bir kopyasını alıp buraya getirelim [cp /etc/passwd].

3
00:00:16,700 --> 00:00:24,480
Ve sonra, üzerinde çalışmak için, "emacs passwd &" demem gerekiyor.

4
00:00:24,480 --> 00:00:34,560
Şimdi, buradaki varsayılan yazı tipi biraz küçük. Bu yüzden, ufak bir numara yapacağım ve yazı tipinin boyutunu kontrol etmek için Ctrl + fare tekerini kullanacağım,

5
00:00:34,560 --> 00:00:38,180
ve sonra pencereyi biraz daha büyüteceğim.

6
00:00:38,180 --> 00:00:52,080
Tamam. Bunu yaptık. Şimdi, belirli bir dizgeyi aramak istediğimi varsayalım, diyelim ki "ftp" olsun, Ctrl-s'ye basıp en alt satıra "ftp" yazıyorum.

7
00:00:52,080 --> 00:00:53,660
Aşağıda görebilirsiniz.

8
00:00:53,660 --> 00:01:01,080
Beni ftp'ye getirdiğini görüyorsunuz. Ctrl-a'ya basarak satır başına döneceğim,

9
00:01:01,080 --> 00:01:13,220
ve sonra, diyelim ki "ftp" dizgisinin tüm örneklerini başka bir şeyle değiştirmek istiyorum, Escape % işaretine bastım ve sonra alt satırda tekrar "ftp" diyeceğim.

10
00:01:13,220 --> 00:01:25,740
Ve diyelim ki bunu PTF yapalım. Görüyorsunuz, Boşluk tuşuna basıyorum, birincisini değiştiriyor, Boşluk, ikincisini değiştiriyor, Boşluk, üçüncüsünü değiştiriyor.

11
00:01:25,740 --> 00:01:31,140
Ünlem işaretine basmış olsaydım, tüm dosyada hepsini değiştirirdi.

12
00:01:31,140 --> 00:01:40,100
Bir satırı silmek istediğimi varsayalım. Ctrl-K tuşlarına basabilirim ve silindi. Ctrl-K tekrar silindi.

13
00:01:40,100 --> 00:01:56,000
Bir dizi satırı silmek istediğimi varsayalım. Ctrl-boşluk tuşuna basıyorum ve sonra ok tuşu veya Ctrl-N ile birkaç satır aşağı iniyorum, Ctrl-W'ye basıyorum ve silindiler.

14
00:01:56,000 --> 00:02:05,240
Onları dosyada daha öteye taşımak istersem, birkaç satır aşağı inerim ve kopyalama için Ctrl-Y tuşlarına basarım ve geri döndüler.

15
00:02:05,240 --> 00:02:10,840
Emacs ile yapabileceğim güzel bir şey de aynı anda birden fazla pencere açabilmektir.

16
00:02:10,840 --> 00:02:20,880
Öyleyse, Ctrl-X-2'ye basmama izin verin ve şimdi iki pencerem var ve Ctrl-X-O tuşlarına basarak alt pencereye geçebilirim,

17
00:02:20,880 --> 00:02:25,620
ve sonra Ctrl-X-F tuşlarına basar ve bu arabelleğe farklı bir dosya koyabilirim.

18
00:02:25,620 --> 00:02:38,860
Diyelim ki /etc/group dosyasını açtım. Tamam, bir kez daha, Ctrl ve fare tekeri ile yazı tipini biraz daha büyüteceğim.

19
00:02:38,860 --> 00:02:47,120
Ve alt satırda gördüğünüz gibi, dosyanın aslında yazmaya karşı korumalı olduğunu fark edeceksiniz, bu yüzden hiçbir şeyi gerçekten silemem çünkü o root'a ait.

20
00:02:47,120 --> 00:02:52,000
Bu satırı silmeye çalışırsam, bunu yapmama izin vermez. Arabelleğin salt okunur olduğunu söylüyor.

21
00:02:52,000 --> 00:02:59,460
Ancak genel olarak, aynı izinlere sahip iki dosyam olsaydı, kesip yapıştırabilir ve birinden diğerine taşıyabilirdim.

22
00:02:59,460 --> 00:03:11,300
Tek bir pencereye geri dönmek için Ctrl-X-1 yapabilirim ve artık sadece buna sahibim. Ama diğer pencereyi gerçekten istiyorsam, Ctrl-X-B tuşlarına basabilirim ve passwd dosyasına geri dönerim.

23
00:03:11,300 --> 00:03:27,440
Yeniden yazmak istersem Ctrl-X-W ... Ctrl-X Ctrl-W tuşlarına basabilirim ve bu da onu farklı bir adla yazmama izin verir. İsim olarak "passwdrevised" diyeceğim ve oldu.

24
00:03:27,440 --> 00:03:36,640
Programı sonlandırmak istersem Ctrl-X-S yapabilirim. Her şeyin kaydedildiğinden emin olun ve ardından Ctrl-X-C yapın ve oldu.

25
00:03:36,640 --> 00:03:47,880
Dolayısıyla, emacs'de Ctrl tuşunu epeyce kullandığımızı görebilirsiniz. Bu günlerde çoğu klavyenin Ctrl tuşunu yerleştirdiği konum doğal gelmeyebilir, sol veya sağ alt köşede.

26
00:03:47,880 --> 00:03:55,640
Bu nedenle, deneyimli Emacs kullanıcıları bazı tuşları yeniden atama eğilimindedir, mesela Caps Lock tuşunun Ctrl olarak da çalışması sağlanabilir.

27
00:03:55,640 --> 00:04:02,420
Özetle bunlar, Emacs'in günlük kullanımında yapacağınız temel işlemlerden bazılarıdır.

