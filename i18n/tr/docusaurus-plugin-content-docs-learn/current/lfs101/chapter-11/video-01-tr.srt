﻿1
00:00:04,474 --> 00:00:09,519
Elbette bilgisayarlar verileri düzenlemek, verileri değiştirmek ve verileri yönetmekle ilgilidir.

2
00:00:09,525 --> 00:00:13,126
Linux sistemlerinde, bu veriler genellikle metin dosyaları biçiminde olurlar.

3
00:00:13,140 --> 00:00:16,486
Bazen bu metin dosyaları başka uygulamalar olmadan oluşturulur,

4
00:00:16,586 --> 00:00:21,608
ancak bazen bunları doğrudan düzenlemeniz gerekir ve bunları Linux'ta, metin düzenleyici adı verilen bir şey kullanarak yaparsınız.

5
00:00:21,640 --> 00:00:27,658
Bu derste, nano, emacs ve gedit gibi bazı popüler metin editörlerini ele alacağız

6
00:00:27,758 --> 00:00:31,869
ve ayrıca vim metin düzenleyicisinin kullanımı ve yapılandırmasını adım adım inceleyeceğiz.

7
00:00:32,200 --> 00:00:37,415
Bu dersi bitirdiğinizde, metin dosyalarınızı en sevdiğiniz metin düzenleyicide rahatça düzenleyebiliyor olmalısınız.

8
00:00:37,472 --> 00:00:38,534
Hadi başlayalım!

