---
title: ' Gelişmiş Düzenleyiciler: Vi ve Emacs'
---

## Vi ve Emacs

UNIX benzeri sistemler üzerinde çalışma konusunda deneyimli geliştiriciler ve yöneticiler, neredeyse her zaman iki saygıdeğer düzenleyici seçeneğinden birini kullanır: **vi** ve **emacs**. Her ikisi de tüm dağıtımlarda mevcuttur veya kolayca edinilebilir ve diğer işletim sistemlerinde bulunan sürümlerle tamamen uyumludur.

Hem **vi** hem de **emacs**, grafik olmayan ortamlarda çalışabilen, tamamen metin tabanlı temel bir biçime sahiptir. Ayrıca genişletilmiş yeteneklere sahip bir veya daha fazla grafik arabirim seçeneklerine de sahiptirler; bunlar daha az deneyimli bir kullanıcı için daha kullanışlı olabilir. **Vi** ve **emacs**, yeni kullanıcılar için önemli ölçüde dik öğrenme eğrilerine sahip olsalar da, nasıl kullanıldıkları öğrenildiğinde son derece etkilidirler.

Hangi düzenleyicinin daha iyi olduğu konusunda deneyimli kullanıcılar arasındaki kavgaların oldukça yoğun olabileceğini ve genellikle bunların kutsal savaş (holy war) olarak tanımlandığını bilmenizde fayda var!

<figure align="center">
  <img src="/img/lfs101/ch11/img07.png" class="invert"/>
  <figcaption>Linux Metin Düzenleyicileri</figcaption>
</figure>

## vi

### Vi'ye Giriş

Genellikle, sisteminizde yüklü olan asıl program, **V**i **IM**proved anlamına gelen **vim**'dir ve **vi**'nin diğer bir takma adıdır (alias). İsim "vii-ayy" olarak telaffuz edilir.

**Vi**'yi kullanmak istemeseniz bile, biraz aşinalık kazanmak faydalı olacaktır, çünkü **vi** hemen hemen tüm Linux dağıtımlarında yüklü standart bir araçtır. Aslında, sistemde başka bir düzenleyicinin bulunmadığı zamanlar bile gelebilir.

**GNOME**, **vi**'yi **gvim** olarak bilinen grafiksel bir arayüzle genişletir ve **KDE**, **kvim**'i sunar. Bunlardan herhangi birinin kullanımı ilk başta daha kolay olabilir.

**Vi** kullanırken, tüm komutlar klavye aracılığıyla girilir. Düzenleyicinin grafik sürümlerinden birini kullanmıyorsanız, fare veya dokunmatik yüzey gibi bir işaretçi cihazı kullanmak için ellerinizi sürekli hareket ettirmeye gerek yoktur.

<figure align="center">
  <img src="/img/lfs101/ch11/img08.png" />
  <figcaption>vi'ye giriş</figcaption>
</figure>

### Vimtutor (Vim Öğreticisi)

Uçbirimde **`vimtutor`** komutunu çalıştırmak, ilk **vi** komutlarını öğrenmek isteyenler için kısa ama çok kapsamlı bir eğitim başlatır. Yalnızca bir giriş olsa ve yalnızca yedi ders sağlasa da, sizi yetkin bir **vi** kullanıcısı yapacak kadar yeterli malzemeye sahiptir, çünkü çok sayıda komutu kapsar. Bu temel komutları öğrendikten sonra, **vi** komutları listenize dahil etmek için yeni numaralar arayabilirsiniz çünkü **vi**'de her zaman, bir şeyleri daha az yazarak yapmanın daha uygun yolları vardır.

<figure align="center">
  <img src="/img/lfs101/ch11/img09.png" />
  <figcaption>vimtutor</figcaption>
</figure>

### Vi Modları

**Vi**, aşağıdaki tabloda açıklandığı gibi üç mod sunar. Çalışırken hangi modda olduğunuzu unutmamak çok önemlidir. Birçok tuş vuruşu ve komut, farklı modlarda oldukça farklı davranır.

| Mod       | Özellik                                                                                                                                                                             |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Komut** | Varsayılan olarak **vi**, Komut (Command) modunda başlar.                                                                                                                           |
|           | Her tuş bir komuttur.                                                                                                                                                               |
|           | Klavye vuruşları, dosya içeriğini değiştirebilen komutlar olarak yorumlanır.                                                                                                        |
| **Giriş** | Komut modundan Giriş (Insert) moduna geçmek için **i** yazın.                                                                                                                       |
|           | Giriş modu, bir dosyaya metin girmek (eklemek) için kullanılır.                                                                                                                     |
|           | Giriş modu, ekranın altında "**? INSERT ?**" ile gösterilir.                                                                                                                        |
|           | Giriş modundan çıkmak ve Komut moduna dönmek için **Esc** tuşuna basın.                                                                                                             |
| **Satır** | Komut modundan Satır (Line) moduna geçmek için **:** yazın. Her tuş, dosya içeriklerinin diske yazılması veya çıkış yapılması gibi işlemler dahil olmak üzere, harici bir komuttur. |
|           | Eski satır düzenleyicilerden devralınan satır düzenleme komutlarını kullanır. Bu komutların çoğu artık kullanılmamaktadır. Bazı satır düzenleme komutları oldukça güçlüdür.         |
|           | Satır modundan çıkmak ve Komut moduna dönmek için **Esc** tuşuna basın.                                                                                                             |

### Vi'de Dosyalarla Çalışma

Tablo, **vi**'de dosyaları başlatmak, okumak, yazmak ve çıkmak için kullanılan en önemli komutları açıklar. Tüm bu komutlardan sonra **ENTER** tuşuna basılması gerekir.

| Komut          | Açıklama                                                                             |
| -------------- | ------------------------------------------------------------------------------------ |
| `vi myfile`    | Düzenleyiciyi başlat ve **myfile** dosyasını düzenle                                 |
| `vi -r myfile` | **myfile** dosyasını bir sistem çökmesinden sonra kurtarma modunda başlat ve düzenle |
| `:r file2`     | **file2**'yi oku ve mevcut konuma ekle                                               |
| `:w`           | Dosyaya yaz                                                                          |
| `:w myfile`    | **myfile** dosyasına yaz                                                             |
| `:w! file2`    | **file2**'nin üzerine yaz                                                            |
| `:x or :wq`    | Çık ve değiştirilen dosyayı yaz                                                      |
| `:q`           | Çık                                                                                  |
| `:q!`          | Kaydedilmemiş değişiklikler olsa bile çık                                            |

### Vi'de İmleç Konumunu Değiştirme

Tablo, **vi**'de imleç konumunu değiştirirken kullanılan en önemli tuş vuruşlarını açıklar. Satır modu komutları (iki noktadan sonra gelenler ":.."), komut yazıldıktan sonra **ENTER** tuşuna basılmasını gerektirir.

| Tuş                         | Kullanım                               |
| --------------------------- | -------------------------------------- |
| yön tuşları                 | Yukarı, aşağı, sola ve sağa hareket et |
| **j** or **\<ret\>**        | Bir satır aşağı git                    |
| **k**                       | Bir satır yukarı git                   |
| **h** or Backspace          | Bir karakter sola git                  |
| **l** or Space              | Bir karakter sağa git                  |
| **0**                       | Satır başına git                       |
| **$**                       | Satır sonuna git                       |
| **w**                       | Bir sonraki kelimenin başlangıcına git |
| **:0** or **1G**            | Dosyanın başına git                    |
| **:n** or **nG**            | n satırına git                         |
| **:$** or **G**             | Dosyadaki son satıra git               |
| **CTRL-F** or **Page Down** | Bir sayfa ileri git                    |
| **CTRL-B** or **Page Up**   | Bir sayfa geri git                     |
| **^l**                      | Ekranı tazele ve ortala                |

### Video: Vi'de Modları ve İmleç Hareketlerini Kullanma

<!-- [Video](https://player.vimeo.com/video/510180291) | [Alt yazı](video-02-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/510180291?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

### Vi'de Metin Arama

Tablo, vi'de metin ararken kullanılan en önemli _komutları_ açıklar. Arama deseni (pattern) yazıldıktan sonra **ENTER** tuşuna basılmalıdır.

| Komut      | Kullanım                     |
| ---------- | ---------------------------- |
| **/desen** | Desen için ileriye doğru ara |
| **?desen** | Desen için geriye doğru ara  |

Tablo, vi'de metin ararken kullanılan en önemli _tuş vuruşlarını_ açıklar.

| Tuş   | Kullanım                          |
| ----- | --------------------------------- |
| **n** | Bir sonraki desen eşleşmesine git |
| **N** | Bir önceki desen eşleşmesine git  |

### Vi'de Metinlerle Çalışma

Tablo, vi'de metinleri değiştirirken, eklerken ve silerken kullanılan en önemli tuş vuruşlarını açıklar.

Ayrıca, [vi için komutlar](/pdf/VI_Editor.pdf) içeren birleştirilmiş bir PDF dosyası da indirebilirsiniz.

| Tuş                | Kullanım                                                                                       |
| ------------------ | ---------------------------------------------------------------------------------------------- |
| **a**              | İmleçten sonra metin ekle; **Escape** tuşu ile durdur                                          |
| **A**              | Geçerli satırın sonuna metin ekle; **Escape** tuşu ile durdur                                  |
| **i**              | İmleçten önce metin ekle; **Escape** tuşu ile durdur                                           |
| **I**              | Geçerli satırın başına metin ekle; **Escape** tuşu ile durdur                                  |
| **o**              | Geçerli satırın altında yeni bir satır başlat ve oraya metin ekle; **Escape** tuşu ile durdur  |
| **O**              | Geçerli satırın üzerinde yeni bir satır başlat ve oraya metin ekle; **Escape** tuşu ile durdur |
| **r**              | Geçerli konumdaki karakteri değiştir                                                           |
| **R**              | Geçerli konumdan başlayarak metni değiştir; **Escape** tuşu ile durdur                         |
| **x**              | Geçerli konumdaki karakteri sil                                                                |
| **Nx**             | Geçerli konumdan başlayarak N karakter sil                                                     |
| **dw**             | Geçerli konumdaki kelimeyi sil                                                                 |
| **D**              | Geçerli satırın geri kalanını sil                                                              |
| **dd**             | Geçerli satırı sil                                                                             |
| **Ndd** or **dNd** | N satır sil                                                                                    |
| **u**              | Önceki işlemi geri al                                                                          |
| **yy**             | Geçerli satırı kopyala (yank/copy) ve arabelleğe yerleştir                                     |
| **Nyy** or **yNy** | N satır kopyala ve arabelleğe yerleştir                                                        |
| **p**              | Arabellekten, geçerli konuma, kopyalanmış satır(lar)ı yapıştır                                 |

### Vi'de Harici Komutları Kullanma

Vi'de **sh komut** yazmak, harici bir komut kabuğu açar. Kabuktan çıktığınızda, düzenleme oturumunuza devam edebilirsiniz.

**!** yazmak, vi içerisinden bir komut çalıştırır. Komut, ünlem işaretini takip eder. Bu teknik, etkileşimli olmayan **: ! wc %** gibi komutlar için en uygun olanıdır. Bunu yazmak, dosyada **wc** (word count - kelime sayısı) komutunu çalıştırır; **%** karakteri o anda düzenlenmekte olan dosyayı temsil eder.

<figure align="center">
  <img src="/img/lfs101/ch11/img10.png" />
  <figcaption>Vi Harici Komutları</figcaption>
</figure>

### Video: Vi Düzenleyicide Harici Komutları Kullanma, Kaydetme ve Kapatma

<!-- [Video](https://player.vimeo.com/video/510218892) | [Alt yazı](video-03-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/510218892?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Emacs

### Emacs'e Giriş

**Emacs** düzenleyicisi **vi** için popüler bir rakiptir. **Vi**'den farklı olarak, modlarla çalışmaz. **Emacs** son derece özelleştirilebilir ve çok sayıda özellik içerir. Başlangıçta bir konsolda kullanılmak üzere tasarlanmış, ancak kısa süre sonra bir GUI (grafik arabirim) ile de çalışacak şekilde uyarlanmıştır. **Emacs**, basit metin düzenleme dışında birçok başka yeteneğe sahiptir. Örneğin, e-posta, hata ayıklama vb. için kullanılabilir.

**Vi** gibi komut ve ekleme için farklı moda sahip olmak yerine, **emacs** özel komutlar için **CTRL** ve Meta (**Alt** veya **Esc**) tuşlarını kullanır.

<figure align="center">
  <img src="/img/lfs101/ch11/img11.png" />
  <figcaption>emacs</figcaption>
</figure>

### Emacs ile Çalışmak

Tablo, **emacs**'de dosyaları başlatırken, okurken, yazarken ve çıkarken kullanılan en önemli tuş kombinasyonlarından bazılarını listeler.

| Tuş               | Kullanım                                               |
| ----------------- | ------------------------------------------------------ |
| **emacs myfile**  | **emacs**i başlat ve `myfile` dosyasını düzenle        |
| **CTRL-x i**      | Geçerli konuma girmek için dosya girme diyaloğu göster |
| **CTRL-x s**      | Tüm dosyaları kaydet                                   |
| **CTRL-x CTRL-w** | Yeni bir ad vererek dosyaya yaz                        |
| **CTRL-x CTRL-s** | Geçerli dosyayı kaydet                                 |
| **CTRL-x CTRL-c** | Değiştirilen dosyaları kaydetme uyarısından sonra çık  |

**Emacs** öğreticisi, temel komutları öğrenmeye başlamak için iyi bir yerdir. **Emacs** içindeyken **CTRL-h** (help - yardım) ve ardından eğitim için **t** (tutorial) harfini yazarak ulaşılabilir.

### Emacs'de İmleç Konumunu Değiştirme

Tablo, **emacs**'de imleç konumunu değiştirmek için kullanılan bazı tuş ve tuş kombinasyonlarını listeler.

| Tuş                         | Kullanım                                                   |
| --------------------------- | ---------------------------------------------------------- |
| Yön tuşları                 | Yukarı, aşağı, sola ve sağa hareket et                     |
| **CTRL-n**                  | Bir satır aşağı                                            |
| **CTRL-p**                  | Bir satır yukarı                                           |
| **CTRL-f**                  | Bir karakter ileri/sağa                                    |
| **CTRL-b**                  | Bir karakter geri/sola                                     |
| **CTRL-a**                  | Satır başına git                                           |
| **CTRL-e**                  | Satır sonuna git                                           |
| **Meta-f**                  | Sonraki kelimenin başına git                               |
| **Meta-b**                  | Önceki kelimenin başına geri git                           |
| **Meta-<**                  | Dosyanın başına git                                        |
| **Meta-g-g-n**              | n satırına git ('**Esc-x Goto-line n**' de kullanılabilir) |
| **Meta->**                  | Dosyanın sonuna git                                        |
| **CTRL-v** or **Page Down** | Bir sayfa ileri git                                        |
| **Meta-v** or **Page Up**   | Bir sayfa geri git                                         |
| **CTRL-l**                  | Tazele ve ekranı ortala                                    |

### Emacs'de Metin Arama

Tablo, **emacs**'de metin aramak için kullanılan tuş kombinasyonlarını listeler.

| Tuş        | Kullanım                                                               |
| ---------- | ---------------------------------------------------------------------- |
| **CTRL-s** | İstenen desen (pattern) için ileriye doğru veya sonraki desen için ara |
| **CTRL-r** | İstenen desen (pattern) için geriye doğru veya sonraki desen için ara  |

### Emacs'de Metinlerle Çalışma

Tablo, **emacs**'de metni değiştirmek, eklemek ve silmek için kullanılan bazı tuş kombinasyonlarını listeler:

| Tuş                             | Kullanım                                                            |
| ------------------------------- | ------------------------------------------------------------------- |
| **CTRL-o**                      | Boş satır ekle                                                      |
| **CTRL-d**                      | Geçerli konumdaki karakteri sil                                     |
| **CTRL-k**                      | Geçerli satırın geri kalanını sil                                   |
| **CTRL-\_**                     | Önceki operasyonu geri al                                           |
| **CTRL-** (space or **CTRL-@**) | Seçili bölgenin başlangıcını işaretle. Son, imleç konumunda olacak. |
| **CTRL-w**                      | Geçerli işaretli metni sil ve arabelleğe yaz                        |
| **CTRL-y**                      | En son silinen içeriği geçerli imleç konumuna ekle                  |

Ayrıca [emacs için komutlar](/pdf/emacs.pdf) içeren birleştirilmiş bir PDF dosyasını da indirebilirsiniz.

### Video: Emacs İşlemleri

<!-- [Video](https://player.vimeo.com/video/510256268) | [Alt yazı](video-04-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/510256268?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Lab Çalışmaları

### Lab 11.3: Vi ve Emacs Öğreticileri

Mevcut temel düzenleyicilerin kullanımıyla ilgili çevrimiçi öğreticilerde bol miktarda seçenek vardır. Örneklerden biri [etkileşimli vi öğreticisi](https://www.openvim.com/)dir.

**Emacs** öğreticisine erişmek, programı başlatmak (bir dosya adı vermek zorunda bile değilsiniz) ve sonra **Ctrl-h** ve sonra **t** yazmak kadar basittir. Bu, yerleşik **emacs** öğreticisini başlatacaktır.

Bu öğreticilere aşina olmak için biraz zaman ayırmalısınız.

### Lab 11.4: Vi ile Çalışmak

Linux sistemlerinde yaygın olarak bulunan düzenleyicilerden biriyle rahat çalışabiliyor olmanız gerekecek. Aşağıdaki laboratuar çalışmalarında, her Linux sisteminde bulunan ve her sistem yöneticisi ve geliştiricinin, en azından bir miktar kullanma becerisi edindiği **vi**'ye odaklanacağız.

Aynı zamanda, geniş bir kullanıcı kitlesine ve çok sayıda sadık hayrana sahip olan **emacs** için de benzer egzersizler yapmalıyız. Ancak, kursun sınırlı süresi göz önüne alındığında, bu laboratuvarları ev ödevi olarak yapmanızı öneririz. **Vi** veya **emacs** konusunda henüz yetkin değilseniz, kurs için **nano** veya **gedit** veya **kedit** kullanabilirsiniz, çünkü bu basit düzenleyiciler neredeyse hiç öğrenme eğrisine sahip değildirler.

Bireysel Linux kullanıcıları ve yöneticilerinin güçlü tercihlere sahip olabileceklerini unutmayın. Örneğin, yazarınız, vi'yi farklı modları nedeniyle kafa karıştırıcı buluyor ve mecbur kalmadıkça asla kullanmıyor. Belki de burada emacs için kapsamlı laboratuvarlarımız olmamasının nedeni, yazarınızın emacs kullanımını çok daha kolay ve sezgisel bulmasıdır!

Laboratuar alıştırmalarının çözümlerini görüntülemek için aşağıdaki bağlantıları izleyin.

[Laboratuvar Çözümü: Vi'de Gezinme](labsol-vi-navigating)

[Laboratuvar Çözümü: Vi'de Düzenleme](labsol-vi-editing)

[Laboratuvar Çözümü: Vi'de Harici Komutlar](labsol-vi-extcmds)
