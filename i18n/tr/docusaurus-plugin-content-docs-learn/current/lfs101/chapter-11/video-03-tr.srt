﻿1
00:00:00,543 --> 00:00:05,776
Vim düzenleyicide harici komutları kullanmaya, kaydetmeye ve kapatmaya başlayalım.

2
00:00:05,900 --> 00:00:10,220
1. vi ve ardından dosya adını yazarak vi editörünü açın.

3
00:00:10,368 --> 00:00:12,614
Vi Komut modunda açılır.

4
00:00:13,108 --> 00:00:15,626
2. Giriş moduna geçmek için i yazın.

5
00:00:15,749 --> 00:00:18,415
Vi'nin Giriş modu görüntülenir.

6
00:00:18,489 --> 00:00:20,316
3. Şu cümleleri yazın.

7
00:00:20,365 --> 00:00:26,166
Nobody expects the Spanish Inquisition! Nobody expects the 2020 revolution!

8
00:00:26,253 --> 00:00:29,758
4. Giriş modundan çıkmak ve Komut moduna geçmek için Esc tuşuna basın.

9
00:00:29,943 --> 00:00:33,696
5. Dosyayı yazmak ve çıkmak için :wq yazın.

10
00:00:33,819 --> 00:00:36,880
Dosya değişikliklerle güncellenir ve kapatılır.

11
00:00:37,312 --> 00:00:41,299
6. vi ve ardından dosya adını yazarak vi'yi açın.

12
00:00:42,126 --> 00:00:48,396
7. Geçerli dosyadaki kelimeleri saymak için şunu yazın :!wc %.

13
00:00:48,803 --> 00:00:50,741
Kelime sayısı görüntülenir.

14
00:00:51,234 --> 00:00:53,197
8. Düzenlemeye devam etmek için ENTER tuşuna basın.

15
00:00:53,518 --> 00:00:57,443
9. Dosyada herhangi bir düzenleme yapılmadıysa çıkmak için :q yazın.

16
00:00:57,566 --> 00:00:58,788
Dosya kapatılır.

17
00:00:58,887 --> 00:01:02,800
10. vi ve ardından dosya adını yazarak vi'yi açın.

18
00:01:03,306 --> 00:01:08,095
11. Dosyayı kaydetmeden çıkmak için :q! yazın.

19
00:01:08,169 --> 00:01:10,810
Dosya, değişiklikleri kaydetmeden kapatılır.

