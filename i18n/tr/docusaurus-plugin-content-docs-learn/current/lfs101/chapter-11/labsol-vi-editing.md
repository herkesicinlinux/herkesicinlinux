---
title: Vi'de Düzenleme
---

Bir dosya içinde rahatça hareket etmek önemlidir, ancak aslında, dosyadaki metni düzenlemek, bir düzenleyici kullanmanın tüm amacıdır.

1.  Önceki alıştırmada oluşturduğunuz dosyayı düzenleyeceğiz; yani şunu yapın,

    ```bash
    student:/tmp> vi myfile

    ```

2.  `/` ve ardından `the` tuşlarına basarak `the` kelimesini arayın ve ardından `Enter` tuşuna basın

    İmleciniz, `over` ve `lazy` kelimeleri arasındaki ilk satırdaki `the` kelimesine geçmelidir.

3.  Şimdi `the`'nın sonraki geçtiği yeri arayın. `n` tuşuna basın. İmleciniz, ikinci satırdaki `expects` ile `Spanish` arasındaki `the` sözcüğüne atlamış olmalıdır.

4.  Bu kelimeyi `the`'dan `a`'ya dönüştürelim. İmleciniz `the` kelimesindeki `t`'nin üzerineyken, kelimeyi değiştirmek için `cw` yazın. `the` kelimesinin kaybolduğunu fark ettiniz mi?

    Şimdi Giriş modundasınız, bu yüzden bu noktada basitçe `a` harfini yazın. Buraya daha fazla karakter yazmak isterseniz, bunu yapabilirsiniz. Örneğin, isterseniz `the` kelimesini `a bright and shiny` olarak değiştirebilirdiniz. `the` kelimesini `a` kelimesine değiştirmeyi bitirdiğinizde, giriş modundan komut moduna dönmek için `ESC` tuşuna basın.

5.  Ekrandaki son satıra gitmek için `L` (`shift + l`) tuşuna basın. Bu kısa dosya için bu aynı zamanda son satırdır. `dd` yazın. Ne oldu?

    (Son satır silinmiş olmalıdır.)

6.  Ekrandaki ilk satıra gitmek için `H` (`shift + h`) tuşuna basın. Yine, bu kısa dosya için, bu aynı zamanda dosyanın ilk satırı oluyor. `"1P` (çift tırnak, `1` ve `P`) yazın. Ne oldu?

    Son silinen öğenin içeriğini mevcut satırın üstüne koydunuz. Bu durumda, eski üçüncü satırı ilk satırın üstüne taşıdınız. Dosyanız şimdi şöyle görünmelidir:

    ```bash
    This is the third line.
    The quick brown fox jumped over the lazy dog.
    Nobody expects a Spanish Inquisition!
    ```

7.  Tercih ettiğiniz herhangi bir yöntemi kullanarak imlecinizi `third` kelimesine getirin. Unutmayın, bunu birkaç şekilde yapabilirsiniz. Her seferinde bir karakter hareket edebilir, her seferinde bir kelimeyi atlayabilir veya kelimeyi arayabilirsiniz. `third` sözcüğünü `first` sözcüğüyle değiştirmek için `cw` komutunu kullanın. Giriş modundan çıkmak için `first` kelimesini yazmayı bitirdiğinizde `ESC` tuşuna basmayı unutmayın.

8.  Ekrandaki son satıra gitmek için `L` tuşuna basın. Bunu isimlendirilmiş bir arabelleğe çekelim (yank); `c` ismini kullanalım. Yani, tüm bu satırı `c`'ye çekmek için `cY` yazabiliriz. Satır hala orada mı?

    (Son satırı `c` isimli arabelleğe aktardıktan sonra, son satır hala mevcut olmalı. Bir satırı çekmek yalnızca satırı kopyalar.)

9.  Ekranda en üst satıra gitmek için `H` tuşuna basın. İlk satırımızdan sonra `c`'nin içeriğini yerleştirelim. `c` adlı arabelleğin içeriğini geçerli satırdan sonraki satıra yazmak için `"cp` yazın.

    Kopyalanmış satırı ilk satırın sonrasına yazdıktan sonra dosyanız şöyle görünmelidir:

    ```bash
    This is the third line.
    Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over the lazy dog.
    Nobody expects a Spanish Inquisition!

    ```

10. Bir kelimenin tüm geçtiği yerleri başka bir şeyle değiştirmek için yerine koyma (substitute) komutunu deneyelim. Vurgu için, `the` kelimesinin tüm örneklerini `THE`'daki gibi büyük harflerle değiştirelim. Bunu yapmak için `:%s/the/THE/g` komutunu yazıyoruz. Değişiklikleri fark ettiniz mi?

    Yerine koyma komutunu çalıştırdıktan sonra, dosyanız şöyle görünmelidir:

    ```bash
    This is THE third line.
    Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over THE lazy dog.
    Nobody expects a Spanish Inquisition!

    ```

    Üçüncü satırın başındaki ilk `The`'nın, bizim komutumuzdaki `the` (tümü küçük harf) ile eşleşmediği için değiştirilmediğine dikkat edin.

11. Şöyle bir düşününce, tümü büyük harflerle `THE` kullanımı, sanki bağırıyormuşuz gibi hissettiriyor. Hadi bu son değişikliği geri alalım. `u` tuşuna basın (`undo` - `geri al`). Her iki değişikliğin de bu tek komutla geri alındığını fark ettiniz mi? `u` tuşu, bir değişiklik, dosyadaki birkaç yeri etkilemiş olsa bile son değişikliği geri alacaktır.

12. Son olarak, dosyayı **fmt** komutu ile çalıştırarak formatını iyileştirelim. **%!fmt** yazın. Nasıl görünüyor?

    Biçimlendirilmiş dosyanız şuna benzer görünmelidir:

    ```bash
    This is the first line. Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over the lazy dog. Nobody expects a
    Spanish Inquisition!

    ```

13. **Vi**'den çıkın. Değişikliklerinizi dosyaya yazıp ardından çıkabilir (`:wq`) veya kaydetmeden çıkabilirsiniz (`:q!`).
