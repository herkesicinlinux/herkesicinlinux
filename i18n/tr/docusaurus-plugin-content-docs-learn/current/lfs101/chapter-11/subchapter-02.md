---
title: 'Temel Düzenleyiciler: Nano ve Gedit'
---

## Linux Metin Düzenleyicilerine Genel Bakış

Bir noktada, metin dosyalarını manuel olarak düzenlemeniz gerekecektir. Çevrimdışı bir e-posta oluşturuyor, `bash` veya diğer komut yorumlayıcıları için kullanılacak bir komut dosyası yazıyor, bir sistem veya uygulama yapılandırma dosyasını değiştiriyor veya C, Python veya Java gibi bir programlama dili için kaynak kodu geliştiriyor olabilirsiniz.

Linux yöneticileri, sistem yapılandırma dosyalarını oluşturmak ve değiştirmek için grafiksel yardımcı programlar kullanmak yerine bir metin düzenleyici kullanmaya yönelebilirler. Bununla birlikte, bu, doğrudan bir metin düzenleyici kullanmaktan daha zahmetli olabilir ve yeterlilik açısından daha sınırlı kalabilir. Kelime işlemci uygulamalarının (ofis uygulama paketlerinin parçası olanlar dahil), temel metin düzenleyicileri olmadığını unutmayın. Bunlar muhtemelen, sistem yönetimi yapılandırma dosyalarını kullanılamaz hale getirecek birçok ekstra (genellikle görünmez) biçimlendirme bilgisi eklerler. Bu nedenle, bir veya daha fazla metin düzenleyicinin nasıl kullanılacağını bilmek, Linux için gerçekten sahip olunması gereken önemli bir beceridir.

Şimdiye kadar, Linux'un seçeneklerle dolu olduğunu kesinlikle fark etmiş olmalısınız. Metin düzenleyicileri söz konusu olduğunda, oldukça basitten çok karmaşığa kadar birçok seçenek vardır:

- **nano**
- **gedit**
- **vi**
- **emacs**

Bu bölümde, önce görece basit ve öğrenmesi kolay olan `nano` ve `gedit` düzenleyicileri ve daha sonra daha karmaşık seçimler olan `vi` ve `emacs` hakkında bilgi edineceğiz. Başlamadan önce, bir düzenleyiciye ihtiyaç duyulmayan bazı durumlara bir göz atalım.

<figure align="center">
  <img src="/img/lfs101/ch11/img02.png" class="invert"/>
  <figcaption>Linux Metin Düzenleyicileri</figcaption>
</figure>

## Düzenleyici Kullanmadan Dosya Oluşturma

Bazen, kısa bir dosya oluşturmak isteyebilir ve tüm bir metin düzenleyiciyi açmakla uğraşmak istemeyebilirsiniz. Ek olarak, bunu yapmak, daha uzun dosyalar oluştururken bile, betik (script) dosyalarından kullanıldığında oldukça yararlı olabilir. Kabuk betiği yazmayı (shell scripting) kapsayan sonraki bölümlere başladığınızda, kendinizi bu yöntemi kullanırken bulacağınızdan hiç şüpheniz olmasın!

Düzenleyici kullanmadan bir dosya oluşturmak istiyorsanız, komut satırından bir dosya oluşturmanın ve onu içerikle doldurmanın iki standart yolu vardır.

Birincisi, **echo**'yu tekrar tekrar kullanmaktır:

```bash
$ echo birinci satır > myfile
$ echo ikinci satır >> myfile
$ echo üçüncü satır >> myfile
```

Tek bir büyüktür işareti (>) bir komutun çıktısını bir dosyaya gönderirken, çift büyüktür işaretinin (>>) yeni çıktıyı mevcut bir dosyaya ekleyeceğini unutmayın!

İkinci yol, yeniden yönlendirme (redirection) ile birlikte **cat** kullanmaktır:

```bash
$ cat << EOF > myfile
> birinci satır
> ikinci satır
> üçüncü satır
> EOF # End of file - Dosya sonu belirteci
> $
```

Her iki teknik de aşağıdaki satırları içeren bir dosya oluşturur:

```bash
birinci satır
ikinci satır
üçüncü satır
```

ve betik (script) dosyaları tarafından kullanıldığında son derece kullanışlıdır.

<figure align="center">
  <img src="/img/lfs101/ch11/img03.png" />
  <figcaption>Düzenleyici Kullanmadan Dosya Oluşturma</figcaption>
</figure>

## Nano ve Gedit

<img src="/img/lfs101/ch11/img04.png" align="right" class="invert"/>

Bazı metin editörleri vardır, öğrenmek için özel bir deneyime ihtiyaç duymazlar ve aslında oldukça yeteneklidirler, hatta güçlüdürler. Bunlardan, kullanımı özellikle kolay olanlarından bir tanesi, uçbirim tabanlı düzenleyici **nano**'dur. Kullanmaya başlamak için uçbirimde argüman olarak bir dosya adı vererek **nano**'yu çağırın (`nano yeni_dosya`). İhtiyacınız olan tüm yardım, ekranın alt kısmında görüntülenir ve herhangi bir sorun yaşamadan devam edebilmeniz gerekir.

Bir grafik düzenleyici olarak **gedit**, GNOME masaüstü sisteminin bir parçasıdır (**kwrite**, KDE'nin parçasıdır). **Gedit** ve **kwrite** düzenleyicilerinin kullanımı çok kolaydır ve son derece yeteneklidirler. Ayrıca yapılandırmaya oldukça açıktırlar. Windows Not Defteri'ne benzerler. **Kate** gibi diğer varyantlar da KDE tarafından desteklenmektedir.

### Nano

**Nano**'nun kullanımı kolaydır ve öğrenmesi çok az çaba gerektirir. Bir dosyayı açmak için **nano <dosya adı>** yazın ve Enter tuşuna basın. Dosya yoksa, oluşturulacaktır.

**nano**, ekranın altında, mevcut komutları listeleyen iki satırlık bir kısayol çubuğu sağlar. Bu komutlardan bazıları şunlardır:

- **CTRL-G**

  Yardım ekranını görüntüler.

- **CTRL-O**

  Dosyaya yazar.

- **CTRL-X**

  Dosyadan çıkar.

- **CTRL-R**

  Başka bir dosyadaki içerikleri geçerli ara belleğe (buffer) ekler.

- **CTRL-C**

  Önceki komutları iptal eder.

<figure align="center">
  <img src="/img/lfs101/ch11/img05.png" />
  <figcaption>nano</figcaption>
</figure>

### Gedit

**Gedit** (İngilizce 'ci-edit' olarak telaffuz edilir), yalnızca Grafik Masaüstü Ortamında çalıştırılabilen, kullanımı kolay bir grafik düzenleyicidir. Görsel olarak Windows Not Defteri metin düzenleyicisine oldukça benzer, ancak aslında çok daha yetenekli ve çok daha yapılandırılabilirdir ve yeteneklerini daha da genişletmek için çok sayıda eklentiye sahiptir.

Yeni bir dosya açmak için programı masaüstünüzün menü sisteminde bulun veya komut satırından **gedit <dosya adı>** yazın. Dosya yoksa, oluşturulacaktır.

**Gedit**'i kullanmak oldukça basittir ve fazla eğitim gerektirmez. Arayüzü oldukça tanıdık unsurlardan oluşur.

<figure align="center">
  <img src="/img/lfs101/ch11/img06.png" />
  <figcaption>gedit</figcaption>
</figure>

## Lab Çalışmaları

### Lab 11.1: Nano Kullanımı

Nano kullanarak, **myname.txt** adlı bir dosya oluşturacağız ve ilk satıra adımızı ve son satıra da geçerli tarihi ekleyeceğiz. Bunu yapmak için:

1. Uçbirime **`nano myfile.txt`** yazarak nano'yu başlatın.
2. Adınızı dosyanın ilk satırına yazın.
3. Dosyanın son satırına tarihi ekleyin.
4. Dosyayı kapatın.

### Lab 11.2: Gedit Kullanımı

Gedit'i kullanarak, **myname.txt** adlı bir dosya oluşturacağız (veya var olan dosyayı yeniden kullanacağız) ve ikinci satıra adresinizi ve son satıra şehrinizin adını ekleyeceğiz:

1. Uçbirime **`gedit myfile.txt`** yazarak gedit'i başlatın.
2. Adresinizi dosyanın ikinci satırına ekleyin.
3. Şehrinizin adını dosyanın son satırına ekleyin.
4. Dosyayı kapatın.
