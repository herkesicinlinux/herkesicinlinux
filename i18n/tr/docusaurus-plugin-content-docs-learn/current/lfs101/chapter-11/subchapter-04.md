---
title: Bölüm Sonu
---

## Öğrenme Hedefleri (Gözden Geçirin)

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Artık şunlara aşina olmuş olmalısınız:

- Mevcut Linux metin düzenleyicilerini kullanarak dosya oluşturma ve düzenleme.
- `nano`, basit bir metin tabanlı düzenleyici.
- **gedit**, basit bir grafiksel metin düzenleyici.
- `vi` ve `emacs`, hem metin tabanlı hem de grafik arabirimlere sahip iki gelişmiş düzenleyici.

## Özet

<img src="/img/lfs101/common/tux-grad-cap.png" align="right"/>

11. Bölümü tamamladınız. Kapsanan temel kavramları özetleyelim:

- Linux'ta metin düzenleyicileri (kelime işlem programları haricindekiler), sistem yapılandırma dosyalarını oluşturma veya değiştirme, komut dosyası yazma, kaynak kodu geliştirme vb. gibi görevler için oldukça sık kullanılmaktadır.

- Nano, ekrandaki komut istemlerini kullanan, kullanımı kolay, metin tabanlı bir düzenleyicidir.

- Gedit, Windows Not Defteri'ne oldukça benzeyen bir grafik düzenleyicidir.

- Vi editörü, tüm Linux sistemlerinde mevcuttur ve yaygın olarak kullanılmaktadır. Vi'nin grafiksel uzantı sürümleri de yaygın olarak mevcuttur.

- Emacs, vi'ye popüler bir alternatif olarak tüm Linux sistemlerinde mevcuttur. Emacs hem bir grafik kullanıcı arabirimini hem de bir metin modu arabirimini destekleyebilir.

- Vi öğreticisine erişmek için, uçbirimde **vimtutor** yazın.

- Emacs öğreticisine erişmek için, emacs içinden **Ctrl-h** ve ardından **t** yazın.

- **Vi** üç moda sahiptir: Komut, Giriş ve Satır. **Emacs** yalnızca bir moda sahiptir, ancak **Control** ve **Escape** gibi özel tuşların kullanılmasını gerektirir.

- Her iki editör de görevleri gerçekleştirmek için çeşitli tuş vuruşları kombinasyonlarını kullanır. Bunlarda ustalaşmak için gereken süre uzun olabilir, ancak ustalaştıktan sonra her iki düzenleyicinin kullanımı da son derece verimlidir.
