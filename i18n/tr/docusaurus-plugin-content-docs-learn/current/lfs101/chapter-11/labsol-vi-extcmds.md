---
title: Vi'de Harici Komutlar
---

Kaydedilmeden önce epeyce iş kaybedebileceğiniz, aşağıdakine benzer durumlar er ya da geç ortaya çıkacaktır.

1.  Düzenlemekte olduğunuz dosyanın izinlerini salt-okunur olarak değiştirin ve şunları yaparak düzenlemek için açın:

    ```bash
    student:/tmp> chmod 444 myfile
    student:/tmp> vi myfile

    ```

2.  `dd` yazarak dosyadaki ilk satırı silin. Kısa bir hata mesajı görebilirsiniz, ancak satır silinecektir.

3.  Şimdi değiştirilen dosyayı kaydetmeyi deneyelim. `:w` yazın. Ne oldu?

    Bu dosyayı kaydedemediniz, değil mi? Çünkü salt-okunur! Ekranın alt satırında, dosyanın `readonly` (`salt-okunur`) olduğunu belirten bir hata mesajı almış olmalısınız.

4.  Belki de uzun bir düzenleme oturumunda bu dosyada birçok değişiklik yaptınız. Tüm bu sıkı çalışma kayboldu mu demek oluyor? Hayır!

5.  Birkaç seçeneğiniz var. Birincisi, düzenleyiciden kabuğa gidebilir, dosyanın izinlerini değiştirebilir, kabuktan çıkabilirsiniz ve dosyanızı kaydetmek için düzenleme oturumunuza geri dönebilirsiniz.

    Bunu `:sh` yazarak yapın. Bu size bir kabuk istemi vermelidir. Şimdi komutu yazın:

    ```bash
    student:/tmp> chmod 644 myfile
    ```

    ve ardından şu komutla kabuktan çıkın:

    ```bash
    student:/tmp> exit

    ```

    **Vi** düzenleme oturumunuza geri dönmelisiniz. Genellikle, dosyanın değiştiğini belirten bir hata mesajı görürsünüz. Tamam için `O` (`shift + o`) yazın.

    Şimdi `:w!` yazın (eklenen ünlem işaretine (`!`) dikkat edin). Bu dosya şimdi kaydedilebilmelidir!

6.  Yapabileceğiniz başka bir şey de dosyanın içeriğini yeni bir dosyaya yazmaktır. `:w` komutu, dosyayı kaydetmek istediğiniz dosya adı olarak bir bağımsız değişken (argument) alabilir. Örneğin, tüm değişikliklerinizi `new_myfile` dosyasına kaydetmek için `:w new_myfile` yazabilirsiniz.

    Bu teknikleri bilmek iyidir çünkü buna benzer durumlar, **Linux** ile çalışırken zaman zaman ortaya çıkacaktır. Her iki teknikle oynayın ve her ikisine de aşina olun.
