---
title: 'Hoş Geldiniz!'
---

## Video: LFS101'e Hoş Geldiniz

<!-- [Video](https://player.vimeo.com/video/509833103?texttrack=tr) | [Alt yazı](video-01-tr.srt) -->

<iframe 
    title="vimeo-player" 
    src="https://player.vimeo.com/video/509833103?texttrack=tr" 
    width="640" 
    height="360" 
    frameborder="0" 
    allowfullscreen="true">
</iframe>

## Başlamadan Önce

LFS101 - Linux'a Giriş kursuna hoş geldiniz!

İçeriğe geçmeden önce kurs müfredatını gözden geçirmenizi şiddetle tavsiye ederiz. Aşağıdakiler kursla ilgili en önemli bilgileri sağlar:

- Kursa genel bakış
- Eğitmen biyografileri ve hedef kitle
- Kurs ön koşulları ve uzunluğu
- Kurs öğrenme hedefleri ve ana hat
- edX platform kuralları
- Tartışma forumları, kurs zamanlaması ve öğrenme yardımcıları
- Not verme, ilerleme ve kurs tamamlama
- Profesyonel Sertifika Programı
- Denetim ve doğrulanmış parçalar
- Linux Vakfı'nın geçmişi, etkinlikleri, eğitimleri ve sertifikaları.

## Yardım Almak

Platformla ilgili herhangi bir teknik sorun için lütfen ekranınızın sağ üst tarafında bulunan Yardım simgesini kullanın.

Bu kursu alan diğer kişilerle etkileşim kurmanın ve içerikle ilgili sorunları çözmenin harika bir yolu Tartışma Forumları'dır. Bu forumlar aşağıdaki şekillerde kullanılabilir:

- Bu kursta sunulan veya kurs materyalinde tartışılan konularla ilgili kavramları, araçları ve teknolojileri tartışmak.
- Ders içeriği hakkında sorular sormak.
- Linux ile ilgili kaynakları ve fikirleri paylaşmak.

Sadece soru sormanızı değil, aynı zamanda kurs içeriği ve ilgili değerli kaynaklar hakkındaki fikirlerinizi de diğer kişilerle paylaşmanızı şiddetle tavsiye ederiz. Tartışma Forumları, Linux Vakfı personeli tarafından periyodik olarak gözden geçirilecektir. Ancak esasen bir topluluk kaynağıdır, 'eğitmene sor' hizmeti değildir.

Not: Sorununuzla ilgili bir ileti dizisi başlatmadan önce, sorunuzun daha önce ele alınıp alınmadığını görmek için hızlı bir arama yapmanızı önemle tavsiye ederiz. Çoğunlukla yanıtlanmış olan aynı soruyu soran bir veya iki ileti dizisi olacaktır.

Bunları nasıl kullanacağınızla ilgili daha fazla ipucu öğrenmek için şu makaleyi okuyun: "[edX Tartışma Forumlarından En İyi Şekilde Yararlanmak](https://blog.edx.org/getting-most-out-edx-discussion-forums)".

## Eğitmeninizle Tanışın: Jerry Cooperstein

<figure>
    <img src="/img/lfs101/ch00/img01.png"/>
    <figcaption>Jerry Cooperstein</figcaption>
</figure>

Jerry Cooperstein, 1994 yılından beri Linux ile çalışıyor ve hem çekirdek hem de kullanıcı alanında eğitim geliştiriyor ve veriyor. Linux Vakfı'ndaki tüm eğitim içeriğinden genel olarak kendisi sorumludur. Nükleer astrofizikteki yirmi yıllık kariyeri boyunca, birçok süper bilgisayar türünde son teknoloji simülasyon yazılımı geliştirdi ve hem lisans hem de yüksek lisans düzeyinde öğretmenlik yaptı. Jerry, Linux Vakfı'na 2009 yılında Eğitim Programı Direktörü olarak katıldı.

## Yazardan Bir Not

Bu kurs, Linux Vakfı'nın ya fiziksel bir sınıfta ya da internet üzerinden sanal bir sınıfta her zaman canlı bir eğitmenle öğretilen orijinal "Linux'a Giriş" kursundan gelmektedir. Bu kursu ücretsiz bir şekilde geniş bir topluluğun kullanımına açık bir kursa dönüştürmek, Linux Vakfı'nın değerli olduğuna ve takdir edileceğine inandığı bir görevdi. EdX için Massive Open Online Course (MOOC) formatında kendi hızımıza uygun bir kursa dönüştürme sürecine başladığımızda, bunun ne kadar zor bir iş olacağı ve sunumda ne kadar değişiklik yapmamız gerektiği konusunda hiçbir fikrimiz yoktu. Böylece hem canlı bir eğitmen olmadan kendi başına durabilir hem de öğrencilerin ilgisini çekecek kadar eğlenceli olabilir.

Bu kursun yazarı ve eğitmeni olarak listelenmiş olsam da gerçek şu ki, gelişimine zaman ve yeteneklerini katan birçok kişinin çabası olmadan asla üretilemezdi. İlk ve en önemlisi, içerik geliştirme ve sunumun açıklığa kavuşturulması esnasında amansız döngülerde derinden yer almış olan Linux Vakfı'ndan Flavia Cioanca, Magdalena Stepien ve Clyde Seepersad'a teşekkür etmeliyim.

Son olarak, Linux Vakfı'ndaki tüm meslektaşlarıma bunu bir ekip çalışması haline getirdikleri, bunun gerçekleşmesi için kaynaklara yatırım yaptıkları, biz bu işle meşgulken diğer sorumluluklarımın bazılarını karşılamaya yardımcı oldukları için ve ayrıca materyal hazırlanırken gözden geçirdikleri için teşekkür ederim. Kursun her bölümünü başlatan video tanıtımlarda bazı yüzleri ve isimleri görmekten keyif alacağınızı umuyorum.
