1
00:00:04,260 --> 00:00:09,220
Hey, merhaba. Ben Linus Torvalds ve Linux'u 1991 yılında başlattım.

2
00:00:09,220 --> 00:00:18,080
"Linux'a Giriş" kursuna hoşgeldiniz. Umarım ondan benim yıllar içinde aldığım kadar keyif alırsınız.

3
00:00:18,080 --> 00:00:24,060
Linux ile ilgili ilginç kısımlardan biri en beklenmedik yerlerde nasıl ortaya çıktığıdır.

4
00:00:24,060 --> 00:00:31,740
Yani, Linux'u başlattığımda kendi kullanımım için bir işletim sistemine ihtiyacım vardı ve bugün küçük gömülü cihazlarda ...

5
00:00:31,740 --> 00:00:37,760
süper bilgisayarlarda, her yerde Linux buluyorsunuz. 

6
00:00:37,760 --> 00:00:45,080
Bu teknolojinin birçok farklı nişe genişlemenize nasıl izin verdiği biraz ilginç.

7
00:00:45,080 --> 00:00:52,190
Linux'u benim için ilginç kılan şey tüm bu ilginç teknoloji, ama aynı zamanda dahil olan tüm insanlar ...

8
00:00:52,190 --> 00:00:58,620
yüzlerce, potansiyel olarak binlerce kişiyle açık kaynak projelerinde çalışmak ...

9
00:00:58,620 --> 00:01:01,250
tüm bu teknolojiyi daha da ilginç hale getiriyor.

10
00:01:01,250 --> 00:01:07,660
Umarım bu, yapmayı gerçekten ilginç bulduğunuz bir şey bulmanız için bir fırsata dönüşür.

11
00:01:07,660 --> 00:01:13,740
Linux ve genel olarak açık kaynakla ilgili güzel şeylerden biri, birçok farklı şeyin yanında ...

12
00:01:13,740 --> 00:01:17,460
herkesin projeye katabileceği bir şeye sahip olmasıdır.

13
00:01:17,460 --> 00:01:22,460
Bu kursa katıldığınız için teşekkürler ve iyi eğlenceler!

