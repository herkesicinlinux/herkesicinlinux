---
title: Grafik Masaüstü
---

## Grafik Masaüstü

Linux kullanırken bir komut satırı arayüzü(**C**ommand **L**ine **I**nterface veya **CLI**) veya bir grafik kullanıcı arayüzü(**G**raphical **U**ser **I**nterface veya **GUI**) kullanabilirsiniz. CLI'da çalışmak için; görevleri gerçekleştirmek için hangi programların ve komutların kullanıldığını, bunların kullanımı ve seçenekleri hakkında nasıl hızlı ve doğru bir şekilde daha fazla bilgi edinebileceğinizi hatırlamanız gerekir. Öte yandan, GUI kullanmak genellikle hızlı ve kolaydır. Grafik simgeler ve ekranlar aracılığıyla sisteminizle etkileşim kurmanıza olanak tanır. Tekrarlayan görevler için CLI genellikle daha verimlidir, fakat tüm ayrıntıları hatırlamadığınız veya bir şeyleri nadiren yaptığınız durumlarda GUI'da gezinmek daha kolaydır.

Bu kursta en çok ele aldığımız üç Linux dağıtım ailesi için GUI kullanarak oturumları nasıl yöneteceğimizi öğreneceğiz: Red Hat (CentOS, Fedora), SUSE (openSUSE) ve Debian (Ubuntu, Mint). OpenSUSE'nin KDE tabanlı varyantı yerine GNOME tabanlı varyantını kullanıyoruz, yine de hepsi oldukça benzer. KDE (veya XFCE gibi diğer Linux masaüstlerini) kullanıyorsanız, deneyiminiz gösterilenden biraz farklı olacaktır, ancak doğası gereği zor olmayacaktır çünkü kullanıcı arayüzleri, modern işletim sistemlerinde, benimsenmiş belirli davranışlara yakınlaşmışlardır. Bu kursun sonraki bölümlerinde, tüm dağıtımlarda hemen hemen aynı olan komut satırı arayüzüne detaylıca odaklanacağız.

<figure align="center">
  <img src="/img/lfs101/ch04/img02.png" />
  <figcaption>Ubuntu, CentOS, and openSUSE Desktops</figcaption>
</figure>

## X Pencere Sistemi

Genel olarak, bir Linux masaüstü sisteminde, X Pencere Sistemi, önyükleme işleminin son adımlarından biri olarak yüklenir. Genellikle sadece X olarak adlandırılır.

**Görüntü Yöneticisi** adı verilen bir hizmet, sağlanan ekranların kaydını tutar ve X sunucusunu yükler (sözde, çünkü uygulamalara grafiksel hizmetler sağlar, bazen X istemcileri olarak adlandırılır). Görüntü yöneticisi ayrıca grafiksel oturum açma işlemlerini de yönetir ve bir kullanıcı oturum açtıktan sonra uygun masaüstü ortamını başlatır.

X oldukça eski bir yazılımdır; 1980'lerin ortalarına kadar uzanır ve bu nedenle, orijinal amaçlarından oldukça uzak olduğu için modern sistemlerde (örneğin, güvenlik) bazı eksiklikleri vardır. [Wayland](https://wayland.freedesktop.org/) olarak bilinen daha yeni bir sistem yavaş yavaş onun yerini alıyor ve Fedora, RHEL 8 ve diğer yeni dağıtımlar için varsayılan görüntüleme sistemidir. Kaputun altında oldukça farklı olmasına rağmen, kullanıcıya büyük oranda tıpkı X gibi görünür.

<figure align="center">
  <img src="/img/lfs101/ch04/img03.png" class="invert"/>
  <figcaption>Görüntü Yöneticisi</figcaption>
</figure>

### X Hakkında Daha Fazla Bilgi

Bir masaüstü ortamı, grafiksel oturumun bileşenlerini başlatan ve koruyan bir oturum yöneticisinden ve pencerelerin, pencere başlık çubuklarının ve kontrollerin yerleşimini ve hareketini kontrol eden pencere yöneticisinden oluşur.

Bunlar karıştırılabilse de, genellikle bir dizi yardımcı program, oturum yöneticisi ve pencere yöneticisi bir birim olarak birlikte kullanılır ve birlikte sorunsuz bir masaüstü ortamı sağlar.

Görüntü yöneticisi varsayılan olarak varsayılan çalışma düzeyinde başlatılmazsa, metin modu konsolunda oturum açtıktan sonra komut satırından **startx** çalıştırarak grafik masaüstünü farklı bir şekilde başlatabilirsiniz. Veya ekran yöneticisini (**gdm**, **lightdm**, **kdm**, **xdm**, vb.) komut satırından manuel olarak başlatabilirsiniz. Ekran yöneticileri bir oturum açma ekranı yansıtacağından, bu, **startx** çalıştırmaktan farklıdır. Sırada onlar var.

<figure align="center">
  <img src="/img/lfs101/ch04/img04.png" class="invert"/>
  <figcaption>Masaüstü Ortamı</figcaption>
</figure>

## GUI Başlangıcı

<img src="/img/lfs101/ch04/img05.png" align="right" class="invert"/>

Bir masaüstü ortamı kurduğunuzda, X görüntü yöneticisi önyükleme işleminin sonunda başlar. Grafik sistemini başlatmaktan, kullanıcının oturumunu açmaktan ve kullanıcının masaüstü ortamını başlatmaktan sorumludur. Sistemde oturum açarken genellikle çeşitli masaüstü ortamları arasından seçim yapabilirsiniz.

GNOME için varsayılan görüntü yöneticisi **gdm** olarak adlandırılır. Diğer popüler ekran yöneticileri arasında **lightdm** (18.04 LTS sürümünden önce Ubuntu'da kullanılır) ve **kdm** (KDE ile ilişkili) bulunur.

## GNOME Masaüstü Ortamı

<img src="/img/lfs101/ch04/img06.png" align="right" class="invert"/>

GNOME, kullanımı kolay bir grafik kullanıcı arayüzüne sahip popüler bir masaüstü ortamıdır. Red Hat Enterprise Linux (RHEL), Fedora, CentOS, SUSE Linux Enterprise, Ubuntu ve Debian dahil olmak üzere çoğu Linux dağıtımı için varsayılan masaüstü ortamı olarak paketlenmiştir. GNOME'un menü tabanlı navigasyonu vardır ve bazen Windows kullanıcıları için gerçekleştirilmesi kolay bir geçiştir. Ancak, göreceğiniz gibi, hepsi GNOME kullanıyor olsalar bile, görünüm ve his dağıtımlar arasında oldukça farklı olabilir.

Linux tarihinde çok önemli olan ve aynı zamanda yaygın olarak kullanılan diğer bir yaygın masaüstü ortamı, genellikle SUSE ve openSUSE ile birlikte kullanılan KDE'dir. Bir masaüstü ortamı için diğer alternatifler arasında Unity (eski Ubuntu'da mevcut, ancak yine de GNOME'ye dayanmaktadır), XFCE ve LXDE bulunur. Daha önce bahsedildiği gibi, çoğu masaüstü ortamı GNOME'ye benzer bir yapı izler ve işleri daha az karmaşık tutmak için kendimizi çoğunlukla bununla sınırlayacağız.

## Video: Sistem Başlatma ve Oturum Açma ve Kapatma

Bir openSUSE sisteminde sistemin nasıl başlatıldığını ve kullanıcı oturum açma bilgilerine nasıl ulaştığını göstermek için aşağıdaki <img src="/img/lfs101/common/play.png"/> butonuna tıklayın. Red Hat/CentOS ve Ubuntu/Debian dahil olmak üzere tüm yeni dağıtımlarda önyükleme, oturum açma ve oturumu kapatma prosedürleri yalnızca küçük kozmetik şekillerde farklılık gösterir. Daha sonra diğer dağıtımlar için de örnekler göstereceğiz.

### Sistem Başlatma ve Oturum Açma ve Kapatma

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V007800_DTH.mp4) | [Alt yazı](video-02-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Grafik Masaüstü Arka Planı

<img src="/img/lfs101/ch04/img08.png" align="right" class="invert"/>

Her Linux dağıtımı kendi masaüstü arka plan setiyle birlikte gelir. Yeni bir duvar kağıdı seçerek veya masaüstü arka planı olarak ayarlanacak özel bir resim seçerek varsayılanı değiştirebilirsiniz. Arka plan olarak bir görüntü kullanmak istemiyorsanız, bunun yerine masaüstünde görüntülenecek bir renk seçebilirsiniz.

Ek olarak, Linux sisteminin görünümünü ve hissini değiştiren masaüstü temasını da değiştirebilirsiniz. Tema ayrıca uygulama pencerelerinin görünümünü de tanımlar.

Masaüstü arka planını ve temasını nasıl değiştireceğimizi öğreneceğiz.

### Masaüstü Arka Planını Özelleştirme

Arka planı değiştirmek için, masaüstünde herhangi bir yeri sağ tıklayıp **Arka Planı Değiştir**'i seçebilirsiniz.

<figure align="center">
  <img src="/img/lfs101/ch04/img09.png" />
  <figcaption>Masaüstü Arka Planı Nasıl Özelleştirilir</figcaption>
</figure>

### Video: Masaüstü Arka Planı Nasıl Değiştirilir

Red Hat 7/CentOS 7'de arka planın nasıl değiştirileceğine dair bir gösteri izlemek için aşağıdaki <img src="/img/lfs101/common/play.png"/> butonuna tıklayın.

**Not**: Tüm yeni GNOME tabanlı masaüstü ortamları bunu aynı şekilde ele alır.

### Masaüstü Arka Planı Nasıl Değiştirilir

[Video](https://edx-video.net/LINILXXX2017-V000600_DTH.mp4) | [Alt yazı](video-03-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Kendin Dene: Masaüstü Arka Planını Değiştirme

Bu bölümde, openSUSE'de masaüstü arka planını değiştirme alıştırması yapacağız. Kesin ayrıntılar, Linux dağıtımınız için çok az farklılık gösterebilir.

Lütfen aşağıdaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/backgroundopensuse/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/backgroundopensuse/index.html?lang=tr" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## gnome-tweak-tool

Hem kişisel hem de sistem genelinde en yaygın ayarlar, Linux dağıtımınıza bağlı olarak sağ üst köşede bir dişli veya başka bir belirgin simgeye tıklayarak bulunabilir.

Bununla birlikte, birçok kullanıcının değiştirmek istediği ve bu şekilde erişilemeyen birçok ayar vardır; varsayılan ayarlar yardımcı programı, modern GNOME tabanlı dağıtımlarda ne yazık ki oldukça sınırlıdır. Ne yazık ki, basitlik arayışı aslında sisteminizi zevklerinize ve ihtiyaçlarınıza göre uyarlamayı zorlaştırdı.

Neyse ki, çok daha fazla ayar seçeneğini ortaya çıkaran standart bir yardımcı program, gnome-tweak-tool, vardır. Ayrıca, harici taraf uzantıları kolayca yüklemenize izin verir. Her Linux dağıtımı bu aracı varsayılan olarak kurmaz, ancak her zaman kullanılabilirdir. Bazı yeni dağıtımlar bu aracı **gnome-tweaks** olarak yeniden adlandırdı. **Alt-F2** tuşlarına basarak ve ardından adını yazarak çalıştırmanız gerekebilir. Bunu tartışacağımız için **Favoriler** listenize eklemek isteyebilirsiniz.

Aşağıdaki ekran görüntüsünde, klavye eşlemesi, gereksiz **CapsLock** tuşunun ek bir **Ctrl** tuşu olarak kullanılabilmesi için ayarlanmaktadır; Bu, **Ctrl**'yi çok kullanan (**emacs** meraklıları gibi) kullanıcıları serçe parmağı zorlanmasından fiziksel olarak zarar görmekten kurtarır.

<figure align="center">
  <img src="/img/lfs101/ch04/img10.png" />
  <figcaption>gnome-tweak-tool</figcaption>
</figure>

## Temayı Değiştirme

Uygulamaların görsel görünümü (düğmeler, kaydırma çubukları, araçlar ve diğer grafik bileşenler) bir tema tarafından kontrol edilir. GNOME, uygulamalarınızın görünümünü değiştirebilecek bir dizi farklı temayla birlikte gelir.

Temanızı değiştirmenin kesin yöntemi dağıtımınıza bağlı olabilir. Ancak, tüm GNOME tabanlı dağıtımlar için, Ubuntu'nun ekran görüntüsünde gösterildiği gibi basitçe **gnome-tweak-tool** çalıştırabilirsiniz.

Varsayılan seçimin ötesinde ek temalar almak için başka seçenekler de vardır. Temaları [GNOME'nin Wiki](https://wiki.gnome.org/Personalization) web sitesinden indirebilir ve yükleyebilirsiniz.

<figure align="center">
  <img src="/img/lfs101/ch04/img11.png" />
  <figcaption>Temayı Değiştirme</figcaption>
</figure>

## Lab Çalışmaları

### Lab 4.1: Masaüstünü Özelleştirme

Bu bölümün uzunluğuna rağmen, Linux dağıtımlarının ve sürümlerinin çeşitliliği nedeniyle ve temel kod tabanı aynı olsa bile her biri masaüstlerini özelleştirdikleri için adım adım laboratuar çalışmaları yapmayacağız. Kesin talimat vermeye çalışmak boşuna bir egzersizdir; sadece birçok varyasyon olması değil, Linux dağıtımları her yeni sürümü yayınlandığında değişmeye meyillidirler.

Çoğunlukla bu bir sorun değil. Grafik arayüzler, gezinmesi ve anlaşılması kolay olacak şekilde tasarlanmıştır ve yalnızca bir dağıtımdan diğerine değil, işletim sistemleri arasında bile çok fazla değişiklik göstermezler. Bu nedenle, masaüstünüzde verimli bir şekilde çalışma konusunda daha usta olmanın tek yolu, sadece keşfetmek, oynamak ve değişiklik yapmaktır. Aynı noktalar bir sonraki bölüm, grafik sistem konfigürasyonu için de geçerli olacaktır.

Linux o kadar özelleştirilebilir ki, onu kullanan çok az kişi masaüstünün varsayılan görünümü ve hissini kullanmayı sürdürür. Şimdi masaüstünüzü beğenilerinizi ve kişiliğinizi yansıtmaya başlayabilirsiniz.

- Masaüstü arka planını zevkinize daha uygun bir şeye değiştirerek başlayın; belki sağlanan arka planlardan biri, seçtiğiniz tek bir renk veya Linux ortamınıza aktarabileceğiniz kişisel bir resim.
- Ardından, dağıtımınız için mevcut temalardan zevkinize ve kişiliğinize uyan bir tema seçin. Bu egzersizle eğlenin ve keşfedin.

Laboratuvar alıştırmasının çözümünü görüntülemek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-desktop)
