---
title: Dosya sıralama seçeneklerini görüntüleme
---

Dosya tarayıcı penceresini açmanız ve **/var/log** dizinine gitmeniz gerekecektir. Bu, dağıtımdan dağıtıma biraz farklılık gösterecektir. En yeni dağıtımlarda **Diğer Konumlar -> Bilgisayar** seçeneğini tıklayacaksınız.

**/var/log** dizinine gidin. Görünüm seçeneğini **Simge** yerine **Liste** olarak ayarlayın ve ardından tarihe tıklayın.
