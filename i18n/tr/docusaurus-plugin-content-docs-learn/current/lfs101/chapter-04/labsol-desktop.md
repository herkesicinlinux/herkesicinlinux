---
title: Masaüstünü Özelleştirme
---

Masaüstünüzü özelleştirmenin en kolay yolu, bir menü açması için masaüstünüze sağ tıklamaktır. Bu noktada, çalıştırdığınız Linux dağıtımına bağlı olarak **Masaüstü Arka Planını Değiştir** veya **Varsayılan Masaüstü Ayarlarını** seçin.

Bu, daha sonra mevcut arka plan resimlerini, düz renkli bir arka planı seçebileceğiniz veya ev dizininizdeki Resimler klasörünüze eklediğiniz kendi resminizi kullanabileceğiniz bir pencere açar.

**Temanızı** değiştirmek için, pencerede **Tema** etiketli arka planı değiştirdiğiniz bir sekme fark etmiş olabilirsiniz. Varsa, bu sekmeye tıklayıp mevcut temalardan birini seçebilir veya kendi kreasyonlarınızı denemek için Özelleştir'e tıklayabilirsiniz.

Ancak, **GNOME 3** için, artık bu şekilde arka planı ayarlamak dışında bir şey yapamazsınız. Bunun yerine **gnome-tweaks** (veya **gnome-tweak-tool**) çalıştırmanız ve ardından **Tema** veya **Görünüm-> Tema** seçeneği için kaydırmanız gerekir.

Gerçekte **Ayarlar** menüsünde olması beklenebilecek diğer birçok ayarın artık yalnızca **gnome-tweaks** ile yapılandırılabildiğini unutmayın. Bu, siz bu gerçeği keşfedene kadar saçınızı başınızı yolmanıza neden olabilir.
