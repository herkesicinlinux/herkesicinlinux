---
title: Oturum Yönetimi
---

## Oturum Açma ve Kapatma

<img src="/img/lfs101/ch04/img12.png" align="right" class="invert"/>

Sonraki birkaç ekran, bu kursta ele aldığımız üç Linux dağıtım ailesinin her bir üyesi için gösterileri ve Kendiniz Deneyin faaliyetlerini kapsamaktadır. Seçtiğiniz dağıtım türü için bir gösteri görüntüleyebilir ve ilgili Kendiniz Deneyin etkinliği aracılığıyla prosedürü uygulayabilirsiniz.

### Video: Ubuntu, openSUSE ve CentOS'ta GUI Kullanarak Oturum Açma ve Oturumu Kapatma

Üç kurumsal Linux dağıtımında grafik masaüstü yöneticisinde oturum açma ve oturum kapatma hakkında bir gösteri görüntülemek için aşağıdaki <img src="/img/lfs101/common/play.png"/> butonuna tıklayın: CentOS 7, openSUSE ve Ubuntu 18.04. Fedora, Debian ve Gentoo dahil olmak üzere diğer son GNOME tabanlı Linux dağıtımlarında deneyimler temelde aynı olacaktır.

### Ubuntu, openSUSE ve CentOS'ta GUI kullanarak Oturum Açma ve Oturumu Kapatma

[Video](https://edx-video.net/LINILXXX2017-V000700_DTH.mp4) | [Alt yazı](video-04-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Kendin Dene: Oturum Açma ve Oturumu Kapatma

Bu bölümde, GUI openSUSE kullanarak oturum açma ve oturumu kapatma alıştırması yapacağız.

**_Not:_** Diğer dağıtımları neredeyse tamamen aynı olduklarından göstermedik.

Lütfen aşağıdaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/loginsuse/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/loginsuse/index.html?lang=tr" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Ekranı Kilitleme

Başkalarının siz bilgisayarınızdan uzaktayken oturumunuza erişmesini önlemek için ekranınızı kilitlemek genellikle iyi bir fikirdir.

**_Not:_** Bu, bilgisayarı askıya almaz; tüm uygulamalarınız ve işlemleriniz ekran kilitliyken çalışmaya devam eder.

Ekranınızı kilitlemenin iki yolu vardır:

- Grafik arayüzün kullanmak
  Masaüstünün sağ üst köşesine tıklayın ve ardından kilit simgesine tıklayın.
- Klavye kısayolunu kullanarak **SUPER-L**
  (**SUPER** tuşu, Windows tuşu olarak da bilinir).
  Ekranı kilitlemek için klavye kısayolu, klavye ayarları değiştirilerek değiştirilebilir, tam yönerge dağıtıma göre değişir, ancak belirlenmesi zor değildir.

Masaüstü oturumuna yeniden girmek için parolanızı yeniden girmeniz yeterlidir.

Aşağıdaki ekran görüntüsü, Ubuntu için ekranın nasıl kilitleneceğini gösterir. Modern dağıtımlarda ayrıntılar çok az değişiklik gösterir.

<figure align="center">
  <img src="/img/lfs101/ch04/img13.png" />
  <figcaption>Ubuntu için Ekranı Kilitleme</figcaption>
</figure>

### Ekranı Kilitleme ve Kilidi Açma Hakkında Ayrıntılı Bilgi

OpenSUSE'de ekranınızı kilitlemek ve kilidini açmak için aşağıdaki adımları uygulayın:

1. Masaüstünün sağ üst köşesindeki güç simgesine tıklayın.
2. Kilit simgesine tıklayın. Ekran hemen kilitlenir.
3. Enter tuşuna basın. Oturum açma ekranı görüntülenir.
4. Ekranın kilidini açmak için parolayı girin.
5. Kilidi Aç'a tıklayın ve masaüstü ekranı görüntülenir.

**_Not:_** Ekranı kilitlediğinizde, GNOME ayarlarınıza bağlı olarak ekranı karartacak veya bir ekran koruyucu çalıştıracaktır.

<figure align="center">
  <img src="/img/lfs101/ch04/img14.png" />
  <figcaption>OpenSUSE'de Ekranı Kilitleme ve Açma</figcaption>
</figure>

## Kullanıcı Değiştirme

<img src="/img/lfs101/ch04/img15.png" align="right" class="invert"/>

Linux, birden fazla kullanıcının aynı anda oturum açmasına izin veren gerçek bir çok kullanıcılı işletim sistemidir. Sistemi birden fazla kişi kullanıyorsa, her kişinin kendi kullanıcı hesabı ve şifresine sahip olması en iyisidir. Bu, kişiselleştirilmiş ayarlara, ev dizinlerine ve diğer dosyalara izin verir. Kullanıcılar, herkesin oturumlarını canlı tutarken makineyi sırayla kullanabilir ve hatta ağ üzerinden aynı anda oturum açabilirler.

**_Not:_** Sonraki birkaç ekran, bu kursta ele aldığımız üç Linux dağıtım ailesinin her bir üyesi için gösterileri ve Kendiniz Deneyin faaliyetlerini kapsamaktadır. Seçtiğiniz dağıtım türü için bir gösteri görüntüleyebilir ve ilgili Kendiniz Deneyin etkinliği aracılığıyla prosedürü uygulayabilirsiniz.

### Video: Ubuntu'da Kullanıcı Değiştirme

Ubuntu makinesi çalıştırırken kullanıcılar arasında nasıl geçiş yapılacağına dair bir demo görüntülemek için aşağıdaki <img src="/img/lfs101/common/play.png"/> butonuna tıklayın. Adımlar esasen tüm Linux dağıtımlarında aynıdır.

### Ubuntu'da Kullanıcı Değiştirme

[Video](https://edx-video.net/LINILXXX2017-V000800_DTH.mp4) | [Alt yazı](video-05-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Kendin Dene: Kullanıcı Değiştirme

Bu bölümde, Ubuntu'da kullanıcı değiştirme alıştırması yapacağız.

**_Not:_** Neredeyse tamamen aynı olduklarından diğer dağıtımları göstermedik.

Lütfen aşağıdaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/switchuserubuntu/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/switchuserubuntu/index.html?lang=tr" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Kapatma ve Yeniden Başlatma

<img src="/img/lfs101/ch04/img16.png" align="right" class="invert"/>

Bilgisayarın normal günlük başlatılması ve durdurulmasının yanı sıra, belirli büyük sistem güncellemelerinin bir parçası olarak, genellikle yalnızca yeni bir Linux çekirdeği yüklemeyi içerenler, sistemin yeniden başlatılması gerekebilir.

Kapatma sürecini grafik masaüstünden başlatmak, tüm mevcut Linux dağıtımlarında oldukça basittir ve çok az değişiklik vardır. **shutdown** komutunu kullanarak, bunu komut satırından nasıl yapacağımızı daha sonra tartışacağız.

Her durumda, bir ayara (dişli) veya bir güç simgesine tıklarsınız ve yönergeleri takip edersiniz. Biz detayları sadece Ubuntu Linux dağıtımı için göstereceğiz.

### Ubuntu'da Kapatma ve Yeniden Başlatma

Bilgisayarı herhangi bir GNOME tabanlı Linux dağıtımında kapatmak için aşağıdaki adımları uygulayın:

1. Ekranın sağ üst köşesindeki **Güç** veya **Dişli** simgesini tıklayın.
2. **Kapat**, **Yeniden Başlat** veya **İptal** seçeneğine tıklayın. Hiçbir şey yapmazsanız, sistem 60 saniye içinde kapanacaktır.

Kapatma, yeniden başlatma ve oturumu kapatma işlemleri devam etmeden önce onay isteyecektir. Bunun nedeni, birçok uygulamanın bu şekilde sonlandırıldığında verilerini düzgün şekilde kaydetmemesidir.

Yeniden başlatmadan, kapatmadan veya oturumu kapatmadan önce daima belgelerinizi ve verilerinizi kaydedin.

<figure align="center">
  <img src="/img/lfs101/ch04/img17.png" />
  <figcaption>Kapatma ve Yeniden Başlatma</figcaption>
</figure>

## Askıya Alma

Tüm modern bilgisayarlar, bilgisayarınızı bir süre kullanmayı bırakmak istediğinizde **Askıya Alma** (veya **Uyku**) **Modunu** destekler. _Askıya Alma Modu_ mevcut sistem durumunu kaydeder ve açık kalırken oturumunuza daha hızlı devam etmenize izin verir, ancak uyku durumunda çok az güç kullanır. Sisteminizin uygulamalarını, masaüstünü vs. sistem belleğinde tutarak çalışır, ancak diğer tüm donanımları kapatır. Bu, tam sistem başlatma süresini kısaltır ve pil gücünü korur. Modern Linux dağıtımlarının aslında çok hızlı önyüklendiğini ve tasarruf edilen zaman miktarının genellikle küçük olduğunu belirtmek gerekir.

### Sistemi Askıya Alma

Sistemi askıya almak için prosedür, ekranı kapatma veya kilitlemeyle aynı şekilde başlar.

Yöntem, son GNOME tabanlı dağıtımlarda oldukça basit ve evrenseldir. **Güç** simgesine tıklar ve kısa bir süre basılı tutup bırakırsanız, aşağıda görüntülenen çift çizgi simgesini göreceksiniz, ardından sistemi askıya almak için tıklayacaksınız.

**_Not:_** Sisteminizi uyandırmak ve oturumunuzu devam ettirmek için fareyi hareket ettirin veya klavye üzerindeki herhangi bir düğmeye basın. Sistem, sanki elle kilitlemişsiniz gibi ekran kilitliyken uyanacaktır; devam etmek için parolanızı yazın.

<figure align="center">
  <img src="/img/lfs101/ch04/img18.png" class="invert"/>
  <figcaption>Sistemi Askıya Alma</figcaption>
</figure>
