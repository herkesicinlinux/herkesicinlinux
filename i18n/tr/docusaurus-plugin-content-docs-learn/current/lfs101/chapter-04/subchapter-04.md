---
title: Temel İşlemler
---

## Temel İşlemler

Deneyimli kullanıcılar bile, bir uygulamayı başlatan kesin komutu veya tam olarak hangi seçenekleri ve argümanları gerektirdiğini unutabilir. Neyse ki Linux, uygulamaları grafik arayüzü kullanarak hızlı bir şekilde açmanıza izin verir.

Uygulamalar Linux'ta (ve GNOME içinde) farklı yerlerde bulunur:

- Sol üst köşedeki **Uygulamalar** menüsünden
- Sol üst köşedeki **Etkinlikler** menüsünden
- Bazı **Ubuntu** sürümlerinde, sol üst köşedeki panel butonundan
- **KDE** ve diğer bazı ortamlar için, uygulamalar sol alt köşedeki butondan açılabilir.

Sonraki sayfalarda, grafik arayüzü kullanarak Linux'ta temel işlemlerin nasıl gerçekleştirileceğini öğreneceksiniz.

<figure align="center">
  <img src="/img/lfs101/ch04/img19.png" />
  <figcaption>CentOS'ta Uygulamaları Açma</figcaption>
</figure>

<figure align="center">
  <img src="/img/lfs101/ch04/img20.png" />
  <figcaption>OpenSUSE'de Uygulamaları Açma</figcaption>
</figure>

<figure align="center">
  <img src="/img/lfs101/ch04/img21.png" />
  <figcaption>Ubuntu'da Açılış Uygulamaları</figcaption>
</figure>

## Uygulamaları Bulma

Diğer işletim sistemlerinden farklı olarak, Linux'un ilk kurulumu genellikle bilgisayarınızla çok çeşitli görevleri gerçekleştirmenizi sağlayan çok çeşitli uygulamalar ve binlerce program içeren yazılım arşivleriyle birlikte gelir. Çoğu temel görev için, varsayılan bir uygulama genellikle zaten yüklenmiştir. Ancak, her zaman daha fazla uygulama yükleyebilir ve farklı seçenekleri deneyebilirsiniz.

Örneğin, Firefox, birçok Linux dağıtımında varsayılan tarayıcı olarak popülerdir; Epiphany, Konqueror ve Chromium (Google Chrome için açık kaynak tabanı) genellikle yazılım depolarından kurulabilir. Opera ve Chrome gibi sahipli web tarayıcıları da mevcuttur.

Uygulamaları GNOME ve KDE menülerinden bulmak kolaydır, çünkü bunlar işlevsel alt menüler içinde özenle düzenlenmiştir.

<figure align="center">
  <img src="/img/lfs101/ch04/img22.png" />
  <figcaption>Uygulamaları Bulma</figcaption>
</figure>

## Varsayılan Uygulamalar

Çeşitli görevleri gerçekleştirmek ve belirli bir türdeki bir dosyayı açmak için birden çok uygulama mevcuttur. Örneğin, bir e-postayı okurken bir web adresini tıklayabilir ve Firefox veya Chrome gibi bir tarayıcı başlatabilirsiniz.

Varsayılan uygulamaları ayarlamak için **Ayarlar** menüsüne (tüm son Linux dağıtımlarında) girin ve ardından **Ayrıntılar->Sistem->Varsayılan Uygulamalar**'a tıklayın. Tam liste, sisteminizde gerçekte kurulu ve mevcut olanlara göre, buradaki Ubuntu ekran görüntüsünde gösterilenden farklı olacaktır.

<figure align="center">
  <img src="/img/lfs101/ch04/img23.png" />
  <figcaption>Varsayılan Uygulamalar</figcaption>
</figure>

### Video: Varsayılan Uygulamaları Ayarlama

Red Hat 7 sisteminde varsayılan uygulamaları ayarlamanın bir gösterimini görüntülemek için aşağıdaki <img src="/img/lfs101/common/play.png"/> butonuna tıklayın.

### Varsayılan Uygulamaları Ayarlama

[Video](https://edx-video.net/LINILXXX2017-V002500_DTH.mp4) | [Alt yazı](video-06-tr.srt)

### Kendin Dene: Varsayılan Uygulamaları Bulma ve Ayarlama

Bu bölümde, seçtiğiniz dağıtımda varsayılan uygulamaları bulma ve ayarlama alıştırması yapacağız.

1. Lütfen aşağıdaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefubuntu/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefubuntu/index.html?lang=tr" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

2. Lütfen aşağıdaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefsuse/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefsuse/index.html?lang=tr" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Dosya Yöneticisi

Her dağıtım, dosya sisteminde gezinmek için kullanılan **Nautilus** (**Dosya Yöneticisi**) yardımcı programını kullanır. Dosyaları bulabilir ve bir dosyaya tıklandığında ya -bir programsa- çalışır ya da dosyayı veri olarak kullanarak ilişkili bir uygulama başlatılır. Diğer işletim sistemlerini kullananlar bu davranışa tamamen aşinadır.

Dosya yöneticisini başlatmak için, genellikle **Sık Kullanılanlar** veya **Aksesuarlar** altında kolayca bulunan simgesine (dosya dolabı) tıklamanız gerekir. **Dosyalar** adına sahip olacaktır.

Bu, **Ev** dizininizin görüntülendiği bir pencere açacaktır. Dosya Yöneticisi penceresinin sol paneli, **Masaüstü**, **Belgeler**, **İndirmeler** ve **Resimler** gibi yaygın olarak kullanılan dizinlerin bir listesini tutar.

Dosyaları veya dizinleri (klasörler) aramak için sağ üstteki _Büyüteç_ simgesine tıklayabilirsiniz.

<figure align="center">
  <img src="/img/lfs101/ch04/img24.png" />
  <figcaption>Dosya Yöneticisi</figcaption>
</figure>

## Ev Dizinleri

Dosya Yöneticisi, Ev dizini, **Masaüstü**, **Belgeler**, **Resimler** ve **Diğer Konumlar** dahil, bilgisayarınız ve ağdaki farklı konumlara erişmenizi sağlar.

Sistemde hesabı olan her kullanıcının, genellikle **/home** altında oluşturulan ve genellikle **/home/ogrenci** gibi kullanıcıya göre adlandırılmış bir ev dizini olacaktır.

Varsayılan olarak, kullanıcının kaydettiği dosyalar buradan başlayan bir dizin ağacına yerleştirilecektir. İster sistem kurulumu sırasında ister daha sonra, yeni bir kullanıcı eklendiğinde, hesap oluşturma, kullanıcının ev dizini altında **Belgeler**, **Masaüstü** ve **İndirilenler** gibi varsayılan dizinlerin oluşturulmasını da sağlar.

Ubuntu için gösterilen ekran görüntüsünde, liste formatını seçtik ve ayrıca gizli dosyaları da gösteriyoruz (nokta ile başlayanlar). Kendi dağıtımınızda aynısını yapıp yapamayacağınıza bir bakın.

<figure align="center">
  <img src="/img/lfs101/ch04/img25.png" />
  <figcaption>Ev Dizinleri</figcaption>
</figure>

<figure align="center">
  <img src="/img/lfs101/ch04/img26.png" />
  <figcaption>Diğer Konumlar</figcaption>
</figure>

## Dosyaları Görüntüleme

Dosya Yöneticisi, dosyaları ve dizinleri birden fazla şekilde görüntülemenize izin verir.

Üst çubuktaki tanıdık simgelere tıklayarak Simgeler ve Liste biçimleri arasında geçiş yapabilir veya ayrı ayrı **CTRL-1** ve **CTRL-2** tuşlarına basabilirsiniz.

Ayrıca, dosyaları ve dizinleri ada, boyuta, türe veya değiştirme tarihine göre sıralama için düzenleyebilirsiniz. Bunu yapmak için, **Görünüm**'ü tıklayın ve **Öğeleri Düzenle**'yi seçin.

Diğer bir kullanışlı seçenek, varsayılan olarak gizli olan ve adı bir noktayla başlayan, genellikle yapılandırma dosyaları olan gizli dosyaları (bazen kesin olmayarak sistem dosyaları olarak adlandırılır) göstermektir. Gizli dosyaları göstermek için menüden **Gizli Dosyaları Göster** seçeneğini seçin veya **CTRL-H** tuşlarına basın.

Dosya tarayıcısı, kolay sürükle ve bırak dosya işlemlerini kolaylaştırmak amacıyla pencere görünümünüzü özelleştirmek için birçok yol sağlar. _Görünüm_ menüsü altında **Yakınlaştır** ve **Uzaklaştır**'ı seçerek simgelerin boyutunu da değiştirebilirsiniz.

<figure align="center">
  <img src="/img/lfs101/ch04/img27.png" />
  <figcaption>OpenSUSE'de Dosyaları Görüntüleme</figcaption>
</figure>

## Dosyaları Arama

<img src="/img/lfs101/ch04/img28.png" align="right" class="invert"/>

Dosya Yöneticisi, dosya tarayıcı penceresinin içinde harika bir arama aracı içerir.

1. Araç çubuğundaki **Ara** seçeneğine tıklayın (bir metin kutusu getirmek için).
2. Metin kutusuna anahtar kelimeyi girin. Bu, sistemin, anahtar kelimenin bir bölümünü içeren herhangi bir dosya veya dizin için geçerli dizinden özyinelemeli bir arama yapmasını sağlar.

_Dosya Yöneticisi_'ni komut satırından açmak için, çoğu sistemde **nautilus** yazmanız yeterlidir.
Arama metin kutusuna ulaşmak için kısayol tuşu **CTRL-F**'dir. Ara düğmesini tıklayarak veya **CTRL-F** kısayolu ile arama metin kutusu görünümünden çıkabilirsiniz.

Belirli bir dizine erişmenin diğer bir hızlı yolu, bir dizine giden yolu yazmanız için size bir **Konum** metin kutusu verecek olan **CTRL-L**'ye basmaktır.

### Dosya Arama Hakkında Daha Fazla Bilgi

Aramayı daha fazla filtrelemek için açılır menüleri kullanabilir, aramanızı anahtar kelimenin ötesinde hassaslaştırabilirsiniz.

1. **Konum** veya **Dosya Türü**'ne göre, açılır menüden ek kriterler seçin.
2. Aramayı yeniden oluşturmak için **Yeniden Yükle** düğmesini tıklayın.
3. Birden fazla arama kriteri eklemek için + düğmesine tıklayın ve _Ek Arama Kriterleri_'ni seçin.

Örneğin, ev dizininizde **Linux** kelimesini içeren bir PDF dosyası bulmak istiyorsanız, **ev** dizininize gidin ve "Linux" kelimesini arayın. Varsayılan arama kriterinin aramayı zaten **ev** dizininizle sınırladığını görmelisiniz. İşi bitirmek için, başka bir arama kriteri eklemek üzere **+** düğmesine tıklayın, kriter türü için **Dosya Türü**'nü seçin ve **Dosya Türü** açılır menüsünden **PDF**'yi seçin.

<figure align="center">
  <img src="/img/lfs101/ch04/img29.png" />
  <figcaption>Dosyaların Aranması</figcaption>
</figure>

## Bir Dosyayı Düzenleme

<img src="/img/lfs101/ch04/img30.png" align="right" />

Herhangi bir metin dosyasını grafik arayüz aracılığıyla düzenlemek, GNOME masaüstü ortamında kolaydır. Dosyayı varsayılan metin düzenleyiciyle açmak için masaüstünde veya Nautilus dosya tarayıcı penceresinde dosyaya çift tıklamanız yeterlidir.

GNOME'deki varsayılan metin düzenleyici **gedit**'tir. Basit ama güçlüdür; belgeleri düzenlemek, hızlı notlar almak ve programlama için idealdir. **gedit** genel amaçlı bir metin düzenleyici olarak tasarlanmış olsa da; yazım denetimi, vurgulama, dosya listeleri ve istatistikler için ek özellikler sunar.

Daha sonraki bir bölümde metin editörlerini kullanma hakkında çok daha fazlasını öğreneceksiniz.

## Bir Dosyayı Silme

<img src="/img/lfs101/ch04/img31.png" align="right" class="invert"/>

Deleting a file in Nautilus will automatically move the deleted files to the **.local/share/Trash/files/** directory (a trash can of sorts) under the user's home directory. There are several ways to delete files and directories using Nautilus.

Nautilus'ta bir dosyanın silinmesi, silinen dosyaları otomatik olarak kullanıcının ev dizini altındaki **.local/share/Trash/files/** dizinine (bir tür çöp kutusu) taşıyacaktır. Nautilus'u kullanarak dosya ve dizinleri silmenin birkaç yolu vardır.

1. Silmek istediğiniz tüm dosyaları ve dizinleri seçin
2. Klavyenizdeki **CTRL-Delete** tuşlarına basın veya dosyaya sağ tıklayın
3. **Çöp Kutusuna Taşı** seçeneğini seçin.

Çöp klasörünü atlayan bir **Kalıcı Olarak Sil** seçeneğine sahip olabileceğinizi ve bu seçeneğin her zaman veya yalnızca (simge yerine) liste modunda görünebileceğini unutmayın.

### Dosya Silme Hakkında Daha Fazla Bilgi

Bir dosyayı _kalıcı_ olarak silmek için:

1. Nautilus dosya tarayıcısı penceresinin içindeki sol panelde, **Çöp Kutusu** dizinine sağ tıklayın.
2. Çöpü _Boşalt_'ı seçin.

Alternatif olarak, kalıcı olarak silmek istediğiniz dosyayı veya dizini seçin ve **Shift-Delete** tuşlarına basın.

Bir önlem olarak, _Ev_ dizininizi asla silmemelisiniz, çünkü bunu yapmak büyük olasılıkla tüm GNOME yapılandırma dosyalarınızı silecek ve muhtemelen oturum açmanızı engelleyecektir. Birçok kişisel sistem ve program yapılandırması ev dizininiz altında saklanır.

## Video: Varsayılan Uygulamaları Bulma ve Ayarlama, ve openSUSE'de Dosya Sistemlerini Keşfetme

Varsayılan uygulamaları bulma ve ayarlama, ve openSUSE'de dosya sistemlerini keşfetme gösterilerini görüntülemek için aşağıdaki <img src="/img/lfs101/common/play.png" /> butonuna tıklayın.

### Varsayılan Uygulamaları Bulma ve Ayarlama, ve openSUSE'de Dosya Sistemlerini Keşfetme

[Video](https://edx-video.net/LINILXXX2017-V001000_DTH.mp4) | [Alt yazı](video-07-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Lab Çalışmaları

### Lab 4.2: Dosya sıralama seçeneklerini görüntüleme

**/var/log** içinde en son değiştirilmiş dosyayı bulun.

Laboratuvar alıştırmasının çözümünü görmek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-filesort)

### Lab 4.3: Silinmiş Dosyaları Kurtarma

GNOME veya KDE masaüstü fark etmeksizin, temel işlemler burada aynı olacaktır, ancak kesin prosedürler ve seçimler biraz farklılık gösterebilir.

1. Grafik dosya yöneticisini kullanarak masaüstünde **lab.txt** adlı yeni bir metin dosyası oluşturun.
2. Dosyayı **Çöp Kutusu**'na göndererek silin.
3. Dosyanın artık **~/.local/share/Trash** veya bunun bir alt dizininde olduğunu doğrulayın. **_Not_**: Dosya tarayıcınızın **.** ile başlayan gizli dosyaları ve dizinleri göstermesini sağlamalısınız.
4. Dosyayı kurtarın ve orijinal konumunda olduğundan emin olun.

Laboratuvar alıştırmasının çözümünü görmek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-recoverfiles)
