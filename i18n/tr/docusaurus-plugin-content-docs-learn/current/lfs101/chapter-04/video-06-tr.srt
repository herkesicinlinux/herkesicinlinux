﻿1
00:00:00,020 --> 00:00:05,700
Şimdi bir Red Hat 7 sisteminde varsayılan uygulamaların nasıl ayarlanacağını gösterelim.

2
00:00:05,700 --> 00:00:13,760
Sağ üst köşeye gidiyoruz, üst çubuğa tıklıyoruz ve ardından ayarlar için teçhizat simgesine kaydırıyoruz.

3
00:00:13,760 --> 00:00:16,840
Ayrıntılar için aşağıya inin.

4
00:00:16,840 --> 00:00:26,500
Sonra Varsayılan Uygulamalar'a tıklayabilirim. Gördüğünüz gibi, Web veya Tarayıcı altında, bu sistemde üç seçenek var.

5
00:00:26,500 --> 00:00:31,780
Google Chrome, Firefox veya Opera var. Google Chrome'da bırakacağız.

6
00:00:31,780 --> 00:00:42,680
Video için varsayılan seçenek, aslında Totem adlı bir program olan Videos'tur. Peki, onu VLC medya oynatıcısına geçirelim.

7
00:00:42,680 --> 00:00:51,480
Resim görüntüleyici için, yerleşik Resim Görüntüleyici'ye sahibim ya da daha çok GIMP olarak bilinen GNU Görüntü İşleme Programı'na geçebilirim.

8
00:00:51,480 --> 00:00:59,940
Dolayısıyla, bir GNOME masaüstüne sahip tüm yeni dağıtımlarda prosedür bire bir ve kesinlikle aynıdır.

9
00:00:59,940 --> 00:01:05,040
Varsayılan uygulamalarınızı değiştirmek çok basittir.

