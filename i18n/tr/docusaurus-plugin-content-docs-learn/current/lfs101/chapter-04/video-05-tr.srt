﻿1
00:00:01,300 --> 00:00:08,180
Birkaç hesapla bir Ubuntu makinesinde kullanıcıların nasıl değiştirileceğini gösterelim.

2
00:00:08,180 --> 00:00:18,720
Bu yüzden ilk olarak, oturum açacağım hesap olarak Linux Vakfı Öğrencisi'ni seçeceğim, parolamı yazacağım ve oturum açacağım.

3
00:00:18,720 --> 00:00:31,740
Ve sonra, sağ üst köşeye bakıp orayı tıklarsam, LV Öğrencisi'ni göreceğim ve "Oturumu Kapat" veya "Kullanıcı Değiştir" seçeneğine tıklayabilirim.

4
00:00:31,740 --> 00:00:44,800
Dikkat edin, oturumu kapatırsam, gerçekten oturumu kapatırım, ancak kullanıcı değiştirirsem, yalnızca kullanıcı hesabını kilitlerim ve ardından yeniden başlattığımda, tüm oturumlarımı, açık pencereleri vs. muhafaza ederim.

5
00:00:44,800 --> 00:00:51,100
Bazı dağıtımlarda aslında bu ekranda bir kullanıcı listesi görebilirsiniz, ancak Ubuntu'da değil.

6
00:00:51,100 --> 00:01:04,440
Bu yüzden "Kullanıcı Değiştir" diyeceğim, ve karşılama ekranına geri döndüm. İkincisi olan Theodore Cleaver'ı seçmeme ve diğer kullanıcı olarak oturum açmama izin verin.

7
00:01:04,440 --> 00:01:15,920
Ve işte başlıyoruz. Şimdi buraya tekrar bakarsam, aynı şeyi yapabilirim. Öyleyse, isme tıklayayım, "Kullanıcı Değiştir" diyeceğim,

8
00:01:15,920 --> 00:01:28,640
ve tekrar öğrenci seçersem, burada "Yeniden Başlat" değil "Kilidi Aç" yazdığını fark edeceksiniz. Bu, şimdi oturum açarsam, daha önceki tüm açık pencereleri ve çalışan uygulamaları koruyacağım anlamına gelir.

9
00:01:28,640 --> 00:01:41,509
Bu, esasen son dönemdeki tüm Linux dağıtımlarında aynı görünecektir. Bu nedenle, kullanıcılar arasında geçiş yapmak ve halihazırda yaptığınız şeyden herhangi bir bilgi kaybetmemek çok kolaydır.

