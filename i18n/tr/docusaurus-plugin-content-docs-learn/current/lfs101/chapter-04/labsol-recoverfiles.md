---
title: Silinmiş Dosyaları Kurtarma
---

1. Dosya yöneticisini açın ve ev dizininize gidin. Kullanıcı yapılandırmanız bunun için ayarlandıktan sonra, bu, dizine sağ tıklayıp **Yeni Oluştur-> Metin Dosyası**'nı seçip ona bir ad vermek kadar basittir. (**Metin Dosyası** dışında bir şey görebilirsiniz.)

   **GNOME** dosya yöneticisi ile, varsayılan kurulumda böyle bir seçenek yoktur. Önce **Templates** dizininizde **yeni** adlı bir dosya oluşturmanız gerekir. Bunu yapmanın en kolay yolu, **Alt-F2** tuşlarına basmak ve ardından açılan küçük pencereye şunu yazmaktır:

   ```bash
   student:/tmp> touch ~/Templates/yeni
   ```

   Bunu yaptıktan sonra, dizine sağ veya başlık çubuğunun sağ tarafında bir yere tıklayabilir, herhangi bir türden yeni bir dosya oluşturma seçeneğinin bulunduğu bir iletişim kutusu açabilirsiniz.

   Bu özelliğin neden varsayılan olarak açık olmadığı, modern bilim tarafından anlaşılmayan bir gizem olarak kabul edilebilir. Ancak mantık, yeni dosyaların genellikle bir boşlukta (en azından GUI'lerde) yaratılmamasıdır; bunların bir uygulamada yapılması beklenir. Bazı **GNOME** dağıtımları veya sürümleri bu saçma adıma ihtiyaç duymayabilir.

2. Bu oldukça açık olmalı, soldaki Çöp Kutusu klasörüne sürükleyip bırakarak veya dosyaya sağ tıklayarak.

3. **.local** dizininizde gezinmek için, gizli dosyaları görünür yapmanız gerekir. **GNOME** sistemlerinde, ya basitçe **CTRL-H**'ye basabilir ya da dişli simgesinin yanındaki küçük aşağı oka tıklayabilir ve gizli dosyaları açıp kapatabilirsiniz.

4. En kolay yol, **Çöp Kutusu** simgesine tıklamak ve ardından **Geri Yükle**'yi seçmektir; bu, onu kolayca doğrulayabileceğiniz orijinal konumuna geri getirecektir.

**GNOME** Dosya Yöneticisinin varsayılan olarak **Sil** veya **Kalıcı Olarak Sil** seçeneği içermediğini unutmayın. Bunu etkinleştirmek için tercihlere gitmeli ve açmalısınız. Dosya Yöneticisini başlatarak veya masaüstündeki üst görev çubuğunda onun simgesine tıklayarak ve tercih menülerine giderek, tercihler menüsüne girebilirsiniz. Açık olmayan başka bir adım :(.
