﻿1
00:00:00,464 --> 00:00:03,417
Sistemde oturum açmak için bu adımları uygulayın:

2
00:00:03,474 --> 00:00:07,984
1. İstenilen kullanıcıya tıklayın. Bu durumda test2'dir.

3
00:00:08,038 --> 00:00:10,350
Şifre kutusu görüntülenir.

4
00:00:10,556 --> 00:00:13,206
2. Kullanıcı testi2'nin şifresini girin.

5
00:00:15,983 --> 00:00:16,978
3. Oturum Aç'ı tıklayın.

6
00:00:17,917 --> 00:00:19,688
Masaüstü görüntülenir.

7
00:00:19,793 --> 00:00:25,281
Oturumun varsayılan veya önceki oturumun masaüstü ortamı seçimini yükleyeceğini unutmayın.

8
00:00:25,302 --> 00:00:33,505
Kullanıcı adınızı ve şifrenizi girdikten sonra, kişisel ayarlarınıza bağlı olarak grafik ortamının başlaması biraz zaman alabilir.

9
00:00:33,808 --> 00:00:36,847
Sistem oturumunu kapatmak için bu adımları uygulayın:

10
00:00:37,185 --> 00:00:42,173
1. openSUSE ekranında, sağ üst köşedeki güç simgesine tıklayın.

11
00:00:44,131 --> 00:00:45,349
2. test2'ye tıklayın.

12
00:00:46,216 --> 00:00:47,304
3. Oturumu Kapat'a tıklayın.

13
00:00:47,646 --> 00:00:50,329
Onay kutusunda Oturumu Kapat'a tıklayın.

