﻿1
00:00:00,340 --> 00:00:07,380
Son kurumsal Linux dağıtımlarının tümü, başlama ve kapanma şekli açısından oldukça benzerdir.

2
00:00:07,380 --> 00:00:18,380
Bunun nedeni, GNOME masaüstünün oldukça benzer sürümlerini kullanmaları ve ayrıca başlatma hizmetleri için systemd kullanmalarıdır.

3
00:00:18,380 --> 00:00:30,740
Bunu göstermek üzere, bir Red Hat 7 ana bilgisayar sisteminde, VMware Player kullanarak üç sanallaştırılmış kurumsal Linux dağıtımını çalıştıracağız.

4
00:00:30,740 --> 00:00:35,340
Öyleyse, bu ayarlamayı yapmama izin verin. 3 örneği başlatacağım.

5
00:00:35,340 --> 00:00:46,800
Ve ilkinde Ubuntu 18.04'ü çalıştıracak şekilde ayarlayacağım ve ikincisinde de CentOS 7'yi çalıştıracak şekilde ayarlayacağım. Üçüncüsünü openSUSE çalıştırmak için ayarlayacağım.

6
00:00:46,800 --> 00:00:55,760
Şimdi, sanallaştırılmış örnekler için uygun grafik sürücülerine sahip olmadığımız için bazı uyarı mesajları olacak,

7
00:00:55,760 --> 00:01:01,900
ikinci ve üçüncü makineyi başlattığımda da aynı anda birden fazla sanal makineyi çalıştırma konusunda bazı uyarılar olacak.

8
00:01:01,900 --> 00:01:04,380
Ancak bu uyarıları güvenle görmezden gelebiliriz.

9
00:01:04,379 --> 00:01:09,020
İlk önce Ubuntu 18.04'ü başlatacağım.

10
00:01:09,020 --> 00:01:14,020
İkinci olarak, CentOS 7'yi başlatacağım,

11
00:01:14,020 --> 00:01:20,360
ve üçüncü olarak openSUSE sanal makinesini başlatacağım.

12
00:01:20,360 --> 00:01:24,619
Bunlar bahsettiğim birden fazla çalıştırma konusundaki uyarılar.

13
00:01:24,620 --> 00:01:34,360
Biraz farklı başlangıç ekranlarım olacak, ancak başlangıç aşamasına girdiklerinde hepsi birbirine oldukça benziyor.

14
00:01:34,360 --> 00:01:43,280
Genel olarak, Ubuntu muhtemelen biraz daha hızlı başlar, ardından CentOS en hızlı ikinci, ve openSUSE biraz daha uzun sürer.

15
00:01:43,280 --> 00:01:59,119
Ubuntu'da şimdi parolayı yazmama izin verin. Ve CentOS birde biraz bekleyeceğim çünkü size CentOS ve openSUSE'nin tamamen aynı göründüğünü göstermek istiyorum.

16
00:01:59,120 --> 00:02:17,698
SUSE hala başlatılırken, CentOS için parolayı yazacağım ve karşılayıcı adı verilen giriş ekranının openSUSE ile tamamen aynı göründüğünü fark edeceksiniz, tek fark logo.

17
00:02:17,698 --> 00:02:19,440
İkisini de otutum açıyorum,

18
00:02:19,440 --> 00:02:27,500
ve hem CentOS hem de openSUSE için arka planı özelleştirmiştik.

19
00:02:27,500 --> 00:02:39,980
Sadece bir resim seçtik. Ubuntu için hiçbir şey yapmadık, bu yüzden, bu, kurulum sırasında varsayılan olarak gelen standart arka plan ekranıdır.

20
00:02:39,980 --> 00:02:52,180
Kapatmak için sağ üst köşeye gidiyorum, güç düğmesine tıklıyorum, güç simgesine tıklıyorum ve sonra yeniden başlatmayı seçebilirim, ancak sadece "Kapat" diyeceğim.

21
00:02:52,180 --> 00:03:08,300
CentOS 7 için de aynı prosedür geçerlidir. Göreceksiniz... Sadece kapatacağım. Ve sonra aynı şeyi openSUSE makinesi için yapacağım.

22
00:03:08,300 --> 00:03:13,620
Bir kez daha, kesinlikle aynı görünüyor. Oldukça temiz bir şekilde kapanacak.

23
00:03:13,620 --> 00:03:26,489
Gördüğünüz gibi, son dağıtımlar için işler oldukça benzer ve kullandığınız dağıtıma bağlı olarak bazı şeylerin ne kadar farklı olacağı konusunda endişelenmenize gerek yok.

