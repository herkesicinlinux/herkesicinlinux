﻿1
00:00:00,040 --> 00:00:05,360
Firefox uygulamasını "Firefox Bul" bölümünde nasıl bulacağımızı görelim.

2
00:00:05,360 --> 00:00:09,258
Masaüstünüzün sol üst köşesindeki "Etkinlikler"i tıklayın.

3
00:00:09,260 --> 00:00:13,380
Sol panelde "Uygulamaları göster" simgesini tıklayın.

4
00:00:13,380 --> 00:00:17,080
Firefox simgesine tıklayın.

5
00:00:17,090 --> 00:00:23,349
Firefox penceresinin sağ üst köşesindeki "Kapat"ı tıklayarak Firefox'u kapatın.

6
00:00:23,349 --> 00:00:33,980
Ardından, Dosya Yöneticisini kullanarak Ev dizinini inceleyelim: dosyaları liste modunda görüntülemeye çalışın, yeni bir boş belge oluşturun, kaydedin ve kaldırın.

7
00:00:33,980 --> 00:00:38,540
Masaüstünüzün sol üst köşesindeki "Etkinlikler"i tıklayın.

8
00:00:38,540 --> 00:00:42,260
Sol panelde, Dosya Yöneticisi simgesini tıklayın.

9
00:00:42,260 --> 00:00:45,260
Bu, varsayılan olarak Ev dizinini açacaktır.

10
00:00:45,260 --> 00:00:51,860
Dosyaları liste modunda görüntülemek için, Dosya Yöneticisi penceresinin sağ üst kısmındaki Liste simgesine tıklayın.

11
00:00:51,920 --> 00:00:55,260
Açmak için Belge dizinine çift tıklayın.

12
00:00:55,260 --> 00:01:01,420
Gedit'i başlatmak için masaüstünüzün sol üst köşesindeki Etkinlikler'i tıklayın.

13
00:01:01,430 --> 00:01:04,758
Arama kutusuna gedit yazın.

14
00:01:04,757 --> 00:01:08,430
Açmak için gedit simgesine tıklayın.

15
00:01:08,430 --> 00:01:15,280
Adınızı yazın ve bir dosya oluşturmak ve kapatmak için gedit penceresinin sağ üst köşesindeki Kapat'ı tıklayın.

16
00:01:15,280 --> 00:01:19,010
Oluşturulan dosyayı kaydetmek için Farklı Kaydet'i tıklayın.

17
00:01:19,010 --> 00:01:23,260
Farklı Kaydet penceresinin sol panelinden Belgeler'i seçin.

18
00:01:23,260 --> 00:01:27,120
Dosyayı Test adıyla kaydetmek için Test yazın.

19
00:01:27,120 --> 00:01:30,800
Dosyayı kaydetmek ve gedit'i kapatmak için Kaydet'e tıklayın.

20
00:01:30,800 --> 00:01:33,840
Bu sizi Belge dizinine geri getirir.

21
00:01:33,840 --> 00:01:37,080
Belge penceresindeki Test dosyasına sağ tıklayın.

22
00:01:37,080 --> 00:01:41,960
Dosyayı kaldırmak için Çöp Kutusuna Taşı'yı seçin.

