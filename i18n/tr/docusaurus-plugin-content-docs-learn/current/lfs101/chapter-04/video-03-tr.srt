﻿1
00:00:00,200 --> 00:00:07,420
Red Hat 7 sisteminde arka plan duvar kağıdının nasıl değiştirileceğini göstereceğiz.

2
00:00:07,420 --> 00:00:14,240
Tüm yeni Linux dağıtımları tamamen aynı yönteme sahiptir, bu nedenle bu herkes için yeterli olacaktır.

3
00:00:14,240 --> 00:00:23,600
Tek yapmamız gereken sağ tuş ile arka plana tıklamak ve "Arka Planı Değiştir" seçeneğini seçmektir.

4
00:00:23,600 --> 00:00:31,860
Tekrar "Arka Plan"a tıklayın ve ardından sistemin zaten bildiği duvar kağıtları arasından seçim yapabiliriz.

5
00:00:31,860 --> 00:00:41,480
Ve diğerlerini yüklemek mümkündür. Diyelim ki burada bunu seçtik ve şimdi arka planın değiştiğini görüyorsunuz.

6
00:00:41,480 --> 00:00:49,660
Ya da örneğin makinemde ev dizinin altındaki resimler dizininde depolanan bir resmi seçebilirim.

7
00:00:49,660 --> 00:00:55,720
Yani, bunu seçeceğim ve onun değiştiğini göreceksiniz.

8
00:00:55,720 --> 00:01:02,400
Dolayısıyla, herhangi yeni bir Linux sisteminin arka planını değiştirmek için yapmamız gereken tek şey bu.

