---
title: Bölüm Sonu
---

## Öğrenme Hedefleri (Gözden Geçirin)

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Artık şunları yapabiliyor olmalısınız:

- Grafik arayüz oturumlarını yönetmek
- Grafik arayüzü kullanarak temel işlemleri gerçekleştirmek
- Grafik masaüstünü ihtiyaçlarınıza göre değiştirmek

## Özet

<img src="/img/lfs101/common/tux-grad-cap.png" align="right" />

4. bölümü tamamladınız. Kapsanan temel kavramları özetleyelim:

- **GNOME**, Linux işletim sisteminin üzerinde çalışan popüler bir masaüstü ortamı ve grafik kullanıcı arayüzüdür.
- GNOME için varsayılan görüntü yöneticisine **gdm** denir.
- Gdm ekran yöneticisi, kullanıcıya, kullanıcı adı ve parolasını soran oturum açma ekranını sunar.
- Masaüstü ortamın kullanarak çıkış yapmak, mevcut **X** oturumunuzdaki tüm işlemleri sonlandırır ve görüntü yöneticisi oturum açma ekranına geri döner.
- Linux, kullanıcıların açık oturumlar arasında geçiş yapmasına olanak sağlar.
- Askıya alma, bilgisayarı uyku moduna geçirir.
- Her bir temel görev için, genellikle kurulu bir varsayılan uygulama vardır.
- Sistemde oluşturulan her kullanıcının bir **ev** dizini olacaktır.
- _Yerler_ menüsü, bilgisayarın ve ağın farklı bölümlerine erişmenizi sağlayan girişler içerir.
- **Nautilus**, dosyaları görüntülemek için üç biçim sunar.
- Metin düzenleyicilerin çoğu _Aksesuarlar_ alt menüsünde bulunur.
- Her Linux dağıtımı kendi masaüstü arka plan setiyle birlikte gelir.
- GNOME, uygulamalarınızın görünümünü değiştirebilen bir dizi farklı temayla birlikte gelir.
