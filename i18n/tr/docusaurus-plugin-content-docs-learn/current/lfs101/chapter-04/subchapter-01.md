---
title: Giriş ve Öğrenme Hedefleri
---

## Video: Bölüm 4 Giriş

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V005900_DTH.mp4) | [Alt yazı](video-01-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Öğrenme Hedefleri

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Bu bölümün sonunda şunları yapabiliyor olmalısınız:

- Grafik arayüz oturumlarını yönetmek
- Grafik arayüzü kullanarak temel işlemleri gerçekleştirmek
- Grafik masaüstünü ihtiyaçlarınıza göre değiştirmek
