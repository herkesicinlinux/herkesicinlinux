---
title: Linux Tarihi
---

## Giriş

<img src="/img/lfs101/ch02/img02.png" align="right" />

Linux, başlangıçta Intel x86 tabanlı kişisel bilgisayarlar üzerinde ve bunlar için geliştirilmiş açık kaynaklı bir bilgisayar işletim sistemidir. Daha sonra, küçük gömülü cihazlardan dünyanın en büyük süper bilgisayarlarına kadar, diğer donanım platformlarından oluşan şaşırtıcı derecede uzun bir listeye uyarlanarak taşındı.

Bu bölümde, Finli bir üniversite öğrencisinin projesi olarak başlayan Linux'un, günümüz dünyası üzerinde muazzam bir etkiye sahip olan, büyük bir başarıya nasıl evrildiğinin şaşırtıcı tarihini takip ediyoruz.

## Linux Tarihi

Linus Torvalds, 1991 yılında Finlandiya'nın Helsinki kentinde bir öğrenciydi ve bir projeye başladı: kendi işletim sistemi **çekirdeğini** yazmak. Ayrıca çekirdeği merkezde olacak şekilde bütün bir işletim sistemi oluşturmak için gerekli diğer temel bileşenleri bir araya topladı ve/veya geliştirdi. Bunun Linux çekirdeği olarak tanınması çok uzun sürmedi.

Linux, 1992'de **GNU**(Özgür Yazılım Vakfı'nın veya FSF'nin, özgür yazılımı teşvik eden bir projesi) tarafından **Genel Kamu Lisansı** (**GPL**) kullanılarak yeniden lisanslandı ve dünya çapında bir geliştiriciler topluluğu oluşturulmasını mümkün kıldı. Çekirdeği GNU projesindeki diğer sistem bileşenleriyle birleştiren çok sayıda başka geliştirici, 90'ların ortalarında Linux dağıtımları adı verilen eksiksiz sistemler yarattı.

<img src="/img/lfs101/ch02/img03.png" />

## Video: Linux Dünyasına Hoş Geldiniz

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V006400_DTH.mp4) | [Alt yazı](video-04-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Linux Tarihi Hakkında Daha Fazla Bilgi

<img src="/img/lfs101/ch02/img04.png" align="right" class="invert"/>

90'ların ortalarında yaratılan Linux dağıtımları, tamamen özgür bilgi işlem için temel oluşturdu ve açık kaynak yazılım hareketinde itici bir güç haline geldi. 1998'de IBM ve Oracle gibi büyük şirketler Linux platformuna desteklerini duyurdular ve aynı zamanda büyük geliştirme çalışmalarına da başladılar.

Linux, günümüzde İnternet üzerindeki sunucuların yarısından fazlasına, akıllı telefonların çoğuna(Linux üzerine kurulu Android sistemi aracılığıyla) ve dünyanın en güçlü süper bilgisayarlarının tamamına güç veriyor.
