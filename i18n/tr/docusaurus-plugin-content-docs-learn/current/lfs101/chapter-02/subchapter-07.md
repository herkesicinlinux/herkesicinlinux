---
title: Bölüm Sonu
---

## Öğrenme Hedefleri (Gözden Geçirin)

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Artık şunları yapabiliyor olmalısınız:

- Linux'un tarihini ve felsefesini tartışmak
- Linux topluluğunu tarif etmek
- Linux ile bağdaşmış terimleri tanımlamak
- Bir Linux dağıtımının bileşenleri hakkında tartışmak

## Özet

<img src="/img/lfs101/common/tux-grad-cap.png" align="right" />

2. bölümü tamamladınız. Kapsanan temel kavramları özetleyelim:

- Linux, yaratıcılarının çok bilgili olduğu UNIX işletim sisteminden büyük ölçüde ödünç alıyor.
- Linux, dosyalar ve dosya benzeri nesneler aracılığıyla birçok özelliğe ve hizmete erişir.
- Linux, yerleşik ağ iletişimi ve arka plan yordamları olarak bilinen hizmet süreçleri ile tamamen çok görevli, çok kullanıcılı bir işletim sistemidir.
- Linux, dünyanın her yerinden gelen geliştiricilerden oluşan ve başında Linus Torvalds olmak üzere İnternet üzerinden işbirliği yapmakta olan gevşek bir konfederasyon tarafından geliştirilmiştir. Katılım için gerekli nitelikler sadece teknik beceri ve katkıda bulunma isteğidir.
- Linux topluluğu, Linux işletim sistemini destekleyen ve geliştiren geliştiriciler, satıcılar ve kullanıcılardan oluşan geniş kapsamlı bir ekosistemdir.
- Linux'ta kullanılan yaygın terimlerden bazıları şunlardır: çekirdek, dağıtım, önyükleyici, hizmet, dosya sistemi, X Pencere Sistemi, masaüstü ortamı ve komut satırı.
- Tam bir Linux dağıtımı, çekirdeğin yanı sıra dosyayla ilgili işlemler, kullanıcı yönetimi ve yazılım paketi yönetimi için bir dizi başka yazılım aracından oluşur.
