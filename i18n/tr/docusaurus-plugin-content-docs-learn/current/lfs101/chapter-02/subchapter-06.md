---
title: Linux Dağıtımları
---

## Giriş

<img src="/img/lfs101/ch02/img08.png" align="right" />

Bir Linux platformu için bir ürün oluşturan bir projeye atandığınızı varsayalım. Proje gereksinimleri, projenin en yaygın kullanılan Linux dağıtımlarında düzgün çalıştığından emin olmayı içerir. Bunu başarmak için, her dağıtımla ilişkili farklı bileşenler, hizmetler ve yapılandırmalar hakkında bilgi edinmeniz gerekir. Tam olarak bunu nasıl yapacağınıza bakmak üzereyiz.

## Linux Dağıtımları

Öyleyse, Linux dağıtımı nedir ve Linux çekirdeği ile nasıl ilişkilidir?

Linux çekirdeği, işletim sisteminin merkezidir. Tam bir Linux dağıtımı, çekirdeğin yanı sıra dosya ile ilgili işlemler, kullanıcı yönetimi ve yazılım paketi yönetimi için bir dizi başka yazılım aracından oluşur. Bu araçların her biri, eksiksiz sistemin bir parçasını oluşturur. Her araç genellikle kendi geliştiricilerinin sistemin bu parçasını mükemmelleştirmek için çalıştığı kendi ayrı projesidir.

En yeni Linux çekirdeği (ve önceki sürümler) her zaman The Linux Kernel Archives'de bulunabilirken, Linux dağıtımları farklı çekirdek sürümlerini temel alabilir. Örneğin, çok popüler olan RHEL 7 dağıtımı, yeni olmayan ancak son derece kararlı olan 3.10 çekirdeğine dayanmaktadır. Diğer dağıtımlar, en son çekirdek sürümlerini benimserken daha hızlı hareket edebilir. Çekirdeğin bir ya hep ya hiç önermesi olmadığına dikkat etmek önemlidir. Örneğin, RHEL 7/CentOS 7 daha yeni çekirdek geliştirmelerinin çoğunu eski sürümlerine dahil etmiştir, Ubuntu, openSUSE, SLES vb. gibi.

Dağıtımlarla sağlanan diğer temel araçlara ve bileşenlere örnekler arasında C/C ++ derleyicisi, gdb hata ayıklayıcı, çalışmak için bağlantı kurması gereken temel sistem kütüphaneleri, ekranda grafik çizmek için alt düzey arabirim ve ayrıca üst düzey masaüstü ortamı ve çekirdeğin kendisi de dahil olmak üzere çeşitli bileşenlerin yüklenmesi ve güncellenmesi için sistem. Ve tüm dağıtımlar, halihazırda kurulu oldukça eksiksiz bir uygulama paketiyle birlikte gelir.

<figure align="center">
  <img src="/img/lfs101/ch02/img09.png" class="invert"/>
  <figcaption>Dağıtımın rolleri</figcaption>
</figure>

## Dağıtımlarla İlişkili Servisler

Çok çeşitli Linux dağıtımları, özel ihtiyaçları ve zevklerine göre birçok farklı hedef kitleye ve kuruluşa hitap edecek şekilde tasarlanmıştır. Bununla birlikte, şirketler ve devlet kurumları gibi büyük kuruluşlar ve diğer kuruluşlar, Red Hat, SUSE ve Canonical'dan (Ubuntu) ticari olarak desteklenen başlıca dağıtımları seçme eğilimindedir.

CentOS, Red Hat Enterprise Linux'a (RHEL) ücretsiz ve popüler bir alternatiftir ve genellikle ücretli teknik destek olmadan rahatça çalışan kuruluşlar tarafından kullanılır. Ubuntu ve Fedora, geliştiriciler tarafından yaygın olarak kullanılmaktadır ve aynı zamanda eğitim alanında da popülerdir. Scientific Linux, bilimsel ve matematiksel yazılım paketleriyle uyumluluğu nedeniyle bilimsel araştırma topluluğu tarafından tercih edilmektedir. Hem CentOS hem de Scientific Linux, RHEL ile ikili uyumluluğa sahiptir; yani çoğu durumda, ikili yazılım paketleri dağıtımlar arasında düzgün bir şekilde kurulacaktır.

Red Hat, Ubuntu, SUSE ve Oracle dahil olmak üzere birçok ticari dağıtımcı, dağıtımları için uzun vadeli ücrete dayalı destek ile beraber donanım ve yazılım sertifikasyonu sağlar. Tüm büyük dağıtıcılar, sisteminizi en son güvenlik ve hata düzeltmeleri ve performans iyileştirmeleriyle hazır tutmak için güncelleme hizmetleri sağlamanın yanı sıra çevrim içi destek kaynakları sağlar.

<figure align="center">
  <img src="/img/lfs101/ch02/img10.png" class="invert"/>
  <figcaption>Dağıtımlarla İlişkili Servisler</figcaption>
</figure>
