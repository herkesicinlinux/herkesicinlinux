---
title: Linux Felsefesi
---

## Giriş

<img src="/img/lfs101/ch02/img05.png" align="right" />

Her başarılı proje veya organizasyon, hedeflerini çerçeveleyen ve büyüme yolunu tasarlayan örtük veya açık bir felsefeye ihtiyaç duyar. Bu bölüm bir "Linux felsefesi" tanımını ve Linux'un şaşırtıcı evrimini nasıl etkilediğini içerir.

Linux, dünyanın her yerinden İnternet üzerinden iş birliği yapan geliştiricilerden oluşan ve başında Linus Torvalds'ın bulunduğu bir ağ tarafından sürekli geliştirilmekte ve iyileştirilmektedir. Katılım için gerekli nitelikler sadece teknik beceri, katkıda bulunma isteği ve başkalarıyla işbirliği yapma yeteneğidir.

## Linux Felsefesi

Linux, köklü UNIX işletim sisteminden büyük ölçüde ödünç alır. O zamanlar PC'lerden çok daha güçlü bilgisayarlar için tasarlanmış ve oldukça pahalı olan UNIX yerine kullanılmak üzere özgür ve açık kaynaklı bir sistem olarak yazılmıştır. Dosyalar, sistemin en üst düğümü **kök** veya basitçe "**/**" olacak şekilde hiyerarşik bir dosya sisteminde saklanır. Mümkün olduğunda Linux, bileşenlerini dosyalar veya dosyalara benzeyen nesneler aracılığıyla kullanılabilir hâle getirir. İşlemler, cihazlar ve ağ soketlerinin tümü dosya benzeri nesnelerle temsil edilir ve genellikle normal dosyalar için kullanılan aynı yardımcı programlar kullanılarak üzerinde çalışılabilir. Linux, UNIX dünyasında arka plan yordamları olarak bilinen yerleşik ağ ve hizmet süreçlerine sahip, tamamen çok görevli(yani birden fazla iş parçacığı aynı anda yürütülür), çok kullanıcılı bir işletim sistemidir.

Not: Linux, UNIX'ten esinlenmiştir, ancak UNIX değildir.

## Video: Linux Nasıl İnşa Edildi

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V008200_DTH.mp4) | [Alt yazı](video-05-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->
