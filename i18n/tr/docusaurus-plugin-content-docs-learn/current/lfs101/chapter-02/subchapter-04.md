---
title: Linux Topluluğu
---

## Giriş

<img src="/img/lfs101/ch02/img06.png" align="right" />

Diyelim ki, işinizin bir parçası olarak, bir Linux dosya sunucusu yapılandırmanız gerekiyor ve bazı zorluklarla karşılaşıyorsunuz. Cevabı kendiniz bulamazsanız veya bir iş arkadaşınızdan yardım alamazsanız, Linux topluluğu günü kurtarabilir!

Linux topluluğuyla etkileşim kurmanın birçok yolu vardır:

- İlgili tartışma forumlarına sorularınızı gönderin.
- Tartışma konularına abone olun.
- Bölgenizde buluşan yerel Linux gruplarına katılın.

## Video: Linux Topluluğu

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V009900_DTH.mp4) | [Alt Yazı](video-06-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Linux Topluluğu Hakkında Daha Fazla Bilgi

<img src="/img/lfs101/ch02/img06.png" align="right" />

Linux topluluğu, birbirleriyle bağlantı kurmak için birçok farklı forumu kullanan geliştiriciler, sistem yöneticileri, kullanıcılar ve satıcılardan oluşan geniş kapsamlı bir ekosistemdir. En popüler olanlar:

- Internet Relay Chat (IRC) yazılımı (WeeChat, HexChat, Pidgin ve XChat gibi)
- Linux Kullanıcı Grupları dahil çevrimiçi topluluklar ve tartışma panoları (hem yerel hem de çevrimiçi)
- GitHub gibi hizmetlerde barındırılan birçok ortak proje
- Linux Kernel Posta Listesi dahil haber grupları ve posta listeleri
- Topluluk etkinlikleri, ör. Hackathonlar, Kurulum Festivalleri, Açık Kaynak Zirveleri ve Gömülü Linux Konferansları.

En güçlü çevrimiçi kullanıcı topluluklarından birine giden bir portal [linux.com](https://www.linux.com/) adresinde bulunabilir. Bu site Linux Vakfı tarafından barındırılmaktadır ve her ay bir milyondan fazla benzersiz ziyaretçiye hizmet vermektedir. Şu konularda aktif bölümleri vardır:

- Haberler
- Topluluk tartışma konuları
- Ücretsiz eğitimler ve kullanıcı ipuçları.

Bu kursta birkaç kez bu sitedeki ilgili makalelere veya rehberlere atıfta bulunacağız.
