﻿1
00:00:04,186 --> 00:00:32,600
1991 yılının Ağustos ayıydı ve Linus Torvalds adlı 20 yaşındaki bir bilgisayar bilimi öğrencisi, şu anda bilgisayar tarihinin en ünlü kayıtlarından birini göndermek için Helsinki'deki bilgisayarının başına oturdu, "Herkese merhaba... Özgür bir işletim sistemi yapıyorum (sadece bir hobi, gnu gibi büyük ve profesyonel bir şey olmayacak)...muhtemelen AT sabit diskler dışında hiçbir şeyi desteklemeyecek, çünkü tek sahip olduğum bu… “

2
00:00:32,814 --> 00:00:49,196
Linux açık kaynak projeleri sözü hızla tüm dünyaya yayıldı ve geliştiriciler kodlarına katkıda bulundular. Linus, işletim sistemi çekirdeğine Linux adını verdi ve hayvanat bahçesinde küçük bir olaydan sonra maskotu olarak bir pengueni gösteriyor.

3
00:00:49,757 --> 00:01:01,211
Kısa süre sonra, teknoloji kadar Linux'un geleceğini de şekillendiren çok önemli bir karar verdi. Richard Stallman adlı bir vizyoner tarafından oluşturulan GPL lisansını seçti.

4
00:01:01,425 --> 00:01:14,462
Linux çekirdeği, GPL lisansı ve diğer gnu bileşenleri ile birlikte bilgisayar endüstrisinde çok basit ama çok önemli özgürlüklerle devrim yarattı: yazılımı herhangi bir amaç için kullanma özgürlüğü.

5
00:01:14,956 --> 00:01:25,488
Yazılımı ihtiyaçlarınıza göre değiştirme özgürlüğü. Yazılımı arkadaşlarınız ve komşularınızla paylaşma özgürlüğü ve yaptığınız değişiklikleri paylaşma özgürlüğü.

6
00:01:26,180 --> 00:01:37,486
Bu radikal fikirler, onun dünyaya yayılmasını hızlandırdı ve biraz paradoksal olarak, bir hobi ve deneyden, büyük ve gelişen bir ticari ekosistemin temeline yükselişini körükledi.

7
00:01:37,766 --> 00:01:56,044
Şirketler Linux etrafında iş kurdu. 1999'da, Red Hat'in hisseleri halka açılan ilk Linux şirketi olurken üç katına çıktı. Aynı yıl IBM, Linux'u geliştirmek ve tanıtmak için bir milyar dolar harcadı.

8
00:01:56,143 --> 00:02:11,503
Kısa süre sonra Linux, özgür yazılımıyla internetin yükselişini körükleyerek sektörün ağır toplarını alt etti. Kısaca, Linux bilgi işlemde devrim yarattı.

9
00:02:11,833 --> 00:02:26,616
Ancak bir şey bu kadar yıkıcı olduğunda, rekabetçi çapraz ateş olması kaçınılmazdır. Ancak Linux sadece hayatta kalmadı, gelişti. Bugün çekirdek geliştirme topluluğu, Linux'u geliştirmede işbirliği yapan yüzlerce şirket ile binlerce kişiden oluşuyor.

10
00:02:26,897 --> 00:02:32,780
Her üç ayda bir, yeni bir Linux sürümü yayınlanır. Peki bugün Linux nerede?

11
00:02:33,060 --> 00:02:42,488
Dünya çapındaki borsaların% 75'inde çalışıyor ve Amazon, Facebook, Twitter, ebay ve Google'ye olanak sağlayan sunucuları güçlendiriyor.

12
00:02:42,784 --> 00:02:55,953
İnternette her gezindiğinizde kelimenin tam anlamıyla Linux kullanıyorsunuz. Telefonunuzda, TV'nizde, süper bilgisayarların %95'inde ve her gün kullandığınız birçok cihazda çalışıyor.

13
00:02:56,200 --> 00:03:09,039
Linux her yerde ve her şeyi başlatan Helsinki merkezli bir programcı mı? O, dünya çapındaki bu geliştirici ordusunu Portland, Oregon'daki ev ofisinden Linux Vakfı'nda bir üye olarak yönetiyor.

