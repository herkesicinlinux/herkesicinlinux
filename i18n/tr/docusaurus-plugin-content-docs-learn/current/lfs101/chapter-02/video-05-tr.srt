﻿1
00:00:06,611 --> 00:00:10,033
Bilseniz de bilmeseniz de her gün Linux kullanıyorsunuz.

2
00:00:10,102 --> 00:00:15,892
Her gün 850.000'den fazla Linux çalıştıran Android telefon etkinleştiriliyor.

3
00:00:15,911 --> 00:00:21,454
Bunu son raporlarda belirtilen 30.000 Windows telefonuyla karşılaştırın. Bu, siz bu videoyu

4
00:00:21,455 --> 00:00:26,275
izlemeye başladığınızdan beri yüz Android cihazının çevrimiçi olduğu anlamına gelir.

5
00:00:26,277 --> 00:00:32,416
Her gün çoğu Linux çalıştıran yaklaşık 700.000 TV satılıyor. On finansal işlemden sekizi

6
00:00:32,417 --> 00:00:38,453
Linux'tan güç almaktadır. Dünyanın her on süper bilgisayarından dokuzu Linux kullanıyor.

7
00:00:38,455 --> 00:00:44,369
Google, Twitter, Facebook ve Amazon Linux'tan güç almaktadır. Peki tüm bunları başarmak için

8
00:00:44,370 --> 00:00:50,178
Linux nasıl geliştirildi? Windows veya IOS gibi diğer işletim sistemlerinden farklı olarak,

9
00:00:50,179 --> 00:00:56,264
Linux şirketler, coğrafyalar ve pazarlar arasında işbirliği içinde inşa edilmiştir ve bu,

10
00:00:56,265 --> 00:01:01,922
bilgi işlem tarihindeki işbirliğine dayalı en büyük geliştirme projesi olmasıyla sonuçlanmıştır.

11
00:01:01,923 --> 00:01:07,012
2005 yılından bu yana, yaklaşık 800 şirketten yaklaşık 8000 geliştirici Linux çekirdeğine

12
00:01:07,013 --> 00:01:12,053
katkıda bulundu. Bu katkılar, sadece son birkaç yılda yazılmış

13
00:01:12,054 --> 00:01:19,900
1.5 milyon satır olmak üzere 15 milyon satır kodla sonuçlandı. Homeros'un

14
00:01:19,902 --> 00:01:24,156
destansı İlyada şiirinin yalnızca 15.000 satırlık bir metin olduğunu,

15
00:01:24,158 --> 00:01:27,398
Savaş ve Barış romanının yalnızca 56.000 kelime olduğunu düşünün.

16
00:01:27,399 --> 00:01:32,335
Ancak mesele sadece kod satırları değil, aynı zamanda Linux'un ne kadar hızlı geliştirilip

17
00:01:32,336 --> 00:01:37,537
piyasaya sürüldüğüyle de ilgilidir. Örneğin, her iki ila üç ayda bir yeni bir

18
00:01:37,538 --> 00:01:42,427
büyük çekirdek çıkar. Bunu rakip işletim sistemleri için yıllarla kıyaslayın.

19
00:01:42,428 --> 00:01:47,461
Bu, benzersiz bir işbirliğine dayalı geliştirme süreci ile mümkün kılınmıştır. Linux çekirdeğine

20
00:01:47,462 --> 00:01:52,040
kod gönderirken, geliştiriciler değişiklikleri yama adı verilen ayrı birimlere bölerler.

21
00:01:52,041 --> 00:01:56,513
Yama, kaynak koddan değiştirilmesi, eklenmesi veya çıkarılması gereken satırları açıklar.

22
00:01:56,514 --> 00:02:01,224
Her yama yeni bir özellik, bir cihaz için yeni destek ekleyebilir, bir sorunu çözebilir,

23
00:02:01,225 --> 00:02:05,914
performansı artırabilir veya daha kolay anlaşılması için metni yeniden işleyebilir. Geliştiriciler,

24
00:02:05,915 --> 00:02:10,351
yamalarını diğer geliştiricilerin geri bildirimle yanıt verebilecekleri ilgili posta listesine gönderir.

25
00:02:10,352 --> 00:02:17,670
Yama, piyasaya sürülmeye yakın olduğunda, çekirdeğin bir veya daha fazla yüz farklı bölümünü yöneten kıdemli bir Linux çekirdeği

26
00:02:17,671 --> 00:02:25,150
geliştiricisi veya bakımcısı tarafından kabul edilir. Bu, ana hatta gideceğinin garantisi değildir, fakat kesinlikle iyi bir işaret.

27
00:02:25,151 --> 00:02:29,511
Burada daha kapsamlı bir değerlendirmeye tabi tutulur. Bakımcı

28
00:02:29,512 --> 00:02:33,550
incelemesini bitirdiğinde, yamayı imzalayacak ve bir sonraki sürümde neyin

29
00:02:33,551 --> 00:02:37,494
kabul edilip neyin kabul edilmeyeceği konusunda nihai yetkiye sahip

30
00:02:37,495 --> 00:02:41,682
olan Linux'un yaratıcısı ve Linux Vakfı üyesi Linus Torvalds'a gönderecektir.

31
00:02:41,683 --> 00:02:49,292
Her yeni sürümde yaklaşık 10.000 yama kullanılır; çekirdeğe her bir saatte yaklaşık 6 yama uygulanır.

32
00:02:49,293 --> 00:02:57,630
Linux’un gelişme hızı benzersizdir. Günümüzde Linux; mobil cihazlarda, kurumsalda ve web altyapısında,

33
00:02:57,630 --> 00:03:01,909
veri merkezlerinde, süper hesaplamada ve daha fazlasında hakim konumdadır.

34
00:03:01,842 --> 00:03:03,189
Sıradaki ne?

35
00:03:03,192 --> 00:03:06,229
Çünkü biz birlikte hazırız.

