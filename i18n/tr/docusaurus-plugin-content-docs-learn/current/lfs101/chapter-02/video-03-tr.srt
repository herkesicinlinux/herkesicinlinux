﻿1
00:00:04,585 --> 00:00:06,855
Linux'u öğrenmenin en iyi yolu onu kullanmaktır.

2
00:00:07,101 --> 00:00:09,371
Ve bu da uygulamalı bir ders olacak.

3
00:00:09,375 --> 00:00:15,721
Yani doğrudan donımınızın üzerinde çalışan yerel bir Linux sistemine, USB bellek ya da CD üzerinden çalışan bir

4
00:00:16,035 --> 00:00:21,850
canlı sisteme yada hipervizör üzerinden çalışan sanal makineye ihtiyacınız olacak.

5
00:00:21,851 --> 00:00:24,367
Size bu yöntemlerin hepsini göstereceğiz, hadi başlayalım.

