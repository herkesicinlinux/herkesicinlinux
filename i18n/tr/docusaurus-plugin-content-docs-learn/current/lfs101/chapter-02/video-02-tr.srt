﻿1
00:00:03,953 --> 00:00:07,344
Merhaba, benim adım Greg Kroah-Hartman. Bir üye olarak Linux Vakfı ile çalışıyorum.

2
00:00:07,345 --> 00:00:11,898
Ben bir Linux çekirdek geliştiricisiyim. Sürücü geliştirmelerinin yanı sıra, kararlı Linux çekirdeklerini de yayınlıyorum.

3
00:00:11,899 --> 00:00:16,902
Linux geliştirme çekirdeklerini her üç ayda bir yayınlar, ben ise kararlı çekirdekleri haftada bir.

4
00:00:16,903 --> 00:00:21,143
Linux ile yıllar önce temas kurdum. Daha önce iş yerimde kullanırdım.

5
00:00:21,144 --> 00:00:25,584
Ben bir programcıydım. Gömülü aygıtları, gömülü aygıtlar için yazılımı icat ettim.

6
00:00:25,585 --> 00:00:33,053
Cihazlarımı her türlü işletim sistemiyle etkileşime sokmak zorundaydım. Linux kendini yeni yeni gösteriyordu. USB desteğini yeni kazanıyordu.

7
00:00:33,054 --> 00:00:35,943
Ben de bir USB aygıtı yaratıyordum ve onu çalışır duruma getirmek zorundaydım.

8
00:00:35,944 --> 00:00:40,122
Gerekli değişikliği yapmak için bir yama ile katkıda bulundum ve kabul edildi, o an bunun harika olduğunu düşündüm.

9
00:00:40,123 --> 00:00:43,700
Zaman içerisinde daha fazla yama sundum ve Linux'un desteklemediği bir aygıt aygıt

10
00:00:43,701 --> 00:00:46,702
için sürücü yazdım çünkü kendi masaüstü bilgisayarımda Linux kullanıyordum.

11
00:00:46,703 --> 00:00:51,569
Zaman geçtikçe, aniden, bedavaya yazdığım sürücünün şimdiye kadar ödeme alarak yaptıklarımdan

12
00:00:51,570 --> 00:00:55,422
daha fazla kullanıldığını fark ettim çünkü çalıştığım şirketler hiçbir zaman bunu çok iyi yapmadı.

13
00:00:55,423 --> 00:00:59,526
Sonra Dot-com balonu başladı ve Linux geliştiricilerine ihtiyaç arttıkça arttı.

14
00:00:59,527 --> 00:01:04,280
Linux çekirdek geliştiricisi olarak işe başladım, o zamandan beri arkama bakmadım.

15
00:01:04,281 --> 00:01:07,433
Bu benim için çok çok büyük bir fırsattı ve bu işi yaparken keyif alıyorum.

16
00:01:07,434 --> 00:01:12,562
Linux bir işletim sistemidir ve merkezi de Linux çekirdeğidir. Donanım ile konuşarak donanımın çalışmasını sağlar.

17
00:01:12,563 --> 00:01:16,015
Sizin programları çalıştırmanıza ve istediklerinizi yapabilmenize olanak sağlar.

18
00:01:16,016 --> 00:01:23,021
Linux eşsizdir, piyasada pek çok farklı işletim sistemi mevcut fakat bizim geliştirme topluluğumuz çok farklıdır.

19
00:01:23,022 --> 00:01:29,101
Her yıl Linux'a katkıda bulunan 400, belki de 500 şirketten 2000'i aşkın geliştiricimiz var.

20
00:01:29,102 --> 00:01:33,705
İnsanlık tarihindeki en büyük tekil yazılım projesidir.

21
00:01:33,706 --> 00:01:40,961
Günde on yama civarında bir oranda, günde yirmi dört saat, haftada yedi gün bu koda katkıda bulunuyoruz.

22
00:01:40,962 --> 00:01:45,215
Sürekli güncelleniyor, sürekli revize ediliyor, sürekli değişiyor çünkü dünya değişir.

23
00:01:45,216 --> 00:01:52,196
Dünyanın değişimine ayak uydurmalıyız. Eğer bir işletim sisteminin değişimi durursa, ve tabii ki dünya da değişimmeyi bırakmazsa, ölür.

24
00:01:52,197 --> 00:01:57,275
Linux, dünyanın değişimine adapte olmak için sürekli gelişir, sürekli güncellenir.

25
00:01:57,276 --> 00:02:02,329
Linux hakkında ne kadar şey bilirseniz sizin için o kadar iyi olur. Linux her yerde kullanılmakta ve onu kullanmak için yardıma ihtiyacımız var.

26
00:02:02,330 --> 00:02:09,460
Dünya üzerinde Linux'un kullanılmadığı bir bölge neredeyse yok. Eğer Linux kullanma becerisine sahipseniz, yapılabilecek bir sürü iş var.

27
00:02:09,461 --> 00:02:13,439
Bilgisayar programcısılarına ihtiyacımız var, sistem yöneticilerine ihtiyacımız var, begelendirme yapacak insanlara ihtiyacımız var,

28
00:02:13,440 --> 00:02:19,244
test edecek insanlara ihtiyacımız var ve Linux oynaması eğlenceli bir şey. Onunla oynayabilir ve farklı şeyler yaratabilirsiniz.

29
00:02:19,245 --> 00:02:21,045
Yani bunu yaparken eğlenebilirsiniz.

30
00:02:21,046 --> 00:02:25,399
Ayrıca bu, harika şeyler yapan büyük bir topluluğun bir parçası olmayı hissetmeniz için yardım edecek.

31
00:02:25,340 --> 00:02:30,704
Bu kurs size Linux'un temellerini öğretecek; komut satırı nasıl kullanılır, grafiksel kullanıcı arayüzü nasıl kullanılır.

32
00:02:30,705 --> 00:02:35,783
Rehber sizsiniz; istediğiniz zaman kaydolabilir ve dersleri kendi karar verdiğiniz bir hızda alabilirsiniz. Linux'a olan ilginiz için teşekkürler.

33
00:02:35,784 --> 00:02:41,488
Umarım kursu beğenirsiniz. Linux muhteşem bir deneyimdir. İş için, gelecek için,

34
00:02:41,489 --> 00:02:45,784
yeni ve muhteşem aygıtlar yaratmak için büyük fırsatlar sunar. Umarım bundan zevk alırsınız.

