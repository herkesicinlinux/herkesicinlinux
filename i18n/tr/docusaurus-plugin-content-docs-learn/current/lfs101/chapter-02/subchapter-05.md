---
title: Linux Terminolojisi
---

## Giriş

<img src="/img/lfs101/ch02/img07.png" align="right" />

Linux'u keşfetmeye başladıktan kısa süre sonra dağıtım, önyükleyici, masaüstü ortamı vb. alışılmadık bazı terimlerle karşılaşacaksınız. Daha fazla ilerlemeden önce, hızlanmanıza yardımcı olmak için Linux'ta kullanılan bazı temel terminolojilere bir göz atalım.

## Video: Linux Terminolojisi

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V006600_DTH.mp4) | [Alt yazı](video-07-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->
