﻿1
00:00:00,208 --> 00:00:08,384
Linux'u kimse kontrol edemez. Senin sahip olduğundan daha fazla hakkım yok.

2
00:00:08,385 --> 00:00:13,252
Birlikte muazzam ilerlemelere yol açan, tüm endüstrinin büyümesini teşvik eden ve böylece

3
00:00:13,253 --> 00:00:21,901
bilgi çağında Amerikan liderliğini güçlendiren UNIX işletim sistemi ve C programlama dilini birlikte icat ettiği için.

4
00:00:21,902 --> 00:00:29,339
Sadece kararlı olmak yeterli değildir ve sadece hızlı olmak yeterli değildir ve sadece virüssüz olmak da yeterli değildir;

5
00:00:29,340 --> 00:00:32,330
keskin, net ve güzel olmalısın.

6
00:00:32,331 --> 00:00:38,212
Linux yirmi yaşında. Web 7000 günden daha eski.

7
00:00:38,213 --> 00:00:45,117
Birlikte, insan uygarlığını dünya tarihindeki herhangi bir çift icattan daha hızlı değiştirdiler.

8
00:01:14,118 --> 00:01:22,868
Bir bilgisayarı özgürce kullanmayı mümkün kıldık!

