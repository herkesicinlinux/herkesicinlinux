﻿1
00:00:00,738 --> 00:00:19,548
Linux kullanmaya başlamadan önce çekirdek, dağıtım, önyükleyici, hizmet, dosya sistemi, X Pencere Sistemi, masaüstü ortamı ve komut satırı gibi bazı temel terimlerin farkında olmanız gerekir. Bunlar, Linux topluluğu tarafından çok yaygın olarak kullanılmaktadır.

2
00:00:20,028 --> 00:00:39,478
Çekirdek, Linux işletim sisteminin beyni olarak kabul edilir. Donanımı kontrol eder ve donanımın uygulamalarla etkileşimini sağlar. Çekirdeğe bir örnek Linux çekirdeğidir. En son Linux çekirdeği, geçmiş Linux çekirdekleri ile birlikte, kernel.org web sitesinde bulunabilir.

3
00:00:40,142 --> 00:00:57,280
Distro olarak da bilinen dağıtım, Linux tabanlı bir işletim sistemi oluşturmak için Linux çekirdeği ile birleştirilmiş bir program koleksiyonudur. Bazı yaygın dağıtım örnekleri Red Hat Enterprise Linux, Fedora, Ubuntu ve Gentoo'dur.

4
00:00:58,215 --> 00:01:07,387
Adından da anlaşılacağı gibi önyükleme yükleyicisi, işletim sistemini önyükleyen bir programdır. İki önyükleyici örneği GRUB ve ISOLINUX'tur.

5
00:01:08,100 --> 00:01:21,894
Hizmet, arka plan işlemi olarak çalışan bir programdır. Hizmetin bazı örnekleri httpd, nfsd, ntpd, ftpd ve named'dir

6
00:01:22,656 --> 00:01:37,115
Dosya sistemi, dosyaları Linux'ta depolamak ve düzenlemek için bir yöntemdir. Bazı dosya sistemi örnekleri ext3, ext4, FAT, XFS ve Btrfs'dir.

7
00:01:37,705 --> 00:01:45,770
X Pencere Sistemi, neredeyse tüm Linux sistemlerinde grafiksel kullanıcı arayüzleri oluşturmak için standart araç seti ve protokolü sağlar.

8
00:01:46,360 --> 00:01:58,999
Masaüstü ortamı, işletim sisteminin üzerinde bulunan bir grafik kullanıcı arayüzüdür. GNOME, KDE, Xfce ve Fluxbox masaüstü ortamının bazı örnekleridir.

9
00:01:59,847 --> 00:02:04,544
Komut satırı, işletim sisteminin üzerinde komut yazmak için bir arayüzdür.

10
00:02:05,884 --> 00:02:20,859
Kabuk, komut satırı girişini yorumlayan ve işletim sistemine gerekli görevleri ve komutları gerçekleştirmesi talimatını veren komut satırı yorumlayıcısıdır. Örneğin; bash, tcsh ve zsh.

