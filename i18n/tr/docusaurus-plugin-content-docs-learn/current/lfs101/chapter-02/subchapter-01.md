---
title: Giriş ve Öğrenme Hedefleri
---

## Video: Bölüm 2 Giriş

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V003600_DTH.mp4) | [Alt yazı](video-01-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Video: Linux'un Gücü

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V010000_DTH.mp4) | [Alt yazı](video-02-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Üç Önemli Bağlam Parçası

Lütfen aşağıdakileri aklınızda bulundurun:

1. **Linux'ta bazı şeyler değişir**

   Güncel kalmak için ne kadar uğraşmış olursak olalım, Linux hem teknik düzeyde(çekirdek özellikleri dahil) hem de dağıtım ve arayüz düzeyinde sürekli olarak gelişmektedir. Bu nedenle, bu kursun yayınlandığı tarihte olabildiğince güncel olmaya çalıştığımızı, ancak daha önce bahsetmediğimiz değişiklikler ve yeni özellikler olabileceğini lütfen unutmayın. Bu kaçınılmazdır.

2. **Sınıf materyalinde bazı şeyleri tekrarladık**

   Daha önce ele alınan konuları tekrar gözden geçirmemek bu kadar kapsamlı bir kursta neredeyse imkansızdır. Kısa tekrarlar faydalıdır, bu sayede hafızanızı tazelemek için önceki bölümleri tekrar ziyaret etmek zorunda kalmazsınız. Bu özellikle geçici kök ayrıcalıklarını olabildiğince güvenli bir şekilde kazanmak için sudo'nun nasıl kullanılacağı gibi sistem yapılandırma öğeleri için geçerlidir. Bunu yaptığımızın farkındayız ve -en azından çoğu durumda- kazara değil, tasarım gereğidir.

3. **Kutsal savaşlardan kaçınmaya çalıştık**

   Linux (ve daha geniş açık kaynak) topluluğunda güçlü tercih anlaşmazlıklarının olduğu birçok alan vardır. Örnekler arasında en iyi düzenleyici: emacs'a karşı vi; en iyi grafik masaüstü: GNOME'ye karşı KDE vb. karşılaşmalar vardır. Genellikle, işleri karmaşıklaştırmamak için belirli bir alternatif seçtik(gerektiğinde). Örneğin, hangisinin daha üstün olduğu konusunda bir pozisyon aldığımız için değil, daha büyük bir kullanıcı tabanına sahip olduğu için KDE'den çok GNOME hakkında konuşuyoruz.

## Son Düşünceler

Bu kurstan en iyi şekilde yararlanabilmeniz için, bu kurs boyunca kullanabileceğiniz bir makineye Linux'un kurulu olmasını tavsiye ediyoruz. Ders materyalini Linux kurulu bir makinede görüntülemenize şart değil(tek ihtiyacınız olan bir tarayıcıdır). Bununla birlikte, yalnızca kendi makinenizde yapabiliyorsanız yararlanacağınız çok sayıda takip etkinliği ve laboratuvar olduğunu göreceksiniz. Kısa bir kurulum kılavuzu hazırladık: "_Bilgisayarınızı LFS101x.2 için Hazırlama_"; yüklemek için bir Linux dağıtımı seçmenize, tek başına saf bir Linux makinesi mi yoksa çift önyükleme mi yapmak istediğinize, fiziksel veya sanal kurulum tercihiniz vb. konularda karar vermenize yardımcı olacak. Ardından adımlar boyunca size rehberlik edecek. Kurulum prosedürünü daha sonraki bir bölümde ayrıntılı olarak tartışacağız.

Her şeyi çok ayrıntılı olarak ele almadık, ancak Linux'taki belgelerin çoğunun aslında sisteminizde zaten ayrıntılı olarak tartışacağımız man sayfaları biçiminde olduğunu unutmayın. Bir şeyi anlamadığınızda veya bir komut, program, konu veya yardımcı program hakkında daha fazla bilgi edinmek istediğinizde, komut satırına **`man komut_adı`** yazabilirsiniz. Bundan sonra bu şekilde düşündüğünüzü varsayacağız ve sürekli "Daha fazla bilgi için, **`komut_adı`** ile ilgili man sayfasına bakın" diye tekrarlamayacağız.

Not düşmek adına, kurs boyunca açık kaynak topluluğunda yaygın olan bir steno kullandığımızı söyleyelim. Kullanıcının ne gireceğini seçmesi gereken durumlara atıfta bulunurken (örneğin, bir programın veya dosyanın adı), **buraya dosya adı girin**'i temsil etmek için '**foo**' kullanılır. Bu yüzden dikkatli olun, aslında dosyaları değiştirmenizi veya '**foo**' adında hizmetleri kurmanızı söylemiyoruz!

## Video: Son Düşünceler

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V007300_DTH.mp4) | [Alt yazı](video-03-tr.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Öğrenme Hedefleri

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Bu bölümün sonunda şunları yapabiliyor olmalısınız:

- Linux'un tarihini ve felsefesini tartışmak
- Linux topluluğunu tarif etmek
- Linux ile bağdaşmış terimleri tanımlamak
- Bir Linux dağıtımının bileşenleri hakkında tartışmak
