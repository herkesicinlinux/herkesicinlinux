---
title: Linux Vakfı Eğitimleri
---

## Linux Vakfı Eğitimleri

Bireysel insiyatif ve yaratıcılığa inanmakla birlikte, Linux ve açık kaynak teknolojileri öğrenmenin göz korkutucu olmaması gerektiğini düşünüyoruz. Öğrenmeye hızlı bir başlangıç yapmak için işbirliğinin gücünden yararlanabilirsiniz. Eğitimlerimiz, bireylerin ve kuruluşların Linux'tan ve açık kaynak teknolojilerden en iyi şekilde yararlanmak ve kendi kendine öğrenmeye devam edebilmek için ihtiyaç duyduğu kritik becerileri oluşturur.

Linux Vakfı birkaç tür eğitim sunar:

- Sınıf ortamında
- İnternet üzerinden
- Yerinde eğitim
- Etkinliklere dayalı eğitimler.

## Değer Katan Eğitimler

[Linux Vakfı eğitimleri](https://training.linuxfoundation.org/) topluluk içindir ve topluluk üyeleri tarafından tasarlanmıştır. Sistem Yönetimi kursları, Kurumsal Linux ortamlarına odaklanır ve sistem yöneticilerini, teknik desteği ve sistem mimarlarını hedefler. Geliştirici kursları, doğrudan açık kaynak geliştirici topluluğunun liderlerinden gelen eğitmenleri ve içerikleri içerir.

Linux Vakfı tarafından sunulan kurslar hakkında daha fazla bilgi almak için kataloğumuzu ziyaret edin:

- Kurumsal BT ve Linux Sistem Yönetimi Kursları
- Linux Programlama ve Geliştirme Kursları
- Açık Kaynak Uyumluluk Kursları
- Ve daha fazlası.

Linux Vakfı eğitimleri, dağıtım açısından esnektir, teknik olarak ileri düzeydedir ve açık kaynak geliştirme topluluğunun gerçek liderleriyle birlikte oluşturulmuştur. Çoğu kurs, %50'den fazlası uygulamalı laboratuvarlara ve gerçek dünya becerilerini geliştirmeye yönelik aktivitelere odaklanmıştır.

## Video: Linux Vakfı Eğitim Seçenekleri

<!-- [Video](https://player.vimeo.com/video/509219308) | [Alt yazı](video-03-tr.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509219308?texttrack=tr" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>
