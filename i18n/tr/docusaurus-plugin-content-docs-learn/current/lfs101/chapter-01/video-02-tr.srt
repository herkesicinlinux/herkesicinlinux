﻿1
00:00:06,210 --> 00:00:08,070
Gezegenin hiçbir yerinde

2
00:00:08,319 --> 00:00:10,432
buna benzer başka bir olay yok, nokta.

3
00:00:10,849 --> 00:00:13,281
Çok fazla enerji ve fikir benimseniyor.

4
00:00:13,381 --> 00:00:15,547
Topluluk ve coşku inanılmaz.

5
00:00:15,848 --> 00:00:17,413
Bu etkinlik her açıdan her yıl

6
00:00:17,513 --> 00:00:19,769
dört gözle beklediğim bir etkinlik.

7
00:00:21,081 --> 00:00:22,761
Linux Vakfı etkinlikleri benzersizdir çünkü

8
00:00:22,861 --> 00:00:26,721
topluluk ve endüstri geliştiricilerinin gerçek teknik kararlar almak için

9
00:00:26,821 --> 00:00:29,550
gerçek zamanlı olarak buluştuğu bir yerdir.

10
00:00:30,746 --> 00:00:34,477
Açık kaynak topluluğunda çok yüksek değerli ve etkili insanlar var.

11
00:00:34,478 --> 00:00:39,534
ve Linux topluluğunun neler sunabileceğine dair turnusol testi yapmanın harika bir yolu.

12
00:00:39,535 --> 00:00:45,307
Konuşmacılar burada başka hiçbir yerde söylemeyecekleri şeyler söylüyor.

13
00:00:45,308 --> 00:00:52,212
Linux Vakfı etkinlikleri, endüstri ve topluluk arasında iyi bir alışveriştir.

14
00:00:52,338 --> 00:01:01,328
Linux ve diğer özgür ve açık kaynaklı yazılım geliştirme ile ilgili en önemli şey, insanlar arasındaki işbirliğidir.

15
00:01:01,329 --> 00:01:05,029
Birlikte çalıştığımız insanları tanıdığımızda daha iyi çalışırız.

16
00:01:05,030 --> 00:01:08,481
Ağ oluşturma fırsatları harikaydı.

17
00:01:08,482 --> 00:01:13,847
Birkaç gün içinde, sadece e-posta ve telefon görüşmeleriyle yıllarca yaptığınızdan çok daha fazlasını yaparsınız.

18
00:01:13,848 --> 00:01:20,777
Hem zaten tanıdığınız insanlarla iletişim kurmak hem de farklı bakış açıları olabilecek bazı insanlarla tanışmak için birçok farklı fırsat.

19
00:01:20,778 --> 00:01:25,198
Linux Con ve Cloud Open'ın örtüşmesinin, büyük bir grup farklı insanı

20
00:01:25,199 --> 00:01:28,924
bir tür işbirliği yapmak ve ilginç sohbetler etmek için bir araya getirdiğini düşünüyorum.

21
00:01:28,925 --> 00:01:33,445
Bence bu, bu konferansın başka hiçbir yerde bulamadığımız benzersiz bir yönü.

22
00:01:33,446 --> 00:01:38,661
Linux konferansı ile ilgili en sevdiğim şeylerden biri, her zaman farklı bir şehirde olmasıdır.

23
00:01:38,662 --> 00:01:41,791
Bulunduğunuz yere dair küçük de olsa bir deneyim yaşıyorsunuz.

24
00:01:41,792 --> 00:01:47,081
Linux Vakfı etkinlikleri, gerçekten ilginç bir grup insanın karışımıyla gerçekleşen gerçekten eğlenceli etkinliklerdir.

25
00:01:47,082 --> 00:01:53,316
New Orleans'ta, bir New Orleans geçit töreninde, sokaklarda olmak gibiydi. Harikaydı.

26
00:01:53,317 --> 00:01:57,539
Gerçek şu ki, partiler bazen seanslardan daha etkilidir.

27
00:01:57,540 --> 00:02:03,599
İnsanların, daha etkili bir şekilde çalışabilmeleri için iletişime geçme zamanı.

28
00:02:03,600 --> 00:02:09,983
Tükettiğiniz ürün ve hizmetlerin geleceğini şekillendirmeye yardımcı olmak istiyorsanız,

29
00:02:09,984 --> 00:02:15,125
yeni nesil teknolojinizi yaratan insanlara girdi vermek için harika bir yer.

30
00:02:15,126 --> 00:02:20,167
Dünyanız tanıştığınız her kişiyle değişecek; herkes açık.

31
00:02:20,168 --> 00:02:24,042
Ne yaptıklarını öğrenmeniz için bir fırsat yaratıyorlar.

32
00:02:24,043 --> 00:02:24,837
Bu heyecan verici!

33
00:02:24,838 --> 00:02:27,097
Pek çok insan, çok fazla etkileşim. Harika bir topluluk.

34
00:02:27,098 --> 00:02:28,364
Her şeyden biraz biraz.

35
00:02:28,365 --> 00:02:29,979
İnekçe mutluluk!

