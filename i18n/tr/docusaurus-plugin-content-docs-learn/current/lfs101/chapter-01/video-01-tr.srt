﻿1
00:00:04,246 --> 00:00:06,612
Linux dünyasına girişinize

2
00:00:06,636 --> 00:00:08,453
bazı temel bilgileri ele alarak başlayalım.

3
00:00:08,983 --> 00:00:12,484
İlk olarak, Linux Vakfı ve neler yaptığımız hakkında konuşacağız.

4
00:00:12,767 --> 00:00:16,200
Ardından, bu çevrimiçi kursun lojistiği hakkında konuşacağız.

5
00:00:16,671 --> 00:00:17,738
Ve son olarak,

6
00:00:18,078 --> 00:00:19,990
henüz yapmadıysanız,

7
00:00:20,180 --> 00:00:24,582
size uygun bir Linux dağıtımını seçme sürecinde size yol göstereceğiz.

8
00:00:25,396 --> 00:00:26,416
Hadi başlayalım.

