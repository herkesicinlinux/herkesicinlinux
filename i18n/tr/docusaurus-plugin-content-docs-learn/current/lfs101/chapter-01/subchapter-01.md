---
title: Giriş ve Öğrenme Hedefleri
---

## Video: Bölüm 1 Giriş

<!-- [Video](https://player.vimeo.com/video/509178924?texttrack=tr) | [Alt yazı](video-01-tr.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509178924?texttrack=tr" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>

## Öğrenme Hedefleri

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Bu bölümün sonunda şunları yapabilmelisiniz:

- Linux Vakfı'nın rolünü tartışın.
- Linux Vakfı'nın eğitim programı tarafından sağlanan öğrenme fırsatlarını keşfedin.
- Bu kurs için gereken yazılım ortamını açıklayın.
- Üç ana Linux dağıtımı ailesini tanımlayın.
