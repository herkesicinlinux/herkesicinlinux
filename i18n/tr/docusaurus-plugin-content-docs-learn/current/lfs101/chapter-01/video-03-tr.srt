﻿1
00:00:07,254 --> 00:00:10,954
Linux uzmanlarından eğitim alın:
Doğrudan Kaynağından!

2
00:00:15,245 --> 00:00:17,053
LF411 Gömülü Linux Geliştirme

3
00:00:17,111 --> 00:00:24,902
Bugün piyasada mevcut bir ton Linux işi var ve bu talebi karşılayacak yeterli sistem yöneticisi ve geliştiricisi yok.

4
00:00:24,902 --> 00:00:32,136
Linux Vakfı, platformu ilerletmek ve piyasanın ihtiyaçlarını karşılamak için güçlü bir yetenek havuzu olduğundan emin olmak için var.

5
00:00:32,236 --> 00:00:39,385
Onlara Linux'un ne olduğunu ve onu nasıl hızlı bir şekilde kullanmaya başlayabileceklerini ve nasıl katkıda bulunabileceklerini öğretmek için buradayız.

6
00:00:39,485 --> 00:00:47,349
Ve Linux'taki pek çok şeyin dağınık doğası nedeniyle, insanların tüm bu malzemeleri kendi başlarına bir araya getirmeleri zor olacaktır.

7
00:00:47,678 --> 00:00:54,682
Bu kursu almak, Linux ekibinde lider pozisyonuna ulaşmamı sağladı.

8
00:00:54,712 --> 00:00:57,954
Yapmanız gereken bir görev var ve bunu hızlı bir şekilde yapmanıza yardımcı olmak istiyoruz.

9
00:00:58,169 --> 00:01:08,377
Sunucu kullanımını ve IO kullanımını izleyebileceğim birçok kısayol, birçok yol öğrendim.

10
00:01:09,093 --> 00:01:11,021
Linux Vakfı eğitimlerini benzersiz yapan şey ne?

11
00:01:11,845 --> 00:01:20,019
Birincisi, bizler dağıtım bağımsızız, özellikle bir Linux çeşidini diğerine tercih etmiyoruz.

12
00:01:20,167 --> 00:01:23,078
Her şeyin her dağıtımda çalışacağından emin olmaya çalışıyoruz.

13
00:01:23,178 --> 00:01:25,834
İkincisi, her şeyi son derece güncel tutuyoruz.

14
00:01:25,934 --> 00:01:31,175
Örneğin, tüm çekirdek seviyesi eğitimlerimizde, her yeni çekirdek çıktığında materyalin yeni bir sürümünü yayınlıyoruz.

15
00:01:31,201 --> 00:01:35,432
Beni şok eden şey, çekirdeğin çok hızlı bir şekilde ilerlemesine rağmen

16
00:01:35,532 --> 00:01:42,516
her daim güncel olması ve neyin nerede olduğunu ve nelerin tarihinin geçtiğini tam olarak biliyor olmasıydı.

17
00:01:42,596 --> 00:01:50,464
Üçüncüsü, vakfın birçok önde gelen çekirdek geliştiricisiyle güçlü bir bağlantısı olduğu için,

18
00:01:50,564 --> 00:01:54,314
toplulukta neler olup bittiğine dair çok iyi bir fikre sahip olmamız.

19
00:01:54,414 --> 00:02:00,679
Bu insanlardan bazılarının materyallerimizi gözden geçirmesini ve önerilerde bulunmasını ve onların geliştirilmesine katkıda bulunmasını sağlıyoruz.

20
00:02:00,779 --> 00:02:02,812
Eğitim Detayları

21
00:02:03,149 --> 00:02:11,872
Eğitimlerimizi yaklaşık yüzde elli uygulamalı egzersiz ve yaklaşık yüzde elli ders anlatımı formunda tutmaya çalışıyoruz.

22
00:02:11,892 --> 00:02:19,712
Jerry harikaydı. Başka sorularımız olursa onunla iletişime geçebilmemiz için bize e-postasını verdi; ki yaptım.

23
00:02:19,714 --> 00:02:24,456
Öğrenciler eğitime katıldıklarında, ayrıntılı eğitim kılavuzları alırlar.

24
00:02:24,556 --> 00:02:35,453
Bunlar bazı durumlarda oldukça kalın kitaplar olabiliyorlar, sadece birkaç liste ve bir dizi slayt değil, ayrıntılı kitaplar.

25
00:02:35,667 --> 00:02:41,423
Bir kitapçıkla geldiğine sevindim çünkü bu kullanmazsan unutursun türünde bir şey.

26
00:02:41,523 --> 00:02:51,725
Birkaç yıl içinde bir şey çıkarsa, bir kitapçık açıp hatırlayıp şunu söyleyebilirim; bunu bir danışman tutmadan yapabiliriz.

27
00:02:52,716 --> 00:02:55,266
Kurs Sonrası

28
00:02:55,876 --> 00:02:59,426
Linux Vakfı kaynakları havuzuna giriyorsunuz.

29
00:02:59,526 --> 00:03:03,451
Zirvelerimiz, etkinliklerimiz, materyallerimiz, ek teknik raporlar.

30
00:03:03,551 --> 00:03:12,840
Kariyerinizi ve ağınızı büyütmeniz, hem profesyonel topluluğa hem de açık kaynak için geliştirici topluluğuna entegre olmanız için bir kaynağız.

31
00:03:12,991 --> 00:03:24,536
Daha sonra, Linux Vakfı tarafından, şirketlere çekirdek geliştiricileri için pozisyonları doldurma konusunda yardım eden Mike Wooster tarafından, benimle iletişime geçildiğini görünce şaşırdım.

32
00:03:26,928 --> 00:03:30,240
Kariyerinizi İlerletin.
Şirketinizi İlerletin.
Neyi bekliyorsunuz?

