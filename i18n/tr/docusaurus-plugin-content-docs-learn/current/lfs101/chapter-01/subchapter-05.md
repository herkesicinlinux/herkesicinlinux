---
title: Bölüm Sonu
---

## Öğrenme Hedefleri (Gözden Geçirin)

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Artık şunları yapabilmelisiniz:

- Linux Vakfı'nın rolünü tartışın.
- Linux Vakfı'nın eğitim programı tarafından sağlanan öğrenme fırsatlarını keşfedin.
- Bu kurs için gereken yazılım ortamını açıklayın.
- Üç ana Linux dağıtımı ailesini tanımlayın.

## Özet

<img src="/img/lfs101/common/tux-grad-cap.png" align="right"/>

Bölüm 1'i tamamladınız. Şimdi işlenen temel kavramları özetleyelim:

- Linux Vakfı, tüm endüstri sektörlerini kapsayan, şirketlere güç veren birçok kritik açık kaynak projesinin çatı kuruluşudur. Bugünkü çalışmaları Linux'un çok ötesine uzanıyor ve yazılım yığınının her katmanında yeniliği teşvik ediyor.
- Linux Vakfı eğitimi topluluk tarafından ve topluluk içindir. Linux eğitimi dağıtımlar açısından esnektir, teknik olarak ileri düzeydedir ve Linux geliştirme topluluğunun liderleriyle oluşturulmuştur.
- Linux içinde üç ana dağıtım ailesi vardır: **Red Hat**, **SUSE** ve **Debian**. Bu kursta, tüm bu ailelerin temsilci üyeleriyle birlikte çalışacağız.
