---
title: Linux Vakfı
---

## Linux Vakfı Hakkında

1991'deki başlangıcından bu yana Linux, bilgi işlem dünyasında, New York Borsası'ndan cep telefonlarına, süper bilgisayarlara ve tüketici cihazlarına kadar her şeyi güçlendiren büyük bir güç haline geldi.

Linux Vakfı, en zor teknoloji sorunlarını çözmek ve açık teknoloji gelişimini ve ticari benimsemeyi hızlandırmak için dünyanın önde gelen geliştiricileri ve şirketleriyle ortaklık yapmaktadır. Linux Vakfı, açık kaynak işbirliği yoluyla karmaşık sorunları çözmek için çalışan her girişime deneyim ve uzmanlık sağlamayı misyon edinerek, açık kaynaklı projeleri ölçeklendirmek için araçlar sağlar: en iyi güvenlik uygulamaları, denetim, operasyonlar ve ekosistem geliştirme, eğitim ve sertifika, lisanslama ve tanıtım gibi.

Linux, dünyanın en büyük ve en yaygın açık kaynak yazılım projesidir. Linux Vakfı, Linux yaratıcısı Linus Torvalds ve baş bakımcı Greg Kroah-Hartman'a ev sahipliği yapar ve Linux çekirdek gelişiminin yıllarca korunabileceği ve hızlandırılabileceği tarafsız bir ev sağlar. Linux'un başarısı, açık kaynak topluluğundaki büyümeyi hızlandırdı, açık kaynağın ticari etkinliğini gösterdi ve tüm sektörlerde ve teknoloji yığınının tüm seviyelerinde sayısız yeni projeye ilham verdi.

Linux Vakfı'nın bugünkü çalışmaları, Linux'un çok ötesine uzanıyor ve yazılım yığınının her katmanında yeniliği teşvik ediyor. Linux Vakfı, günümüzde şirketlere güç veren ve tüm endüstri sektörlerini kapsayan birçok kritik açık kaynak projesinin çatı kuruluşudur:

- Büyük veri ve analitik: [ODPi](https://www.odpi.org/), [R Consortium](https://www.r-consortium.org/)
- Ağ uygulamaları: [OpenDaylight](https://www.opendaylight.org/), [ONAP](https://www.onap.org/), [OPNFV](https://www.opnfv.org/)
- Gömülü sistemler: [Dronecode](https://www.dronecode.org/), [Zephyr](https://www.zephyrproject.org/)
- Web araçları: [JS Foundation](https://js.foundation/), [Node.js](https://nodejs.org/en/)
- Bulut bilişim: [Cloud Foundry](https://www.cloudfoundry.org/), [Cloud Native Computing Foundation](https://cncf.io/), [Open Container Initiative](https://www.opencontainers.org/)
- Otomotiv: [Automotive Grade Linux](https://www.automotivelinux.org/)
- Güvenlik: [The Core Infrastructure Initiative](https://www.coreinfrastructure.org/)
- Blokzincir: [Hyperledger](https://www.hyperledger.org/)
- Ve daha fazlası.

Linux Vakfı hakkında daha fazla bilgi edinmek için [Linux Vakfı web sitesini](https://www.linuxfoundation.org/) ziyaret edin.

<div align="center">
  <img src="/img/lfs101/ch01/img02.png" />
</div>

## Linux Vakfı Etkinlikleri

Linux Vakfı, dünya çapında topluluk üyelerini bir araya getiren konferanslara ve diğer etkinliklere ev sahipliği yapar. Bu etkinlikler:

- Bir sonraki (Linux) çekirdek sürümünün geliştirilmesi için açık bir forum sağlar.
- Sorunları gerçek zamanlı bir ortamda çözmek için geliştiricileri ve sistem yöneticilerini bir araya getirir.
- Aktif tartışmalar için çalışma grupları ve topluluk gruplarına ev sahipliği yapar.
- Kuruluşlarda Linux kullanımını artırmak için son kullanıcılar, sistem yöneticileri ve çekirdek geliştiricileri arasında köprü görevi görür.
- Tüm topluluk içerisinde işbirliğini teşvik eder.
- Platformu ilerletme kabiliyeti açısından eşsiz bir atmosfer sunar.

<div align="center">
  <img src="/img/lfs101/ch01/img03.png" />
</div>

## Linux Vakfı Etkinlikleri Hakkında Daha Fazla Bilgi

[Linux Vakfı etkinlikleri](https://events.linuxfoundation.org/) şunları içerir:

- Açık Kaynak Zirvesi Kuzey Amerika, Avrupa, Japonya ve Çin
- MesosCon Kuzey Amerika, Avrupa ve Çin
- Gömülü Linux Konferansı / OpenIoT Zirvesi Kuzey Amerika ve Avrupa
- Açık Kaynak Liderlik Zirvesi
- Otomotiv Linux Zirvesi
- Apache: Büyük Veri Kuzey Amerika ve ApacheCon
- KVM Forumu
- Linux Depolama Dosya Sistemi ve Bellek Yönetimi Zirvesi
- Vault
- Açık Ağ Oluşturma Zirvesi
- Ve daha fazlası.

## Video: Linux Vakfı Etkinlikleri

<!-- [Video](https://player.vimeo.com/video/509213283) | [Alt yazı](video-02-tr.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509213283?texttrack=tr" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>
