---
title: Kurs Linux Gereksinimleri
---

## Kurs Yazılım Gereksinimleri

Bu kurstan tam anlamıyla yararlanabilmek için en az bir Linux dağıtımına ihtiyacınız olacak (dağıtım terimine henüz aşina değilseniz, yakında olacaksınız!).

Bir sonraki sayfada, mevcut birçok Linux dağıtımı ve ait oldukları düşünülebilecek aileler hakkında daha fazla ayrıntı öğreneceksiniz. Kelimenin tam anlamıyla yüzlerce dağıtım olduğu için, hepsini bu kursta ele almadık. Bunun yerine, üç ana dağıtım ailesine odaklanmaya karar verdik ve illüstrasyonlar, örnekler ve alıştırmalar için her ailenin içinden dağıtımları seçtik. Bu, bu belirli dağıtımları desteklediğimiz anlamına gelmiyor; basitçe, oldukça yaygın olarak kullanıldıkları ve her biri kendi ailesi için iyi birer temsilci oldukları için seçildiler.

Kullandığımız aileler ve temsilci dağıtımları şunlardır:

- **Red Hat Ailesi Sistemleri** (**CentOS** ve **Fedora** dahil)
- **SUSE Ailesi Sistemleri** (**openSUSE** dahil)
- **Debian Ailesi Sistemleri** (**Ubuntu** ve **Linux Mint** dahil).

<figure>
  <img src="/img/lfs101/ch01/img04.png"/>
  <figcaption>Ubuntu, CentOS, ve openSUSE Masaüstü Görüntüleri</figcaption>
</figure>

## Üç Büyük Linux Dağıtım Ailesine Odaklanın

Bir sonraki bölümde, bir Linux dağıtımını oluşturan bileşenler hakkında bilgi edineceksiniz.

Şimdilik bilmeniz gereken şey, bu kursun şu anda var olan üç ana Linux dağıtım ailesine odaklandığıdır. Ancak, yetenekli katılımcılar olduğu sürece, dağıtım aileleri ve bu aileler içindeki dağıtımlar değişmeye ve büyümeye devam edecektir. İnsanlar bir ihtiyaç görürler ve bu ihtiyaca cevap vermek için özel yapılandırmalar ve yardımcı programlar geliştirirler. Bazen bu çaba yepyeni bir Linux dağıtımı yaratır. Bazen ise bu çaba, mevcut bir ailenin üyelerini genişletmek için mevcut bir dağıtımdan yararlanır.

Mevcut dağıtımların oldukça uzun bir listesi için, [LWN.net Linux Dağıtım Listesi](https://lwn.net/Distributions/)'ne bakın.

<figure align="center">
  <img src="/img/lfs101/ch01/img05.png" class="invert"/>
  <figcaption>Linux Çekirdek Dağıtım Aileleri ve Özgün Dağıtımlar</figcaption>
</figure>

## Red Hat Ailesi

<img src="/img/lfs101/ch01/img06.png" class="invert" align="right"/>

Red Hat Enterprise Linux (RHEL - Red Hat Kurumsal Linux); CentOS, Fedora ve Oracle Linux'u içeren ailenin başında gelir.

Fedora'nın RHEL ile yakın bir ilişkisi vardır ve Red Hat'in kurumsal sürümünden önemli ölçüde daha fazla yazılım içerir. Bunun bir nedeni, Fedora'nın oluşturulmasına, pek çoğu Red Hat için çalışmayan katılımcılardan oluşan geniş bir topluluğun dahil olmasıdır. Ayrıca Fedora, gelecekteki RHEL sürümleri için bir test platformu olarak kullanılır.

Bu kursta, genellikle aktiviteler, demolar ve laboratuvarlar için CentOS kullanılacaktır, çünkü son kullanıcı için ücretsiz olarak mevcuttur ve Fedora'dan çok daha uzun bir sürüm döngüsüne sahiptir (altı ayda bir yeni bir sürüm).

CentOS'un temel sürümü, kurumsal ortamlarda en popüler Linux dağıtımı olan RHEL ile neredeyse aynıdır.

### Red Hat Ailesi Hakkında Temel Gerçekler

Red Hat dağıtım ailesiyle ilgili temel gerçeklerden bazıları şunlardır:

- Fedora, RHEL için bir üstkaynak (upstream) test platformu olarak hizmet vermektedir.
- CentOS, RHEL'in yakın bir klonudur, Oracle Linux ise çoğunlukla bazı değişikliklerin olduğu bir kopyadır (aslında, CentOS, 2014'ten beri Red Hat'in bir parçasıdır).
- RHEL/CentOS 7'de oldukça yamalı sürüm 3.10 çekirdeği kullanılırken, RHEL/CentOS 8'de sürüm 4.18 kullanılır.
- Intel x86, Arm, Itanium, PowerPC ve IBM System Z gibi donanım platformlarını destekler.
- Sistemdeki paketleri kurmak, güncellemek ve kaldırmak için yum ve dnf adlı RPM tabanlı paket yöneticilerini (daha sonra ayrıntılı olarak ele alınacaktır) kullanır.
- RHEL, kendi sistemlerini barındıran işletmeler tarafından yaygın olarak kullanılmaktadır.

## SUSE Ailesi

<img src="/img/lfs101/ch01/img07.png" class="invert" align="right"/>

SUSE (SUSE Linux Enterprise Server (SLES)) ve openSUSE arasındaki ilişki; RHEL, CentOS ve Fedora arasında anlatılana benzer.

Bu kursta, son kullanıcılara ücretsiz olarak sunulduğundan, SUSE ailesi için referans dağıtımı olarak openSUSE kullanıyoruz. İki ürün son derece benzer olduğundan, openSUSE'yi kapsayan konular tipik olarak SLES'e de oldukça az problemle uygulanabilir.

### SUSE Ailesi Hakkında Temel Gerçekler

SUSE ailesi ile ilgili temel gerçeklerden bazıları aşağıda listelenmiştir:

- SUSE Linux Enterprise Server (SLES), openSUSE için üstkaynaktır (upstream).
- openSUSE Leap 15'te çekirdek sürüm 4.12 kullanılır.
- Sistemdeki paketleri kurmak, güncellemek ve kaldırmak için RPM tabanlı zypper paket yöneticisini (daha sonra ayrıntılı olarak ele alacağız) kullanır.
- Sistem yönetimi amacıyla YaST (Yet Another Setup Tool - Bir Başka Kurulum Aracı) uygulamasını içerir.
- SLES, perakende ve diğer birçok sektörde yaygın olarak kullanılmaktadır.

## Debian Ailesi

<img src="/img/lfs101/ch01/img08.png" class="invert" align="right"/>

Debian dağıtımı, Ubuntu dahil olmak üzere diğer birkaç dağıtım için üstkaynaktır (upstream). Benzer şekilde Ubuntu; Linux Mint ve bir dizi başka dağıtım için üstkaynaktır. Yaygın olarak hem sunucularda hem de masaüstü bilgisayarlarda kullanılır. Debian saf bir açık kaynak topluluk projesidir (herhangi bir şirkete ait değildir) ve kararlılığa odaklanır.

Debian, herhangi bir Linux dağıtımının kullanıcılarına sunabileceği, açık ara en büyük ve en eksiksiz yazılım havuzunu sağlar.

Ubuntu, uzun vadeli istikrar ve kullanım kolaylığı arasında iyi bir uzlaşma sağlamayı amaçlamaktadır. Ubuntu, paketlerinin çoğunu Debian’ın kararlı dalından aldığından, çok büyük bir yazılım havuzuna da erişebilir. Bu nedenlerden dolayı, bu kursta Debian ailesi dağıtımları için referans olarak Ubuntu 18.04 ve 20.04 LTS'yi (Long Term Support - Uzun Süreli Destek) kullanacağız. Ubuntu, Canonical Ltd.'nin tescilli ticari markasıdır ve bu kurs boyunca onların izniyle kullanılmıştır.

### Debian Ailesi Hakkında Temel Gerçekler

Debian ailesiyle ilgili bazı temel gerçekler aşağıda listelenmiştir:

- Debian ailesi Ubuntu için üstkaynaktır (upstream) ve Ubuntu, Linux Mint ve diğerleri için üstkaynaktır.
- Ubuntu 18.04 LTS'de çekirdek sürümü 4.15 kullanılır.
- Sistemdeki paketleri kurmak, güncellemek ve kaldırmak için DPKG tabanlı APT paket yöneticisini (daha sonra ayrıntılı olarak ele alacağımız `apt`, `apt-get`, `apt-cache` vb. komutlarını kullanarak) kullanır.
- Ubuntu, bulut kurulumları için yaygın olarak kullanılmaktadır.
- Ubuntu, Debian'ın üzerine kurulmuş ve GNOME tabanlı olsa da, standart Debian'daki arayüzden ve diğer dağıtımlardan görsel olarak farklıdır.

## Yazılım Ortamı Hakkında Daha Fazla Bilgi

Linux Vakfı tarafından üretilen içerikler dağıtımlar açısından esnektir. Bu, teknik açıklamaların, laboratuvarların ve prosedürlerin neredeyse tüm modern dağıtımlarda çalışması gerektiği anlamına gelir. Mevcut Linux sistemleri arasında seçim yaparken, teknik farklılıkların esas olarak paket yönetim sistemleri, yazılım sürümleri ve dosya konumları ile ilgili olduğunu fark edeceksiniz. Bu farklılıkları kavradığınızda, bir Linux dağıtımından diğerine geçmek nispeten zahmetsiz hale gelir.

Bu kurs için kullanılan masaüstü ortamı **GNOME**'dir. _Bölüm 4: Grafik Arayüz_'de göreceğimiz gibi, farklı masaüstü ortamları vardır, ancak biz en yaygın olarak kullanılan GNOME'u seçtik.
