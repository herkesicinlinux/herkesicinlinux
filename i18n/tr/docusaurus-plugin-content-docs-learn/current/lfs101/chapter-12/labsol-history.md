---
title: Komut Geçmişi
---

**`history`** komutu, daha önce yazdığınız komutları görüntülemenin yoludur:

```bash
student:/tmp> history
1 cd /
2 ls
3 cd
4 pwd
5 echo $SHELL
6 ls /var/
7 ls /usr/bin
8 ls /usr/local/bin
9 man fstab
10 ls
. . .
```

Önceki bir komutu yeniden çalıştırmak için birkaç seçeneğiniz var. Diyelim ki, ilk giriş yaptığınızda çalıştırdığınız **`man`** komutunu tekrar çalıştırmak istediniz. Şunu yazabilirsiniz:

```bash
student:/tmp> !9
```

Bu, **#9** olarak listelenen komutu yeniden çalıştıracaktır. Bu komut, yazdığınız tek **`man`** komutuysa, şunu da yazabilirsiniz:

```bash
student:/tmp> !man
```

Şimdi yazdığınız komut adını hatırlıyorsunuz. Son olarak, birkaç **`man`** komutu yazmış olsaydınız, **CTRL-R** ile, tekrar çalıştırmak istediğiniz **`man`** komutunu bulmak için geçmişinizde arama yapabilir ve sonra **Enter** tuşu ile çalıştırabilirdiniz.
