---
title: PATH Değişkenine /tmp Ekleme
---

İlk olarak, bir düzenleyici kullanarak veya sadece şunu yaparak sahte ls programını oluşturun:

```bash
student:/tmp>echo "echo MERHABA, bu sahte ls programı." > /tmp/ls
student:/tmp>chmod +x /tmp/ls
```

Bir sonraki iki adım için, başka bir terminal penceresinde çalışmak ya da sadece yeni bir kabuk başlatmak iyi bir fikir olacaktır, böylece değişikliklerin kalıcı olması önlenecektir. Sadece **`bash`** yazarak yeni bir kabuk başlatabilirsiniz.

1.  ```bash
    student:/tmp>bash
    student:/tmp>PATH=$PATH:/tmp
    student:/tmp>ls /usr
    ```

    ```bash
    bin etc games include lib lib64 libexec local sbin share src tmp
    ```

    ```bash
    student:/tmp>exit
    ```

2.  ```bash
    student:/tmp>bash
    student:/tmp>PATH=/tmp:$PATH
    student:/tmp>ls /usr
    ```

    ```bash
    MERHABA, bu sahte ls programı.
    ```

    ```bash
    student:/tmp>exit
    ```

İkinci formun çok tehlikeli olduğuna ve sisteminize **Truva Atı** programı eklemenin kolay bir yolu olduğuna dikkat edin. Birisi `/tmp` dizinine kötü amaçlı bir program yerleştirebilirse, bunu yanlışlıkla çalıştırabilirsiniz.
