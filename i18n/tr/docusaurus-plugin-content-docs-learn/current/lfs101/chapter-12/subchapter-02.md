---
title: Hesaplar, Kullanıcılar ve Gruplar
---

## Mevcut Kullanıcıyı Tanımlama

Bildiğiniz gibi, Linux çok kullanıcılı bir işletim sistemidir, yani aynı anda birden fazla kullanıcı oturum açabilir.

- Mevcut kullanıcıyı tanımlamak için **`whoami`** yazın.

- O anda oturum açmış kullanıcıları listelemek için **`who`** yazın.

**who** komutuna **`-a`** seçeneğinin verilmesi daha ayrıntılı bilgi verecektir.

<figure align="center">
  <img src="/img/lfs101/ch12/img01.png"/>
  <figcaption>Mevcut Kullanıcıyı Tanımlama</figcaption>
</figure>

## Kullanıcı Başlangıç Dosyaları

Linux'ta, komut kabuğu programı (genellikle **`bash`**), kullanıcı ortamını yapılandırmak için bir veya daha fazla başlangıç dosyası kullanır. **/etc** dizinindeki dosyalar tüm kullanıcılar için genel ayarları tanımlarken, kullanıcının ana dizinindeki başlatma dosyaları genel ayarları içerebilir ve/veya geçersiz kılabilir.

Başlangıç dosyaları, kullanıcının, her komut kabuğunda yapmak istediği her şeyi yapabilir, örneğin:

- Bilgi istemini (prompt) özelleştirme
- Komut satırı kısayolları ve takma adları tanımlama
- Varsayılan metin düzenleyiciyi ayarlama
- Çalıştırılabilir programların nerede bulunacağına ilişkin yolu ayarlama

<figure align="center">
  <img src="/img/lfs101/ch12/img02.png" class="invert"/>
  <figcaption>Kullanıcı Başlangıç Dosyaları</figcaption>
</figure>

### Başlangıç Dosyaları Sıralaması

Standart olarak, Linux'a ilk giriş yaptığınızda, **/etc/profile** dosyası okunur ve değerlendirilir, ardından aşağıdaki dosyalar (eğer varsa) listelenen sırada aranır:

1. **~/.bash_profile**
2. **~/.bash_login**
3. **~/.profile**

**~**, kullanıcının ev dizinini gösterir. Linux oturum açma kabuğu (login shell), ilk karşılaştığı başlangıç dosyasını değerlendirir ve geri kalanını yok sayar. Bu, örneğin, önce **~/.bash_profile** dosyasını bulursa, **~/.bash_login** ve **~/.profile** dosyalarını yok sayacağı anlamına gelir. Farklı dağıtımlar farklı başlangıç dosyaları kullanabilirler.

Bununla birlikte, her yeni kabuk veya uçbirim penceresi vb. oluşturduğunuzda, tam sistem oturumu açma işlemi yapmazsınız, bunun yerine sadece **~/.bashrc** adlı bir dosya okunur ve değerlendirilir. Bu dosya oturum açma kabuğuyla birlikte okunup değerlendirilmese de, çoğu dağıtım ve/veya kullanıcı, **~/.bashrc** dosyasını, kullanıcının sahip olduğu üç başlangıç dosyasından birine dahil eder.

Yaygın olarak, kullanıcılar yalnızca **~/.bashrc** ile meşgul olurlar, çünkü bu dosya, her yeni komut satırı kabuğu başlatıldığında veya uçbirim penceresinden yeni bir program başlatıldığında okunur. Diğer dosyalar yalnızca ilk oturum açmada kullanılır.

Güncel dağıtımlarda bazen **.bash_profile** ve/veya **.bash_login** dosyası dahi bulunmaz ve bazıları da bu dosyalarla, **.bashrc** dosyasını dahil etmekten biraz fazlasını yapar.

<figure align="center">
  <img src="/img/lfs101/ch12/img03.png" class="invert"/>
  <figcaption>Başlangıç Dosyaları Sıralaması</figcaption>
</figure>

## Takma Adlar Oluşturma

**Takma adlar** (alias) oluşturarak, özelleştirilmiş komutlar oluşturabilir veya zaten mevcut olanların davranışını değiştirebilirsiniz. Çoğu zaman, bu takma adlar **~/.bashrc** dosyanıza yerleştirilir, böylece oluşturduğunuz tüm komut kabuklarında kullanılabilirler. **`unalias`** ile bir takma adı silebilirsiniz.

Herhangi bir bağımsız değişken olmadan **`alias`** yazmak, şu anda tanımlı olan takma adları listeleyecektir.

Takma ad oluştururken, eşittir işaretinin her iki yanında da **boşluk olmaması** gerektiğini ve takma ad tanımının, boşluk içeriyorsa, tek veya çift tırnak içine alınması gerektiğini lütfen unutmayın.

<figure align="center">
  <img src="/img/lfs101/ch12/img04.png"/>
  <figcaption>Takma Adlar Oluşturma</figcaption>
</figure>

## Kullanıcılar ve Grupların Temelleri

Tüm Linux kullanıcılarına, her biri bir tam sayı olan benzersiz bir kullanıcı kimliği (User ID - **uid**) atanır. Bu kimlik, normal kullanıcılar için 1000 veya daha büyük bir sayı ile başlar.

Linux, kullanıcıları düzenlemek için **gruplar** (group) kullanır. Gruplar, belirli paylaşılan izinlere sahip hesap koleksiyonlarıdır. Grup üyeliğinin kontrolü, grupların ve üyelerinin bir listesini gösteren **/etc/group** dosyası aracılığıyla yönetilir. Varsayılan olarak, her kullanıcı bir varsayılan veya birincil gruba aittir. Bir kullanıcı oturum açtığında, grup üyeliği birincil grupları için ayarlanır ve tüm üyeler aynı erişim ve ayrıcalığa sahip olurlar. Çeşitli dosya ve dizinler üzerindeki izinler, grup düzeyinde değiştirilebilir.

Kullanıcılar ayrıca, kullanıcı kimliğiyle aynı olan varsayılan bir tane de dahil olmak üzere, bir veya daha fazla grup kimliğine (Group ID - **gid**) sahiptir. Bu numaralar **/etc/passwd** ve **/etc/group** dosyaları aracılığıyla isimlerle ilişkilendirilir. Gruplar, erişim hakları, ayrıcalıklar ve güvenlikle ilgili hususlar için ortak ilgi alanlarına sahip bir dizi kullanıcı oluşturmak için kullanılır. Dosyalara (ve aygıtlara) erişim hakları, kullanıcıya ve ait oldukları gruba göre verilir.

Örneğin, **/etc/passwd** dosyası **`george:x:1002:1002:George Metesky:/home/george:/bin/bash`** girdisini içerebilir ve **/etc/group** dosyası **`george:x:1002`** girdisini içerebilir.

<figure align="center">
  <img src="/img/lfs101/ch12/img05.png"/>
  <figcaption>Kullanıcılar ve Grupların Temelleri</figcaption>
</figure>

### Kullanıcı Ekleme ve Kaldırma

Dağıtımlar, kullanıcıları ve grupları oluşturmak, kaldırmak ve grup üyeliğini değiştirmek için basit grafik arayüzler sunarlar. Ancak, bunu komut satırından veya kabuk betikleri içinden yapabilmek de faydalı olacaktır. Yalnızca kök kullanıcı, kullanıcıları ve grupları ekleyip kaldırabilir.

Yeni bir kullanıcı eklemek **`useradd`** ile, mevcut bir kullanıcıyı silmek ise **`userdel`** ile yapılır. En basit haliyle, **bjmoose** adında yeni bir kullanıcı hesabı şu şekilde oluşturulur:

```bash
$ sudo useradd bjmoose
```

OpenSUSE için, **`useradd`** komutunun normal kullanıcıların `PATH` değişkeninde yer almadığını, dolayısıyla komutun şöyle olması gerektiğini unutmayın:

```bash
$ sudo /usr/sbin/useradd bjmoose
```

bu, varsayılan olarak, kullanıcı ana dizinini **/home/bjmoose** olarak ayarlar, dizini bazı temel dosyalarla doldurur (**/etc/skel**'den kopyalanır) ve **/etc/passwd** dosyasına şöyle bir satır ekler:

```bash
bjmoose:x:1002:1002::/home/bjmoose:/bin/bash
```

ve varsayılan kabuğu **/bin/bash** olarak ayarlar. Bir kullanıcı hesabını kaldırmak, **`userdel bjmoose`** yazmak kadar kolaydır. Ancak bu, **/home/bjmoose** dizinini olduğu gibi bırakacaktır. Bu durum, hesabın geçici olarak etkisizleştirilmesi için faydalı olabilir. Hesabı kaldırırken kullanıcı dizinini de kaldırmak için, **`userdel`** komutunun **`-r`** bayrağı (flag) ile birlikte kullanılması gerekir.

Bağımsız değişken (argument) olmadan **`id`** yazmak, geçerli kullanıcı hakkında şuna benzer bir bilgi verir:

```bash
$ id
uid=1002(bjmoose) gid=1002(bjmoose) groups=106(fuse),1002(bjmoose)
```

Bağımsız değişken olarak başka bir kullanıcının adı verilirse, **`id`** bu diğer kullanıcı hakkında bilgi rapor edecektir.

<figure align="center">
  <img src="/img/lfs101/ch12/img06.png"/>
  <figcaption>Kullanıcı Ekleme ve Kaldırma</figcaption>
</figure>

### Video: Kullanıcı Hesaplarını Kullanma

<!-- [Video](https://player.vimeo.com/video/512286029) | [Alt yazı](video-02-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/512286029?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

### Grup Ekleme ve Kaldırma

Yeni bir grup eklemek için **`groupadd`** kullanılır:

```bash
$ sudo /usr/sbin/groupadd anewgroup
```

Grup, **`groupdel`** ile kaldırılabilir:

```bash
$ sudo /usr/sbin/groupdel anewgroup
```

Zaten var olan bir gruba kullanıcı eklemek **`usermod`** ile yapılır. Örneğin, önce kullanıcının zaten ait olduğu gruplara bakarsınız:

```bash
$ groups rjsquirrel
bjmoose : rjsquirrel
```

ve sonra yeni grubu eklersiniz:

```bash
$ sudo /usr/sbin/usermod -a -G anewgroup rjsquirrel
```

```bash
$ groups rjsquirrel
rjsquirrel: rjsquirrel anewgroup
```

Bu yardımcı programlar **/etc/group** dosyasını gerektiği gibi günceller. Halihazırda var olan grupları kaldırmamak, sadece ekleme yapmak için **`-a`** (append - ekle) seçeneğini kullandığınızdan emin olun. **`groupmod`**, grup özelliklerini değiştirmek için kullanılabilir; **`-g`** seçeneğiyle Grup Kimliği (gid) veya **`-n`** seçeneğiyle grup adını değiştirmek gibi.

Gruptan bir kullanıcıyı çıkarmak biraz daha zordur. **`usermod`** programını **`-G`** seçeneği ile çalıştırmak, grupların tam bir listesini vermelidir. Yani, şunu yapmak:

```bash
$ sudo /usr/sbin/usermod -G rjsquirrel rjsquirrel
```

```bash
$ groups rjsquirrel
rjsquirrel : rjsquirrel
```

geriye yalnızca **rjsquirrel** grubunu bırakacaktır.

<figure align="center">
  <img src="/img/lfs101/ch12/img07.png"/>
  <figcaption>Grup Ekleme ve Kaldırma</figcaption>
</figure>

## Kök Kullanıcı (root) Hesabı

<img src="/img/lfs101/ch12/img08.png" align="right"/>

**Kök kullanıcı** (root) hesabı çok güçlüdür ve sisteme tam erişime sahiptir. Diğer işletim sistemleri buna genellikle **yönetici** (administrator) hesabı derler; Linux'ta buna genellikle **süper kullanıcı** (superuser) hesabı denir. Bir kullanıcıya tam kök erişimi vermeden önce son derece dikkatli olmalısınız, ki zaten buna nadiren ihtiyaç duyulur. Dış saldırılar genellikle kök hesaba erişmek için kullanılan hilelerden oluşur.

Bununla birlikte, şu gibi durumlarda, kullanıcı hesaplarına daha sınırlı ayrıcalıklar atamak için **sudo** kullanabilirsiniz:

- Sadece geçici olarak
- Yalnızca belirli komutlar için.

### su ve sudo

Yükseltilmiş (elevated) ayrıcalıkları atarken, başka bir kullanıcı olarak çalışan yeni bir kabuk başlatmak için **su** komutunu (switch or substitute user - kullanıcıyı değiştir) kullanabilirsiniz (ilgili kullanıcının parolasını yazmalısınız). Çoğu zaman, bu diğer kullanıcı **kök** (root) kullanıcıdır ve yeni kabuk, çıkış yapılana kadar yükseltilmiş ayrıcalıkların kullanımına izin verir. **su** kullanarak **kök** kullanıcı olmak, hem güvenlik hem de kararlılık açısından tehlikeli olduğu için, neredeyse her zaman kötü bir uygulamadır! Ortaya çıkan hatalar, önemli dosyaların sistemden silinmesini ve güvenlik ihlallerini içerebilir.

Diğer taraftan, ayrıcalıkları **sudo** kullanarak vermek daha az tehlikelidir ve tercih edilir. Varsayılan olarak, **sudo**, kullanıcı bazında etkinleştirilmelidir. Ancak, bazı dağıtımlar (Ubuntu gibi) bunu en az bir ana kullanıcı için varsayılan olarak etkinleştirir veya bunu bir kurulum seçeneği olarak sunar.

_Bölüm 18: Yerel Güvenlik İlkeleri_'nde, **su** ve **sudo**'yu ayrıntılı olarak açıklayıp karşılaştıracağız.

### Kök Hesaba Yükseltme

Bir dizi komut için geçici olarak **süper kullanıcı** olmak için, **`su`** yazabilir ve ardından **kök** şifresini girebilirsiniz.

Kök ayrıcalığına sahip olarak tek bir komut çalıştırmak için **`sudo <komut>`** yazın. Komut tamamlandığında, normal, ayrıcalıksız bir kullanıcı olmaya geri döneceksiniz.

**`sudo`** yapılandırma dosyaları **/etc/sudoers** dosyasında ve **/etc/sudoers.d/** dizininde saklanır. Varsayılan olarak **sudoers.d** dizini boştur.

<figure align="center">
  <img src="/img/lfs101/ch12/img09.png"/>
  <figcaption>Kök Hesaba Yükseltme</figcaption>
</figure>

### Kendin Dene: Şu Anda Oturum Açmış Kullanıcıyı ve Kullanıcı Adınızı Belirleyin

Lütfen sıradaki _Kendiniz Deneyin_ uygulamasına bir göz atın: [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingwho/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingwho/index.html?lang=tr&nofocus&theme=dark" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Lab Çalışmaları

### Lab 12.1: Takma Adlar Oluşturma

Uzun komutları ve dosya adlarını tekrar tekrar yazmak oldukça sıkıcı hale gelebilir ve yazım hataları gibi birçok küçük soruna yol açabilir.

Takma adlar (alias) oluşturmak, tüm bu yazmanın acısını hafifletmek için kısayollar tanımlamamıza olanak tanır.

Projeniz için ortak, paylaşılan bir dizinde çalışan bir proje ekibinin üyesi olduğunuzu varsayalım. Bu dizin **/home/staff/RandD/projects/projectX/src** konumunda bulunsun.

Project X üzerinde çalışırken, genellikle dosyalarınızı bu dizinde oluşturmanız ve değiştirmeniz gerekir. Tekrar tekrar şunu yazmak:

```bash
cd /home/staff/RandD/projects/projectX/src
```

sıkıcı olacaktır.

Yukarıdaki **`cd`** komutunu sizin için yapan **projx** adında bir takma ad tanımlayın ve kullanın.

Laboratuar alıştırmasının çözümünü görüntülemek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-alias)
