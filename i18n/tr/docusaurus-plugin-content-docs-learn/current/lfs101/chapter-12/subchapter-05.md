---
title: Dosya İzinleri
---

## Dosya Sahipliği

Linux ve diğer UNIX tabanlı işletim sistemlerinde, her dosya, sahibi olan bir kullanıcıyla ilişkilendirilir. Her dosya aynı zamanda, dosya üzerinde belirli haklara veya izinlere (okuma, yazma ve yürütme) sahip olan bir grupla (tüm kullanıcıların bir alt kümesi) ilişkilidir.

Aşağıdaki yardımcı programlar, kullanıcı ve grup sahipliği ve izin ayarlarını yönetmek için kullanılır:

| **Komut**   | **Kullanım**                                                                                                                             |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| **`chown`** | Bir dosyanın veya dizinin kullanıcı sahipliğini değiştirmek için kullanılır                                                              |
| **`chgrp`** | Grup sahipliğini değiştirmek için kullanılır                                                                                             |
| **`chmod`** | Dosya izinlerini değiştirmek için kullanılır; **owner**, **group** ve geriye kalanlar (diğerleri - **other**) için ayrı ayrı yapılabilir |

### Dosya İzni Modları ve chmod

Dosyalar üç tür izne sahiptir: okuma (read **r**), yazma (write **w**), çalıştırma (execute **x**). Bunlar genellikle **rwx** olarak temsil edilir. Bu izinler üç sahiplik grubunu etkiler: kullanıcı/sahibi (**u**), grup (**g**) ve diğerleri (**o**).

Sonuç olarak, aşağıdaki üç izne sahip üç gruba sahipsiniz:

| rwx: | rwx: | rwx |
| ---- | ---- | --- |
| u:   | g:   | o   |

**`chmod`** kullanmanın birkaç farklı yolu vardır. Örneğin, dosya sahibine (u) ve diğerlerine (o) çalıştırma izni (x) vermek ve gruptan (g) yazma iznini (w) kaldırmak için:

```bash
$ ls -l somefile
-rw-rw-r-- 1 student student 1601 Mar 9 15:04 somefile
$ chmod uo+x,g-w somefile
$ ls -l somefile
-rwxr--r-x 1 student student 1601 Mar 9 15:04 somefile
```

Bu tür bir sözdiziminin yazılması ve hatırlanması zor olabilir, bu yüzden genellikle tüm izinleri bir adımda ayarlamanızı sağlayan bir teknik kullanılır. Bu, basit bir algoritma ile yapılır ve her bir küme için üç izin bitinin de belirtilmesi için tek bir hane yeterlidir. Bu, şunların toplamıdır:

- **4** okuma izni (r) isteniyorsa
- **2** yazma izni (w) isteniyorsa
- **1** çalıştırma izni (x) isteniyorsa

Böylece, **7**; **oku/yaz/çalıştır**, **6**; **oku/yaz** ve **5**; **oku/çalıştır** anlamına gelir.

Bunu **`chmod`** komutuna uyguladığınızda, her özgürlük derecesine karşılık bir adet olmak üzere toplamda üç rakam vermeniz gerekir:

```bash
$ chmod 755 somefile
$ ls -l somefile
-rwxr-xr-x 1 student student 1601 Mar 9 15:04 somefile
```

<figure align="center">
  <img src="/img/lfs101/ch12/img17.png"/>
  <figcaption>Dosya İzni Modları ve chmod</figcaption>
</figure>

### chown örneği

Aşağıdaki ekran görüntüsünde gösterildiği gibi, **`chown`** kullanarak dosya sahipliğini değiştirmenin bir örneğini görelim. Önce, **`touch`** kullanarak iki boş dosya oluşturuyoruz.

**`file2`** sahibini kök olarak değiştirmek için **`sudo`** gerektiğine dikkat edin. İkinci **`chown`** komutu, hem sahibi hem de grubu aynı anda değiştiriyor!

Son olarak, sadece süper kullanıcı (superuser) dosyaları kaldırabiliyor.

<figure align="center">
  <img src="/img/lfs101/ch12/img18.png"/>
  <figcaption>chown</figcaption>
</figure>

### chgrp Örneği

Şimdi, **`chgrp`** kullanarak grup sahipliğini değiştirmenin bir örneğini görelim:

<figure align="center">
  <img src="/img/lfs101/ch12/img19.png"/>
  <figcaption>chgrp</figcaption>
</figure>

### Kendin Dene: Dosya İzinlerini Değiştirmek için chmod Kullanma

Lütfen sıradaki Kendin Dene alıştırmasına bir göz atın [veya yeni sekmede açın](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingchmod/index.html?lang=tr).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingchmod/index.html?lang=tr&nofocus" 
  width="100%"
  height="960px"
  frameborder="0">
</iframe>
