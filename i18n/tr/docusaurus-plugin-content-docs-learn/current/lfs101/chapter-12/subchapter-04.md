---
title: Önceki Komutları Geri Çağırma
---

## Önceki Komutları Geri Çağırma

**`bash`** önceden girilmiş komutların ve ifadelerin kaydını geçmiş arabelleğinde tutar. Daha önce kullanılan komutları **Yukarı** ve **Aşağı** yön tuşlarını kullanarak geri çağırabilirsiniz. Önceden çalıştırılan komutların listesini görmek için komut satırına **`history`** yazabilirsiniz.

Komutların listesi, komutların girilme sırasına uygun bir listede görüntülenir. Bu bilgi **~/.bash_history** dosyasında depolanır. Birden fazla uçbirim açıksa, her oturumda girilen komutlar oturum sonlandırılıncaya kadar kaydedilmez.

<figure align="center">
  <img src="/img/lfs101/ch12/img15.png"/>
  <figcaption>Önceki Komutları Geri Çağırma</figcaption>
</figure>

### Geçmiş (HISTORY) Ortam Değişkenlerini Kullanma

**`history`** dosyası hakkında bilgi almak için birkaç ilgili ortam değişkeni kullanılabilir.

- **`HISTFILE`**
  - Geçmiş dosyasının konumu.
- **`HISTFILESIZE`**
  - Geçmiş dosyasındaki maksimum satır sayısı (varsayılan 500).
- **`HISTSIZE`**
  - Geçmiş dosyasındaki maksimum komut sayısı.
- **`HISTCONTROL`**
  - Komutlar nasıl saklanıyor.
- **`HISTIGNORE`**
  - Kaydedilmeyecek komutlar.

Bu ortam değişkenlerinin kullanımının tam bir açıklaması için, bkz. **`man bash`**.

<figure align="center">
  <img src="/img/lfs101/ch12/img16.png"/>
  <figcaption>Geçmiş Ortam Değişkenlerini Kullanma</figcaption>
</figure>

### Önceki Komutları Bulma ve Kullanma

Çeşitli görevleri gerçekleştirmek için özel tuşlar:

| **Tuş**                                 | **Kullanım**                                      |
| --------------------------------------- | ------------------------------------------------- |
| **Yukarı/Aşağı** ok tuşları             | Daha önce yürütülen komutların listesine göz atın |
| **!!** (bang-bang diye telaffuz edilir) | Önceki komutu yürütün                             |
| **CTRL-R**                              | Daha önce kullanılan komutları arayın             |

Geçmiş listesindeki bir komutu geri çağırmak istiyor ancak ok tuşlarına tekrar tekrar basmak istemiyorsanız, akıllı arama yapmak için **CTRL-R** tuşlarına basabilirsiniz.

Yazmaya başladığınızda, arama, yazdığınız harflerle eşleşen ilk komuta geri döner. Daha daha fazla harf yazarak, eşleşmeleri daha başarılı ve daha spesifik hale getirirsiniz.

Aşağıda, komut geçmişinde arama yapmak için **CTRL-R** komutunu nasıl kullanabileceğinize bir örnek verilmiştir:

```bash
$ ^R # Bütün bunlar 1 satırda olur
(reverse-i-search)'s': sleep 1000 # 's' için arama yapıldı; "sleep" eşleşti
$ sleep 1000 # Aranan komutu yürütmek için Enter tuşuna basıldı
$
```

### Önceki Komutları Çalıştırma

Tablo, daha önce kullanılmış komutları yürütmek için kullanılan sözdizimini açıklar:

| Sözdizimi    | Görev                                      |
| ------------ | ------------------------------------------ |
| **`!`**      | Bir geçmişe başvuru eylemi başlat.         |
| **`!$`**     | Bir satırdaki son argümana başvur.         |
| **`!n`**     | n. komuta başvur.                          |
| **`!metin`** | `metin` ile başlayan en son komuta başvur. |

Tüm geçmiş başvuruları **`!`** ile başlar. Şu komutu yazarken, **`ls -l /bin /etc`**, komutun son argümanı **`/etc`** değerini ifade eder.

İşte birkaç örnek:

```bash
$ history
1 echo $SHELL
2 echo $HOME
3 echo $PS1
4 ls -a
5 ls -l /etc/ passwd
6 sleep 1000
7 history
```

```bash
$ !1 # Yukarıdaki 1 numaralı komutu yürütür
echo $SHELL
/bin/bash
$ !sl # "sl" ile başlayan komutu yürütür
sleep 1000
$
```

### Klavye kısayolları

Farklı görevleri hızlı bir şekilde gerçekleştirmek için klavye kısayollarını kullanabilirsiniz. Tablo, bu klavye kısayollarından bazılarını ve kullanımlarını listeler. Not; kısayol tuşunun büyük küçük harf durumu önemli değildir, örn. **CTRL-a** yapmak **CTRL-A** yapmak ile aynıdır.

| Klavye kısayolu | Görev                                                  |
| --------------- | ------------------------------------------------------ |
| **CTRL-L**      | Ekranı temizler                                        |
| **CTRL-D**      | Mevcut kabuktan çıkar                                  |
| **CTRL-Z**      | Mevcut süreci askıya alınmış arka plana atar           |
| **CTRL-C**      | Mevcut işlemi sonlandırır                              |
| **CTRL-H**      | Geri tuşu (Backspace) ile aynı şekilde çalışır         |
| **CTRL-A**      | Satır başlangıcına gider                               |
| **CTRL-W**      | İmleçten önceki kelimeyi siler                         |
| **CTRL-U**      | Satır başlangıcından imleç pozisyonuna kadar siler     |
| **CTRL-E**      | Satır sonuna gider                                     |
| **Tab**         | Dosya, dizin ve ikili dosya adlarını otomatik tamamlar |

## Lab Çalışmaları

### Lab 12.4: Komut Geçmişi

Komut kabuğuna yaklaşık 100 komut yazacak kadar uzun süredir Linux iş istasyonunuzla çalışmakla meşguldünüz.

Bir noktada, yeni bir komut kullanmıştınız, ancak tam ismini unuttunuz.

Ya da, belki de, bir sürü seçenek ve argüman içeren oldukça karmaşık bir komuttu ve tekrar ne yazacağınızı bulmak için, hataya meyilli bir süreçten geçmek istemiyorsunuz.

Komutun ne olduğunu nasıl belirlersiniz?

Komutu geçmişinizde bulduğunuzda, komut satırına hepsini yazmak zorunda kalmadan nasıl kolayca tekrar yürütürsünüz?

Laboratuar egzersizine bir çözümü görüntülemek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-history)
