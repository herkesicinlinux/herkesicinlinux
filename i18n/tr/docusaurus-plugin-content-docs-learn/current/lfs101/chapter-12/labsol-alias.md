---
title: 'Lab Çözümü: Takma Adlar Oluşturma'
---

Yazmanız gereken **alias** komutu şu şekilde görünecektir:

```bash
student:/tmp> alias projx='cd /home/staff/RandD/projects/projectX/src'
```

Tek tırnak yerine çift tırnak kullanabileceğinizi veya takma adınızda boşluk olmadığı için hiç tırnak işareti kullanmayabileceğinizi unutmayın. Dizine geçmek için şimdi tek yapmanız gereken:

```bash
student:/tmp> projx
```

Takma adı kalıcı hale getirmek için, bunu **$HOME/.bashrc** dosyanıza yerleştirmeniz yeterlidir.
