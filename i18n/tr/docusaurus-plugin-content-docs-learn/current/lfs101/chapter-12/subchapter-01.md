---
title: Giriş ve Öğrenme Hedefleri
---

## Video: Bölüm 12 Giriş

<!-- [Video](https://player.vimeo.com/video/511277207) | [Alt yazı](video-01-tr.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/511277207?texttrack=tr"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Öğrenme Hedefleri

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Bu bölümün sonunda şunları yapabilmelisiniz:

- Kullanıcı hesaplarını ve kullanıcı gruplarını kullanın ve yapılandırın.
- Ortam değişkenlerini kullanın ve ayarlayın.
- Önceki kabuk komut geçmişini kullanın.
- Klavye kısayollarını kullanın.
- Takma adlar (alias) kullanın ve tanımlayın.
- Dosya izinlerini ve sahipliğini kullanın ve ayarlayın.
