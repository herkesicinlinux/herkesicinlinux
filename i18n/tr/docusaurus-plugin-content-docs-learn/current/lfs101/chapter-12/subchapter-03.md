---
title: Ortam Değişkenleri
---

## Ortam Değişkenleri

**Ortam değişkenleri**, **bash** komut kabuğu veya diğer yardımcı programlar ve uygulamalar tarafından kullanılabilen, belirli değerlere sahip niceliklerdir. Bazı ortam değişkenlerine sistem tarafından önceden ayarlanmış değerler verilir (bunlar genellikle değiştirilebilir), diğerleri ise doğrudan kullanıcı tarafından, komut satırında veya başlangıç ve diğer betik dosyalarında ayarlanır.

Bir ortam değişkeni aslında, bir veya daha fazla uygulama tarafından kullanılan bilgileri içeren bir karakter dizgesidir. Halihazırda ayarlanmış olan ortam değişkenlerinin değerlerini görüntülemenin birkaç yolu vardır; **set**, **env** veya **export** gibi. Sisteminizin durumuna bağlı olarak, **set**, diğer iki yöntemden çok daha fazla satır yazdırabilir.

<figure align="center">
  <img src="/img/lfs101/ch12/img10.png"/>
  <figcaption>Ortam Değişkenleri</figcaption>
</figure>

### Ortam Değişkenlerini Ayarlama

Varsayılan olarak, bir betik içinde oluşturulan değişkenler yalnızca geçerli kabukta kullanılabilir; alt süreçler (alt kabuklar) ayarlanmış veya değiştirilmiş değerlere erişemez. Alt işlemlerin değerleri görmesine izin vermek, **`export`** komutunun kullanılmasını gerektirir.

| Görev                                    | Örnek Komut                                                                                                                             |
| ---------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| Belirli bir değişkenin değerini gösterin | **`echo $SHELL`**                                                                                                                       |
| Yeni bir değişken değerini dışa aktarın  | **`export VARIABLE=value`** (veya **`VARIABLE=value; export VARIABLE`**)                                                                |
| Kalıcı olarak bir değişken ekleyin       | 1. **~/.bashrc** dosyasını düzenleyin ve **`export VARIABLE=value`** satırını ekleyin                                                   |
|                                          | 2. **`source ~/.bashrc`** yazın veya sadece **`. ~/.bashrc`** (nokta ~/.bashrc); veya sadece **`bash`** yazarak yeni bir kabuk başlatın |

Ayrıca aşağıdaki gibi, bir komutu tek seferliğine besleyecek ortam değişkenlerini de ayarlayabilirsiniz:

```bash
$ SDIRS=s_0* KROOT=/lib/modules/$(uname -r)/build make modules_install
```

Bu örnek, **`SDIRS`** ve **`KROOT`** ortam değişkenlerinin değerleri ile **`make modules_install`** komutunu besler.

### HOME Değişkeni

**`HOME`**, kullanıcının ev (veya oturum açma) dizinini temsil eden bir ortam değişkenidir. Bağımsız değişken (argument) olmadan **`cd`** yazmak, geçerli çalışma dizinini **`HOME`** değerine değiştirir. *Yaklaşık karakteri*nin (**`~`**) çoğu zaman **`$HOME`** için bir kısaltma olarak kullanıldığını unutmayın. Yani, **`cd $HOME`** ve **`cd ~`** tamamen eşdeğer ifadelerdir.

| Komut              | Açıklama                                                                                                   |
| ------------------ | ---------------------------------------------------------------------------------------------------------- |
| **`$ echo $HOME`** | **`HOME`** ortam değişkeninin değerini gösterin...                                                         |
| **`/home/me`**     |                                                                                                            |
| **`$ cd /bin`**    | ...ardından dizini **/bin** olarak değiştirin (**`cd`** - change directory).                               |
| **`$ pwd`**        | Neredeyiz? Öğrenmek için çalışma dizinini göster (**`pwd`** - print/present working directory) kullanın... |
| **`/bin`**         | ...Beklendiği gibi **/bin**.                                                                               |
| **`$ cd`**         | Dizini bağımsız değişken olmadan değiştirin...                                                             |
| **`$ pwd`**        | _Neredeyiz?_                                                                                               |
| **`/home/me`**     | ...bizi **`HOME`** değişkeninin değerine geri götürüyor, gördüğünüz gibi.                                  |

Ekran görüntüsü bunu gösteriyor.

<figure align="center">
  <img src="/img/lfs101/ch12/img11.png"/>
  <figcaption>HOME Değişkeni</figcaption>
</figure>

### PATH Değişkeni

**PATH**, bir komut verildiğinde uygun programı veya komut dosyasını çalıştırmak için taranan bir dizin listesidir. PATH değişkenindeki her dizin iki nokta işaretiyle (**:**) ayrılır. Boş (null) bir dizin adı (veya **./**), geçerli dizini gösterir.

- **`:path1:path2`**
- **`path1::path2`**

**`:path1:path2`** örneğinde, ilk iki nokta üst üste işaretinden (**:**) önce boş bir dizin vardır. Benzer şekilde, **`path1::path2`** için **path1** ve **path2** arasında boş bir dizin vardır.

PATH değişkeninin önüne özel bir **bin** dizini eklemek için:

```bash
$ export PATH=$HOME/bin:$PATH
$ echo $PATH
/home/student/bin:/usr/local/bin:/usr/bin:/bin/usr
```

<figure align="center">
  <img src="/img/lfs101/ch12/img12.png"/>
  <figcaption>PATH Değişkeni</figcaption>
</figure>

### SHELL Değişkeni

<img src="/img/lfs101/ch12/img13.png" align="right"/>

Ortam değişkeni **`SHELL`**, kullanıcının varsayılan komut kabuğuna (bir komut penceresine yazdıklarınızı işleyen program, genellikle bash) işaret eder ve kabuğun tam yol adını içerir:

```bash
$ echo $SHELL
/bin/bash
$
```

### PS1 Değişkeni ve Komut Satırı İstemi

Komut Satırı İstemi İfadesi (Prompt Statement - **`PS`**), terminal pencerelerinizdeki komut istemi dizginizi istediğiniz bilgileri görüntülemek üzere özelleştirmek için kullanılır.

**`PS1`**, komut satırı isteminizin neye benzediğini kontrol eden birincil istem değişkenidir. Aşağıdaki özel karakterler **`PS1`**'e dahil edilebilir:

- **`\u`** - Kullanıcı adı
- **`\h`** - Ana bilgisayar adı
- **`\w`** - Mevcut çalışma dizini
- **`\!`** - Bu komutun tarih numarası
- **`\d`** - Tarih

Aşağıdaki örnekte olduğu gibi kullanıldıklarında, tek tırnak işaretleriyle çevrelenmiş olmalıdırlar:

```bash
$ echo $PS1
$
$ export PS1='\u@\h:\w$ '
student@example.com:~$ # yeni istem
```

Değişiklikleri geri almak için:

```bash
student@example.com:~$ export PS1='$ '
$
```

Daha iyi bir yöntem, eski istemi önce kaydetmek ve sonra aşağıdaki gibi geri yüklemek olacaktır:

```bash
$ OLD_PS1=$PS1
```

istemi değiştirin ve en nihayetinde şununla tekrar değiştirin:

```bash
$ PS1=$OLD_PS1
$
```

<figure align="center">
  <img src="/img/lfs101/ch12/img14.png"/>
  <figcaption>PS1 Değişkeni ve Komut Satırı İstemi</figcaption>
</figure>

## Lab Çalışmaları

### Lab 12.2: PATH Değişkenine /tmp Ekleme

**`/tmp/ls`** konumunda, yalnızca aşağıdaki satırı içeren küçük bir dosya oluşturun:

```bash
echo MERHABA, bu sahte ls programı.
```

Ardından, şunu yaparak çalıştırılabilir hale getirin:

```bash
$ chmod +x /tmp/ls
```

1. PATH değişkeninin **sonuna** **`/tmp`** ekleyin, böylece, yalnızca normal yolunuz değerlendirildikten sonra aranacaktır. **`ls`** yazın ve hangi programın çalıştırıldığını görün: **`/bin/ls`** mi yoksa **`/tmp/ls`** mi?
2. PATH değişkeninin **başına** **`/tmp`** ekleyin, böylece, her zamanki yolunuz göz önünde bulundurulmadan önce aranacaktır. Bir kez daha, **`ls`** yazın ve hangi programın çalıştırıldığını görün: **`/bin/ls`** mi yoksa **`/tmp/ls`** mi?

Yolun bu şekilde değiştirilmesindeki güvenlik hususları nelerdir?

Laboratuar egzersizine bir çözümü görüntülemek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-path)

### Lab 12.3: Komut Satırı İstemini Değiştirme

Mevcut çalışma dizininize komut isteminin bir parçası olarak sahip olmak güzeldir, böylece, her seferinde **`pwd`** yazmak zorunda olmadan, bir bakışta bazı bilgiler edinebilirsiniz.

Sık sık birden çok bilgisayarda çalışıyorsanız, özellikle ssh ile birinden diğerine bağlanıyorsanız, bilgisayar adının isteminizin bir parçası olması çok kullanışlıdır.

1. Mevcut çalışma dizininizi komut satırı isteminize yerleştirin.
2. Bilgisayar (cihaz) adınızı komut istemine yerleştirin.
3. Hem geçerli dizininizi hem de bilgisayar adınızı isteminize girin.

Bunu nasıl kalıcı hale getirebilirsiniz, öyle ki bir bash komut kabuğunu her başlattığınızda komut isteminiz bu olsun?

Laboratuar alıştırmasının çözümünü görüntülemek için aşağıdaki bağlantıya tıklayın.

[Lab Çözümü](labsol-prompt)
