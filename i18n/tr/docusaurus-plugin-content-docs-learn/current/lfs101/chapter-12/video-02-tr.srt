﻿1
00:00:00,000 --> 00:00:10,500
Yeni bir kullanıcı hesabı oluşturma, değiştirme ve kaldırma konusunda biraz deneyim kazanalım. Bunu Ubuntu 17.04'te yapacağız.

2
00:00:10,500 --> 00:00:23,000
Yeni bir hesap oluşturulduğunda tam olarak nelerin oluşturulduğu ve hangi dosyaların bulunduğu vb. konusunda dağıtımlar arasında bazı farklılıklar vardır.

3
00:00:23,000 --> 00:00:30,500
Bu, çoğunlukla, uygun şekilde adlandırılmış '/etc/default/useradd' altındaki bir dosya tarafından kontrol edilir.

4
00:00:31,000 --> 00:00:39,500
Dolayısıyla, örneğin burada, varsayılan kabuğun 'SHELL=/bin/sh' olarak ayarlandığını görebilirsiniz.

5
00:00:39,500 --> 00:00:47,500
Red Hat gibi bazı dağıtımlarda bu dosya oldukça kısadır ve çok iyi belgelenmemiştir. Ubuntu'da oldukça uzundur.

6
00:00:47,500 --> 00:00:58,000
Öyleyse hesabı oluşturalım. Yani, 'sudo useradd' ve bir ev dizini oluşturduğundan emin olmak için '-m' diyeceğim.

7
00:00:58,000 --> 00:01:06,000
Bazı dağıtımlar bunu varsayılan olarak yapmaz. Mesela Ubuntu, veya openSUSE.

8
00:01:06,500 --> 00:01:22,500
Tam adı 'Eric Dolphy' olarak vereceğim. Varsayılan kabuğu '-s' seçeneğiyle '/bin/bash' olarak belirteceğim. Ve kullanıcı adı olarak da 'edolphy'.

9
00:01:23,000 --> 00:01:33,000
Şimdi, hala bir parola belirlemem gerekiyor, bu yüzden bunu 'sudo passwd edolphy' ile yapacağım.

10
00:01:33,500 --> 00:01:52,000
Sonra bir parola yazıyorum. Ve şimdi hesap oluşturuldu. Ve '/etc/passwd' ve '/etc/group' dosyalarına giriş yapıldığından emin olarak bunu doğrulayabilirim. Ve işte oldu.

11
00:01:52,000 --> 00:02:02,500
Dikkat ederseniz, kullanıcı kimliği '1001'. Unutmayın, normal kullanıcılar 1000'den başlar ve 'student' kullanıcısının kimliği 1000, yani herşey yolunda.

12
00:02:03,000 --> 00:02:11,000
Ayrıca, tam kullanıcı adı ve '/bin/bash' gibi belirttiğim diğer bilgiler de orada.

13
00:02:11,500 --> 00:02:23,500
Ve grup kimliğinin, kullanıcı kimliği ile aynı olan '1001' olduğu söyleniyor. Tüm kullanıcılar, kullanıcı adlarıyla aynı numaraya sahip en az bir grupla oluşturulur.

14
00:02:24,000 --> 00:02:40,500
Şimdi, bu hesaba giriş yapmayı deneyelim ve bunu 'ssh edolphy@localhost' ile yapacağım. Ve parolayı vereceğim. Ve giriş yaptım.

15
00:02:40,500 --> 00:02:52,500
Öyleyse, kullanıcı dizininde neler olduğunu görelim. Çok fazla dosya olmadığını görüyoruz, ki bunlar tüm yeni kullanıcılar için oluşturulan dosyalar. Şimdi çıkış yapalım.

16
00:02:53,000 --> 00:03:09,500
Ve bu dosyalar, '/etc/skel' dizini tarafından kontrol edilir. Oraya koyduğunuz her şey ... '-a' seçeneğini yapayım. Oraya koyduğunuz her şey, her yeni kullanıcı hesabında görünecektir.

17
00:03:10,000 --> 00:03:19,500
Öyleyse, ortalığı temizleyelim ve bunu 'userdel -r edolphy' ile yapacağım.

18
00:03:20,000 --> 00:03:32,500
Ve '-r', kullanıcı ana dizinini kaldırdığından emin olmak için gereklidir. Dikkat ederseniz bir 'mail spool' dosyası oluşturmadığıma dair bir uyarı mesajı aldım. Bu zararsızdır.

19
00:03:33,000 --> 00:03:39,500
Ve şimdi '-l /home' yaparsam, hesabın gittiğini görürüz. Yani, temizliği tamamladık.

