---
title: Bölüm Sonu
---

## Öğrenme Hedefleri (Gözden Geçirin)

<img src="/img/lfs101/common/items-list.png" align="right" class="invert"/>

Şimdi şunları yapabilmelisiniz:

- Kullanıcı hesaplarını ve kullanıcı gruplarını kullanın ve yapılandırın.
- Ortam değişkenlerini kullanın ve ayarlayın.
- Önceki komut kabuğu geçmişini kullanın.
- Klavye kısayollarını kullanın.
- Takma adlar kullanın ve tanımlayın.
- Dosya izinlerini ve sahipliğini kullanın ve ayarlayın.

## Özet

<img src="/img/lfs101/common/tux-grad-cap.png" align="right"/>

Bölüm 12'yi tamamladınız. Kapsanan temel kavramları özetleyelim:

- Linux, çok kullanıcılı bir sistemdir.
- Oturum açmış kullanıcıları bulmak için **`who`** komutunu kullanabilirsiniz.
- Mevcut kullanıcı kimliğini (ID) bulmak için **`whoami`** komutunu kullanabilirsiniz.
- **Kök** (root) hesabın sisteme tam erişimi vardır. Bir kullanıcıya tam kök erişimi vermek asla mantıklı değildir.
- **`sudo`** komutunu kullanarak, geçici olarak, normal kullanıcı hesaplarına kök ayrıcalıkları atayabilirsiniz.
- Kabuk programı (bash), kullanıcı ortamını oluşturmak için birden çok başlangıç dosyası kullanır. Her dosya etkileşimli ortamı farklı bir şekilde etkiler. **`/etc/profile`** genel ayarları sağlar.
- Başlangıç dosyalarının avantajları arasında, kullanıcının komut istemini özelleştirmek, kullanıcının uçbirim türünü ayarlamak, komut satırı kısayollarını ve takma adları ayarlamak ve varsayılan metin düzenleyiciyi ayarlamak vb. yer alır.
- Ortam değişkeni, bir veya daha fazla uygulama tarafından kullanılan verileri içeren bir karakter dizesidir. Yerleşik kabuk değişkenleri, gereksinimlerinize uyacak şekilde özelleştirilebilir.
- **`history`** komutu, düzenlenebilen ve tekrar kullanılabilen önceki komutların bir listesini geri çağırır.
- Linux'ta, komut isteminde, uzun komutlar yerine çeşitli klavye kısayolları kullanılabilir.
- Komutları takma adlar oluşturarak özelleştirebilirsiniz. **`~/.bashrc`** dosyasına bir takma ad eklemek, diğer kabuklar için de kullanılabilir hale getirecektir.
- Dosya izinleri şöyle değiştirilebilir: **`chmod permissions filename`**.
- Dosya sahipliği şöyle değiştirilebilir: **`chown owner filename`**.
- Dosya grubu sahipliği şöyle değiştirilebilir: **`chgrp group filename`**.
