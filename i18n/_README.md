## Key Facts

- This directory houses internationalized contents.

- The default language of the website is Turkish, which has its contents under [`/tr`](tr).

### Things to keep in mind:

- Regarding LFS101,

  - Place contents under `/LANG_CODE/docusaurus-plugin-content-docs-learn/current/lfs101`. For example, contents in Turkish would go under [/tr/docusaurus-plugin-content-docs-learn/current/lfs101](./tr/docusaurus-plugin-content-docs-learn/current/lfs101). (Note: the directory `current` is related to the doc versioning but we currently don't version our work so, we are always working in the _current_ context.)

  - Keep the directory structure flat. Do not create subdirectories under `chapter-xx` directories. Place the main contents alongside any other translatable content at the same level.

  - List chapter contents in [sidebarsLearn.js](../sidebarsLearn.js) file, in accordance with the established structure.

- Place the images under `/static/img/` with additional subdirectories as appropriate, in `png` format, numbered starting from `00` according to the order of appearance. For example, the first image of your current context should be named `img00.png`, followed by `img01.png` and so on.

  - You can use [mogrify](https://imagemagick.org/script/mogrify.php) to batch convert images into png with a command such as:

    ```bash
    mogrify -format png *.jpg && rm *.jpg
    ```

- When using static resources in documents;

  - For Turkish locale:

    - Do not prefix resource path since Turkish is the default language so, the resources will be directly available under the root of the `static` directory. For example:

      ```html
      <img src="/img/lfs101/ch00/img01.png" />
      ```

  - For other locales:

    - Prefix any resource with the language code, which must be the same as the related directory. For example, for French documents which will reside in `/fr`, use something like:

      ```html
      <img src="/fr/img/lfs101/ch00/img01.png" />
      ```

- Append `class="invert"` to images suitable for color inversion. Do not use on screenshots or anything that may end up weird when colors are inverted.

- For your own convenience, start a development server while working on the contents.

  ```bash
  npm start # For Turkish locale
  npm start -- --locale LANG_CODE # For other locales
  ```

### Snippets

Use following snippets to embed contents into a document.

- Image:

  ```html
  <img src="img-path" align="left|center|right" />
  ```

- Figure with caption:

  ```html
  <figure align="left|center|right">
    <img src="img-path" />
    <figcaption>Caption</figcaption>
  </figure>
  ```

- Vimeo video:

  ```html
  <iframe
    title="vimeo-player"
    src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
    width="640"
    height="360"
    frameborder="0"
    allowfullscreen="true"
  >
  </iframe>
  ```

- Try-it-Yourself exercise:

  ```html
  <iframe
    title="try-it-yourself"
    src="EMBED_LINK?nofocus&lang=LANG"
    width="100%"
    height="900px"
    frameborder="0"
  >
  </iframe>
  <!-- adjust height accordingly -->
  ```
