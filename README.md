<!-- Mark completed items with :heavy_check_mark: -->

# Herkes için Linux

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-00">Bölüm 0: Hoş Geldiniz!</a> :heavy_check_mark:</summary>

- [x] [Hoş Geldiniz!](src/contents/lessons/lfs101/chapter-00/subchapter-01)

  - [x] [Video: LFS101'e Hoş Geldiniz](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-01/tr)
  - [x] [Başlamadan Önce](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-02/tr)
  - [x] [Yardım Almak](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-03/tr)
  - [x] [Eğitmeninizle Tanışın: Jerry Cooperstein](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-04/tr)
  - [x] [Yazardan Bir Not](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-05/tr)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-01">Bölüm 1: Linux Vakfı</a> :heavy_check_mark:</summary>

- [x] [Giriş ve Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-01/subchapter-01)

  - [x] [Video: Bölüm 1 Giriş](src/contents/lessons/lfs101/chapter-01/subchapter-01/lesson-01/tr)
  - [x] [Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-01/subchapter-01/lesson-02/tr)

- [x] [Linux Vakfı](src/contents/lessons/lfs101/chapter-01/subchapter-02)

  - [x] [Linux Vakfı Hakkında](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-01/tr)
  - [x] [Linux Vakfı Etkinlikleri](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-02/tr)
  - [x] [Linux Vakfı Etkinlikleri Hakkında Daha Fazla Bilgi](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-03/tr)
  - [x] [Video: Linux Vakfı Etkinlikleri](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-04/tr)

- [x] [Linux Vakfı Eğitimleri](src/contents/lessons/lfs101/chapter-01/subchapter-03)

  - [x] [Linux Vakfı Eğitimleri](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-01/tr)
  - [x] [Değer Katan Eğitimler](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-02/tr)
  - [x] [Video: Linux Vakfı Eğitim Seçenekleri](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-03/tr)

- [x] [Kurs Linux Gereksinimleri](src/contents/lessons/lfs101/chapter-01/subchapter-04)

  - [x] [Kurs Yazılım Gereksinimleri](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-01/tr)
  - [x] [Üç Büyük Linux Dağıtım Ailesine Odaklanın](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-02/tr)
  - [x] [Red Hat Ailesi](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-03/tr)
  - [x] [Red Hat Ailesi Hakkında Temel Gerçekler](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-04/tr)
  - [x] [SUSE Ailesi](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-05/tr)
  - [x] [SUSE Ailesi Hakkında Temel Gerçekler](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-06/tr)
  - [x] [Debian Ailesi](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-07/tr)
  - [x] [Debian Ailesi Hakkında Temel Gerçekler](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-08/tr)
  - [x] [Yazılım Ortamı Hakkında Daha Fazla Bilgi](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-09/tr)

- [x] [Öğrenme Hedefleri (Gözden Geçirin) ve Özet](src/contents/lessons/lfs101/chapter-01/subchapter-05)

  - [x] [Öğrenme Hedefleri (Gözden Geçirin)](src/contents/lessons/lfs101/chapter-01/subchapter-05/lesson-01/tr)
  - [x] [Özet](src/contents/lessons/lfs101/chapter-01/subchapter-05/lesson-02/tr)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-02">Bölüm 2: Linux Felsefesi ve Kavramlar</a> :heavy_check_mark:</summary>

- [x] [Giriş ve Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-02/subchapter-01)

  - [x] [Video: Bölüm 2 Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-01/tr)
  - [x] [Video: Linux'un Gücü](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-02/tr)
  - [x] [Üç Önemli Bağlam Parçası](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-03/tr)
  - [x] [Son Düşünceler](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-04/tr)
  - [x] [Video: Son Düşünceler](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-05/tr)
  - [x] [Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-06/tr)

- [x] [Linux Tarihi](src/contents/lessons/lfs101/chapter-02/subchapter-02)

  - [x] [Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-01/tr)
  - [x] [Linux Tarihi](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-02/tr)
  - [x] [Video: Linux Dünyasına Hoş Geldiniz](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-03/tr)
  - [x] [Linux Tarihi Hakkında Daha Fazla Bilgi](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-04/tr)

- [x] [Linux Felsefesi](src/contents/lessons/lfs101/chapter-02/subchapter-03)

  - [x] [Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-01/tr)
  - [x] [Linux Felsefesi](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-02/tr)
  - [x] [Video: Linux Nasıl İnşa Edildi](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-03/tr)

- [x] [Linux Topluluğu](src/contents/lessons/lfs101/chapter-02/subchapter-04)

  - [x] [Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-01/tr)
  - [x] [Video: Linux Topluluğu](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-02/tr)
  - [x] [Linux Topluluğu Hakkında Daha Fazla Bilgi](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-03/tr)

- [x] [Linux Terminolojisi](src/contents/lessons/lfs101/chapter-02/subchapter-05)

  - [x] [Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-05/lesson-01/tr)
  - [x] [Video: Linux Terminolojisi](src/contents/lessons/lfs101/chapter-02/subchapter-05/lesson-02/tr)

- [x] [Linux Dağıtımları](src/contents/lessons/lfs101/chapter-02/subchapter-06)

  - [x] [Giriş](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-01/tr)
  - [x] [Linux Dağıtımları](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-02/tr)
  - [x] [Dağıtımlarla İlişkili Servisler](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-03/tr)

- [x] [Öğrenme Hedefleri (Gözden Geçirin) ve Özet](src/contents/lessons/lfs101/chapter-02/subchapter-07)

  - [x] [Öğrenme Hedefleri (Gözden Geçirin)](src/contents/lessons/lfs101/chapter-02/subchapter-07/lesson-01/tr)
  - [x] [Özet](src/contents/lessons/lfs101/chapter-02/subchapter-07/lesson-02/tr)

</details>

<details>
  <summary>Chapter 3: Linux Basics and System Startup</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 3 Introduction
  - [ ] Learning Objectives

- [ ] The Boot Process

  - [ ] The Boot Process
  - [ ] BIOS - The First Step
  - [ ] Master Boot Record (MBR) and Boot Loader
  - [ ] Boot Loader in Action
  - [ ] Initial RAM Disk
  - [ ] Text-Mode Login

- [ ] Kernel, init and Services

  - [ ] The Linux Kernel
  - [ ] /sbin/init and Services
  - [ ] Startup Alternatives
  - [ ] systemd Features

- [ ] Linux Filesystem Basics

  - [ ] Linux Filesystems
  - [ ] Partitions and Filesystems
  - [ ] The Filesystem Hierarchy Standard
  - [ ] More About the Filesystem Hierarchy Standard
  - [ ] Video: Viewing the Filesystem Hierarchy from the Graphical Interface in Ubuntu
  - [ ] Video: Viewing the Filesystem Hierarchy from the Graphical Interface in openSUSE
  - [ ] Try-It-Yourself: Viewing the Filesystem Hierarchy

- [ ] Linux Distribution Installation

  - [ ] Choosing a Linux Distribution
  - [ ] Questions to Ask When Choosing a Distribution
  - [ ] Linux Installation: Planning
  - [ ] Linux Installation: Software Choices
  - [ ] Linux Installation: Install Source
  - [ ] Linux Installation: The Process
  - [ ] Linux Installation: The Warning
  - [ ] Video: Steps to Install Ubuntu
  - [ ] Video: Steps to Install CentOS
  - [ ] Video: Steps to Install openSUSE

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Bölüm 4: Grafik Arayüzü :heavy_check_mark:</summary>

- [x] Giriş ve Öğrenme Hedefleri

  - [x] Video: Bölüm 4 Giriş
  - [x] Öğrenme hedefleri

- [x] Grafik Masaüstü

  - [x] Grafik Masaüstü
  - [x] X Pencere Sistemi
  - [x] X Hakkında Daha Fazla Bilgi
  - [x] GUI Başlangıcı
  - [x] GNOME Masaüstü Ortamı
  - [x] Video: Sistem Başlatma ve Oturum Açma ve Kapatma
  - [x] Grafik Masaüstü Arka Planı
  - [x] Masaüstü Arka Planını Özelleştirme
  - [x] Video: Masaüstü Arka Planı Nasıl Değiştirilir
  - [x] Kendiniz Deneyin: Masaüstü Arka Planını Değiştirme
  - [x] gnome-tweak-tool
  - [x] Temayı Değiştirme
  - [x] Lab 4.1: Masaüstünü Özelleştirme

- [x] Oturum Yönetimi

  - [x] Oturum Açma ve Kapatma
  - [x] # Video: Ubuntu, openSUSE ve CentOS'ta GUI Kullanarak Oturum Açma ve Oturumu Kapatma
  - [x] Kendiniz Deneyin: Oturum Açma ve Oturumu Kapatma
  - [x] Ekranı Kilitleme
  - [x] Ekranı Daha Ayrıntılı Olarak Kilitleme ve Kilidini Açma
  - [x] Kullanıcı Değiştirme
  - [x] Video: Ubuntu'da Kullanıcı Değiştirme
  - [x] Kendiniz Deneyin: Kullanıcı Değiştirme
  - [x] Kapatma ve Yeniden Başlatma
  - [x] Ubuntu'da Kapatma ve Yeniden Başlatma
  - [x] Askıya Alma
  - [x] Sistemi Askıya Alma

- [x] Temel İşlemler

  - [x] Temel İşlemler
  - [x] Uygulamaları Bulma
  - [x] Varsayılan Uygulamalar
  - [x] Video: Varsayılan Uygulamaları Ayarlama
  - [x] Kendiniz Deneyin: Varsayılan Uygulamaları Bulma ve Ayarlama
  - [x] Dosya Yöneticisi
  - [x] Ev Dizinleri
  - [x] Dosyaları Görüntüleme
  - [x] Dosyaları Arama
  - [x] Dosya Arama Hakkında Daha Fazla Bilgi
  - [x] Bir Dosyayı Düzenleme
  - [x] Bir Dosyayı Silme
  - [x] Dosya Silme Hakkında Daha Fazla Bilgi
  - [x] Video: Varsayılan Uygulamaları Bulma ve Ayarlama, ve openSUSE'de Dosya Sistemlerini Keşfetme
  - [x] Lab 4.2: Dosya sıralama seçeneklerini görüntüleme
  - [x] Lab 4.3: Silinmiş Dosyaları Kurtarma

- [x] Öğrenme Hedefleri (Gözden Geçirin) ve Özet

  - [x] Öğrenme Hedefleri (Gözden Geçirin)
  - [x] Özet (1 of 3)
  - [x] Özet (2 of 3)
  - [x] Özet (3 of 3)

</details>

<details>
  <summary>Chapter 5: System Configuration from the Graphical Interface</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 5 Introduction
  - [ ] Learning Objectives

- [ ] System, Display, Date and Time Settings

  - [ ] System Settings
  - [ ] System Settings Menus
  - [ ] gnome-tweak-tool
  - [ ] Display Settings
  - [ ] Setting Resolution and Configuring Multiple Screens
  - [ ] Video: Applying System and Display Settings in openSUSE and CentOS
  - [ ] Video: Applying System and Display Settings in Ubuntu
  - [ ] Date and Time Settings
  - [ ] Network Time Protocol
  - [ ] Try-It-Yourself: Adjusting Display Settings
  - [ ] Lab 5.1: Getting and Setting Screen Resolution
  - [ ] Lab 5.2: Working with Time Settings

- [ ] Network Manager

  - [ ] Network Configuration
  - [ ] Wired and Wireless Connections
  - [ ] Configuring Wireless Connections
  - [ ] Video: Managing Network Settings
  - [ ] Mobile Broadband and VPN Connections
  - [ ] Lab 5.3: Managing Network Connections

- [ ] Installing and Updating Software

  - [ ] Installing and Updating Software
  - [ ] Debian Packaging
  - [ ] Red Hat Package Manager (RPM)
  - [ ] openSUSE’s YaST Software Management
  - [ ] Video: Installing and Updating Software in CentOS
  - [ ] Video: Installing and Updating Software in openSUSE
  - [ ] Video: Installing and Updating Software in Ubuntu
  - [ ] Lab 5.4: Installing and Removing Software Packages

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Chapter 6: Common Applications</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 6 Introduction
  - [ ] Learning Objectives

- [ ] Internet Applications

  - [ ] Internet Applications
  - [ ] Web Browsers
  - [ ] Email Applications
  - [ ] Other Internet Applications

- [ ] Productivity and Development Applications

  - [ ] Office Applications
  - [ ] LibreOffice Components
  - [ ] Development Applications

- [ ] Multimedia Applications

  - [ ] Sound Players
  - [ ] Movie Players
  - [ ] Movie Editors

- [ ] Graphics Editors and Utilities

  - [ ] GIMP (GNU Image Manipulation Program)
  - [ ] Graphics Utilities

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 7: Command Line Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 7 Introduction
  - [ ] Learning Objectives

- [ ] Command-Line Mode Options

  - [ ] Introduction to the Command Line
  - [ ] Using a Text Terminal on the Graphical Desktop
  - [ ] Launching Terminal Windows
  - [ ] Some Basic Utilities
  - [ ] The Command Line
  - [ ] sudo
  - [ ] Steps for Setting Up and Running sudo
  - [ ] Switching Between the GUI and the Command Line
  - [ ] Virtual Terminals
  - [ ] Turning Off the Graphical Desktop
  - [ ] Lab 7.1: Killing the Graphical User Interface

- [ ] Basic Operations

  - [ ] Basic Operations
  - [ ] Logging In and Out
  - [ ] Rebooting and Shutting Down
  - [ ] Locating Applications
  - [ ] Accessing Directories
  - [ ] Video: Accessing Directories
  - [ ] Try-It-Yourself: Accessing Directories Using Command Prompt
  - [ ] Understanding Absolute and Relative Paths
  - [ ] Exploring the Filesystem
  - [ ] Video: Exploring the Filesystem
  - [ ] Hard Links
  - [ ] Soft (Symbolic) Links
  - [ ] Navigating the Directory History
  - [ ] Video: Navigating the Directory History
  - [ ] Lab 7.2: Locating Applications

- [ ] Working with Files

  - [ ] Working with Files
  - [ ] Viewing Files
  - [ ] Video: More on Viewing Files
  - [ ] touch
  - [ ] mkdir and rmdir
  - [ ] Moving, Renaming or Removing a File
  - [ ] Renaming or Removing a Directory
  - [ ] Modifying the Command Line Prompt
  - [ ] Video: Working With Files and Directories at the Command Prompt
  - [ ] Try-It-Yourself: Working With Files and Directories Using the Command Prompt
  - [ ] Lab 7.3: Creating, Moving and Removing Files

- [ ] Searching for Files

  - [ ] Standard File Streams
  - [ ] I/O Redirection
  - [ ] Pipes
  - [ ] Searching for Files
  - [ ] locate
  - [ ] Video: Locating Files
  - [ ] Try-It-Yourself: Locating Files
  - [ ] Wildcards and Matching File Names
  - [ ] Video: Using Wildcards to Search for Files
  - [ ] Try-It-Yourself: Using Wildcards with ls
  - [ ] The find Program
  - [ ] Using find
  - [ ] Using Advanced find Options
  - [ ] Finding Files Based on Time and Size
  - [ ] Video: Finding Files In a Directory
  - [ ] Try-It-Yourself: Using find
  - [ ] Lab 7.4: Finding Directories and Creating Symbolic Links

- [ ] Installing Software

  - [ ] Package Management Systems on Linux
  - [ ] Package Managers: Two Levels
  - [ ] Working With Different Package Management Systems
  - [ ] Video: Low-Level Debian Package Management with dpkg
  - [ ] Video: Low-Level RPM Package Management with rpm
  - [ ] Video: High-Level Package Management with yum on CentOS 7
  - [ ] Video: High-Level Package Management with zypper on openSUSE
  - [ ] Video: High-Level Package Management with apt on Ubuntu
  - [ ] Lab 7.5: Installing and Removing Software Packages

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 8: Finding Linux Documentation</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 8 Introduction
  - [ ] Learning Objectives

- [ ] Documentation Sources

  - [ ] Linux Documentation Sources

- [ ] The man pages

  - [ ] The man pages
  - [ ] man
  - [ ] Manual Chapters
  - [ ] Video: Using man
  - [ ] Lab 8.1: Working with man

- [ ] GNU Info

  - [ ] The GNU Info System
  - [ ] Using info from the Command Line
  - [ ] info Page Structure
  - [ ] Video: Using info
  - [ ] Lab 8.2: Working with info

- [ ] The --help Option and help Command

  - [ ] The --help Option
  - [ ] The help Command
  - [ ] Lab 8.3: Working with Command Line help

- [ ] Other Documentation Sources

  - [ ] Other Documentation Sources
  - [ ] Graphical Help Systems
  - [ ] Package Documentation
  - [ ] Online Resources
  - [ ] Lab 8.4: Working with Graphical Help Systems

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Chapter 9: Processes</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 9 Introduction
  - [ ] Learning Objectives

- [ ] Introduction to Processes and Process Attributes

  - [ ] What Is a Process?
  - [ ] Process Types
  - [ ] Process Scheduling and States
  - [ ] Process and Thread IDs
  - [ ] Terminating a Process
  - [ ] User and Group IDs
  - [ ] More About Priorities
  - [ ] Video: Using renice to Set Priorities

- [ ] Process Metrics and Process Control

  - [ ] Load Averages
  - [ ] Interpreting Load Averages
  - [ ] Background and Foreground Processes
  - [ ] Managing Jobs
  - [ ] Lab 9.1: Getting uptime and Load Averages
  - [ ] Lab 9.2: Background and Foreground Jobs

- [ ] Listing Processes: ps and top

  - [ ] The ps Command (System V Style)
  - [ ] The ps Command (BSD Style)
  - [ ] Video: Using ps
  - [ ] The Process Tree
  - [ ] top
  - [ ] First Line of the top Output
  - [ ] Second Line of the top Output
  - [ ] Third Line of the top Output
  - [ ] Fourth and Fifth Lines of the top Output
  - [ ] Process List of the top Output
  - [ ] Interactive Keys with top
  - [ ] Video: Using top
  - [ ] Video: Using System Monitoring

- [ ] Starting Processes in the Future

  - [ ] Scheduling Future Processes Using at
  - [ ] cron
  - [ ] sleep
  - [ ] Lab 9.3: Using at for Future Batch Processing
  - [ ] Lab 9.4: Scheduling a Periodic Task with cron

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 10: File Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 10 Introduction
  - [ ] Learning Objectives

- [ ] Filesystems

  - [ ] Introduction to Filesystems
  - [ ] Filesystem Varieties
  - [ ] Linux Partitions
  - [ ] Mount Points
  - [ ] Mounting and Unmounting
  - [ ] NFS and Network Filesystems
  - [ ] NFS on the Server
  - [ ] NFS on the Client
  - [ ] Lab 10.1: Exploring Mounted Filesystems

- [ ] Filesystem Architecture

  - [ ] Overview of User Home Directories
  - [ ] The /bin and /sbin Directories
  - [ ] The /proc Filesystem
  - [ ] The /dev Directory
  - [ ] The /var Directory
  - [ ] The /etc Directory
  - [ ] The /boot Directory
  - [ ] The /lib and /lib64 Directories
  - [ ] Removable media: the /media, /run and /mnt Directories
  - [ ] Additional Directories Under /:
  - [ ] The /usr Directory Tree

- [ ] Comparing Files and File Types

  - [ ] Comparing Files with diff
  - [ ] Using diff3 and patch
  - [ ] Using the file Utility
  - [ ] Try-It-Yourself: Comparing Files
  - [ ] Try-It-Yourself: Using file
  - [ ] Lab 10.2: Using diff and patch

- [ ] Backing Up and Compressing Data

  - [ ] Backing Up Data
  - [ ] Using rsync
  - [ ] Compressing Data
  - [ ] Compressing Data Using gzip
  - [ ] Compressing Data Using bzip2
  - [ ] Compressing Data Using xz
  - [ ] Handling Files Using zip
  - [ ] Archiving and Compressing Data Using tar
  - [ ] Relative Compression Times and Sizes
  - [ ] Disk-to-Disk Copying (dd)
  - [ ] Lab 10.3: Archiving (Backing Up) the Home Directory

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-11">Bölüm 11: Metin Düzenleyicileri</a> :heavy_check_mark:</summary>

- [x] [Giriş ve Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-11/subchapter-01)

  - [x] [Video: Bölüm 11 Giriş](src/contents/lessons/lfs101/chapter-11/subchapter-01/lesson-01/tr)
  - [x] [Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-11/subchapter-01/lesson-02/tr)

- [x] [Temel Düzenleyiciler: Nano ve Gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02)

  - [x] [Linux Metin Düzenleyicilerine Genel Bakış](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-01/tr)
  - [x] [Düzenleyici Kullanmadan Dosya Oluşturma](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-02/tr)
  - [x] [Nano ve Gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-03/tr)
  - [x] [Nano](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-04/tr)
  - [x] [Gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-05/tr)
  - [x] [Lab 11.1: Nano Kullanımı](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-06/tr)
  - [x] [Lab 11.2: Gedit Kullanımı](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-07/tr)

- [x] [Gelişmiş Düzenleyiciler: Vi ve Emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03)

  - [x] [Vi ve Emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-01/tr)
  - [x] [Vi'ye Giriş](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-02/tr)
  - [x] [vimtutor (Vim Öğreticisi)](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-03/tr)
  - [x] [Vi Modları](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-04/tr)
  - [x] [Vi'de Dosyalarla Çalışma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-05/tr)
  - [x] [Vi'de İmleç Konumunu Değiştirme](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-06/tr)
  - [x] [Video: Vi'de Modları ve İmleç Hareketlerini Kullanma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-07/tr)
  - [x] [Vi'de Metin Arama](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-08/tr)
  - [x] [Vi'de Metinlerle Çalışma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-09/tr)
  - [x] [Vi'de Harici Komutları Kullanma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-10/tr)
  - [x] [Video: Vi Düzenleyicide Harici Komutları Kullanma, Kaydetme ve Kapatma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-11/tr)
  - [x] [Emacs'e Giriş](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-12/tr)
  - [x] [Emacs ile Çalışmak](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-13/tr)
  - [x] [Emacs'de İmleç Konumunu Değiştirme](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-14/tr)
  - [x] [Emacs'de Metin Arama](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-15/tr)
  - [x] [Emacs'de Metinlerle Çalışma](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-16/tr)
  - [x] [Video: Emacs İşlemleri](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-17/tr)
  - [x] [Lab 11.3: Vi ve Emacs Öğreticileri](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-18/tr)
  - [x] [Lab 11.4: Vi ile Çalışmak](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-19/tr)

- [x] [Öğrenme Hedefleri (Gözden Geçirin) ve Özet](src/contents/lessons/lfs101/chapter-11/subchapter-04)

  - [x] [Öğrenme Hedefleri (Gözden Geçirin)](src/contents/lessons/lfs101/chapter-11/subchapter-04/lesson-01/tr)
  - [x] [Özet](src/contents/lessons/lfs101/chapter-11/subchapter-04/lesson-02/tr)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-12">Bölüm 12: Kullanıcı Ortamı</a> :heavy_check_mark:</summary>

- [x] [Giriş ve Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-12/subchapter-01)

  - [x] [Video: Bölüm 12 Giriş](src/contents/lessons/lfs101/chapter-12/subchapter-01/lesson-01/tr)
  - [x] [Öğrenme Hedefleri](src/contents/lessons/lfs101/chapter-12/subchapter-01/lesson-02/tr)

- [x] [Hesaplar, Kullanıcılar ve Gruplar](src/contents/lessons/lfs101/chapter-12/subchapter-02)

  - [x] [Mevcut Kullanıcıyı Tanımlama](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-01/tr)
  - [x] [Kullanıcı Başlangıç Dosyaları](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-02/tr)
  - [x] [Başlangıç Dosyaları Sıralaması](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-03/tr)
  - [x] [Takma Adlar Oluşturma](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-04/tr)
  - [x] [Kullanıcılar ve Grupların Temelleri](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-05/tr)
  - [x] [Kullanıcı Ekleme ve Kaldırma](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-06/tr)
  - [x] [Video: Kullanıcı Hesaplarını Kullanma](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-07/tr)
  - [x] [Grup Ekleme ve Kaldırma](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-08/tr)
  - [x] [Kök Kullanıcı Hesabı](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-09/tr)
  - [x] [su ve sudo](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-10/tr)
  - [x] [Kök Hesaba Yükseltme](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-11/tr)
  - [x] [Kendin Dene: Şu Anda Oturum Açmış Kullanıcıyı ve Kullanıcı Adınızı Belirleyin](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-12/tr)
  - [x] [Lab 12.1: Takma Adlar Oluşturma](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-13/tr)

- [x] [Ortam Değişkenleri](src/contents/lessons/lfs101/chapter-12/subchapter-03)

  - [x] [Ortam Değişkenleri](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-01/tr)
  - [x] [Ortam Değişkenlerini Ayarlama](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-02/tr)
  - [x] [HOME Değişkeni](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-03/tr)
  - [x] [PATH Değişkeni](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-04/tr)
  - [x] [SHELL Değişkeni](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-05/tr)
  - [x] [PS1 Değişkeni ve Komut Satırı İstemi](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-06/tr)
  - [x] [Lab 12.2: PATH Değişkenine /tmp Ekleme](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-07/tr)
  - [x] [Lab 12.3: Komut Satırı İstemini Değiştirme](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-08/tr)

- [x] [Önceki Komutları Geri Çağırma](src/contents/lessons/lfs101/chapter-12/subchapter-04)

  - [x] [Önceki Komutları Geri Çağırma](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-01/tr)
  - [x] [Geçmiş (HISTORY) Ortam Değişkenlerini Kullanma](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-02/tr)
  - [x] [Önceki Komutları Bulma ve Kullanma](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-03/tr)
  - [x] [Önceki Komutları Çalıştırma](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-04/tr)
  - [x] [Klavye kısayolları](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-05/tr)
  - [x] [Lab 12.4: Komut Geçmişi](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-06/tr)

- [x] [Dosya İzinleri](src/contents/lessons/lfs101/chapter-12/subchapter-05)

  - [x] [Dosya Sahipliği](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-01/tr)
  - [x] [Dosya İzni Modları ve chmod](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-02/tr)
  - [x] [chown Örneği](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-03/tr)
  - [x] [chgrp Örneği](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-04/tr)
  - [x] [Kendin Dene: Dosya İzinlerini Değiştirmek için chmod Kullanma](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-05/tr)

- [x] [Öğrenme Hedefleri (Gözden Geçirin) ve Özet](src/contents/lessons/lfs101/chapter-12/subchapter-06)

  - [x] [Öğrenme Hedefleri (Gözden Geçirin)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-01/tr)
  - [x] [Özet (1/2)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-02/tr)
  - [x] [Özet (2/2)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-03/tr)

</details>

<details>
  <summary>Chapter 13: Manipulating Text</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 13 Introduction
  - [ ] Learning Objectives

- [ ] cat and echo

  - [ ] Command Line Tools for Manipulating Text Files
  - [ ] cat
  - [ ] Using cat Interactively
  - [ ] Video: Using cat
  - [ ] Try-It-Yourself: Using cat
  - [ ] echo
  - [ ] Try-It-Yourself : Using echo

- [ ] Working with Large and Compressed Files

  - [ ] Working with Large Files
  - [ ] head
  - [ ] tail
  - [ ] Try-It-Yourself: Using head and tail
  - [ ] Viewing Compressed Files

- [ ] sed and awk

  - [ ] Introduction to sed and awk
  - [ ] sed
  - [ ] sed Command Syntax
  - [ ] sed Basic Operations
  - [ ] Video: Using sed
  - [ ] awk
  - [ ] awk Basic Operations
  - [ ] Lab 13.1: Using sed

- [ ] File Manipulation Utilities

  - [ ] File Manipulation Utilities
  - [ ] sort
  - [ ] uniq
  - [ ] Try-It-Yourself: Using sort and uniq
  - [ ] paste
  - [ ] Using paste
  - [ ] join
  - [ ] Using join
  - [ ] split
  - [ ] Using split
  - [ ] Regular Expressions and Search Patterns
  - [ ] Using Regular Expressions and Search Patterns
  - [ ] Lab 13.2: Parsing Files with awk (and sort and uniq)

- [ ] grep and strings

  - [ ] grep
  - [ ] strings
  - [ ] Lab 13.3: Using grep

- [ ] Miscellaneous Text Utilities

  - [ ] tr
  - [ ] Try-It-Yourself : Using tr
  - [ ] tee
  - [ ] wc
  - [ ] Try-It-Yourself: Using wc
  - [ ] cut
  - [ ] Lab 13.4: Using tee
  - [ ] Lab 13.5: Using wc

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 3)
  - [ ] Summary (2 of 3)
  - [ ] Summary (3 of 3)

</details>

<details>
  <summary>Chapter 14: Network Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 14 Introduction
  - [ ] Learning Objectives

- [ ] Network Addresses and DNS

  - [ ] Introduction to Networking
  - [ ] IP Addresses
  - [ ] IPv4 and IPv6
  - [ ] Decoding IPv4 Addresses
  - [ ] Class A Network Addresses
  - [ ] Class B Network Addresses
  - [ ] Class C Network Addresses
  - [ ] IP Address Allocation
  - [ ] Name Resolution
  - [ ] Video: Using Domain Name System (DNS) and Name Resolution Tools
  - [ ] Try-It-Yourself: Using Domain Name System (DNS) and Name Resolution Tools

- [ ] Networking Configuration and Tools

  - [ ] Network Configuration Files
  - [ ] Network Interfaces
  - [ ] The ip Utility
  - [ ] ping
  - [ ] route
  - [ ] traceroute
  - [ ] Try-It-Yourself: Using ping, route, and traceroute
  - [ ] More Networking Tools
  - [ ] Video: Using More Networking Tools
  - [ ] Try-It-Yourself: Using Network Tools

- [ ] Browsers, wget and curl

  - [ ] Graphical and Non-Graphical Browsers
  - [ ] wget
  - [ ] curl
  - [ ] Try-It-Yourself: Using wget and curl

- [ ] Transferring Files

  - [ ] FTP (File Transfer Protocol)
  - [ ] FTP Clients
  - [ ] Video: Connecting to an FTP server
  - [ ] Try-It-Yourself: Connecting to an FTP server
  - [ ] SSH: Executing Commands Remotely
  - [ ] Copying Files Securely with scp
  - [ ] Video: Using SSH between Two Virtual Machines
  - [ ] Lab 14.1: Network Troubleshooting
  - [ ] Lab 14.2: Non-Graphical Browsers

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 15: The Bash Shell and Basic Scripting</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 15 Introduction
  - [ ] Learning Objectives

- [ ] Features and Capabilities

  - [ ] Shell Scripting
  - [ ] Command Shell Choices
  - [ ] Shell Scripts
  - [ ] A Simple bash Script
  - [ ] Interactive Example Using bash Scripts
  - [ ] Return Values
  - [ ] Viewing Return Values
  - [ ] Lab 15.1: Exit Status Codes

- [ ] Syntax

  - [ ] Basic Syntax and Special Characters
  - [ ] Splitting Long Commands Over Multiple Lines
  - [ ] Putting Multiple Commands on a Single Line
  - [ ] Output Redirection
  - [ ] Input Redirection
  - [ ] Built-In Shell Commands
  - [ ] Commands Built in to bash
  - [ ] Script Parameters
  - [ ] Using Script Parameters
  - [ ] Command Substitution
  - [ ] Environment Variables
  - [ ] Exporting Environment Variables
  - [ ] Functions
  - [ ] Lab 15.2: Working with Files and Directories in a Script
  - [ ] Lab 15.3: Passing Arguments
  - [ ] Lab 15.4: Environment Variables
  - [ ] Lab 15.5: Working with Functions

- [ ] Constructs

  - [ ] The if Statement
  - [ ] Using the if Statement
  - [ ] The elif Statement
  - [ ] Testing for Files
  - [ ] Boolean Expressions
  - [ ] Tests in Boolean Expressions
  - [ ] Example of Testing of Strings
  - [ ] Numerical Tests
  - [ ] Example of Testing for Numbers
  - [ ] Arithmetic Expressions
  - [ ] Lab 15.6: Arithmetic and Functions

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 16: More on Bash Shell Scripting</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 16 Introduction
  - [ ] Learning Objectives

- [ ] String Manipulation

  - [ ] String Manipulation
  - [ ] Example of String Manipulation
  - [ ] Parts of a String
  - [ ] Lab 16.1: String Tests and Operations

- [ ] The case Statement

  - [ ] The case Statement
  - [ ] Structure of the case Statement
  - [ ] Example of Use of the case Construct
  - [ ] Lab 16.2: Using the case Statement

- [ ] Looping Constructs

  - [ ] Looping Constructs
  - [ ] The for Loop
  - [ ] The while Loop
  - [ ] The until Loop

- [ ] Script Debugging

  - [ ] Debugging bash Scripts
  - [ ] Script Debug Mode
  - [ ] Redirecting Errors to File and Screen

- [ ] Some Additional Useful Techniques

  - [ ] Creating Temporary Files and Directories
  - [ ] Example of Creating a Temporary File and Directory
  - [ ] Discarding Output with /dev/null
  - [ ] Random Numbers and Data
  - [ ] How the Kernel Generates Random Numbers
  - [ ] Lab 16.3: Using Random Numbers

- [ ] Challenge Assignment from the Mooqita Project (Optional)

  - [ ] Challenge Assignment from the Mooqita Project (Optional)

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 17: Printing</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 17 Introduction
  - [ ] Learning Objectives

- [ ] Configuration

  - [ ] Printing on Linux
  - [ ] CUPS Overview
  - [ ] How Does CUPS Work?
  - [ ] Scheduler
  - [ ] Configuration Files
  - [ ] Job Files
  - [ ] Log Files
  - [ ] Filters, Printer Drivers, and Backends
  - [ ] Managing CUPS
  - [ ] Video: Managing the CUPS Daemon
  - [ ] Configuring a Printer from the GUI
  - [ ] Video: Adding a Network Printer
  - [ ] Adding Printers from the CUPS Web Interface

- [ ] Printing Operations

  - [ ] Printing from the Graphical Interface
  - [ ] Printing from the Command-Line Interface
  - [ ] Using lp
  - [ ] Video: Printing Using lp
  - [ ] Try-It-Yourself: Printing with the lp Command
  - [ ] Managing Print Jobs
  - [ ] Try-It-Yourself: Managing Print Jobs

- [ ] Manipulating Postscript and PDF Files

  - [ ] Working with PostScript and PDF
  - [ ] Working with enscript
  - [ ] Converting between PostScript and PDF
  - [ ] Viewing PDF Content
  - [ ] Manipulating PDFs
  - [ ] Using qpdf
  - [ ] Video: Using qpdf
  - [ ] Using pdftk
  - [ ] Encrypting PDF Files with pdftk
  - [ ] Using Ghostscript
  - [ ] Video: Using pdftk
  - [ ] Using Additional Tools
  - [ ] Lab 17.1: Creating PostScript and PDF from Text Files
  - [ ] Lab 17.2: Combining PDFs

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 18: Local Security Principles</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 18 Introduction
  - [ ] Learning Objectives

- [ ] Understanding Linux Security

  - [ ] User Accounts
  - [ ] Types of Accounts
  - [ ] Understanding the root Account

- [ ] When Are root Privileges Required?

  - [ ] Operations Requiring root Privileges
  - [ ] Operations Not Requiring root Privileges

- [ ] sudo, Process Isolation, Limiting Hardware Access and Keeping Systems Current

  - [ ] Comparing sudo and su
  - [ ] sudo Features
  - [ ] The sudoers File
  - [ ] Command Logging
  - [ ] Process Isolation
  - [ ] Hardware Device Access
  - [ ] Keeping Current
  - [ ] Lab 18.1: sudo

- [ ] Working with passwords

  - [ ] How Passwords Are Stored
  - [ ] Password Algorithm
  - [ ] Good Password Practices
  - [ ] Lab 18.2: Password Aging

- [ ] Securing the Boot Process and Hardware Resources

  - [ ] Requiring Boot Loader Passwords
  - [ ] Hardware Vulnerability
  - [ ] Software Vulnerability

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>
