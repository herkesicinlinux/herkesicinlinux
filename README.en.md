<!-- Mark completed items with :heavy_check_mark: -->

# Linux for Everyone

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-00">Chapter 0: Welcome!</a> :heavy_check_mark:</summary>

- [x] [Welcome!](src/contents/lessons/lfs101/chapter-00/subchapter-01)

  - [x] [Video: Welcome to LFS101x](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-01/en)
  - [x] [Before You Begin](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-02/en)
  - [x] [Getting Help](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-03/en)
  - [x] [Meet Your Instructor: Jerry Cooperstein](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-04/en)
  - [x] [A Note from the Author](src/contents/lessons/lfs101/chapter-00/subchapter-01/lesson-05/en)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-01">Chapter 1: The Linux Foundation</a> :heavy_check_mark:</summary>

- [x] [Introduction and Learning Objectives](src/contents/lessons/lfs101/chapter-01/subchapter-01)

  - [x] [Video: Chapter 1 Introduction](src/contents/lessons/lfs101/chapter-01/subchapter-01/lesson-01/en)
  - [x] [Learning Objectives](src/contents/lessons/lfs101/chapter-01/subchapter-01/lesson-02/en)

- [x] [The Linux Foundation](src/contents/lessons/lfs101/chapter-01/subchapter-02)

  - [x] [About The Linux Foundation](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-01/en)
  - [x] [The Linux Foundation Events](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-02/en)
  - [x] [More About The Linux Foundation Events](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-03/en)
  - [x] [Video: The Linux Foundation Events](src/contents/lessons/lfs101/chapter-01/subchapter-02/lesson-04/en)

- [x] [The Linux Foundation Training](src/contents/lessons/lfs101/chapter-01/subchapter-03)

  - [x] [The Linux Foundation Training](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-01/en)
  - [x] [Training That Builds Skills](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-02/en)
  - [x] [Video: The Linux Foundation Training Options](src/contents/lessons/lfs101/chapter-01/subchapter-03/lesson-03/en)

- [x] [Course Linux Requirements](src/contents/lessons/lfs101/chapter-01/subchapter-04)

  - [x] [Course Software Requirements](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-01/en)
  - [x] [Focus on Three Major Linux Distribution Families](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-02/en)
  - [x] [The Red Hat Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-03/en)
  - [x] [Key Facts About the Red Hat Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-04/en)
  - [x] [The SUSE Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-05/en)
  - [x] [Key Facts About the SUSE Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-06/en)
  - [x] [The Debian Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-07/en)
  - [x] [Key Facts About the Debian Family](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-08/en)
  - [x] [More About the Software Environment](src/contents/lessons/lfs101/chapter-01/subchapter-04/lesson-09/en)

- [x] [Learning Objectives (Review) and Summary](src/contents/lessons/lfs101/chapter-01/subchapter-05)

  - [x] [Learning Objectives (Review)](src/contents/lessons/lfs101/chapter-01/subchapter-05/lesson-01/en)
  - [x] [Summary](src/contents/lessons/lfs101/chapter-01/subchapter-05/lesson-02/en)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-02">Chapter 2: Linux Philosophy and Concepts</a> :heavy_check_mark:</summary>

- [x] [Introduction and Learning Objectives](src/contents/lessons/lfs101/chapter-02/subchapter-01)

  - [x] [Video: Chapter 2 Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-01/en)
  - [x] [Video: The Power of Linux](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-02/en)
  - [x] [Three Important Pieces of Context](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-03/en)
  - [x] [Final Thoughts](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-04/en)
  - [x] [Video: Final Thoughts](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-05/en)
  - [x] [Learning Objectives](src/contents/lessons/lfs101/chapter-02/subchapter-01/lesson-06/en)

- [x] [Linux History](src/contents/lessons/lfs101/chapter-02/subchapter-02)

  - [x] [Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-01/en)
  - [x] [Linux History](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-02/en)
  - [x] [Video: Welcome to the World of Linux](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-03/en)
  - [x] [More About Linux History](src/contents/lessons/lfs101/chapter-02/subchapter-02/lesson-04/en)

- [x] [Linux Philosophy](src/contents/lessons/lfs101/chapter-02/subchapter-03)

  - [x] [Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-01/en)
  - [x] [Linux Philosophy](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-02/en)
  - [x] [Video: How Linux Is Built](src/contents/lessons/lfs101/chapter-02/subchapter-03/lesson-03/en)

- [x] [Linux Community](src/contents/lessons/lfs101/chapter-02/subchapter-04)

  - [x] [Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-01/en)
  - [x] [Video: Linux Community](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-02/en)
  - [x] [More About Linux Community](src/contents/lessons/lfs101/chapter-02/subchapter-04/lesson-03/en)

- [x] [Linux Terminology](src/contents/lessons/lfs101/chapter-02/subchapter-05)

  - [x] [Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-05/lesson-01/en)
  - [x] [Video: Linux Terminology](src/contents/lessons/lfs101/chapter-02/subchapter-05/lesson-02/en)

- [x] [Linux Distributions](src/contents/lessons/lfs101/chapter-02/subchapter-06)

  - [x] [Introduction](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-01/en)
  - [x] [Linux Distributions](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-02/en)
  - [x] [Services Associated with Distributions](src/contents/lessons/lfs101/chapter-02/subchapter-06/lesson-03/en)

- [x] [Learning Objectives (Review) and Summary](src/contents/lessons/lfs101/chapter-02/subchapter-07)

  - [x] [Learning Objectives (Review)](src/contents/lessons/lfs101/chapter-02/subchapter-07/lesson-01/en)
  - [x] [Summary](src/contents/lessons/lfs101/chapter-02/subchapter-07/lesson-02/en)

</details>

<details>
  <summary>Chapter 3: Linux Basics and System Startup</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 3 Introduction
  - [ ] Learning Objectives

- [ ] The Boot Process

  - [ ] The Boot Process
  - [ ] BIOS - The First Step
  - [ ] Master Boot Record (MBR) and Boot Loader
  - [ ] Boot Loader in Action
  - [ ] Initial RAM Disk
  - [ ] Text-Mode Login

- [ ] Kernel, init and Services

  - [ ] The Linux Kernel
  - [ ] /sbin/init and Services
  - [ ] Startup Alternatives
  - [ ] systemd Features

- [ ] Linux Filesystem Basics

  - [ ] Linux Filesystems
  - [ ] Partitions and Filesystems
  - [ ] The Filesystem Hierarchy Standard
  - [ ] More About the Filesystem Hierarchy Standard
  - [ ] Video: Viewing the Filesystem Hierarchy from the Graphical Interface in Ubuntu
  - [ ] Video: Viewing the Filesystem Hierarchy from the Graphical Interface in openSUSE
  - [ ] Try-It-Yourself: Viewing the Filesystem Hierarchy

- [ ] Linux Distribution Installation

  - [ ] Choosing a Linux Distribution
  - [ ] Questions to Ask When Choosing a Distribution
  - [ ] Linux Installation: Planning
  - [ ] Linux Installation: Software Choices
  - [ ] Linux Installation: Install Source
  - [ ] Linux Installation: The Process
  - [ ] Linux Installation: The Warning
  - [ ] Video: Steps to Install Ubuntu
  - [ ] Video: Steps to Install CentOS
  - [ ] Video: Steps to Install openSUSE

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Chapter 4: Graphical Interface</summary>

- [x] Introduction and Learning Objectives

  - [x] Video: Chapter 4 Introduction
  - [x] Learning Objectives

- [x] Graphical Desktop

  - [x] Graphical Desktop
  - [x] X Window System
  - [x] More About X
  - [x] GUI Startup
  - [x] GNOME Desktop Environment
  - [x] Video: System Startup and Logging In and Out
  - [x] Graphical Desktop Background
  - [x] Customizing the Desktop Background
  - [x] Video: How to Change the Desktop Background
  - [x] Try-It-Yourself: Changing the Desktop Background
  - [x] gnome-tweak-tool
  - [x] Changing the Theme
  - [x] Lab 4.1: Customizing the Desktop

- [x] Session Management

  - [x] Logging In and Out
  - [x] Video: Logging In and Logging Out Using the GUI in Ubuntu, openSUSE and CentOS
  - [x] Try-It-Yourself: Logging In and Logging Out
  - [x] Locking the Screen
  - [x] Locking and Unlocking the Screen in More Detail
  - [x] Switching Users
  - [x] Video: Switching Users in Ubuntu
  - [x] Try-It-Yourself: Switching Users
  - [x] Shutting Down and Restarting
  - [x] Shutting Down and Restarting in Ubuntu
  - [x] Suspending
  - [x] Suspending the System

- [x] Basic Operations

  - [x] Basic Operations
  - [x] Locating Applications
  - [x] Default Applications
  - [x] Video: Setting Default Applications
  - [x] Try-It-Yourself: Locating and Setting Default Applications
  - [x] File Manager
  - [x] Home Directories
  - [x] Viewing Files
  - [x] Searching for Files
  - [x] More About Searching for Files
  - [x] Editing a File
  - [x] Removing a File
  - [x] More About Removing a File
  - [x] Video: Locating and Setting Default Applications, and Exploring Filesystems in openSUSE
  - [x] Lab 4.2: Viewing File sort options
  - [x] Lab 4.3: Recovering Deleted Files

- [x] Learning Objectives (Review) and Summary

  - [x] Learning Objectives (Review)
  - [x] Summary (1 of 3)
  - [x] Summary (2 of 3)
  - [x] Summary (3 of 3)

</details>

<details>
  <summary>Chapter 5: System Configuration from the Graphical Interface</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 5 Introduction
  - [ ] Learning Objectives

- [ ] System, Display, Date and Time Settings

  - [ ] System Settings
  - [ ] System Settings Menus
  - [ ] gnome-tweak-tool
  - [ ] Display Settings
  - [ ] Setting Resolution and Configuring Multiple Screens
  - [ ] Video: Applying System and Display Settings in openSUSE and CentOS
  - [ ] Video: Applying System and Display Settings in Ubuntu
  - [ ] Date and Time Settings
  - [ ] Network Time Protocol
  - [ ] Try-It-Yourself: Adjusting Display Settings
  - [ ] Lab 5.1: Getting and Setting Screen Resolution
  - [ ] Lab 5.2: Working with Time Settings

- [ ] Network Manager

  - [ ] Network Configuration
  - [ ] Wired and Wireless Connections
  - [ ] Configuring Wireless Connections
  - [ ] Video: Managing Network Settings
  - [ ] Mobile Broadband and VPN Connections
  - [ ] Lab 5.3: Managing Network Connections

- [ ] Installing and Updating Software

  - [ ] Installing and Updating Software
  - [ ] Debian Packaging
  - [ ] Red Hat Package Manager (RPM)
  - [ ] openSUSE’s YaST Software Management
  - [ ] Video: Installing and Updating Software in CentOS
  - [ ] Video: Installing and Updating Software in openSUSE
  - [ ] Video: Installing and Updating Software in Ubuntu
  - [ ] Lab 5.4: Installing and Removing Software Packages

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Chapter 6: Common Applications</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 6 Introduction
  - [ ] Learning Objectives

- [ ] Internet Applications

  - [ ] Internet Applications
  - [ ] Web Browsers
  - [ ] Email Applications
  - [ ] Other Internet Applications

- [ ] Productivity and Development Applications

  - [ ] Office Applications
  - [ ] LibreOffice Components
  - [ ] Development Applications

- [ ] Multimedia Applications

  - [ ] Sound Players
  - [ ] Movie Players
  - [ ] Movie Editors

- [ ] Graphics Editors and Utilities

  - [ ] GIMP (GNU Image Manipulation Program)
  - [ ] Graphics Utilities

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 7: Command Line Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 7 Introduction
  - [ ] Learning Objectives

- [ ] Command-Line Mode Options

  - [ ] Introduction to the Command Line
  - [ ] Using a Text Terminal on the Graphical Desktop
  - [ ] Launching Terminal Windows
  - [ ] Some Basic Utilities
  - [ ] The Command Line
  - [ ] sudo
  - [ ] Steps for Setting Up and Running sudo
  - [ ] Switching Between the GUI and the Command Line
  - [ ] Virtual Terminals
  - [ ] Turning Off the Graphical Desktop
  - [ ] Lab 7.1: Killing the Graphical User Interface

- [ ] Basic Operations

  - [ ] Basic Operations
  - [ ] Logging In and Out
  - [ ] Rebooting and Shutting Down
  - [ ] Locating Applications
  - [ ] Accessing Directories
  - [ ] Video: Accessing Directories
  - [ ] Try-It-Yourself: Accessing Directories Using Command Prompt
  - [ ] Understanding Absolute and Relative Paths
  - [ ] Exploring the Filesystem
  - [ ] Video: Exploring the Filesystem
  - [ ] Hard Links
  - [ ] Soft (Symbolic) Links
  - [ ] Navigating the Directory History
  - [ ] Video: Navigating the Directory History
  - [ ] Lab 7.2: Locating Applications

- [ ] Working with Files

  - [ ] Working with Files
  - [ ] Viewing Files
  - [ ] Video: More on Viewing Files
  - [ ] touch
  - [ ] mkdir and rmdir
  - [ ] Moving, Renaming or Removing a File
  - [ ] Renaming or Removing a Directory
  - [ ] Modifying the Command Line Prompt
  - [ ] Video: Working With Files and Directories at the Command Prompt
  - [ ] Try-It-Yourself: Working With Files and Directories Using the Command Prompt
  - [ ] Lab 7.3: Creating, Moving and Removing Files

- [ ] Searching for Files

  - [ ] Standard File Streams
  - [ ] I/O Redirection
  - [ ] Pipes
  - [ ] Searching for Files
  - [ ] locate
  - [ ] Video: Locating Files
  - [ ] Try-It-Yourself: Locating Files
  - [ ] Wildcards and Matching File Names
  - [ ] Video: Using Wildcards to Search for Files
  - [ ] Try-It-Yourself: Using Wildcards with ls
  - [ ] The find Program
  - [ ] Using find
  - [ ] Using Advanced find Options
  - [ ] Finding Files Based on Time and Size
  - [ ] Video: Finding Files In a Directory
  - [ ] Try-It-Yourself: Using find
  - [ ] Lab 7.4: Finding Directories and Creating Symbolic Links

- [ ] Installing Software

  - [ ] Package Management Systems on Linux
  - [ ] Package Managers: Two Levels
  - [ ] Working With Different Package Management Systems
  - [ ] Video: Low-Level Debian Package Management with dpkg
  - [ ] Video: Low-Level RPM Package Management with rpm
  - [ ] Video: High-Level Package Management with yum on CentOS 7
  - [ ] Video: High-Level Package Management with zypper on openSUSE
  - [ ] Video: High-Level Package Management with apt on Ubuntu
  - [ ] Lab 7.5: Installing and Removing Software Packages

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 8: Finding Linux Documentation</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 8 Introduction
  - [ ] Learning Objectives

- [ ] Documentation Sources

  - [ ] Linux Documentation Sources

- [ ] The man pages

  - [ ] The man pages
  - [ ] man
  - [ ] Manual Chapters
  - [ ] Video: Using man
  - [ ] Lab 8.1: Working with man

- [ ] GNU Info

  - [ ] The GNU Info System
  - [ ] Using info from the Command Line
  - [ ] info Page Structure
  - [ ] Video: Using info
  - [ ] Lab 8.2: Working with info

- [ ] The --help Option and help Command

  - [ ] The --help Option
  - [ ] The help Command
  - [ ] Lab 8.3: Working with Command Line help

- [ ] Other Documentation Sources

  - [ ] Other Documentation Sources
  - [ ] Graphical Help Systems
  - [ ] Package Documentation
  - [ ] Online Resources
  - [ ] Lab 8.4: Working with Graphical Help Systems

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary

</details>

<details>
  <summary>Chapter 9: Processes</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 9 Introduction
  - [ ] Learning Objectives

- [ ] Introduction to Processes and Process Attributes

  - [ ] What Is a Process?
  - [ ] Process Types
  - [ ] Process Scheduling and States
  - [ ] Process and Thread IDs
  - [ ] Terminating a Process
  - [ ] User and Group IDs
  - [ ] More About Priorities
  - [ ] Video: Using renice to Set Priorities

- [ ] Process Metrics and Process Control

  - [ ] Load Averages
  - [ ] Interpreting Load Averages
  - [ ] Background and Foreground Processes
  - [ ] Managing Jobs
  - [ ] Lab 9.1: Getting uptime and Load Averages
  - [ ] Lab 9.2: Background and Foreground Jobs

- [ ] Listing Processes: ps and top

  - [ ] The ps Command (System V Style)
  - [ ] The ps Command (BSD Style)
  - [ ] Video: Using ps
  - [ ] The Process Tree
  - [ ] top
  - [ ] First Line of the top Output
  - [ ] Second Line of the top Output
  - [ ] Third Line of the top Output
  - [ ] Fourth and Fifth Lines of the top Output
  - [ ] Process List of the top Output
  - [ ] Interactive Keys with top
  - [ ] Video: Using top
  - [ ] Video: Using System Monitoring

- [ ] Starting Processes in the Future

  - [ ] Scheduling Future Processes Using at
  - [ ] cron
  - [ ] sleep
  - [ ] Lab 9.3: Using at for Future Batch Processing
  - [ ] Lab 9.4: Scheduling a Periodic Task with cron

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 10: File Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 10 Introduction
  - [ ] Learning Objectives

- [ ] Filesystems

  - [ ] Introduction to Filesystems
  - [ ] Filesystem Varieties
  - [ ] Linux Partitions
  - [ ] Mount Points
  - [ ] Mounting and Unmounting
  - [ ] NFS and Network Filesystems
  - [ ] NFS on the Server
  - [ ] NFS on the Client
  - [ ] Lab 10.1: Exploring Mounted Filesystems

- [ ] Filesystem Architecture

  - [ ] Overview of User Home Directories
  - [ ] The /bin and /sbin Directories
  - [ ] The /proc Filesystem
  - [ ] The /dev Directory
  - [ ] The /var Directory
  - [ ] The /etc Directory
  - [ ] The /boot Directory
  - [ ] The /lib and /lib64 Directories
  - [ ] Removable media: the /media, /run and /mnt Directories
  - [ ] Additional Directories Under /:
  - [ ] The /usr Directory Tree

- [ ] Comparing Files and File Types

  - [ ] Comparing Files with diff
  - [ ] Using diff3 and patch
  - [ ] Using the file Utility
  - [ ] Try-It-Yourself: Comparing Files
  - [ ] Try-It-Yourself: Using file
  - [ ] Lab 10.2: Using diff and patch

- [ ] Backing Up and Compressing Data

  - [ ] Backing Up Data
  - [ ] Using rsync
  - [ ] Compressing Data
  - [ ] Compressing Data Using gzip
  - [ ] Compressing Data Using bzip2
  - [ ] Compressing Data Using xz
  - [ ] Handling Files Using zip
  - [ ] Archiving and Compressing Data Using tar
  - [ ] Relative Compression Times and Sizes
  - [ ] Disk-to-Disk Copying (dd)
  - [ ] Lab 10.3: Archiving (Backing Up) the Home Directory

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-11">Chapter 11: Text Editors</a> :heavy_check_mark:</summary>

- [x] [Introduction and Learning Objectives](src/contents/lessons/lfs101/chapter-11/subchapter-01)

  - [x] [Video: Chapter 11 Introduction](src/contents/lessons/lfs101/chapter-11/subchapter-01/lesson-01/en)
  - [x] [Learning Objectives](src/contents/lessons/lfs101/chapter-11/subchapter-01/lesson-02/en)

- [x] [Basic Editors: nano and gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02)

  - [x] [Overview of Text Editors in Linux](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-01/en)
  - [x] [Creating Files Without Using an Editor](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-02/en)
  - [x] [nano and gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-03/en)
  - [x] [nano](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-04/en)
  - [x] [gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-05/en)
  - [x] [Lab 11.1: Using nano](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-06/en)
  - [x] [Lab 11.2: Using gedit](src/contents/lessons/lfs101/chapter-11/subchapter-02/lesson-07/en)

- [x] [More Advanced Editors: vi and emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03)

  - [x] [vi and emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-01/en)
  - [x] [Introduction to vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-02/en)
  - [x] [vimtutor](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-03/en)
  - [x] [Modes in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-04/en)
  - [x] [Working with Files in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-05/en)
  - [x] [Changing Cursor Positions in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-06/en)
  - [x] [Video: Using Modes and Cursor Movements in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-07/en)
  - [x] [Searching for Text in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-08/en)
  - [x] [Working with Text in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-09/en)
  - [x] [Using External Commands in vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-10/en)
  - [x] [Video: Using External Commands, Saving, and Closing in the vi Editor](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-11/en)
  - [x] [Introduction to emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-12/en)
  - [x] [Working with emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-13/en)
  - [x] [Changing Cursor Positions in emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-14/en)
  - [x] [Searching for Text in emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-15/en)
  - [x] [Working with Text in emacs](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-16/en)
  - [x] [Video: emacs Operations](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-17/en)
  - [x] [Lab 11.3: vi and emacs Tutorials](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-18/en)
  - [x] [Lab 11.4: Working with vi](src/contents/lessons/lfs101/chapter-11/subchapter-03/lesson-19/en)

- [x] [Learning Objectives (Review) and Summary](src/contents/lessons/lfs101/chapter-11/subchapter-04)

  - [x] [Learning Objectives (Review)](src/contents/lessons/lfs101/chapter-11/subchapter-04/lesson-01/en)
  - [x] [Summary](src/contents/lessons/lfs101/chapter-11/subchapter-04/lesson-02/en)

</details>

<details>
  <summary><a href="src/contents/lessons/lfs101/chapter-12">Chapter 12: User Environment</a> :heavy_check_mark:</summary>

- [x] [Introduction and Learning Objectives](src/contents/lessons/lfs101/chapter-12/subchapter-01)

  - [x] [Video: Chapter 12 Introduction](src/contents/lessons/lfs101/chapter-12/subchapter-01/lesson-01/en)
  - [x] [Learning Objectives](src/contents/lessons/lfs101/chapter-12/subchapter-01/lesson-02/en)

- [x] [Accounts, Users and Groups](src/contents/lessons/lfs101/chapter-12/subchapter-02)

  - [x] [Identifying the Current User](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-01/en)
  - [x] [User Startup Files](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-02/en)
  - [x] [Order of the Startup Files](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-03/en)
  - [x] [Creating Aliases](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-04/en)
  - [x] [Basics of Users and Groups](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-05/en)
  - [x] [Adding and Removing Users](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-06/en)
  - [x] [Video: Using User Accounts](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-07/en)
  - [x] [Adding and Removing Groups](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-08/en)
  - [x] [The root Account](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-09/en)
  - [x] [su and sudo](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-10/en)
  - [x] [Elevating to root Account](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-11/en)
  - [x] [Try-It-Yourself: Identify the Currently Logged-In User and User Name](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-12/en)
  - [x] [Lab 12.1: Deploying aliases](src/contents/lessons/lfs101/chapter-12/subchapter-02/lesson-13/en)

- [x] [Environment Variables](src/contents/lessons/lfs101/chapter-12/subchapter-03)

  - [x] [Environment Variables](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-01/en)
  - [x] [Setting Environment Variables](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-02/en)
  - [x] [The HOME Variable](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-03/en)
  - [x] [The PATH Variable](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-04/en)
  - [x] [The SHELL Variable](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-05/en)
  - [x] [The PS1 Variable and the Command Line Prompt](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-06/en)
  - [x] [Lab 12.2: Adding /tmp to Your Path](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-07/en)
  - [x] [Lab 12.3: Changing the Command Line Prompt](src/contents/lessons/lfs101/chapter-12/subchapter-03/lesson-08/en)

- [x] [Recalling Previous Commands](src/contents/lessons/lfs101/chapter-12/subchapter-04)

  - [x] [Recalling Previous Commands](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-01/en)
  - [x] [Using History Environment Variables](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-02/en)
  - [x] [Finding and Using Previous Commands](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-03/en)
  - [x] [Executing Previous Commands](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-04/en)
  - [x] [Keyboard Shortcuts](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-05/en)
  - [x] [Lab 12.4: Command History](src/contents/lessons/lfs101/chapter-12/subchapter-04/lesson-06/en)

- [x] [File Permissions](src/contents/lessons/lfs101/chapter-12/subchapter-05)

  - [x] [File Ownership](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-01/en)
  - [x] [File Permission Modes and chmod](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-02/en)
  - [x] [Example of chown](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-03/en)
  - [x] [Example of chgrp](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-04/en)
  - [x] [Try-It-Yourself: Using chmod to Change File Permissions](src/contents/lessons/lfs101/chapter-12/subchapter-05/lesson-05/en)

- [x] [Learning Objectives (Review) and Summary](src/contents/lessons/lfs101/chapter-12/subchapter-06)

  - [x] [Learning Objectives (Review)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-01/en)
  - [x] [Summary (1 of 2)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-02/en)
  - [x] [Summary (2 of 2)](src/contents/lessons/lfs101/chapter-12/subchapter-06/lesson-03/en)

</details>

<details>
  <summary>Chapter 13: Manipulating Text</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 13 Introduction
  - [ ] Learning Objectives

- [ ] cat and echo

  - [ ] Command Line Tools for Manipulating Text Files
  - [ ] cat
  - [ ] Using cat Interactively
  - [ ] Video: Using cat
  - [ ] Try-It-Yourself: Using cat
  - [ ] echo
  - [ ] Try-It-Yourself : Using echo

- [ ] Working with Large and Compressed Files

  - [ ] Working with Large Files
  - [ ] head
  - [ ] tail
  - [ ] Try-It-Yourself: Using head and tail
  - [ ] Viewing Compressed Files

- [ ] sed and awk

  - [ ] Introduction to sed and awk
  - [ ] sed
  - [ ] sed Command Syntax
  - [ ] sed Basic Operations
  - [ ] Video: Using sed
  - [ ] awk
  - [ ] awk Basic Operations
  - [ ] Lab 13.1: Using sed

- [ ] File Manipulation Utilities

  - [ ] File Manipulation Utilities
  - [ ] sort
  - [ ] uniq
  - [ ] Try-It-Yourself: Using sort and uniq
  - [ ] paste
  - [ ] Using paste
  - [ ] join
  - [ ] Using join
  - [ ] split
  - [ ] Using split
  - [ ] Regular Expressions and Search Patterns
  - [ ] Using Regular Expressions and Search Patterns
  - [ ] Lab 13.2: Parsing Files with awk (and sort and uniq)

- [ ] grep and strings

  - [ ] grep
  - [ ] strings
  - [ ] Lab 13.3: Using grep

- [ ] Miscellaneous Text Utilities

  - [ ] tr
  - [ ] Try-It-Yourself : Using tr
  - [ ] tee
  - [ ] wc
  - [ ] Try-It-Yourself: Using wc
  - [ ] cut
  - [ ] Lab 13.4: Using tee
  - [ ] Lab 13.5: Using wc

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 3)
  - [ ] Summary (2 of 3)
  - [ ] Summary (3 of 3)

</details>

<details>
  <summary>Chapter 14: Network Operations</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 14 Introduction
  - [ ] Learning Objectives

- [ ] Network Addresses and DNS

  - [ ] Introduction to Networking
  - [ ] IP Addresses
  - [ ] IPv4 and IPv6
  - [ ] Decoding IPv4 Addresses
  - [ ] Class A Network Addresses
  - [ ] Class B Network Addresses
  - [ ] Class C Network Addresses
  - [ ] IP Address Allocation
  - [ ] Name Resolution
  - [ ] Video: Using Domain Name System (DNS) and Name Resolution Tools
  - [ ] Try-It-Yourself: Using Domain Name System (DNS) and Name Resolution Tools

- [ ] Networking Configuration and Tools

  - [ ] Network Configuration Files
  - [ ] Network Interfaces
  - [ ] The ip Utility
  - [ ] ping
  - [ ] route
  - [ ] traceroute
  - [ ] Try-It-Yourself: Using ping, route, and traceroute
  - [ ] More Networking Tools
  - [ ] Video: Using More Networking Tools
  - [ ] Try-It-Yourself: Using Network Tools

- [ ] Browsers, wget and curl

  - [ ] Graphical and Non-Graphical Browsers
  - [ ] wget
  - [ ] curl
  - [ ] Try-It-Yourself: Using wget and curl

- [ ] Transferring Files

  - [ ] FTP (File Transfer Protocol)
  - [ ] FTP Clients
  - [ ] Video: Connecting to an FTP server
  - [ ] Try-It-Yourself: Connecting to an FTP server
  - [ ] SSH: Executing Commands Remotely
  - [ ] Copying Files Securely with scp
  - [ ] Video: Using SSH between Two Virtual Machines
  - [ ] Lab 14.1: Network Troubleshooting
  - [ ] Lab 14.2: Non-Graphical Browsers

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 15: The Bash Shell and Basic Scripting</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 15 Introduction
  - [ ] Learning Objectives

- [ ] Features and Capabilities

  - [ ] Shell Scripting
  - [ ] Command Shell Choices
  - [ ] Shell Scripts
  - [ ] A Simple bash Script
  - [ ] Interactive Example Using bash Scripts
  - [ ] Return Values
  - [ ] Viewing Return Values
  - [ ] Lab 15.1: Exit Status Codes

- [ ] Syntax

  - [ ] Basic Syntax and Special Characters
  - [ ] Splitting Long Commands Over Multiple Lines
  - [ ] Putting Multiple Commands on a Single Line
  - [ ] Output Redirection
  - [ ] Input Redirection
  - [ ] Built-In Shell Commands
  - [ ] Commands Built in to bash
  - [ ] Script Parameters
  - [ ] Using Script Parameters
  - [ ] Command Substitution
  - [ ] Environment Variables
  - [ ] Exporting Environment Variables
  - [ ] Functions
  - [ ] Lab 15.2: Working with Files and Directories in a Script
  - [ ] Lab 15.3: Passing Arguments
  - [ ] Lab 15.4: Environment Variables
  - [ ] Lab 15.5: Working with Functions

- [ ] Constructs

  - [ ] The if Statement
  - [ ] Using the if Statement
  - [ ] The elif Statement
  - [ ] Testing for Files
  - [ ] Boolean Expressions
  - [ ] Tests in Boolean Expressions
  - [ ] Example of Testing of Strings
  - [ ] Numerical Tests
  - [ ] Example of Testing for Numbers
  - [ ] Arithmetic Expressions
  - [ ] Lab 15.6: Arithmetic and Functions

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 16: More on Bash Shell Scripting</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 16 Introduction
  - [ ] Learning Objectives

- [ ] String Manipulation

  - [ ] String Manipulation
  - [ ] Example of String Manipulation
  - [ ] Parts of a String
  - [ ] Lab 16.1: String Tests and Operations

- [ ] The case Statement

  - [ ] The case Statement
  - [ ] Structure of the case Statement
  - [ ] Example of Use of the case Construct
  - [ ] Lab 16.2: Using the case Statement

- [ ] Looping Constructs

  - [ ] Looping Constructs
  - [ ] The for Loop
  - [ ] The while Loop
  - [ ] The until Loop

- [ ] Script Debugging

  - [ ] Debugging bash Scripts
  - [ ] Script Debug Mode
  - [ ] Redirecting Errors to File and Screen

- [ ] Some Additional Useful Techniques

  - [ ] Creating Temporary Files and Directories
  - [ ] Example of Creating a Temporary File and Directory
  - [ ] Discarding Output with /dev/null
  - [ ] Random Numbers and Data
  - [ ] How the Kernel Generates Random Numbers
  - [ ] Lab 16.3: Using Random Numbers

- [ ] Challenge Assignment from the Mooqita Project (Optional)

  - [ ] Challenge Assignment from the Mooqita Project (Optional)

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 17: Printing</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 17 Introduction
  - [ ] Learning Objectives

- [ ] Configuration

  - [ ] Printing on Linux
  - [ ] CUPS Overview
  - [ ] How Does CUPS Work?
  - [ ] Scheduler
  - [ ] Configuration Files
  - [ ] Job Files
  - [ ] Log Files
  - [ ] Filters, Printer Drivers, and Backends
  - [ ] Managing CUPS
  - [ ] Video: Managing the CUPS Daemon
  - [ ] Configuring a Printer from the GUI
  - [ ] Video: Adding a Network Printer
  - [ ] Adding Printers from the CUPS Web Interface

- [ ] Printing Operations

  - [ ] Printing from the Graphical Interface
  - [ ] Printing from the Command-Line Interface
  - [ ] Using lp
  - [ ] Video: Printing Using lp
  - [ ] Try-It-Yourself: Printing with the lp Command
  - [ ] Managing Print Jobs
  - [ ] Try-It-Yourself: Managing Print Jobs

- [ ] Manipulating Postscript and PDF Files

  - [ ] Working with PostScript and PDF
  - [ ] Working with enscript
  - [ ] Converting between PostScript and PDF
  - [ ] Viewing PDF Content
  - [ ] Manipulating PDFs
  - [ ] Using qpdf
  - [ ] Video: Using qpdf
  - [ ] Using pdftk
  - [ ] Encrypting PDF Files with pdftk
  - [ ] Using Ghostscript
  - [ ] Video: Using pdftk
  - [ ] Using Additional Tools
  - [ ] Lab 17.1: Creating PostScript and PDF from Text Files
  - [ ] Lab 17.2: Combining PDFs

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>

<details>
  <summary>Chapter 18: Local Security Principles</summary>

- [ ] Introduction and Learning Objectives

  - [ ] Video: Chapter 18 Introduction
  - [ ] Learning Objectives

- [ ] Understanding Linux Security

  - [ ] User Accounts
  - [ ] Types of Accounts
  - [ ] Understanding the root Account

- [ ] When Are root Privileges Required?

  - [ ] Operations Requiring root Privileges
  - [ ] Operations Not Requiring root Privileges

- [ ] sudo, Process Isolation, Limiting Hardware Access and Keeping Systems Current

  - [ ] Comparing sudo and su
  - [ ] sudo Features
  - [ ] The sudoers File
  - [ ] Command Logging
  - [ ] Process Isolation
  - [ ] Hardware Device Access
  - [ ] Keeping Current
  - [ ] Lab 18.1: sudo

- [ ] Working with passwords

  - [ ] How Passwords Are Stored
  - [ ] Password Algorithm
  - [ ] Good Password Practices
  - [ ] Lab 18.2: Password Aging

- [ ] Securing the Boot Process and Hardware Resources

  - [ ] Requiring Boot Loader Passwords
  - [ ] Hardware Vulnerability
  - [ ] Software Vulnerability

- [ ] Learning Objectives (Review) and Summary

  - [ ] Learning Objectives (Review)
  - [ ] Summary (1 of 2)
  - [ ] Summary (2 of 2)

</details>
