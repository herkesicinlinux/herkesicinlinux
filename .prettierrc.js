module.exports = {
  trailingComma: 'all',
  arrowParens: 'always',
  printWidth: 100,
  tabWidth: 2,
  singleQuote: true,
  semi: true,
};
