# Translating

Translators, follow the steps below.

1. Create a new branch for your work and switch to it:

   ```bash
   git checkout -b my_branch
   Switched to a new branch 'my_branch'

   ```

2. Perform your work and do some commits.

3. Switch back to production branch when you complete your work.

   ```bash
   git checkout main
   Switched to branch 'main'

   ```

4. DO NOT MERGE your branch into production branch. Push your branch to `origin` instead.

   ```bash
   git push origin my_branch
   ```

5. Open a PR for your commit(s).

6. Remove your working branch from your local environment **after your branch has been merged into production**.

   ```bash
   git branch -d my_branch
   ```

   (Optional) Remove branch from remote too.

   ```bash
   git push -d origin my_branch
   ```

7. Pull changes to get in sync with origin.

   ```bash
   git pull
   ```

8. You're done. Return back to step 1!
