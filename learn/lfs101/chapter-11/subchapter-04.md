---
title: End of Chapter
---

## Learning Objectives (Review)

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

You should now be familiar with:

- How to create and edit files using the available Linux text editors.
- `nano`, a simple text-based editor.
- **gedit**, a simple graphical editor.
- `vi` and `emacs`, two advanced editors with both text-based and graphical interfaces.

## Summary

<img src="/en/img/lfs101/common/tux-grad-cap.png" align="right"/>

You have completed Chapter 11. Let’s summarize the key concepts covered:

- Text editors (rather than word processing programs) are used quite often in Linux, for tasks such as creating or modifying system configuration files, writing scripts, developing source code, etc.

- nano is an easy-to-use text-based editor that utilizes on-screen prompts.

- gedit is a graphical editor, very similar to Notepad in Windows.

- The vi editor is available on all Linux systems and is very widely used. Graphical extension versions of vi are widely available as well.

- emacs is available on all Linux systems as a popular alternative to vi. emacs can support both a graphical user interface and a text mode interface.

- To access the vi tutorial, type **vimtutor** at a command line window.

- To access the emacs tutorial type **Ctrl-h** and then **t** from within emacs.

- **vi** has three modes: Command, Insert, and Line. **emacs** has only one, but requires use of special keys, such as **Control** and **Escape**.

- Both editors use various combinations of keystrokes to accomplish tasks. The learning curve to master these can be long, but once mastered using either editor is extremely efficient.
