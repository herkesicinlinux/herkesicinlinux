---
title: External Commands in vi
---

The following situation, where you may lose quite a bit of work before it is saved, is likely to come up sooner or later.

1.  Change the permissions on the file you have been editing to read-only, and open it for editing by doing:

    ```bash
    student:/tmp> chmod 444 myfile
    student:/tmp> vi myfile

    ```

2.  Delete the first line in the file, by typing `dd`. You may see a brief error message, but the line will be deleted.

3.  Now let's try and save the modified file. Type `:w`. What happens?

    You can't save this file, can you? It is read-only! You should have gotten an error message at the bottom line of the screen telling you that the file is `readonly`.

4.  Perhaps you made lots of changes to this file in a long editing session. Is all your hard work lost? No!

5.  You have a couple of alternatives. The first one is that you can go out to the shell from your editor, change the permissions on the file, exit the shell and be returned to your editing session to save your file.

    Do this by typing `:sh`. This should give you a shell prompt. Now type the command:

    ```bash
    student:/tmp> chmod 644 myfile

    ```

    and then exit the shell with:

    ```bash
    student:/tmp> exit

    ```

    You should be returned to your **vi** editing session. You typically see an error message telling you that the file has changed. Type `O` (`shift+o`) for OK.

    Now type `:w!` (note the added `!`). This file should be saved now!

6.  Another thing you could do is to write the contents of the file to a new file. The `:w` command can take an argument as the file name you want to save the file as. For example, you could type `:w new_myfile` to save all your changes to the file `new_myfile`.

    It is good to know these techniques because this will likely come up sometime while you are working with **Linux**. Play around with both techniques and become comfortable with both.
