0
00:00:01,240 --> 00:00:07,580
We will now demonstrate some of the operations you can do with Emacs in your daily work.

1
00:00:07,580 --> 00:00:16,700
So first, let&#39;s get a file to edit. So, working in the /tmp directory, let&#39;s get a copy of /etc/passwd and bring it over here [cp /etc/passwd].

2
00:00:16,700 --> 00:00:24,480
And then, in order to work on it, I just have to say &quot;emacs passwd &amp;&quot; the name of the file.

3
00:00:24,480 --> 00:00:34,560
Now, the default font here is a little small. So, I&#39;ll play a trick and hit ctrl + the middle button of the mouse to control the size of the font,

4
00:00:34,560 --> 00:00:38,180
and then I&#39;ll make the window a little bigger.

5
00:00:38,180 --> 00:00:52,080
Okay. So, we&#39;ve done that. Now, suppose I want to look for a certain string, let&#39;s say ftp, I would hit Ctrl-s and type into the very bottom line here ftp.

6
00:00:52,080 --> 00:00:53,660
So you can see down here.

7
00:00:53,660 --> 00:01:01,080
Then you&#39;ll see it brought me to ftp. I&#39;ll go back to the beginning of the line by hitting Ctrl-a ,

8
00:01:01,080 --> 00:01:13,220
and then, let&#39;s say I want to change all occurrences of the string ftp to something else, I hit Escape % mark, and then I&#39;ll say ftp again in the bottom line.

9
00:01:13,220 --> 00:01:25,740
And then, let&#39;s just do it backwards in capitals PTF. You see, I hit Space, it does the first one, Space, it does the second one, Space, it does the third one.

10
00:01:25,740 --> 00:01:31,140
If I had hit an exclamation point, it would have done all of them in the entire file.

11
00:01:31,140 --> 00:01:40,100
Suppose I want to remove a line. I can just hit Ctrl-K and it&#39;s gone. Ctrl-K is gone again.

12
00:01:40,100 --> 00:01:56,000
Suppose I want to remove a range of lines. I hit Ctrl-space and then I go down a few lines with either the arrow key or Ctrl- N, I hit Ctrl-W and they&#39;re gone.

13
00:01:56,000 --> 00:02:05,240
If I want to move them further on in the file, I go down a few lines and I hit Ctrl-Y for yank and they&#39;re back in there.

14
00:02:05,240 --> 00:02:10,840
One nice thing I can do with Emacs is open up multiple windows at the same time.

15
00:02:10,840 --> 00:02:20,880
So, let me hit  Ctrl-X-2 and now I&#39;ve got two windows and I can switch to the bottom window by hitting Ctrl-X-O, for other,

16
00:02:20,880 --> 00:02:25,620
and then I hit Ctrl-X-F and I could put a different file in that buffer.

17
00:02:25,620 --> 00:02:38,860
So, let&#39;s say I put it in /etc/group. Ok, once again, I&#39;ll make the font a little bigger by hitting Ctrl and the middle button on the mouse. ok

18
00:02:38,860 --> 00:02:47,120
And you&#39;ll notice the bottom line it&#39;s actually right protected, so I can&#39;t really delete anything because it belongs to root.

19
00:02:47,120 --> 00:02:52,000
If I try to delete this line, it won&#39;t let me do it. It says buffer is read-only.

20
00:02:52,000 --> 00:02:59,460
But, in general, like if I had two files with the same permissions, I could cut and paste and go from one to the other.

21
00:02:59,460 --> 00:03:11,300
To get back to just one [window], I can do Ctrl-X-1, and I have only this. But if I really wanted the other window, I can hit Ctrl-X-B, and I&#39;m back in the passwd file.

22
00:03:11,300 --> 00:03:27,440
If I want to rewrite it, I can hit Ctrl-X-W... Ctrl-X Ctrl-W and that would let me write it as a different name. So I&#39;ll say passwdrevised, and it&#39;s there.

23
00:03:27,440 --> 00:03:36,640
If I want to terminate the program, I say Ctrl-X-S. Make sure things have been saved, and then, Ctrl-X-C, and I&#39;m done.

24
00:03:36,640 --> 00:03:47,880
So you can see we use the Ctrl key quite a bit in emacs. The position most keyboards put it in these days is a little unnatural, all the way at the bottom left or right.

25
00:03:47,880 --> 00:03:55,640
So veteran Emacs users tend to remap the keyboard so that the caps lock key also works as Ctrl.

26
00:03:55,640 --> 00:04:02,420
So, that&#39;s some of the basic operations you would do in day-to-day use of Emacs.

