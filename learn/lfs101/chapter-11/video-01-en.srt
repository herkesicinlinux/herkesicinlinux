0
00:00:04,474 --> 00:00:09,519
Off course computers are all about editing data and manipulating data and managing data.

1
00:00:09,525 --> 00:00:13,126
On Linux systems, that data often takes the form of text files.

2
00:00:13,140 --> 00:00:21,625
Sometimes these text files are created without other applications but sometimes you need to edit them directly and you those on Linux using something called a text editor.

3
00:00:21,640 --> 00:00:32,180
In this lesson, we will cover some of the popular text editors, such as nano, emacs and gedit and also go step-by-step through the usage and configuration of the vim text editor.

4
00:00:32,200 --> 00:00:37,415
When you are done with this lesson, you should be comfortable editing your text files on your favorite text editor.

5
00:00:37,472 --> 00:00:38,534
Let’s get started!

