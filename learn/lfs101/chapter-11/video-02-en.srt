﻿1
00:00:00,614 --> 00:00:04,660
Let's get started on using modes and cursor movements in vi.

2
00:00:04,762 --> 00:00:08,892
1. Open vi by typing vi followed by the filename.

3
00:00:09,251 --> 00:00:11,470
Vi opens in Command mode.

4
00:00:11,709 --> 00:00:13,927
2. Type i to enter Insert mode.

5
00:00:14,030 --> 00:00:16,675
Vi's  Insert mode is displayed.

6
00:00:16,880 --> 00:00:25,414
3. Type the following sentences: The quick brown fox jumped over the lazy dog. Nobody expects the Spanish Inquisition!

7
00:00:25,807 --> 00:00:30,142
4. To exit Insert mode and switch to Command mode, press ESC

8
00:00:30,620 --> 00:00:36,354
5.  To exit vi and save the file, type :wq and press Enter.

9
00:00:37,515 --> 00:00:44,240
6. Open the recently saved file and place the cursor on the letter k in the word quick in the first sentence.

10
00:00:44,581 --> 00:00:49,753
To move the cursor four characters to the left, type h four times.

11
00:00:49,958 --> 00:00:53,200
The cursor is moved to letter q of the word quick.

12
00:00:53,491 --> 00:00:56,546
7. To move the cursor to the next line, type j.

13
00:00:57,143 --> 00:01:01,086
8. To move the cursor to the beginning of the next word, type w.

14
00:01:01,325 --> 00:01:05,284
The cursor is moved to the beginning of the word expects.

15
00:01:05,506 --> 00:01:08,749
9. To move the cursor to the end, type $.

16
00:01:08,988 --> 00:01:11,992
The cursor is moved to the end of the second sentence.

17
00:01:12,094 --> 00:01:14,620
10. Type i to enter Insert mode.

18
00:01:15,030 --> 00:01:17,403
Vi insert mode is displayed.

19
00:01:17,590 --> 00:01:22,779
11. To insert text at the end of the second sentence, type A and type History.

20
00:01:22,950 --> 00:01:26,175
The word History is displayed to the end of the sentence.

21
00:01:26,363 --> 00:01:30,459
12. To exit  Insert mode and switch to Command mode, press ESC

22
00:01:30,920 --> 00:01:37,013
13. To exit vi and save the file type :wq and press Enter.

