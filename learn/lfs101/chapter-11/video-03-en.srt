0
00:00:00,543 --> 00:00:05,776
Let's get started on using external commands, saving, and closing in vim editor.

1
00:00:05,900 --> 00:00:10,220
1. Open vi editor by typing VI followed by the filename.

2
00:00:10,368 --> 00:00:12,614
vi opens in  Command mode.

3
00:00:13,108 --> 00:00:15,626
2. Type i to enter Insert mode.

4
00:00:15,749 --> 00:00:18,415
Vi's  Insert mode is displayed.

5
00:00:18,489 --> 00:00:20,316
3. Type the following sentences.

6
00:00:20,365 --> 00:00:26,166
Nobody expects the Spanish Inquisition! Nobody expects the 2020 revolution!

7
00:00:26,253 --> 00:00:29,758
4. To exit  Insert mode and switch to Command mode, press Esc.

8
00:00:29,943 --> 00:00:33,696
5. To write and quit the file, type :wq.

9
00:00:33,819 --> 00:00:36,880
The file is updated with the changes and closed.

10
00:00:37,312 --> 00:00:41,299
6. Open vi by typing VI followed by the filename.

11
00:00:42,126 --> 00:00:48,396
7. To count the words in the current file, type :!wc %.

12
00:00:48,803 --> 00:00:50,741
The word count  is displayed.

13
00:00:51,234 --> 00:00:53,197
8. Press ENTER to continue editing.

14
00:00:53,518 --> 00:00:57,443
9. To quit if no edits were made in the file, type :q.

15
00:00:57,566 --> 00:00:58,788
The file is closed.

16
00:00:58,887 --> 00:01:02,800
10. Open vi  by typing VI followed by filename.

17
00:01:03,306 --> 00:01:08,095
11. To quit without saving the file, type :q!.

18
00:01:08,169 --> 00:01:10,810
The file is closed without saving the changes.

