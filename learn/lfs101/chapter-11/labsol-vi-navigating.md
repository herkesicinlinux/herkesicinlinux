---
title: Navigating in vi
---

1.  Assuming `myfile` does not exist, create it and open it by typing:

    ```bash
    student:/tmp> vi myfile

    ```

2.  You now have a mostly blank screen with your cursor on the top line. You are initially in command mode.

3.  Type `a` to append and put you into insert mode.

4.  Type the sentence

    ```bash
    The quick brown fox jumped over the lazy dog.

    ```

    If you make a mistake in typing, remember that you can backspace to fix it.

5.  Hit the `ESC` key. Your cursor should be on the period at the end of the sentence. You are back in command mode at this point.

6.  Type `a` again, and then hit `Enter`. You should be on a new line under the one you just typed.

7.  Type the sentence:

    ```bash
    Nobody expects the Spanish Inquisition!

    ```

    Hit `Enter` key after typing the `!` character.

8.  At the beginning of the third line, type the sentence:

    ```bash
    This is the third line.

    ```

    Hit the `ESC` key. You are now back in command mode.

9.  Now let's just move around in this file. Your cursor should be positioned over the period at the end of the third line you just typed. Hit the `h` key three times. Hit the `k` key once. Which letter is your cursor over? Which word are you in?

    (Your cursor should be over the `S` in the word `Spanish`.)

10. Hit `h` four times and then hit `j`. Now which letter is under your cursor? Which word are you in?

    (Your cursor should be over the `r` in the word `third`.)

11. Hit `k` twice and then `l` three times. Which letter is your cursor over? In which word?

    (Your cursor should be over the `x` in the word `fox`.)

12. Now hit`w` eight times. What do you notice Which letter is your cursor over? Which word is your cursor over?

    (You should notice that you are now skipping across words. In fact, eight taps on `w` will take you from the first line to the second line! Your cursor should now be over the `e` in the word `expects` on the second line.)

13. Hit `k`, followed by the `$` key. Now hit `b` twice. Which letter is your cursor over and in which word?

    (Your cursor should be over the `l` in the word `lazy`)

14. You should be getting the feel for moving around character-by-character, line-by-line and word-by-word, both forward and backward.

    Now hit the `0` (zero) key followed by two `w` keys and three `l` keys. Which letter is your cursor over and in which word are you?

    (Your cursor should be over the `w` in the word `brown`.

15. Now hit `e` three times. Which letter is your cursor over and in which word?

    (You should be over the `d` in the word `jumped`.)

16. Save the file using the `:w` command. You will use this file in the next exercise. Finally quit **vi** by typing the `:q` command. You can also combine these commands by typing `:wq` if you prefer.

Hopefully, this practice gets you used to how you can move your cursor around in a file. After awhile, these movement keys will become second-nature.
