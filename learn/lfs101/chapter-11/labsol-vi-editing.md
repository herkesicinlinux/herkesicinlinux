---
title: Editing in vi
---

Being comfortable moving around within a file is important, but actually editing the text in the file is the whole point of using an editor.

1.  We will edit the file you created in the previous exercise; i.e., do

    ```bash
    student:/tmp> vi myfile

    ```

2.  Search for the word `the`, by hitting `/` followed by `the`, and then hit `Enter`

    Your cursor should jump to the word `the` in the first line between the words `over` and `azy`.

3.  Now search for the next occurrence of `the`. Hit the `n` key. Your cursor should have jumped to the word `the` between `expects` and `Spanish` on the second line.

4.  Let's change this word from `the` to `a`. With your cursor positioned over the `t` in the word `the`, type `cw` to change the word. Did you notice that the word `the` disappeared?

    You are now in insert mode, so simply type the letter `a` at this point. If you wanted to type more characters here, you can. For example, you could have changed the word `the` to `a bright and shiny` if you wanted to. When you are finished with changing the word `the` to the word `a`, type the `ESC` key to exit from insert mode back to command mode.

5.  Hit the `L` (`shift+l`) key to move to the last line on the screen. For this short file, that also happens to be the last line. Type `dd`. What happens?

    (The last line should now be deleted.)

6.  Hit the `H` (`shift+h`) key to move to the first line on the screen. Again, for this short file, that also happens to be the first line of the file. Type `"1P` (double-quote, `1` and `P`). What happens?

    You have put the contents of the last deleted item above the current line. In this case, you put the former third line above the first line. Your file should now look like this:

    ```bash
    This is the third line.
    The quick brown fox jumped over the lazy dog.
    Nobody expects a Spanish Inquisition!

    ```

7.  Move your cursor to the word `third` using any method you prefer. Remember, you can do this several ways. You can move one character at a time, skip a word at a time, or search for the word. Use the `cw` command to change the word `third` to the word `first`. Remember to hit the `ESC` key when you are finished typing the word `first` in order to exit insert mode.

8.  Hit the `L` key to move down to the last line on the screen. Let's yank this into a named buffer; let's use the named buffer `c`. So, to yank this whole line into `c`, we can type `"cY`. Is the line still there?

    After yanking the last line into the `c` named buffer, the last line is still present. Yanking a line just copies the line.)

9.  Hit the `H` key to move to the top line on the screen. Let's put the contents of the `c` named buffer after our first line. Type `"cp` to put the contents of the `c` named buffer after the current line.

    After putting the yanked line after the first line, your file should look like:

    ```bash
    This is the third line.
    Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over the lazy dog.
    Nobody expects a Spanish Inquisition!

    ```

10. Let's try the substitute command to change all occurrences of a word to something else. For emphasis, let's change all instances of the word `the` to all capitals, as in `THE`. To do this, we type the command `:%s/the/THE/g`. Do you notice the changes?

    After executing the substitute command, your file should look like:

    ```bash
    This is THE third line.
    Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over THE lazy dog.
    Nobody expects a Spanish Inquisition!

    ```

    Note that the first `The` at the beginning of line 3 was not changed because it did not match `the` (all lower-case) in our substitute command.

11. On second thought, use of `THE` in all capital letters looks like I am yelling at you. Let's undo that last change. How Type the `u` key! (For `undo`.) Did you notice that both occurrences of the change were undone by this one command The `u` key will undo the last change even if that change affected several places in the file.

12. Finally, let's improve the file's format by running it through the **fmt** command. Type **%!fmt**. How does it look?

    (Your formatted file should look similar to:

    ```bash
    This is the first line. Nobody expects a Spanish Inquisition!
    The quick brown fox jumped over the lazy dog.  Nobody expects a
    Spanish Inquisition!)

    ```

13. Exit **vi**. You can either save your changes by writing the file and then quitting (`:wq`), or quit without saving (`:q!`).
