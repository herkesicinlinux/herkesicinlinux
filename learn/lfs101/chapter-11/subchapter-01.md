---
title: Introduction and Learning Objectives
---

## Video: Chapter 11 Introduction

<!-- [Video](https://player.vimeo.com/video/509752867) | [Subtitle](video-01-en.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/509752867?texttrack=en"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Learning Objectives

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

By the end of this chapter, you should be familiar with:

- How to create and edit files using the available Linux text editors.
- `nano`, a simple text-based editor.
- **gedit**, a simple graphical editor.
- `vi` and `emacs`, two advanced editors with both text-based and graphical interfaces.
