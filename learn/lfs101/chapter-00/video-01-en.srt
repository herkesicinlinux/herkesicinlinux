0
00:00:04,260 --> 00:00:09,220
Hey, welcome! I'm Linus Torvalds and I started Linux in 1991.

1
00:00:09,220 --> 00:00:18,080
And I welcome you to "Introduction to Linux" course and hope that you will get as much joy out of it as I have gotten over the years.

2
00:00:18,080 --> 00:00:24,060
One of the more interesting parts about Linux is how it turns out in the most unexpected places.

3
00:00:24,060 --> 00:00:31,740
So, when I started Linux, I needed an operating system for my own use, and today you find Linux everywhere...

4
00:00:31,740 --> 00:00:37,760
in small embedded devices, supercomputers, and that is kind of interesting,

5
00:00:37,760 --> 00:00:45,080
how this technology allows you to expand into many different niches.

6
00:00:45,080 --> 00:00:52,190
The thing that makes Linux interesting to me is all the interesting technology, but it's also all the people involved,

7
00:00:52,190 --> 00:00:58,620
and working in open source projects, where you work with hundreds, potentially thousands of people,

8
00:00:58,620 --> 00:01:01,250
it makes the whole technology even more interesting.

9
00:01:01,250 --> 00:01:07,660
Hopefully, this turns into an opportunity for you to find something you find really interesting to do,

10
00:01:07,660 --> 00:01:13,740
and one of the nice things about Linux and open source in general is that there's a lot of different things,

11
00:01:13,740 --> 00:01:17,460
and everybody has something they can give to the project.

12
00:01:17,460 --> 00:01:22,460
Thank you for considering this course and have fun!

