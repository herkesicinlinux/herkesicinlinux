---
title: 'Welcome!'
---

## Video: Welcome to LFS101

<!-- [Video](https://player.vimeo.com/video/509833103?texttrack=en) | [Subtitle](video-01-en.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509833103?texttrack=en" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>

## Before You Begin

Welcome to LFS101x - Introduction to Linux!

We strongly recommend that you review the course syllabus before jumping into the content. It provides the most important information related to the course, including:

- Course overview
- Instructors biographies and targeted audience
- Course prerequisites and length
- Course learning objectives and the outline
- edX platform guidelines
- Discussion forums, course timing, and learning aids
- Grading, progress, and course completion
- Professional Certificate Program
- Audit and verified tracks
- The Linux Foundation's history, events, training, and certifications.

## Getting Help

For any technical issues with the edX platform (including login problems and issues with the Verified Certificate), please use the Help icon located on the upper right side of your screen.

One great way to interact with peers taking this course and resolving any content-related issues is via the Discussion Forums. These forums can be used in the following ways:

- To discuss concepts, tools, and technologies presented in this course, or related to the topics discussed in the course material.
- To ask questions about course content.
- To share resources and ideas related to Linux.

We strongly encourage you not only to ask questions, but to share with your peers opinions about the course content, as well as valuable related resources. The Discussion Forums will be reviewed periodically by The Linux Foundation staff, but it is primarily a community resource, not an 'ask the instructor' service.

Note: Before starting a thread about your issue, we strongly encourage you to do a quick search to see if your question has already been addressed. It’s often the case that there will be one or two threads asking the same question that has been answered.

To learn more tips on how to use them, read the following article: "[Getting the Most Out of the edX Discussion Forums](https://blog.edx.org/getting-most-out-edx-discussion-forums)".

## Meet Your Intructor: Jerry Cooperstein

<figure>
  <img src="/en/img/lfs101/ch00/img01.png" />
  <figcaption>Jerry Cooperstein</figcaption>
</figure>

Jerry Cooperstein, PhD has been working with Linux since 1994, developing and delivering training in both the kernel and user space. He has overall responsibility for all training content at The Linux Foundation. During a two decade career in nuclear astrophysics, he developed state-of-the-art simulation software on many kinds of supercomputers and taught at both the undergraduate and graduate level. Jerry joined The Linux Foundation in 2009 as the Training Program Director.

## A note from the Author

This course is descended from The Linux Foundation's original "Introduction to Linux" course, which was always taught with a live instructor, either in a physical classroom, or in a virtual classroom over the Internet, for which we charged tuition. Converting this course to one that was free of charge and available to a large community was a task that The Linux Foundation believed was worthwhile and would be well-appreciated. When we began the process of converting to a self-paced course in the Massive Open Online Course (MOOC) format for edX, we really had no idea of how much work it would be and how many changes we would have to make in the presentation, so it could both stand on its own without a live instructor and be entertaining enough to hold students' interest.

While I am listed as the author and instructor for this course, the truth is it could never have been produced without the effort of many individuals who have contributed their time and talent to its development. First and foremost, I have to thank Flavia Cioanca, Magdalena Stepien and Clyde Seepersad at The Linux Foundation, who have been deeply involved through relentless cycles of developing content and clarifying presentation.

Finally, I would like to thank all of my colleagues at The Linux Foundation, for making this a team effort and investing the resources to make this come to fruition, helping to cover some of my other responsibilities while we were so occupied with this effort, and also for reviewing material as it was being prepared. I hope you enjoy seeing some of their faces and names in the video introductions that begin each chapter of the course.
