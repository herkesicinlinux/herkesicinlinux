---
title: Customizing the Desktop
---

The easiest way to customize your desktop is to right-click your desktop which should open a menu. At this point, select **Change Desktop Background** or **Default Desktop Settings** depending upon which distribution of Linux you are running.

This will open a window where you can then select available background pictures, a solid color background, or use a picture of your own that you Add to your Pictures folder in your home directory.

To change your **theme**, you may have noticed a tab in the window where you changed the background labeled **Theme**. If it is present, you can click on this tab and select one of the available themes or click on Customize to experiment with your own creations.

However, for **GNOME 3**, you can no longer do anything this way except to set the background. Instead you have to run **gnome-tweaks** (or **gnome-tweak-tool**) and then scroll down to **Theme** or **Appearance->Theme**.

Note that many other settings one might expect to actually be in the **Settings** menu are now configurable only through **gnome-tweaks**. This can cause a lot of hair-pulling until you discover this fact.
