---
title: Introduction and Learning Objectives
---

## Video: Chapter 4 Introduction

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V005900_DTH.mp4) | [Subtitle](video-01-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Learning Objectives

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

By the end of this chapter, you should be able to:

- Manage graphical interface sessions.
- Perform basic operations using the graphical interface.
- Change the graphical desktop to suit your needs.
