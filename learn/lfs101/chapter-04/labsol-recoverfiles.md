---
title: Recovering Deleted Files
---

1. Open the file manager and navigate to your home directory. Once your user configuration is set up for it this is as simple as right clicking in the directory, and selecting **Create New->Text File** and giving it a name. (You may see something other than **Text File**.)

   With the **GNOME** file manager, there is no such option in the default setup. You must create a file called **new** in your **Templates** directory first. The easiest way to do this is to type **Alt-F2** and then in the little window that opens up

   ```bash
   student:/tmp> touch ~/Templates/new
   ```

   Once you have done that, you can either right click in the directory, or somewhere on the right side of the title bar to open up a dialog where there will be an option to create a new file of some type.

   Why this feature is not on by default might be considered a mystery not understood by modern science. However, the logic is that new files are generally not created in a vaccuum (at least in GUIs); one expects to make them in an application. Some distributions or versions of **GNOME** may not need this stupid step.

2. This should be pretty obvious, either by dragging and dropping in the Trash folder on the left, or through a right click on the file.

3. To navigate down through your **.local** directory, you have to make hidden files visible. On **GNOME** systems, you can either just hit **CTRL-H**, or you can click on the little down arrow next to the gear icon and toggle showing hidden files on and off.

4. The easiest way is to click on the **Trash** icon and then pick **Restore**, which will bring it back to its original location which you can easily verify.

Note that the **GNOME** File Manager does not include a **Delete** or **Permanenly Delete** option by default. To enable this you have to go into preferences and turn it on. You can enter the preferences menu by starting the File Manager and on the top task bar on the desktop, click on its icon and get into the preference menus. Another non-obvious step :( .
