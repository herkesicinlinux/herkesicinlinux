---
title: Basic Operations
---

## Basic Operations

Even experienced users can forget the precise command that launches an application, or exactly what options and arguments it requires. Fortunately, Linux allows you to quickly open applications using the graphical interface.

Applications are found at different places in Linux (and within GNOME):

- From the **Applications** menu in the upper-left corner
- From the **Activities** menu in the upper-left corner
- In some **Ubuntu** versions, from the Dash button in the upper-left corner
- For **KDE**, and some other environments, applications can be opened from the button in the lower-left corner.

On the following pages you will learn how to perform basic operations in Linux using the graphical Interface.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img19.png" />
  <figcaption>Opening Applications on CentOS</figcaption>
</figure>

<figure align="center">
  <img src="/en/img/lfs101/ch04/img20.png" />
  <figcaption>Opening Applications on openSUSE</figcaption>
</figure>

<figure align="center">
  <img src="/en/img/lfs101/ch04/img21.png" />
  <figcaption>Opening Applications on Ubuntu</figcaption>
</figure>

## Locating Applications

Unlike other operating systems, the initial install of Linux usually comes with a wide range of applications and software archives that contain thousands of programs that enable you to accomplish a wide variety of tasks with your computer. For most key tasks, a default application is usually already installed. However, you can always install more applications and try different options.

For example, Firefox is popular as the default browser in many Linux distributions, while Epiphany, Konqueror, and Chromium (the open source base for Google Chrome) are usually available for install from software repositories. Proprietary web browsers, such as Opera and Chrome, are also available.

Locating applications from the GNOME and KDE menus is easy, as they are neatly organized in functional submenus.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img22.png" />
  <figcaption>Locating Applications</figcaption>
</figure>

## Default Applications

Multiple applications are available to accomplish various tasks and to open a file of a given type. For example, you can click on a web address while reading an email and launch a browser such as Firefox or Chrome.

To set default applications, enter the **Settings** menu (on all recent Linux distributions) and then click on **Details->System->Default Applications**. The exact list will vary from what is shown here in the Ubuntu screenshot according to what is actually installed and available on your system.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img23.png" />
  <figcaption>Default Applications</figcaption>
</figure>

### Video: Setting Default Applications

Click <img src="/en/img/lfs101/common/play.png"/> below to view a demonstration of setting default applications on a Red Hat 7 system.

### Setting Default Applications

[Video](https://edx-video.net/LINILXXX2017-V002500_DTH.mp4) | [Subtitle](video-06-en.srt)

### Try-It-Yourself: Locating and Setting Default Applications

In this section, we will practice locating and setting default applications in your chosen distribution.

1. Please take a look at the following Try-It-Yourself exercise [open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefubuntu/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefubuntu/index.html" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

2. Please take a look at the following Try-It-Yourself exercise [open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefsuse/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/usingdefsuse/index.html" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## File Manager

Each distribution implements the **Nautilus** (**File Manager**) utility, which is used to navigate the file system. It can locate files and, when a file is clicked upon, either it will run if it is a program, or an associated application will be launched using the file as data. This behavior is completely familiar to anyone who has used other operating systems.

To start the file manager you will have to click on its icon (a file cabinet) which is easily found, usually under **Favorites** or **Accessories**. It will have the name **Files**.

This will open a window with your **Home** directory displayed. The left panel of the File Manager window holds a list of commonly used directories, such as **Desktop**, **Documents**, **Downloads** and **Pictures**.

You can click the _Magnifying_ Glass icon on the top-right to search for files or directories (folders).

<figure align="center">
  <img src="/en/img/lfs101/ch04/img24.png" />
  <figcaption>File Manager</figcaption>
</figure>

## Home Directories

The File Manager lets you access different locations on your computer and the network, including the Home directory, **Desktop**, **Documents**, **Pictures**, and other **Other Locations**.

Every user with an account on the system will have a home directory, usually created under **/home**, and usually named according to the user, such as **/home/student**.

By default, files the user saves will be placed in a directory tree starting there. Account creation, whether during system installation or at a later time, when a new user is added, also induces default directories to be created under the user's home directory, such as **Documents**, **Desktop**, and **Downloads**.

In the screenshot shown for Ubuntu, we have chosen the list format and are also showing hidden files (those starting with a period). See if you can do the same on your distribution.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img25.png" />
  <figcaption>Home Directories</figcaption>
</figure>

<figure align="center">
  <img src="/en/img/lfs101/ch04/img26.png" />
  <figcaption>Other Locations</figcaption>
</figure>

## Viewing Files

The File Manager allows you to view files and directories in more than one way.

You can switch between the Icons and List formats, either by clicking the familiar icons in the top bar, or you can press **CTRL-1** or **CTRL-2** respectively.

In addition, you can also arrange the files and directories by name, size, type, or modification date for further sorting. To do so, click **View** and select **Arrange Items**.

Another useful option is to show hidden files (sometimes imprecisely called system files), which are usually configuration files that are hidden by default and whose name starts with a dot. To show hidden files, select **Show Hidden Files** from the menu or press **CTRL-H**.

The file browser provides multiple ways to customize your window view to facilitate easy drag and drop file operations. You can also alter the size of the icons by selecting **Zoom In** and **Zoom Out** under the _View_ menu.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img27.png" />
  <figcaption>Viewing Files in openSUSE</figcaption>
</figure>

## Searching for Files

<img src="/en/img/lfs101/ch04/img28.png" align="right" class="invert"/>

The File Manager includes a great search tool inside the file browser window.

1. Click **Search** in the toolbar (to bring up a text box).
2. Enter the keyword in the text box. This causes the system to perform a recursive search from the current directory for any file or directory which contains a part of this keyword.

To open the _File Manager_ from the command line, on most systems simply type **nautilus**.
The shortcut key to get to the search text box is **CTRL-F**. You can exit the search text box view by clicking the Search button or **CTRL-F** again.

Another quick way to access a specific directory is to press **CTRL-L**, which will give you a **Location** text box to type in a path to a directory.

### More About Searching for Files

You can refine your search beyond the initial keyword by providing dropdown menus to further filter the search.

1. Based on **Location** or **File Type**, select additional criteria from the dropdown.
2. To regenerate the search, click the **Reload** button.
3. To add multiple search criteria, click the + button and select _Additional Search Criteria_.

For example, if you want to find a PDF file containing the word **Linux** in your home directory, navigate to your **home** directory and search for the word “Linux”. You should see that the default search criterion limits the search to your **home** directory already. To finish the job, click the **+** button to add another search criterion, select **File Type** for the type of criterion, and select **PDF** under the **File Type** dropdown.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img29.png" />
  <figcaption>Searching of Files</figcaption>
</figure>

## Editing a File

<img src="/en/img/lfs101/ch04/img30.png" align="right" />

Editing any text file through the graphical interface is easy in the GNOME desktop environment. Simply double-click the file on the desktop or in the Nautilus file browser window to open the file with the default text editor.

The default text editor in GNOME is **gedit**. It is simple yet powerful, ideal for editing documents, making quick notes, and programming. Although **gedit** is designed as a general purpose text editor, it offers additional features for spell checking, highlighting, file listings and statistics.

You will learn much more about using text editors in a later chapter.

## Removing a File

<img src="/en/img/lfs101/ch04/img31.png" align="right" class="invert"/>

Deleting a file in Nautilus will automatically move the deleted files to the **.local/share/Trash/files/** directory (a trash can of sorts) under the user's home directory. There are several ways to delete files and directories using Nautilus.

1. Select all the files and directories that you want to delete
2. Press **CTRL-Delete** on your keyboard, or right-click the file
3. Select **Move to Trash**.

Note that you may have a **Delete Permanently** option which bypasses the trash folder, and that this option may be visible all the time or only in list (rather than) icon mode.

### More About Removing a File

To _permanently_ delete a file:

1. On the left panel inside a Nautilus file browser window, right-click on the **Trash** directory.
2. Select _Empty_ Trash.

Alternatively, select the file or directory you want to permanently delete and press **Shift-Delete**.

As a precaution, you should never delete your _Home_ directory, as doing so will most likely erase all your GNOME configuration files and possibly prevent you from logging in. Many personal system and program configurations are stored under your home directory.

## Video: Locating and Setting Default Applications, and Exploring Filesystems in openSUSE

Click <img src="/en/img/lfs101/common/play.png" /> below to view demonstrations of locating and setting default applications and exploring filesystems in openSUSE.

### Locating and Setting Default Applications, and Exploring Filesystems in openSUSE

[Video](https://edx-video.net/LINILXXX2017-V001000_DTH.mp4) | [Subtitle](video-07-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Labs

### Lab 4.2: Viewing File sort options

Find the latest modified file in **/var/log**.

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-filesort)

### Lab 4.3: Recovering Deleted Files

The basic operations will be the same here, whether you have a GNOME or KDE desktop, although exact procedures and choices may differ slightly.

1. Create a new text file on the desktop named **lab.txt**, using the graphical file manager.
2. Delete the file by sending it to **Trash**.
3. Verify the file is now in **~/.local/share/Trash**, or a subdirectory thereof. **_Note_**: You will have to get your file browser to show hidden files and directories, those that start with a **.)**.
4. Recover the file and make sure it is in its original location.

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-recoverfiles)
