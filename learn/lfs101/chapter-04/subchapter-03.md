---
title: Session Management
---

## Logging In and Out

<img src="/en/img/lfs101/ch04/img12.png" align="right" class="invert"/>

The next few screens cover the demonstrations and Try-It-Yourself activity for members of each of the three Linux distribution families we cover in this course. You can view a demonstration for the distribution type of your choice and practice the procedure through the relevant Try-It-Yourself activity.

### Video: Logging In and Logging Out Using the GUI in Ubuntu, openSUSE and CentOS

Click <img src="/en/img/lfs101/common/play.png"/> below to view a demonstration on how to log in and log out from the graphical desktop manager on three Enterprise Linux distributions: CentOS 7, openSUSE, and Ubuntu 18.04. Experiences would be essentially identical on other recent GNOME-based Linux distributions, including Fedora, Debian and Gentoo.

### Logging In and Logging Out Using the GUI in Ubuntu, openSUSE and CentOS

[Video](https://edx-video.net/LINILXXX2017-V000700_DTH.mp4) | [Subtitle](video-04-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Try-It-Yourself: Logging In and Logging Out

In this section, we will practice logging in and logging out using the GUI openSUSE.

**_Note:_** We have not shown the other distributions, as they are all almost exactly the same.

Please take a look at the following Try-It-Yourself exercise [or open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/loginsuse/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/loginsuse/index.html" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Locking the Screen

It is often a good idea to lock your screen to prevent other people from accessing your session while you are away from your computer.

**_Note:_** This does not suspend the computer; all your applications and processes continue to run while the screen is locked.

There are two ways to lock your screen:

- Using the graphical interface
  Clicking in the upper-right corner of the desktop, and then clicking on the lock icon.
- Using the keyboard shortcut **SUPER-L**
  (The **SUPER** key is also known as the Windows key).
  The keyboard shortcut for locking the screen can be modified by altering keyboard settings, the exact prescription varying by distribution, but not hard to ascertain.

To re-enter the desktop session you just need to provide your password again.

The screenshot below shows how to lock the screen for Ubuntu. The details vary little in modern distributions.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img13.png" />
  <figcaption>Locking the Screen for Ubuntu</figcaption>
</figure>

### Locking and Unlocking the Screen in More Detail

To lock and unlock your screen in openSUSE, perform the following steps:

1. Click the power icon on the upper-right corner of the desktop.
2. Click the lock icon. The screen is locked immediately.
3. Press Enter. The login screen is displayed.
4. To unlock the screen, enter the password.
5. Click Unlock and the desktop screen is displayed.

**_Note:_** When you lock the screen, GNOME will blank the screen or run a screensaver, depending on your settings.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img14.png" />
  <figcaption>Locking and Unlocking Screen in openSUSE</figcaption>
</figure>

## Switching Users

<img src="/en/img/lfs101/ch04/img15.png" align="right" class="invert"/>

Linux is a true multi-user operating system, which allows more than one user to be simultaneously logged in. If more than one person uses the system, it is best for each person to have their own user account and password. This allows for individualized settings, home directories, and other files. Users can take turns using the machine, while keeping everyone's sessions alive, or even be logged in simultaneously through the network.

**_Note:_** The next few screens cover the demonstrations and Try-It-Yourself activity for members of each of the three Linux distribution families we cover in this course. You can view a demonstration for the distribution type of your choice and practice the procedure through the relevant Try-It-Yourself activity.

### Video: Switching Users in Ubuntu

Click <img src="/en/img/lfs101/common/play.png"/> below to view a demonstration on how to switch users while running an Ubuntu machine. The steps are essentially the same on all Linux distributions.

### Switching Users in Ubuntu

[Video](https://edx-video.net/LINILXXX2017-V000800_DTH.mp4) | [Subtitle](video-05-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Try-It-Yourself: Switching Users

In this section, we will practice switching users in Ubuntu.

**_Note:_** We have not shown the other distributions, as they are all almost exactly the same.

Please take a look at the following Try-It-Yourself exercise [or open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/switchuserubuntu/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/switchuserubuntu/index.html" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## Shutting Down and Restarting

<img src="/en/img/lfs101/ch04/img16.png" align="right" class="invert"/>

Besides normal daily starting and stopping of the computer, a system restart may be required as part of certain major system updates, generally only those involving installing a new Linux kernel.

Initiating the shutdown process from the graphical desktop is rather trivial on all current Linux distributions, with very little variation. We will discuss later how to do this from the command line, using the **shutdown** command.

In all cases, you click on either a settings (gear) or a power icon and follow the prompts. We will only show the detail for the Ubuntu Linux distribution.

### Shutting Down and Restarting in Ubuntu

To shut down the computer in any recent GNOME-based Linux distribution, perform the following steps:

1. Click either the **Power** or the **Gear** icon in the upper-right corner of the screen.
2. Click on **Power Off**, **Restart**, or **Cancel**. If you do nothing, the system will shutdown in 60 seconds.

Shutdown, reboot, and logout operations will ask for confirmation before going ahead. This is because many applications will not save their data properly when terminated this way.

Always save your documents and data before restarting, shutting down, or logging out.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img17.png" />
  <figcaption>Shutting Down and Restarting</figcaption>
</figure>

## Suspending

All modern computers support **Suspend** (or **Sleep**) **Mode** when you want to stop using your computer for a while. _Suspend Mode_ saves the current system state and allows you to resume your session more quickly while remaining on, but uses very little power in the sleeping state. It works by keeping your system’s applications, desktop, and so on, in system RAM, but turning off all of the other hardware. This shortens the time for a full system start-up as well as conserves battery power. One should note that modern Linux distributions actually boot so fast that the amount of time saved is often minor.

### Suspending the System

To suspend the system, the procedure starts the same as that for shutdown or locking the screen.

The method is quite simple and universal in recent GNOME-based distributions. If you click on the **Power** icon and hold for a short time and release, you will get the double line icon displayed below, which you then click to suspend the system.

**_Note:_** To wake your system and resume your session, move the mouse or press any button on the keyboard. The system will wake up with the screen locked, just as if you had manually locked it; type in your password to resume.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img18.png" class="invert"/>
  <figcaption>Suspending the System</figcaption>
</figure>
