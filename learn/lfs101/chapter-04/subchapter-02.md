---
title: Graphical Desktop
---

## Graphical Desktop

You can use either a **C**ommand **L**ine **I**nterface (**CLI**) or a **G**raphical **U**ser **I**nterface (**GUI**) when using Linux. To work at the CLI, you have to remember which programs and commands are used to perform tasks, and how to quickly and accurately obtain more information about their use and options. On the other hand, using the GUI is often quick and easy. It allows you to interact with your system through graphical icons and screens. For repetitive tasks, the CLI is often more efficient, while the GUI is easier to navigate if you do not remember all the details or do something only rarely.

We will learn how to manage sessions using the GUI for the three Linux distribution families that we cover the most in this course: Red Hat (CentOS, Fedora), SUSE (openSUSE), and Debian (Ubuntu, Mint). Since we are using the GNOME-based variant of openSUSE rather than the KDE-based one, all are actually quite similar. If you are using KDE (or other Linux desktops such as XFCE), your experience will vary somewhat from what is shown, but not in any intrinsically difficult way, as user interfaces have converged to certain well-known behaviors on modern operating systems. In subsequent sections of this course we will concentrate in great detail on the command line interface, which is pretty much the same on all distributions.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img02.png" />
  <figcaption>Ubuntu, CentOS, and openSUSE Desktops</figcaption>
</figure>

## X Window System

Generally, in a Linux desktop system, the X Window System is loaded as one of the final steps in the boot process. It is often just called X.

A service called the **Display Manager** keeps track of the displays being provided and loads the X server (so-called, because it provides graphical services to applications, sometimes called X clients). The display manager also handles graphical logins and starts the appropriate desktop environment after a user logs in.

X is rather old software; it dates back to the mid 1980s and, as such, has certain deficiencies on modern systems (for example, with security), as it has been stretched rather far from its original purposes. A newer system, known as [Wayland](https://wayland.freedesktop.org/), is gradually superseding it and is the default display system for Fedora, RHEL 8, and other recent distributions. For the most part, it looks just like X to the user, although under the hood it is quite different.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img03.png" class="invert"/>
  <figcaption>Display Manager</figcaption>
</figure>

### More About X

A desktop environment consists of a session manager, which starts and maintains the components of the graphical session, and the window manager, which controls the placement and movement of windows, window title-bars, and controls.

Although these can be mixed, generally a set of utilities, session manager, and window manager are used together as a unit, and together provide a seamless desktop environment.

If the display manager is not started by default in the default runlevel, you can start the graphical desktop different way, after logging on to a text-mode console, by running **startx** from the command line. Or, you can start the display manager (**gdm**, **lightdm**, **kdm**, **xdm**, etc.) manually from the command line. This differs from running **startx** as the display managers will project a sign in screen. We discuss them next.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img04.png" class="invert"/>
  <figcaption>Desktop Environment</figcaption>
</figure>

## GUI Startup

<img src="/en/img/lfs101/ch04/img05.png" align="right" class="invert"/>

When you install a desktop environment, the X display manager starts at the end of the boot process. It is responsible for starting the graphics system, logging in the user, and starting the user’s desktop environment. You can often select from a choice of desktop environments when logging in to the system.

The default display manager for GNOME is called **gdm**. Other popular display managers include **lightdm** (used on Ubuntu before version 18.04 LTS) and **kdm** (associated with KDE).

## GNOME Desktop Environment

<img src="/en/img/lfs101/ch04/img06.png" align="right" class="invert"/>

GNOME is a popular desktop environment with an easy-to-use graphical user interface. It is bundled as the default desktop environment for most Linux distributions, including Red Hat Enterprise Linux (RHEL), Fedora, CentOS, SUSE Linux Enterprise, Ubuntu and Debian. GNOME has menu-based navigation and is sometimes an easy transition to accomplish for Windows users. However, as you will see, the look and feel can be quite different across distributions, even if they are all using GNOME.

Another common desktop environment very important in the history of Linux and also widely used is KDE, which has often been used in conjunction with SUSE and openSUSE. Other alternatives for a desktop environment include Unity (present on older Ubuntu, but still based on GNOME), XFCE and LXDE. As previously mentioned, most desktop environments follow a similar structure to GNOME, and we will restrict ourselves mostly to it to keep things less complex.

## Video: System Startup and Logging In and Out

Click <img src="/en/img/lfs101/common/play.png"/> below to watch a demonstration of how the system starts up and reaches user login on an openSUSE system. The procedures for boot, logging in and logging out vary only in minor cosmetic ways on all recent distributions, including Red Hat/CentOS and Ubuntu/Debian. We will also show examples for other distributions later.

### System Startup and Logging In and Out

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V007800_DTH.mp4) | [Subtitle](video-02-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Graphical Desktop Background

<img src="/en/img/lfs101/ch04/img08.png" align="right" class="invert"/>

Each Linux distribution comes with its own set of desktop backgrounds. You can change the default by choosing a new wallpaper or selecting a custom picture to be set as the desktop background. If you do not want to use an image as the background, you can select a color to be displayed on the desktop instead.

In addition, you can also change the desktop theme, which changes the look and feel of the Linux system. The theme also defines the appearance of application windows.

We will learn how to change the desktop background and theme.

### Customizing the Desktop Background

To change the background, you can right click anywhere on the desktop and choose **Change Background**.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img09.png" />
  <figcaption>How to Customize the Desktop Background</figcaption>
</figure>

### Video: How to Change the Desktop Background

Click <img src="/en/img/lfs101/common/play.png"/> below to watch a demonstration on how to change the background in Red Hat 7/CentOS 7.

**Note**: All recent GNOME-based desktops handle this identically.

### How to Change the Desktop Background

[Video](https://edx-video.net/LINILXXX2017-V000600_DTH.mp4) | [Subtitle](video-03-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

### Try-It-Yourself: Changing the Desktop Background

In this section, we will practice changing the desktop background in openSUSE. The exact details may vary very slightly for your Linux distribution.

Please take a look at the following Try-It-Yourself exercise [or open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/backgroundopensuse/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-04/backgroundopensuse/index.html" 
  width="100%"
  height="900px"
  frameborder="0">
</iframe>

## gnome-tweak-tool

Most common settings, both personal and system-wide, are to be found by clicking in the upper right-hand corner, on either a gear or other obvious icon, depending on your Linux distribution.

However, there are many settings which many users would like to modify which are not thereby accessible; the default settings utility is unfortunately rather limited in modern GNOME-based distributions. Unfortunately, the quest for simplicity has actually made it difficult to adapt your system to your tastes and needs.

Fortunately, there is a standard utility, gnome-tweak-tool, which exposes many more setting options. It also permits you to easily install extensions by external parties. Not all Linux distributions install this tool by default, but it is always available. Some recent distributions have renamed this tool as **gnome-tweaks**. You may have to run it by hitting **Alt-F2** and then typing in the name. You may want to add it to your **Favorites** list as we shall discuss.

In the screenshot below, the keyboard mapping is being adjusted so the useless **CapsLock** key can be used as an additional **Ctrl** key; this saves users who use **Ctrl** a lot (such as **emacs** aficionados) from getting physically damaged by pinkie strain.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img10.png" />
  <figcaption>gnome-tweak-tool</figcaption>
</figure>

## Changing the Theme

The visual appearance of applications (the buttons, scroll bars, widgets, and other graphical components) are controlled by a theme. GNOME comes with a set of different themes which can change the way your applications look.

The exact method for changing your theme may depend on your distribution. However, for all GNOME-based distributions, you can simply run **gnome-tweak-tool**, as shown in the screenshot from Ubuntu.

There are other options to get additional themes beyond the default selection. You can download and install themes from the [GNOME's Wiki](https://wiki.gnome.org/Personalization) website.

<figure align="center">
  <img src="/en/img/lfs101/ch04/img11.png" />
  <figcaption>Changing the Theme</figcaption>
</figure>

## Labs

### Lab 4.1: Customizing the Desktop

Despite the length of this section, we will not do very elaborate step-by-step lab exercises, because of the diversity of Linux distributions and versions, and because they each customize their desktops, even if the underlying code base is the same. Trying to give exact instructions is an exercise in futility; not only are there many variations, they are susceptible to change every time a new version of a Linux distribution is released.

For the most part, this is not a problem. Graphical interfaces are designed to be easy to navigate and figure out, and they really do not vary very much, not only from one distribution to another, but even between operating systems. So, the only way you can get more adept at working efficiently on your desktop is to simply explore, play, and modify. The same points will apply to the next chapter, on graphical system configuration.

Linux is so customizable that very few people who use it stay with the default look and feel of the desktop. You may as well get started now in making your desktop reflect your likes and personality.

- Start by changing the desktop background to something that better suits yours tastes; perhaps one of the provided backgrounds, a solid color of your choice, or a personal picture that you can transfer onto your Linux environment.
- Next, select a theme from the available themes for your distribution that, again, suits your tastes and personality. Have fun and explore with this exercise.

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-desktop)
