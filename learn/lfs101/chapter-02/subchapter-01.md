---
title: Introduction and Learning Objectives
---

## Video: Chapter 2 Introduction

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V003600_DTH.mp4) | [Subtitle](video-01-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Video: The Power of Linux

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V010000_DTH.mp4) | [Subtitle](video-02-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Three Important Pieces of Context

Please keep in mind the following:

1. **Things change in Linux**

   No matter how hard we have worked to stay current, Linux is constantly evolving, both at the technical level (including kernel features) and at the distribution and interface level. So, please keep in mind we have tried to be as up-to-date as possible at the time this course was released, but there may be changes and new features we have not discussed. This is unavoidable.

2. **We have repeated some things in the class material**

   It is just about impossible in a course this comprehensive to never revisit topics that have previously been covered, and short reviews are helpful so you do not have to go scouring through earlier sections to jog your memory. This is particularly true with system configuration items, like how to use sudo to gain temporary root privileges in as safe a manner as possible. We know we have done this and, at least in most cases, it is by design, not by accident.

3. **We have tried to avoid holy wars**

   There are many areas where there are strong preference disagreements in the Linux (and wider open source) community. Examples include the best editor: emacs vs. vi; the best graphical desktop: GNOME vs. KDE, etc. Usually, we have chosen (when necessary) a particular alternative to emphasize just to keep things clean. For example, we talk much more about GNOME than KDE simply because it has a bigger user base, not because we are taking a position as to which is superior.

## Final Thoughts

In order for you to get the most out of this course, we recommend that you have Linux installed on a machine that you can use throughout this course. You do not need to view the course material on a Linux machine (all you need is a browser). However, you will see that there is a lot of follow-along activities and labs that you will benefit from only if you can do them on your own machine. We have prepared a brief installation guide: "_Preparing Your Computer for LFS101x.2_", that helps you to select a Linux distribution to install, decide on whether you want to do a stand-alone pure Linux machine or a dual-boot one, whether do a physical or virtual install, etc. And then guides through the steps. We will also discuss the installation procedure in detail in a later section.

We have not covered everything in great detail, but keep in mind that most of the documentation in Linux is actually already on your system in the form of man pages, which we will discuss in great detail later. Whenever you do not understand something or want to know more about a command, program, topic, or utility, you can just type **`man topic`** at the command line. We will assume you are thinking this way and not constantly repeat "For more information, look at the man page for **`topic`**.

On a related note, throughout the course we use a shorthand that is common in the open source community. When referring to cases where the user has to make a choice of what to enter (e.g. name of a program or file), we use the short hand '**foo**' to represent **insert file name here**. So beware, we are not actually suggesting that you manipulate files or install services called '**foo**'!

## Video: Final Thoughts

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V007300_DTH.mp4) | [Subtitle](video-03-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## Learning Objectives

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

By the end of this chapter, you should be able to:

- Discuss the history and philosophy of Linux.
- Describe the Linux community.
- Define the common terms associated with Linux.
- Discuss the components of a Linux distribution.
