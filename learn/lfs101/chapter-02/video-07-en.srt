0
00:00:00,738 --> 00:00:19,548
Before you begin using Linux, you need to be aware of some basic terms such as kernel, distribution, boot loader, service, filesystem, X Window system, desktop environment, and command line. These are very commonly used by the Linux community.

1
00:00:20,028 --> 00:00:39,478
The kernel is considered the brain of the Linux operating system. It controls the hardware and makes the hardware interact with the applications. An example of a kernel is the Linux kernel. The most recent Linux kernel, along with past Linux kernels, can be found at the kernel.org web site.

2
00:00:40,142 --> 00:00:57,280
A distribution also known as Distros is a collection of programs combined with the Linux kernel to make up a Linux-based operating system. Some common examples of a distribution are Red Hat Enterprise Linux, Fedora, Ubuntu, and Gentoo.

3
00:00:58,215 --> 00:01:07,387
The boot loader, as the name implies, is a program that boots the operating system. Two examples of a boot loader are GRUB and ISOLINUX.

4
00:01:08,100 --> 00:01:21,894
A service is a program that runs as a background process. Some examples of the service are httpd, nfsd, ntpd, ftpd and named.

5
00:01:22,656 --> 00:01:37,115
A filesystem is a method for storing and organizing files in Linux. Some examples of filesystems are ext3, ext4, FAT, XFS and Btrfs.

6
00:01:37,705 --> 00:01:45,770
The X Window System provides the standard toolkit and protocol to build graphical user interfaces on nearly all Linux systems.

7
00:01:46,360 --> 00:01:58,999
The desktop environment is a graphical user interface on top of the operating system. GNOME, KDE, Xfce and Fluxbox are some examples of the desktop environment.

8
00:01:59,847 --> 00:02:04,544
The command line is an interface for typing commands on top of the operating system.

9
00:02:05,884 --> 00:02:20,859
The Shell is the command line interpreter that interprets the command line input and instructs the operating system to perform any necessary tasks and commands. For example, bash, tcsh and zsh.

