---
title: Linux Terminology
---

## Introduction

<img src="/en/img/lfs101/ch02/img07.png" align="right" />

When you start exploring Linux, you will soon come across some terms which may be unfamiliar, such as distribution, boot loader, desktop environment, etc. Before we proceed further, let's stop and take a look at some basic terminology used in Linux to help you get up to speed.

## Video: Linux Terminology

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V006600_DTH.mp4) | [Subtitle](video-07-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->
