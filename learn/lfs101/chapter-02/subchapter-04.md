---
title: Linux Community
---

## Introduction

<img src="/en/img/lfs101/ch02/img06.png" align="right" />

Suppose that, as part of your job, you need to configure a Linux file server, and you run into some difficulties. If you are not able to figure out the answer yourself or get help from a co-worker, the Linux community might just save the day!

There are many ways to engage with the Linux community:

- Post queries on relevant discussion forums.
- Subscribe to discussion threads.
- Join local Linux groups that meet in your area.

## Video: Linux Community

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V009900_DTH.mp4) | [Subtitle](video-06-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## More About Linux Community

<img src="/en/img/lfs101/ch02/img06.png" align="right" />

The Linux community is a far-reaching ecosystem consisting of developers, system administrators, users and vendors who use many different forums to connect with one another. Among the most popular are:

- Internet Relay Chat (IRC) software (such as WeeChat, HexChat, Pidgin and XChat)
- Online communities and discussion boards including Linux User Groups (both local and online)
- Many collaborative projects hosted on services such as GitHub
- Newsgroups and mailing lists, including the Linux Kernel Mailing List
- Community events, e.g. Hackathons, Install Fests, Open Source Summits and Embedded Linux Conferences.

A portal to one of the most powerful online user communities can be found at [linux.com](https://www.linux.com/). This site is hosted by The Linux Foundation and serves over one million unique visitors every month. It has active sections on:

- News
- Community discussion threads
- Free tutorials and user tips.

We will refer several times in this course to relevant articles or tutorials on this site.
