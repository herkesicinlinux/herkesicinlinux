---
title: Linux Philosophy
---

## Introduction

<img src="/en/img/lfs101/ch02/img05.png" align="right" />

Every successful project or organization needs an implicit or explicit philosophy that frames its objectives and projects its growth path. This section contains a description of the "Linux philosophy" and how it has impacted Linux's amazing evolution.

Linux is constantly enhanced and maintained by a network of developers from all over the world collaborating over the Internet, with Linus Torvalds at the head. Technical skill, a desire to contribute, and the ability to collaborate with others are the only qualifications for participating.

## Linux Philosophy

Linux borrows heavily from the well-established UNIX operating system. It was written to be a free and open source system to be used in place of UNIX, which at the time was designed for computers much more powerful than PCs and was quite expensive. Files are stored in a hierarchical filesystem, with the top node of the system being the **root** or simply "**/**". Whenever possible, Linux makes its components available via files or objects that look like files. Processes, devices, and network sockets are all represented by file-like objects, and can often be worked with using the same utilities used for regular files. Linux is a fully multitasking (i.e. multiple threads of execution are performed simultaneously), multiuser operating system, with built-in networking and service processes known as daemons in the UNIX world.

Note: Linux was inspired by UNIX, but it is not UNIX.

## Video: How Linux Is Built

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V008200_DTH.mp4) | [Subtitle](video-05-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->
