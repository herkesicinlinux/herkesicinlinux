---
title: Linux History
---

## Introduction

<img src="/en/img/lfs101/ch02/img02.png" align="right" />

Linux is an open source computer operating system, initially developed on and for Intel x86-based personal computers. It has been subsequently ported to an astoundingly long list of other hardware platforms, from tiny embedded appliances to the world's largest supercomputers.

In this section, we follow the surprising history of how Linux evolved from a project of one Finnish college student, into a massive effort with an enormous impact on today's world.

## Linux History

Linus Torvalds was a student in Helsinki, Finland, in 1991, when he started a project: writing his own operating system **kernel**. He also collected together and/or developed the other essential ingredients required to construct an entire operating system with his kernel at the center. It wasn't long before this became known as the Linux kernel.

In 1992, Linux was re-licensed using the **General Public License** (**GPL**) by **GNU** (a project of the Free Software Foundation or FSF, which promotes freely available software), which made it possible to build a worldwide community of developers. By combining the kernel with other system components from the GNU project, numerous other developers created complete systems called Linux distributions in the mid-90’s.

<img src="/en/img/lfs101/ch02/img03.png" />

## Video: Welcome to the World of Linux

[Video](https://edx-video.net/LINLFS10/LINLFS102014-V006400_DTH.mp4) | [Subtitle](video-04-en.srt)

<!-- <iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/VIDEO_ID?texttrack=LANG_CODE"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe> -->

## More About Linux History

<img src="/en/img/lfs101/ch02/img04.png" align="right" class="invert"/>

The Linux distributions created in the mid-90s provided the basis for fully free (in the sense of freedom, not zero cost) computing and became a driving force in the open source software movement. In 1998, major companies like IBM and Oracle announced their support for the Linux platform and began major development efforts as well.

Today, Linux powers more than half of the servers on the Internet, the majority of smartphones (via the Android system, which is built on top of Linux), and all of the world’s most powerful supercomputers.
