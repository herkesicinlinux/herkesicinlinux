﻿1
00:00:07,254 --> 00:00:10,954


2
00:00:15,245 --> 00:00:17,053


3
00:00:17,111 --> 00:00:24,902
There are a ton of Linux jobs available on the market today and there are not enough system administrators and developers to fulfill that demand.

4
00:00:24,902 --> 00:00:32,136
The Linux foundation exists to advance the platform and make sure that there is a strong pool of talent, to meet the markets needs.

5
00:00:32,236 --> 00:00:39,385
We are here to teach them what’s in Linux and how they can quickly start using it and how they can make a contribution as well.

6
00:00:39,485 --> 00:00:47,349
And, it’s because of the scattered nature of a lot of things on Linux it would be difficult for people to assemble all that material on their own.

7
00:00:47,678 --> 00:00:54,682
Taking the course here, landed me the lead spot in the Linux team.

8
00:00:54,712 --> 00:00:57,954
You have a task to do and we want to help you to do it quickly.

9
00:00:58,169 --> 00:01:08,377
I have learned a lot of shortcuts, a lot of ways that I could be watching and monitoring server usage and IO usage.

10
00:01:09,093 --> 00:01:11,021


11
00:01:11,845 --> 00:01:20,019
One is we are vendor neutral, we don’t particularly promote one variety of Linux over another one…distribution or whatever.

12
00:01:20,167 --> 00:01:23,078
We try to make sure that everything will work on every distribution.

13
00:01:23,178 --> 00:01:25,834
Secondly, we keep everything extremely up to date.

14
00:01:25,934 --> 00:01:31,175
For instance, all our kernel level classes we issue a new version of the material every time a new kernel comes out.

15
00:01:31,201 --> 00:01:35,432
The one thing that shocked me was because the kernel moved so fastly so quickly,

16
00:01:35,532 --> 00:01:42,516
that he was up to date and knew exactly where things were and what was already (you know) dated in his manual.

17
00:01:42,596 --> 00:01:50,464
The third thing is that because the foundation has a strong connection with a lot of leading kernel developers,

18
00:01:50,564 --> 00:01:54,314
we got a very good idea of (you know) what’s going on in the community.

19
00:01:54,414 --> 00:02:00,679
We have some of those people review our material and make suggestions and contribute to development of them.

20
00:02:00,779 --> 00:02:02,812


21
00:02:03,149 --> 00:02:11,872
We try to keep the classes at about fifty percent hands-on exercise time and about fifty percent lecture time.

22
00:02:11,892 --> 00:02:19,712
Jerry was wonderful. He gave us his email so we could contact him if we had other questions; which I did.

23
00:02:19,714 --> 00:02:24,456
When students take the class, they get detailed courseware manuals.

24
00:02:24,556 --> 00:02:35,453
They can be rather fat books, quite a few hundred pages in some cases that are not just a series of slides that give bullet points but are detailed books.

25
00:02:35,667 --> 00:02:41,423
I am glad it comes with a booklet because this is something (you know) if you do not use it you forget.

26
00:02:41,523 --> 00:02:51,725
But if something comes up in a few years, I can open a booklet and remember and say, hey we can do this without hiring a consultant.

27
00:02:52,716 --> 00:02:55,266


28
00:02:55,876 --> 00:02:59,426
You tap into the pool of the Linux foundation resources.

29
00:02:59,526 --> 00:03:03,451
Our summits, our events, our materials, additional white papers.

30
00:03:03,551 --> 00:03:12,840
We are a resource for you to grow your career and your network and integrate into both the professional community as well as the developer community for open source.

31
00:03:12,991 --> 00:03:24,536
Well I was struck that I was contacted afterwards by the Linux Foundation, by Mike Wooster, who was actually (you know) helping corporations fill positions for kernel developers

32
00:03:26,928 --> 00:03:30,240


