---
title: Introduction and Learning Objectives
---

## Video: Chapter 1 Introduction

<!-- [Video](https://player.vimeo.com/video/509178924?texttrack=en) | [Subtitle](video-01-en.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509178924?texttrack=en" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>

## Learning Objectives

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

By the end of this chapter, you should be able to:

- Discuss the role of The Linux Foundation.
- Appreciate the learning opportunities provided by The Linux Foundation's training program.
- Describe the software environment required for this course.
- Describe the three major Linux distribution families.
