---
title: The Linux Foundation Training
---

## The Linux Foundation Training

Although we believe in individual initiative and creativity, learning Linux and open source technologies need not be intimidating. You can leverage the power of collaboration to jump start your learning. Our classes build the critical skills that individuals and organizations need to get the most out of Linux and open source technologies and continue with self-directed learning.

The Linux Foundation offers several types of training:

- Classroom
- Online
- On-Site
- Events-based.

## Training That Builds Skills

[The Linux Foundation training](https://training.linuxfoundation.org/) is for the community and is designed by members of the community. The System Administration courses focus on Enterprise Linux environments and target system administrators, technical support, and architects. The Developer courses feature instructors and content straight from the leaders of the open source developer community.

To get more information about specific courses offered by The Linux Foundation visit our catalogue:

- Enterprise IT & Linux System Administration Courses
- Linux Programming & Development Courses
- Open Source Compliance Courses
- And many more.

The Linux Foundation training is distribution-flexible, technically advanced, and created with the actual leaders of the open source development community themselves. Most courses are more than 50% focused on hands-on labs and activities to build real world skills.

## Video: The Linux Foundation Training Options

<!-- [Video](https://player.vimeo.com/video/509219308?texttrack=en) | [Subtitle](video-03-en.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509219308?texttrack=en" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>
