---
title: Course Linux Requirements
---

## Course Software Requirements

In order to fully benefit from this course, you will need to have at least one Linux distribution installed (if you are not already familiar with the term distribution, as it relates to Linux, you soon will be!).

On the next page, you will learn some more details about the many available Linux distributions and the families they can be considered to belong to. Because there are literally hundreds of distributions, we have not covered them all in this course. Instead, we have decided to focus on the three major distribution families, and we have chosen distributions from within each family to use for illustrations, examples, and exercises. This is not meant to suggest that we endorse these specific distributions; they were simply chosen because they are fairly widely used and each is broadly representative of its respective family.

The families and representative distributions we are using are:

- **Red Hat Family Systems** (including **CentOS** and **Fedora**)
- **SUSE Family Systems** (including **openSUSE**)
- **Debian Family Systems** (including **Ubuntu** and **Linux Mint**).

<figure>
  <img src="/en/img/lfs101/ch01/img04.png"/>
  <figcaption>Ubuntu, CentOS, and openSUSE Desktops</figcaption>
</figure>

## Focus on Three Major Linux Distribution Families

In the next chapter, you will learn about the components that make up a Linux distribution.

For now, what you need to know is that this course focuses on the three major Linux distribution families that currently exist. However, as long as there are talented contributors, the families of distributions and the distributions within these families will continue to change and grow. People see a need, and develop special configurations and utilities to respond to that need. Sometimes that effort creates a whole new distribution of Linux. Sometimes, that effort will leverage an existing distribution to expand the members of an existing family.

For a rather long list of available distributions, see [The LWN.net Linux Distribution List](https://lwn.net/Distributions/).

<figure align="center">
  <img src="/en/img/lfs101/ch01/img05.png" class="invert"/>
  <figcaption>The Linux Kernel Distribution Families and Individual Distributions</figcaption>
</figure>

## The Red Hat Family

<img src="/en/img/lfs101/ch01/img06.png" class="invert" align="right"/>

Red Hat Enterprise Linux (RHEL) heads the family that includes CentOS, Fedora and Oracle Linux.

Fedora has a close relationship with RHEL and contains significantly more software than Red Hat's enterprise version. One reason for this is that a diverse community is involved in building Fedora, with many contributors who do not work for Red Hat. Furthermore, it is used as a testing platform for future RHEL releases.

In this course, CentOS is often used for activities, demos, and labs because it is available at no cost to the end user and has a much longer release cycle than Fedora (which releases a new version every six months or so).

The basic version of CentOS is also virtually identical to RHEL, the most popular Linux distribution in enterprise environments.

### Key Facts About the Red Hat Family

Some of the key facts about the Red Hat distribution family are:

- Fedora serves as an upstream testing platform for RHEL.
- CentOS is a close clone of RHEL, while Oracle Linux is mostly a copy with some changes (in fact, CentOS has been part of Red Hat since 2014).
- A heavily patched version 3.10 kernel is used in RHEL/CentOS 7, while version 4.18 is used in RHEL/CentOS 8.
- It supports hardware platforms such as Intel x86, Arm, Itanium, PowerPC, and IBM System z.
- It uses the yum and dnf RPM-based yum package managers (covered in detail later) to install, update, and remove packages in the system.
- RHEL is widely used by enterprises which host their own systems.

## The SUSE Family

<img src="/en/img/lfs101/ch01/img07.png" class="invert" align="right"/>

The relationship between SUSE (SUSE Linux Enterprise Server (SLES)) and openSUSE is similar to the one described between RHEL, CentOS, and Fedora.

We use openSUSE as the reference distribution for the SUSE family, as it is available to end users at no cost. Because the two products are extremely similar, the material that covers openSUSE can typically be applied to SLES with few problems.

### Key Facts About the SUSE Family

Some of the key facts about the SUSE family are listed below:

- SUSE Linux Enterprise Server (SLES) is upstream for openSUSE.
- Kernel version 4.12 is used in openSUSE Leap 15.
- It uses the RPM-based zypper package manager (we cover it in detail later) to install, update, and remove packages in the system.
- It includes the YaST (Yet Another Setup Tool) application for system administration purposes.
- SLES is widely used in retail and many other sectors.

## The Debian Family

<img src="/en/img/lfs101/ch01/img08.png" class="invert" align="right"/>

The Debian distribution is upstream for several other distributions, including Ubuntu. In turn, Ubuntu is upstream for Linux Mint and a number of other distributions. It is commonly used on both servers and desktop computers. Debian is a pure open source community project (not owned by any corporation) and has a strong focus on stability.

Debian provides by far the largest and most complete software repository to its users of any Linux distribution.

Ubuntu aims at providing a good compromise between long term stability and ease of use. Since Ubuntu gets most of its packages from Debian’s stable branch, it also has access to a very large software repository. For those reasons, we will use Ubuntu 18.04 and 20.04 LTS (Long Term Support) as the reference Debian family distributions for this course. Ubuntu is a registered trademark of Canonical Ltd. and is used throughout this course with their permission.

### Key Facts About the Debian Family

Some key facts about the Debian family are listed below:

- The Debian family is upstream for Ubuntu, and Ubuntu is upstream for Linux Mint and others.
- Kernel version 4.15 is used in Ubuntu 18.04 LTS.
- It uses the DPKG-based APT package manager (using `apt`, `apt-get`, `apt-cache`, etc. which we cover in detail later) to install, update, and remove packages in the system.
- Ubuntu has been widely used for cloud deployments.
- While Ubuntu is built on top of Debian and is GNOME-based under the hood, it differs visually from the interface on standard Debian, as well as other distributions.

## More About the Software Environment

The material produced by The Linux Foundation is distribution-flexible. This means that technical explanations, labs, and procedures should work on almost all most modern distributions. While choosing between available Linux systems, you will notice that the technical differences are mainly about package management systems, software versions, and file locations. Once you get a grasp of those differences, it becomes relatively painless to switch from one Linux distribution to another.

The desktop environment used for this course is **GNOME**. As we will note in _Chapter 4: Graphical Interface_, there are different environments, but we selected GNOME as it is the most widely used.
