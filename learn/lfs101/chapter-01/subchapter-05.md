---
title: End of Chapter
---

## Learning Objectives (Review)

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

You should now be able to:

- Discuss the role of The Linux Foundation.
- Appreciate the learning opportunities provided by The Linux Foundation's training program.
- Describe the software environment required for this course.
- Describe the three major Linux distribution families.

## Summary

<img src="/en/img/lfs101/common/tux-grad-cap.png" align="right"/>

You have completed Chapter 1. Let’s summarize the key concepts covered:

- The Linux Foundation is the umbrella organization for many critical open source projects that power corporations, spanning all industry sectors. Its work today extends far beyond Linux, fostering innovation at every layer of the software stack.
- The Linux Foundation training is for the community and by the community. Linux training is distribution-flexible, technically advanced, and created with the leaders of the Linux development community.
- There are three major distribution families within Linux: **Red Hat**, **SUSE** and **Debian**. In this course, we will work with representative members of all of these families throughout.
