---
title: The Linux Foundation
---

## About The Linux Foundation

Since its inception in 1991, Linux has grown to become a major force in computing - powering everything from the New York Stock Exchange, to mobile phones, supercomputers, and consumer devices.

The Linux Foundation partners with the world's leading developers and companies to solve the hardest technology problems and accelerate open technology development and commercial adoption. The Linux Foundation makes it its mission to provide experience and expertise to any initiative working to solve complex problems through open source collaboration, providing the tools to scale open source projects: security best practices, governance, operations and ecosystem development, training and certification, licensing, and promotion.

Linux is the world's largest and most pervasive open source software project in history. The Linux Foundation is home to Linux creator Linus Torvalds and lead maintainer Greg Kroah-Hartman, and provides a neutral home where Linux kernel development can be protected and accelerated for years to come. The success of Linux has catalyzed growth in the open source community, demonstrating the commercial efficacy of open source and inspiring countless new projects across all industries and levels of the technology stack.

The Linux Foundation's work today extends far beyond Linux, fostering innovation at every layer of the software stack. The Linux Foundation is the umbrella organization for many critical open source projects that power corporations today, spanning all industry sectors:

- Big data and analytics: [ODPi](https://www.odpi.org/), [R Consortium](https://www.r-consortium.org/)
- Networking: [OpenDaylight](https://www.opendaylight.org/), [ONAP](https://www.onap.org/), [OPNFV](https://www.opnfv.org/)
- Embedded: [Dronecode](https://www.dronecode.org/), [Zephyr](https://www.zephyrproject.org/)
- Web tools: [JS Foundation](https://js.foundation/), [Node.js](https://nodejs.org/en/)
- Cloud computing: [Cloud Foundry](https://www.cloudfoundry.org/), [Cloud Native Computing Foundation](https://cncf.io/), [Open Container Initiative](https://www.opencontainers.org/)
- Automotive: [Automotive Grade Linux](https://www.automotivelinux.org/)
- Security: [The Core Infrastructure Initiative](https://www.coreinfrastructure.org/)
- Blockchain: [Hyperledger](https://www.hyperledger.org/)
- And many more.

To learn more about The Linux Foundation, visit [The Linux Foundation's website](https://www.linuxfoundation.org/).

<div align="center">
  <img src="/en/img/lfs101/ch01/img02.png" />
</div>

## The Linux Foundation Events

The Linux Foundation hosts conferences and other events throughout the world which bring community members together in person. These events:

- Provide an open forum for development of the next kernel (the actual operating system) release.
- Bring together developers and system administrators to solve problems in a real-time environment.
- Host workgroups and community groups for active discussions.
- Connect end users, system administrators, and kernel developers in order to grow Linux use in the enterprise.
- Encourage collaboration among the entire community.
- Provide an atmosphere that is unmatched in its ability to further the platform.

<div align="center">
  <img src="/en/img/lfs101/ch01/img03.png" />
</div>

## More About The Linux Foundation Events

The [Linux Foundation events](https://events.linuxfoundation.org/) include:

- Open Source Summit North America, Europe, Japan, and China
- MesosCon North America, Europe, and China
- Embedded Linux Conference/OpenIoT Summit North America and Europe
- Open Source Leadership Summit
- Automotive Linux Summit
- Apache: Big Data North America & ApacheCon
- KVM Forum
- Linux Storage Filesystem and Memory Management Summit
- Vault
- Open Networking Summit
- And many more.

## Video: The Linux Foundation Events

<!-- [Video](https://player.vimeo.com/video/509213283?texttrack=en) | [Subtitle](video-02-en.srt) -->

<iframe 
  title="vimeo-player" 
  src="https://player.vimeo.com/video/509213283?texttrack=en" 
  width="640" 
  height="360" 
  frameborder="0" 
  allowfullscreen="true">
</iframe>
