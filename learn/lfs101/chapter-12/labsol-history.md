---
title: Command History
---

The **`history`** command is the way to display the commands you have typed in:

```bash
student:/tmp> history
1 cd /
2 ls
3 cd
4 pwd
5 echo $SHELL
6 ls /var/
7 ls /usr/bin
8 ls /usr/local/bin
9 man fstab
10 ls
. . .
```

In order to re-run a previous command, you have a few choices. Let's say that you wanted to re-run the **`man`** command you ran way back when you first logged in. You could type:

```bash
student:/tmp> !9
```

to re-run the command listed as **#9**. If this was the only **`man`** command that you typed in, you could also type:

```bash
student:/tmp> !man
```

now that you remember the command name that you typed. Finally, if you had typed a few **`man`** commands, you could use **CTRL-R** to search backward in your history to find the specific **`man`** command that you want to re-run, and then just hit **Return** to execute it.
