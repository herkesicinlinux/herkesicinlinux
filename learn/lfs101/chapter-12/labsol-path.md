---
title: Adding /tmp to Your Path
---

First create the phony ls program, using an editor, or just simply doing:

```bash
student:/tmp>echo "echo HELLO, this is the phony ls program." > /tmp/ls
student:/tmp>chmod +x /tmp/ls
```

For the next two steps it is a good idea to work in another terminal window, or just start a new shell, so the changes do not persist on later issued commands. You can start a new shell by just typing **`bash`**.

1.  ```bash
    student:/tmp>bash
    student:/tmp>PATH=$PATH:/tmp
    student:/tmp>ls /usr
    ```

    ```bash
    bin etc games include lib lib64 libexec local sbin share src tmp
    ```

    ```bash
    student:/tmp>exit
    ```

2.  ```bash
    student:/tmp>bash
    student:/tmp>PATH=/tmp:$PATH
    student:/tmp>ls /usr
    ```

    ```bash
    HELLO, this is the phony ls program.
    ```

    ```bash
    student:/tmp>exit
    ```

Note the second form is a very dangerous thing to do, and is a trivial way to insert a **Trojan Horse** program; if someone can put a malicious program in `/tmp`, they can trick you into running it accidentally.
