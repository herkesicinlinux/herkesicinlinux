---
title: Introduction and Learning Objectives
---

## Video: Chapter 12 Introduction

<!-- [Video](https://player.vimeo.com/video/511277207) | [Subtitle](video-01-en.srt) -->

<iframe
  title="vimeo-player"
  src="https://player.vimeo.com/video/511277207?texttrack=en"
  width="640"
  height="360"
  frameborder="0"
  allowfullscreen="true">
</iframe>

## Learning Objectives

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

By the end of this chapter, you should be able to:

- Use and configure user accounts and user groups.
- Use and set environment variables.
- Use the previous shell command history.
- Use keyboard shortcuts.
- Use and define aliases.
- Use and set file permissions and ownership.
