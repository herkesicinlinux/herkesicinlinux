---
title: File Permissions
---

## File Ownership

In Linux and other UNIX-based operating systems, every file is associated with a user who is the owner. Every file is also associated with a group (a subset of all users) which has an interest in the file and certain rights, or permissions: read, write, and execute.

The following utility programs involve user and group ownership and permission setting:

| **Command** | **Usage**                                                                                                                                                  |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **`chown`** | Used to change user ownership of a file or directory                                                                                                       |
| **`chgrp`** | Used to change group ownership                                                                                                                             |
| **`chmod`** | Used to change the permissions on the file, which can be done separately for **owner**, **group** and the rest of the **world** (often named as **other**) |

### File Permission Modes and chmod

Files have three kinds of permissions: read (**r**), write (**w**), execute (**x**). These are generally represented as in **rwx**. These permissions affect three groups of owners: user/owner (**u**), group (**g**), and others (**o**).

As a result, you have the following three groups of three permissions:

| rwx: | rwx: | rwx |
| ---- | ---- | --- |
| u:   | g:   | o   |

There are a number of different ways to use **`chmod`**. For instance, to give the owner and others execute permission and remove the group write permission:

```bash
$ ls -l somefile
-rw-rw-r-- 1 student student 1601 Mar 9 15:04 somefile
$ chmod uo+x,g-w somefile
$ ls -l somefile
-rwxr--r-x 1 student student 1601 Mar 9 15:04 somefile
```

where **u** stands for user (owner), **o** stands for other (world), and **g** stands for group.

This kind of syntax can be difficult to type and remember, so one often uses a shorthand which lets you set all the permissions in one step. This is done with a simple algorithm, and a single digit suffices to specify all three permission bits for each entity. This digit is the sum of:

- **4** if read permission is desired
- **2** if write permission is desired
- **1** if execute permission is desired.

Thus, **7** means **read/write/execute**, **6** means **read/write**, and **5** means **read/execute**.

When you apply this to the **`chmod`** command, you have to give three digits for each degree of freedom, such as in:

```bash
$ chmod 755 somefile
$ ls -l somefile
-rwxr-xr-x 1 student student 1601 Mar 9 15:04 somefile
```

<figure align="center">
  <img src="/en/img/lfs101/ch12/img17.png"/>
  <figcaption>File Permission Modes and chmod</figcaption>
</figure>

### Example of chown

Let's see an example of changing file ownership using **`chown`**, as shown in the screenshot below. First, we create two empty files using **`touch`**.

Notice it requires **`sudo`** to change the owner of **`file2`** to root. The second **`chown`** command changes both owner and group at the same time!

Finally, only the superuser can remove the files.

<figure align="center">
  <img src="/en/img/lfs101/ch12/img18.png"/>
  <figcaption>chown</figcaption>
</figure>

### Example of chgrp

Now, let’s see an example of changing the group ownership using **`chgrp`**:

<figure align="center">
  <img src="/en/img/lfs101/ch12/img19.png"/>
  <figcaption>chgrp</figcaption>
</figure>

### Try-It-Yourself: Using chmod to Change File Permissions

Please take a look at the following Try-It-Yourself exercise [or open in a new tab](https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingchmod/index.html).

<iframe 
  title="try-it-yourself"
  src="https://herkesicinlinux.gitlab.io/try-it-yourself/contents/chapter-12/usingchmod/index.html?nofocus" 
  width="100%"
  height="960px"
  frameborder="0">
</iframe>
