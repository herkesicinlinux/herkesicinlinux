---
title: 'Deploying aliases'
---

The **alias** line would look like this:

```bash
student:/tmp> alias projx='cd /home/staff/RandD/projects/projectX/src'
```

Note you can use double quotes instead of single quotes, or use no quotes at all since there is no white space in your defined alias. All you have to do now to change to the directory is:

```bash
student:/tmp> projx
```

To make the alias persistent, just place it in your **$HOME/.bashrc** file.
