---
title: End of Chapter
---

## Learning Objectives (Review)

<img src="/en/img/lfs101/common/items-list.png" align="right" class="invert"/>

You should now be able to:

- Use and configure user accounts and user groups.
- Use and set environment variables.
- Use the previous shell command history.
- Use keyboard shortcuts.
- Use and define aliases.
- Use and set file permissions and ownership.

## Summary

<img src="/en/img/lfs101/common/tux-grad-cap.png" align="right"/>

You have completed Chapter 12. Let's summarize the key concepts covered:

- Linux is a multi-user system.
- To find the currently logged on users, you can use the **`who`** command.
- To find the current user ID, you can use the **`whoami`** command.
- The **root** account has full access to the system. It is never sensible to grant full root access to a user.
- You can assign root privileges to regular user accounts on a temporary basis using the **`sudo`** command.
- The shell program (bash) uses multiple startup files to create the user environment. Each file affects the interactive environment in a different way. **`/etc/profile`** provides the global settings.
- Advantages of startup files include that they customize the user's prompt, set the user's terminal type, set the command-line shortcuts and aliases, and set the default text editor, etc.
- An environment variable is a character string that contains data used by one or more applications. The built-in shell variables can be customized to suit your requirements.
- The **`history`** command recalls a list of previous commands, which can be edited and recycled.
- In Linux, various keyboard shortcuts can be used at the command prompt instead of long actual commands.
- You can customize commands by creating aliases. Adding an alias to **`~/.bashrc`** will make it available for other shells.
- File permissions can be changed by typing **`chmod permissions filename`**.
- File ownership is changed by typing **`chown owner filename`**.
- File group ownership is changed by typing **`chgrp group filename`**.
