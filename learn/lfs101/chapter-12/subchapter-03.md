---
title: Environment Variables
---

## Environment Variables

**Environment variables** are quantities that have specific values which may be utilized by the command shell, such as **bash**, or other utilities and applications. Some environment variables are given preset values by the system (which can usually be overridden), while others are set directly by the user, either at the command line or within startup and other scripts.

An environment variable is actually just a character string that contains information used by one or more applications. There are a number of ways to view the values of currently set environment variables; one can type **set**, **env**, or **export**. Depending on the state of your system, **set** may print out many more lines than the other two methods.

<figure align="center">
  <img src="/en/img/lfs101/ch12/img10.png"/>
  <figcaption>Environment Variables</figcaption>
</figure>

### Setting Environment Variables

By default, variables created within a script are only available to the current shell; child processes (sub-shells) will not have access to values that have been set or modified. Allowing child processes to see the values requires use of the **`export`** command.

| Task                                  | Command                                                                                                                  |
| ------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Show the value of a specific variable | **`echo $SHELL`**                                                                                                        |
| Export a new variable value           | **`export VARIABLE=value`** (or **`VARIABLE=value; export VARIABLE`**)                                                   |
| Add a variable permanently            | 1. Edit **~/.bashrc** and add the line **`export VARIABLE=value`**                                                       |
|                                       | 2. Type **`source ~/.bashrc`** or just **`. ~/.bashrc`** (dot ~/.bashrc); or just start a new shell by typing **`bash`** |

You can also set environment variables to be fed as a one shot to a command as in:

```bash
$ SDIRS=s_0* KROOT=/lib/modules/$(uname -r)/build make modules_install
```

which feeds the values of the **`SDIRS`** and **`KROOT`** environment variables to the command **`make modules_install`**.

### The HOME Variable

**`HOME`** is an environment variable that represents the home (or login) directory of the user. **`cd`** without arguments will change the current working directory to the value of **`HOME`**. Note the tilde character (**`~`**) is often used as an abbreviation for **`$HOME`**. Thus, **`cd $HOME`** and **`cd ~`** are completely equivalent statements.

| Command            | Explanation                                                                       |
| ------------------ | --------------------------------------------------------------------------------- |
| **`$ echo $HOME`** | Show the value of the **`HOME`** environment variable...                          |
| **`/home/me`**     |                                                                                   |
| **`$ cd /bin`**    | ...then change directory (**`cd`**) to **/bin**.                                  |
| **`$ pwd`**        | Where are we? Use print (or present) working directory (**`pwd`**) to find out... |
| **`/bin`**         | ...As expected, **/bin**.                                                         |
| **`$ cd`**         | Change directory without an argument...                                           |
| **`$ pwd`**        | _Where are we?_                                                                   |
| **`/home/me`**     | ...takes us back to **`HOME`**, as you can now see.                               |

The screenshot demonstrates this.

<figure align="center">
  <img src="/en/img/lfs101/ch12/img11.png"/>
  <figcaption>The HOME Variable</figcaption>
</figure>

### The PATH Variable

**PATH** is an ordered list of directories (the path) which is scanned when a command is given to find the appropriate program or script to run. Each directory in the path is separated by colons (**:**). A null (empty) directory name (or **./**) indicates the current directory at any given time.

- **`:path1:path2`**
- **`path1::path2`**

In the example **`:path1:path2`**, there is a null directory before the first colon (**:**). Similarly, for **`path1::path2`** there is a null directory between **path1** and **path2**.

To prefix a private **bin** directory to your path:

```bash
$ export PATH=$HOME/bin:$PATH
$ echo $PATH
/home/student/bin:/usr/local/bin:/usr/bin:/bin/usr
```

<figure align="center">
  <img src="/en/img/lfs101/ch12/img12.png"/>
  <figcaption>The PATH Variable</figcaption>
</figure>

### The SHELL Variable

<img src="/en/img/lfs101/ch12/img13.png" align="right"/>

The environment variable **`SHELL`** points to the user's default command shell (the program that is handling whatever you type in a command window, usually bash) and contains the full pathname to the shell:

```bash
$ echo $SHELL
/bin/bash
$
```

### The PS1 Variable and the Command Line Prompt

Prompt Statement (**`PS`**) is used to customize your prompt string in your terminal windows to display the information you want.

**`PS1`** is the primary prompt variable which controls what your command line prompt looks like. The following special characters can be included in **`PS1`**:

- **`\u`** - User name
- **`\h`** - Host name
- **`\w`** - Current working directory
- **`\!`** - History number of this command
- **`\d`** - Date

They must be surrounded in single quotes when they are used, as in the following example:

```bash
$ echo $PS1
$
$ export PS1='\u@\h:\w$ '
student@example.com:~$ # new prompt
```

To revert the changes:

```bash
student@example.com:~$ export PS1='$ '
$
```

An even better practice would be to save the old prompt first and then restore, as in:

```bash
$ OLD_PS1=$PS1
```

change the prompt, and eventually change it back with:

```bash
$ PS1=$OLD_PS1
$
```

<figure align="center">
  <img src="/en/img/lfs101/ch12/img14.png"/>
  <figcaption>The PS1 Variable and the Command Line Prompt</figcaption>
</figure>

## Labs

### Lab 12.2: Adding /tmp to Your Path

Create a small file **`/tmp/ls`**, which contains just the line:

```bash
echo HELLO, this is the phony ls program.
```

Then, make it executable by doing:

```bash
$ chmod +x /tmp/ls
```

1. Append **`/tmp`** to your path, so it is searched only after your usual path is considered. Type **`ls`** and see which program is run: **`/bin/ls`** or **`/tmp/ls`**?
2. Pre-pend **`/tmp`** to your path, so it is searched before your usual path is considered. Once again, type **`ls`** and see which program is run: **`/bin/ls`** or **`/tmp/ls`**?

What are the security considerations in altering the path this way?

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-path)

### Lab 12.3: Changing the Command Line Prompt

It is nice to have your current working directory as part of your prompt so that a quick glance will give you some information without typing **`pwd`** every time.

If you often work on multiple computers, especially if you network from one into another with ssh, it is very convenient to have the computer name be part of your prompt.

1. Put your current working directory in your command line prompt.
2. Put your computer (machine) name in your prompt.
3. Put both your current directory and computer name in your prompt.

How can you make this persistent, so that whenever you start a bash command shell, this is your prompt?

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-prompt)
