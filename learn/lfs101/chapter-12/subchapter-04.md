---
title: Recalling Previous Commands
---

## Recalling Previous Commands

**`bash`** keeps track of previously entered commands and statements in a history buffer. You can recall previously used commands simply by using the **Up** and **Down** cursor keys. To view the list of previously executed commands, you can just type **`history`** at the command line.

The list of commands is displayed with the most recent command appearing last in the list. This information is stored in **~/.bash_history**. If you have multiple terminals open, the commands typed in each session are not saved until the session terminates.

<figure align="center">
  <img src="/en/img/lfs101/ch12/img15.png"/>
  <figcaption>Recalling Previous Commands</figcaption>
</figure>

### Using History Environment Variables

Several associated environment variables can be used to get information about the **`history`** file.

- **`HISTFILE`**
  - The location of the history file.
- **`HISTFILESIZE`**
  - The maximum number of lines in the history file (default 500).
- **`HISTSIZE`**
  - The maximum number of commands in the history file.
- **`HISTCONTROL`**
  - How commands are stored.
- **`HISTIGNORE`**
  - Which command lines can be unsaved.

For a complete description of the use of these environment variables, see **`man bash`**.

<figure align="center">
  <img src="/en/img/lfs101/ch12/img16.png"/>
  <figcaption>Using History Environment Variables</figcaption>
</figure>

### Finding and Using Previous Commands

Specific keys to perform various tasks:

| **Key**                          | **Usage**                                               |
| -------------------------------- | ------------------------------------------------------- |
| **Up/Down** arrow keys           | Browse through the list of commands previously executed |
| **!!** (Pronounced as bang-bang) | Execute the previous command                            |
| **CTRL-R**                       | Search previously used commands                         |

If you want to recall a command in the history list, but do not want to press the arrow key repeatedly, you can press **CTRL-R** to do a reverse intelligent search.

As you start typing, the search goes back in reverse order to the first command that matches the letters you have typed. By typing more successive letters, you make the match more and more specific.

The following is an example of how you can use the **CTRL-R** command to search through the command history:

```bash
$ ^R # This all happens on 1 line
(reverse-i-search)'s': sleep 1000 # Searched for 's'; matched "sleep"
$ sleep 1000 # Pressed Enter to execute the searched command
$
```

### Executing Previous Commands

The table describes the syntax used to execute previously used commands:

| Syntax        | Task                                                    |
| ------------- | ------------------------------------------------------- |
| **`!`**       | Start a history substitution                            |
| **`!$`**      | Refer to the last argument in a line                    |
| **`!n`**      | Refer to the nth command line                           |
| **`!string`** | Refer to the most recent command starting with `string` |

All history substitutions start with **`!`**. When typing the command: **`ls -l /bin /etc`** will refer to **`/etc`**, the last argument to the command.

Here are more examples:

```bash
$ history
1 echo $SHELL
2 echo $HOME
3 echo $PS1
4 ls -a
5 ls -l /etc/ passwd
6 sleep 1000
7 history
```

```bash
$ !1 # Execute command #1 above
echo $SHELL
/bin/bash
$ !sl # Execute the command beginning with "sl"
sleep 1000
$
```

### Keyboard Shortcuts

You can use keyboard shortcuts to perform different tasks quickly. The table lists some of these keyboard shortcuts and their uses. Note the case of the "hotkey" does not matter, e.g. doing **CTRL-a** is the same as doing **CTRL-A**.

| Keyboard Shortcut | Task                                               |
| ----------------- | -------------------------------------------------- |
| **CTRL-L**        | Clears the screen                                  |
| **CTRL-D**        | Exits the current shell                            |
| **CTRL-Z**        | Puts the current process into suspended background |
| **CTRL-C**        | Kills the current process                          |
| **CTRL-H**        | Works the same as backspace                        |
| **CTRL-A**        | Goes to the beginning of the line                  |
| **CTRL-W**        | Deletes the word before the cursor                 |
| **CTRL-U**        | Deletes from beginning of line to cursor position  |
| **CTRL-E**        | Goes to the end of the line                        |
| **Tab**           | Auto-completes files, directories, and binaries    |

## Labs

### Lab 12.4: Command History

You have been busy working with your Linux workstation long enough to have typed in about 100 commands in one particular bash command shell.

At some point earlier, you used a new command, but the exact name has slipped your mind.

Or perhaps it was a pretty complicated command, with a bunch of options and arguments, and you do not want to go through the error prone process of figuring out what to type again.

How do you ascertain what the command was?

Once you find the command in your history, how do you easily issue the command again without having to type it all in at the prompt?

Click the link below to view a solution to the Lab exercise.

[Lab Solution](labsol-history)
