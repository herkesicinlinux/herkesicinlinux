0
00:00:00,000 --> 00:00:10,500
Let's get some experience with creating, modifying, and removing a new user account. We will do this on Ubuntu 17.04.

1
00:00:10,500 --> 00:00:23,000
There are some variations between distributions about what exactly is created when a new account is created, and exactly which files are there, etc.

2
00:00:23,000 --> 00:00:30,500
This is mostly controlled by a file under ‘/etc/default/useradd’, appropriately named.

3
00:00:31,000 --> 00:00:39,500
So, you can see, for example, in here, the default shell is set to be ‘SHELL=/bin/sh’.

4
00:00:39,500 --> 00:00:47,500
Now, on some distributions such as Red Hat, this file is rather short, and not very well documented. On Ubuntu, it is rather long.

5
00:00:47,500 --> 00:00:58,000
So, let's create the account. So, ‘sudo useradd’, I will say ‘-m’, to make sure it creates a home directory.

6
00:00:58,000 --> 00:01:06,000
Some distributions do not do that by default. Ubuntu does not, for instance, and neither does openSUSE.

7
00:01:06,500 --> 00:01:22,500
I will give the full name to be ‘Eric Dolphy’. I will specify the default shell to be ‘/bin/bash’ with the ‘-s’ option. And here is the name, ‘edolphy’.

8
00:01:23,000 --> 00:01:33,000
Now, I still have to specify a password, so I will do that with ‘sudo passwd edolphy’.

9
00:01:33,500 --> 00:01:52,000
Then, I type something. And now, the account exists. And I can verify that by making sure entries were made in the ‘/etc/passwd’ file and  ‘/etc/group’. And there we go.

10
00:01:52,000 --> 00:02:02,500
Notice, it is now user ‘1001’. Remember, normal users start at 1000 and ‘student’ is user 1000, and so that is appropriate.

11
00:02:03,000 --> 00:02:11,000
And also, the other information I specified is there, such as the full username, and the shell ‘/bin/bash’.

12
00:02:11,500 --> 00:02:23,500
And, the group is said to be ‘1001’, which is the same ID as the user. All users are created with at least one group that has the same number as their username.

13
00:02:24,000 --> 00:02:40,500
Now, let's actually try to log into that account, and I will do that with ‘ssh edolphy@localhost’. And I will give the password. And I succeeded just fine.

14
00:02:40,500 --> 00:02:52,500
So, let's see what is actually in that directory. And, we will see there is not much, but these are files that all new users get. Let me log out now.

15
00:02:53,000 --> 00:03:09,500
And that is controlled by whatever is in the ‘/etc/skel’ directory. Anything you put in there... Let me do the ‘-a’ option. Anything you put in there, will show up in any new user’s account.

16
00:03:10,000 --> 00:03:19,500
So, let's just clean up, and I will do that with ‘userdel -r edolphy’.

17
00:03:20,000 --> 00:03:32,500
And the ‘-r’ is necessary to make sure it removes the home directory. And you notice I get a warning message about not having created a ‘mail spool’ file. That is harmless.

18
00:03:33,000 --> 00:03:39,500
And if I do ‘-l /home’ shell now, we see the account is gone. So, we are all cleaned up.

