---
title: Changing the Command Line Prompt
---

1. ```bash
   $ echo $PWD
   /tmp
   $ PS1='\w>'
   /tmp>
   ```

2. ```bash
   PS1='\h>'
   student>
   ```

3. ```bash
   PS1='\h:\w>'
   student:/tmp>
   ```
