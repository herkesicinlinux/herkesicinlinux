0
00:00:04,377 --> 00:00:11,671
Linux is a multi-user system that may have different users and accounts running at the same time with different custom environments.

1
00:00:11,862 --> 00:00:18,439
In this lesson, we are going to cover the root user, keyboard shortcuts, start up files and file ownership permissions.

2
00:00:18,535 --> 00:00:19,635
Let’s get started!

