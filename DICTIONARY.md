# Dictionary

| English         | Türkçe                         |
| --------------- | ------------------------------ |
| upstream        | üstkaynak                      |
| downstream      | astkaynak                      |
| shell           | kabuk                          |
| script          | betik                          |
| shell script    | kabuk betiği                   |
| shell scripting | kabuk betiği yazma/programlama |
| editor          | düzenleyici                    |
| multitasking    | çok görevli                    |
| pattern         | desen                          |
| argument        | bağımsız değişken              |
| tutorial        | öğretici                       |
