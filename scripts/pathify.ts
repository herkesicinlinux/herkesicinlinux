/* eslint-disable no-console */
import fs from 'fs';
import { pathify } from '../src/utils';

/**
 * Captures and processes command-line arguments.
 * @param dirFlag Flag to indicate directory creation request. Should start with "-" (dash).
 * @param logger Logger function.
 */
const captureAndProcess = (dirFlag: string, logger: (e: string) => void) => {
  // Capture arguments
  let [, , ...args] = process.argv;
  if (!args.length) {
    const sample = 'npm run pathify "Linux\'a Giriş"';
    let msg = 'Usage:\n';
    msg += `\t${sample}  # Prints path to the console\n`;
    msg += `\t${sample} -- ${dirFlag}  # Prints and creates directory\n`;
    logger(msg);
  }

  // Decide to create dir or not
  const createDir = args.includes(dirFlag);
  if (createDir) {
    args = args.filter((a) => a !== dirFlag);
  }

  const slug = pathify(args.join(' '));

  // Print to the console
  logger(slug);

  // Create directory (if requested)
  if (createDir) {
    const dirPath = `${process.cwd()}/${slug}`;
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath, { recursive: true });
      logger(`"${slug}" dir created!`);
    } else {
      logger(`"${slug}" dir already exists!`);
    }
  }
};

captureAndProcess('-d', console.log);
