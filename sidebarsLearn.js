module.exports = {
  lfs101: [
    {
      'Chapter 0: Welcome!': ['lfs101/chapter-00/subchapter-01'],
      'Chapter 1: The Linux Foundation': [
        'lfs101/chapter-01/subchapter-01',
        'lfs101/chapter-01/subchapter-02',
        'lfs101/chapter-01/subchapter-03',
        'lfs101/chapter-01/subchapter-04',
        'lfs101/chapter-01/subchapter-05',
      ],
      'Chapter 2: Linux Philosophy and Concepts': [
        'lfs101/chapter-02/subchapter-01',
        'lfs101/chapter-02/subchapter-02',
        'lfs101/chapter-02/subchapter-03',
        'lfs101/chapter-02/subchapter-04',
        'lfs101/chapter-02/subchapter-05',
        'lfs101/chapter-02/subchapter-06',
        'lfs101/chapter-02/subchapter-07',
      ],
      'Chapter 3: Linux Basics and System Startup': [],
      'Chapter 4: Graphical Interface': [
        'lfs101/chapter-04/subchapter-01',
        'lfs101/chapter-04/subchapter-02',
        {
          'Lab Solution': ['lfs101/chapter-04/labsol-desktop'],
        },
        'lfs101/chapter-04/subchapter-03',
        'lfs101/chapter-04/subchapter-04',
        {
          'Lab Solutions': [
            'lfs101/chapter-04/labsol-filesort',
            'lfs101/chapter-04/labsol-recoverfiles',
          ],
        },
        'lfs101/chapter-04/subchapter-05',
      ],
      'Chapter 5: System Configuration from the Graphical Interface': [],
      'Chapter 6: Common Applications': [],
      'Chapter 7: Command Line Operations': [],
      'Chapter 8: Finding Linux Documentation': [],
      'Chapter 9: Processes': [],
      'Chapter 10: File Operations': [],
      'Chapter 11: Text Editors': [
        'lfs101/chapter-11/subchapter-01',
        'lfs101/chapter-11/subchapter-02',
        'lfs101/chapter-11/subchapter-03',
        {
          'Lab Solutions': [
            'lfs101/chapter-11/labsol-vi-navigating',
            'lfs101/chapter-11/labsol-vi-editing',
            'lfs101/chapter-11/labsol-vi-extcmds',
          ],
        },
        'lfs101/chapter-11/subchapter-04',
      ],
      'Chapter 12: User Environment': [
        'lfs101/chapter-12/subchapter-01',
        'lfs101/chapter-12/subchapter-02',
        {
          'Lab Solution': ['lfs101/chapter-12/labsol-alias'],
        },
        'lfs101/chapter-12/subchapter-03',
        {
          'Lab Solutions': ['lfs101/chapter-12/labsol-path', 'lfs101/chapter-12/labsol-prompt'],
        },
        'lfs101/chapter-12/subchapter-04',
        {
          'Lab Solution': ['lfs101/chapter-12/labsol-history'],
        },
        'lfs101/chapter-12/subchapter-05',
        'lfs101/chapter-12/subchapter-06',
      ],
      'Chapter 13: Manipulating Text': [],
      'Chapter 14: Network Operations': [],
      'Chapter 15: The Bash Shell and Basic Scripting': [],
      'Chapter 16: More on Bash Shell Scripting': [],
      'Chapter 17: Printing': [],
      'Chapter 18: Local Security Principles': [],
    },
  ],
  tlcl: ['tlcl/tlcl1'],
};
