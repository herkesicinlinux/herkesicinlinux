const GIT_CONF = {
  host: 'https://gitlab.com',
  name: 'GitLab',
  org: 'herkesicinlinux',
  project: 'herkesicinlinux',
  branch: {
    prod: 'main',
    dev: 'website-staging',
  },
  orgUrl: function () {
    return `${this.host}/${this.org}`;
  },
  repoUrl: function () {
    return `${this.orgUrl()}/${this.project}`;
  },
  repoPageUrl: function (page, branch, subDir) {
    return `${this.repoUrl()}/-/${page}/${branch}/${subDir ? subDir + '/' : ''}`;
  },
};

const BTC_ADDR = 'bitcoin:1HiL1o1aGZ7ManUPbso6d1xoYLDqAiJkFJ';

const isDev = process.env.NODE_ENV === 'development';

const localeConfigs = {
  en: {
    label: 'English',
  },
  tr: {
    label: 'Türkçe',
  },
};

module.exports = {
  title: 'Herkes için Linux',
  tagline: '',
  url: 'https://herkesicinlinux.gitlab.io',
  baseUrl: 'herkesicinlinux/',
  i18n: {
    defaultLocale: 'tr',
    locales: Object.keys(localeConfigs),
    localeConfigs,
  },
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: GIT_CONF.org,
  projectName: GIT_CONF.project,
  themeConfig: {
    hideableSidebar: true,
    navbar: {
      title: 'Linux for Everyone',
      logo: {
        alt: 'HIL Tux Nerd',
        src: 'img/Tux-nerd.svg',
      },
      items: [
        // Left
        {
          to: 'docs/',
          label: 'Docs',
          activeBasePath: 'docs',
          position: 'left',
        },
        {
          to: 'learn/lfs101/chapter-00/subchapter-01',
          label: 'LFS101',
          activeBaseRegex: '/learn/lfs101/',
          position: 'left',
        },
        {
          to: 'learn/tlcl/tlcl1',
          label: 'TLCL',
          activeBaseRegex: '/learn/tlcl/',
          position: 'left',
        },
        // Right
        { to: 'blog', label: 'Blog', position: 'right' },
        {
          type: 'localeDropdown',
          position: 'right',
          dropdownItemsAfter: [
            {
              to: GIT_CONF.repoUrl(),
              label: 'Help Us Translate',
            },
          ],
        },
        {
          href: BTC_ADDR,
          target: '_self',
          position: 'right',
          className: 'custom-icon-button header-btc-link',
          'aria-label': 'Bitcoin address',
        },
        {
          href: GIT_CONF.repoUrl(),
          position: 'right',
          className: 'custom-icon-button header-gitlab-link',
          'aria-label': `${GIT_CONF.name} repository`,
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Style Guide',
              to: 'docs/',
            },
            {
              label: 'Second Doc',
              to: 'docs/doc2/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Telegram',
              href: 'https://t.me/joinchat/V6PoA-DbNu1KpJAx',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: GIT_CONF.name,
              href: GIT_CONF.orgUrl(),
            },
          ],
        },
      ],
      copyright: `© ${new Date().getFullYear()} <a href="${GIT_CONF.repoPageUrl(
        'graphs',
        GIT_CONF.branch.prod,
      )}">Herkes için Linux Contributors</a>. Built with Docusaurus.`,
    },
  },
  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'learn',
        path: 'learn',
        editUrl: GIT_CONF.repoPageUrl('edit', GIT_CONF.branch.dev),
        editLocalizedFiles: true,
        routeBasePath: 'learn',
        sidebarPath: require.resolve('./sidebarsLearn.js'),
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      },
    ],
  ],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: GIT_CONF.repoPageUrl('edit', GIT_CONF.branch.dev),
          editLocalizedFiles: true,
        },
        blog: {
          showReadingTime: true,
          editUrl: GIT_CONF.repoPageUrl('edit', GIT_CONF.branch.dev),
          editLocalizedFiles: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
